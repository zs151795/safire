close all;
f=figure('Position', [100, 100, 800, 300]);
positions = 1:20;
position_1 = [1 3 5 7  9 11 13 15 17 19]; 
position_2 = [2 4 6 8 10 12 14 16 18 20];

end_1 = [117, 130, 144, 157, 164, 176, 187, 197, 202, 208];
end_2 = [  4,  78,   8, 100,  26,  16,   4,   7,  48,  12];


for i= 1:10
    boxplot(input(1:end_1(i),position_1(i)), 'position', position_1(i), 'widths', 0.65);
    hold on;

end
hold on;


for i=1:10
    boxplot(input(1:end_2(i),position_2(i)), 'position', position_2(i), 'widths', 0.65);
    hold on;
end

ylim([-50 4000]);
ylabel('Sum of Discrepancy Socres (t)','fontsize',12)

xlim([0 21]);
set(gca,'xtick',[ 1.5 3.5 5.5 7.5 9.5 11.5 13.5 15.5 17.5 19.5 ] );
set(gca,'xticklabel',{'1', '2', '3', '4', '5', '6', '7', '8', '9', '10'});
 xlabel({'Scale Factor \bf \it i'},'fontweight','bold','fontsize',14, 'Interpreter','Latex');

colordegree = 0.75;

 h = findobj(gca,'Tag','Box');
 for i=1:10
     patch(get(h(i),'XData'),get(h(i),'YData'), [147/255,112/255,219/255],'FaceAlpha',colordegree);
 end
 
 for i=11:20
       patch(get(h(i),'XData'),get(h(i),'YData'), 'y','FaceAlpha',colordegree);
 end


c = get(gca, 'Children');
legend(c(10:11),'MOEA\D', 'MOEA\D-RS');

