package uk.ac.york.safire.metrics;

public enum SearchDirection { MINIMIZING, MAXIMIZING };

// End ///////////////////////////////////////////////////////////////
