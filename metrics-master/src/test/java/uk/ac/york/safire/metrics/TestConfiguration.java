package uk.ac.york.safire.metrics;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.ControlledMetricType;
import uk.ac.york.safire.metrics.KeyObjectiveType;
import uk.ac.york.safire.metrics.ObservableMetricType;
import uk.ac.york.safire.metrics.Process;
import uk.ac.york.safire.metrics.SampleRate;
import uk.ac.york.safire.metrics.SearchDirection;
import uk.ac.york.safire.metrics.Utility;
import uk.ac.york.safire.metrics.Value;
import uk.ac.york.safire.metrics.ValueTrace;
import uk.ac.york.safire.metrics.ValueType;

///////////////////////////////////

public final class TestConfiguration {

	public static ConfigurationType toyConfigurationType() {
		final List< KeyObjectiveType > keyObjectiveMetricTypes = ImmutableList.of( 
				new KeyObjectiveType( "key objective metric 1", ValueType.realType( 0.0, Double.MAX_VALUE ), "scalar", SearchDirection.MINIMIZING ) 
		); 
			
		final List< ControlledMetricType > controlledMetricTypes = ImmutableList.of( 
			new ControlledMetricType( "controlled metric 1", ValueType.realType( 0.0, Double.MAX_VALUE ), "scalar" )			
		);
			
		final List< ObservableMetricType > observableMetricTypes = ImmutableList.of(
			new ObservableMetricType( "observable metric 1", ValueType.realType( 0.0, Double.MAX_VALUE ), "scalar", SampleRate.eventDriven)		
		);

		return new ConfigurationType.Explicit(keyObjectiveMetricTypes,controlledMetricTypes,observableMetricTypes);
	}

	///////////////////////////////
	
	public static ConfigurationType.Explicit bc1ConfigurationType() {
		final List< KeyObjectiveType > keyObjectiveMetricTypes = ImmutableList.of( 
			new KeyObjectiveType( "boiling detection error", ValueType.realType( 0.0, Double.MAX_VALUE ), "sec", SearchDirection.MINIMIZING ), 
			new KeyObjectiveType( "temperature estimation error", ValueType.realType( 0.0, Double.MAX_VALUE ), "Celsius", SearchDirection.MINIMIZING )
		); 
			
		final List< ControlledMetricType > controlledMetricTypes = ImmutableList.of( 
			new ControlledMetricType( "power/energy profile", ValueType.realType( 50.0, 3200.0 ), "Watts" ),
			new ControlledMetricType( "data acquisition period", ValueType.realType( 0.5, 3.0 ), "sec" )			
		);

		final String [] potTypes = new String [] { "potType1", "potType2" };
		final String [] foodTypes = new String [] { "foodType1", "foodType2" };		
		final List< ObservableMetricType > observableMetricTypes = ImmutableList.of(
			new ObservableMetricType( "type of pot", ValueType.nominalType( "type of pot", potTypes ), "#", SampleRate.eventDriven),		
			new ObservableMetricType( "type of food/water amount", ValueType.nominalType( "type of food/water amount", foodTypes ), "#", SampleRate.eventDriven),
			new ObservableMetricType( "cooking speed", ValueType.realType( 0.0, Double.MAX_VALUE ), "#", SampleRate.eventDriven),
			new ObservableMetricType( "expected cooking level", ValueType.realType( 0.0, Double.MAX_VALUE ), "#", SampleRate.eventDriven),
			new ObservableMetricType( "power", ValueType.realType( 0.0, Double.MAX_VALUE ), "Watts", SampleRate.periodic(10,10000) ),
			new ObservableMetricType( "current", ValueType.realType( 0.0, Double.MAX_VALUE ), "Amps", SampleRate.periodic(10,10000) ),
			new ObservableMetricType( "voltage", ValueType.realType( 0.0, Double.MAX_VALUE ), "Volts", SampleRate.periodic(10,10000) ),				
			new ObservableMetricType( "temperature", ValueType.realType( 0.0, Double.MAX_VALUE ), "Celsius", SampleRate.eventDriven ),
			new ObservableMetricType( "vibration", ValueType.realType( 0.0, Double.MAX_VALUE ), "Hz", SampleRate.eventDriven )
		);

		return new ConfigurationType.Explicit(keyObjectiveMetricTypes,controlledMetricTypes,observableMetricTypes);
	}
	
	///////////////////////////////

	@Test
	public void testTimeBasedIndexing() {
	
		final ConfigurationType typ = toyConfigurationType();
		
		final int numSamples = 10;
		final double startTimeInMilliseconds = 0.0;
		final double endTimeInMilliseconds = 1.0;
		
		List< Pair< Value, Double > > controlledMetricsTrace = new ArrayList<>();
		List< Pair< Value, Double > > observableMetricsTrace = new ArrayList<>();
		List< Pair< Value, Double > > keyObjectiveMetricsTrace = new ArrayList<>();		
		for( int i=0; i<numSamples; ++i ) {
			final double timestamp = jeep.math.LinearInterpolation.apply(i, 0, numSamples - 1, startTimeInMilliseconds, endTimeInMilliseconds );
			controlledMetricsTrace.add( Pair.of( Value.realValue(i, (ValueType.Real)typ.getControlledMetrics().get(0).valueType ), timestamp ) );
			observableMetricsTrace.add( Pair.of( Value.realValue(i, (ValueType.Real)typ.getObservableMetrics().get(0).valueType ), timestamp ) );
			keyObjectiveMetricsTrace.add( Pair.of( Value.realValue(i, (ValueType.Real)typ.getKeyObjectiveMetrics().get(0).valueType ), timestamp ) );			
		}
		
		final Map< ControlledMetricType, ValueTrace > controlledMetrics = 
			Collections.singletonMap(typ.getControlledMetrics().get(0), new ValueTrace( controlledMetricsTrace ) );

		final Map< ObservableMetricType, ValueTrace > observableMetrics = 
				Collections.singletonMap(typ.getObservableMetrics().get(0), new ValueTrace( observableMetricsTrace ) );

		final Map< KeyObjectiveType, ValueTrace > keyObjectives = 
				Collections.singletonMap(typ.getKeyObjectiveMetrics().get(0), new ValueTrace( keyObjectiveMetricsTrace ) );
		
		Process.ExplicitHistorical process = new Process.ExplicitHistorical(typ, controlledMetrics, observableMetrics, keyObjectives );
		System.out.println( process );
		
		for( int i=0; i<numSamples; ++i ) {
			final double timestamp = jeep.math.LinearInterpolation.apply(i, 0, numSamples - 1, startTimeInMilliseconds, endTimeInMilliseconds );			
			assertEquals( controlledMetricsTrace.get(i), process.getControlledMetric(typ.getControlledMetrics().get(0), timestamp) );
			assertEquals( observableMetricsTrace.get(i), process.getObservableMetric(typ.getObservableMetrics().get(0), timestamp) );
			assertEquals( Optional.of(keyObjectiveMetricsTrace.get(i)), process.getKeyObjectiveMetric(typ.getKeyObjectiveMetrics().get(0), timestamp) );
		}
	}
	
	///////////////////////////////	

	@Test
	public void bc1() {
	
		final ConfigurationType configurationType = bc1ConfigurationType();
		
		final long randomSeed = 0xDEADBEEF; 
		final double startTime = 0.0;
		final double endTime = 10.0;
		final int numSamples = 10;		
		final Random random = new Random( randomSeed );

		Process.ExplicitHistorical process = Utility.randomProcess(configurationType, numSamples, startTime, endTime, random);
		System.out.println( process );
	}
}

// End ///////////////////////////////////////////////////////////////