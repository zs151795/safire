
close all;

f= figure;
set(gcf,'Units','centimeters','Position',[0 10 15 6.5]);

b= bar(input);
b(1).FaceColor = 'y' ;
b(1).FaceAlpha = 1;
b(2).FaceColor = [147/255,112/255,219/255];
b(2).FaceAlpha = 1;

legend({'MOEA/D','MOEA/D-RS'});
grid on

ax = gca;
ax.GridLineStyle = '-.';
ax.MinorGridLineStyle = '-.';

ylabel({'Execution Time (s)'},'FontSize',10);


xlim([0.5 10.5])
    xticklabels({'1', '2', '3', '4', '5', '6', '7', '8', '9', '10'});
  xlabel({'Scale Factor \bf \it i'},'fontweight','bold','fontsize',12, 'Interpreter','Latex');
   set(gca,'XTick',1:1:10);
  



