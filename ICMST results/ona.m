close all;

f=figure('Position', [100, 100, 600, 250]);

datax1 = input(1:14,1);
datay1 = input(1:14,2);
a =scatter(datax1,datay1,350,'.');

hold on;
datax2 = input(1:3,3);
datay2 = input(1:3,4);
b =scatter(datax2,datay2,150,'*');

%ylim([11.85 12.2]);
ylabel('Total Value','fontsize',12)

%xlim([42500 47000]);
xlabel('Makespan (min)','fontsize',12)

legend( 'MO standard', 'MO with Selection');


