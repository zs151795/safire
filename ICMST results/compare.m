close all;
f=figure('Position', [100, 100, 450, 250]);

stage = input(1,:);
z = input(2,:);
y = input(3,:);
x = input(4,:);
k = input(5,:);



yyaxis left
plot(stage,x,'b-o',stage,z,'b-+')
ylim([8000 60000]);
ylabel('Makespan (min)','FontSize', 12)

hold on

yyaxis right
plot(stage,y,'r-+',stage,k,'r-o')
ylim([5 14]);
ylabel('Total Value','FontSize', 12)


xlim([0.5 10.5]);
set(gca,'xtick',[ 1 2 3 4 5 6 7 8 9 10 ] );
set(gca,'xticklabel',{'7', '8', '9', '10', '11', '12', '13', '14', '15', '16'});
xlabel({'Number of elements \bf \it i'},'fontweight','bold','fontsize',12, 'Interpreter','Latex');

plt = gca;
plt.YAxis(2).Color = 'b'; % change color of RHS y-axis to black
plt.YAxis(2).Color = 'r'; % change color of RHS y-axis to black
legend('SO makespan','MO makespan', 'SO value', 'MO value');