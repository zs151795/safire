close all;
f=figure('Position', [100, 100, 600, 300]);
positions = 1:20;

position_1 = [1 3 5 7 9   11 13 15 17 19]; 
position_2 = [2 4 6 8 10 12 14 16 18 20];

end_1       = [6, 7, 9, 8, 8, 8, 7, 8, 6, 7];
end_2       = [14, 23, 11, 5, 5, 9, 10, 15, 12, 9];

for i= 1:10
   boxplot(input(1:end_1(i),position_1(i)), 'position', position_1(i), 'widths', 0.65, 'color', 'b');
    hold on;
end
ylabel('Makespan (min)','FontSize', 12)

hold on;

for i=1:10
  boxplot(input(1:end_2(i),position_2(i)), 'position', position_2(i), 'widths', 0.65, 'color', 'r');
    hold on;
end

stage = input1(1,:);
z = input1(2,:);
a= plot(stage,z,'b:+','MarkerSize',6,'color',[30/255,144/255,255/255]);

ylim([2000 60000]);


xlim([0 21]);
set(gca,'xtick',[ 1.5 3.5 5.5 7.5 9.5 11.5 13.5 15.5 17.5 19.5  ] );
set(gca,'xticklabel',{'7', '8', '9', '10', '11', '12', '13', '14', '15', '16'});
 xlabel({'Number of elements \bf \it i'},'fontweight','bold','fontsize',12, 'Interpreter','Latex');
 

  h = findobj(gca,'Tag','Box');
 legend([h(11) h(1),a],'MO with selection','MO standard','SO');
 
% colordegree = 0.6;
% 

%  for i=1:10
%      patch(get(h(i),'XData'),get(h(i),'YData'), 'b','FaceAlpha',colordegree);
%  end
%  
%  for i=11:20
%         patch(get(h(i),'XData'),get(h(i),'YData'), 'r','FaceAlpha',colordegree);
%  end




% legend([h(11) h(1),a,b],'MO makespan','MO value','SO makespan','SO value');

