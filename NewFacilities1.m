close all;
f=figure('Position', [100, 100, 800, 300]);
set(f,'defaultAxesColorOrder',[[0,60/255,255/255];[0,0,0]]);
positions = 1:18;
position_1 = [1 7 13]; 

end1 = 200;
end2 = 68;
end3 = 18;

yyaxis left
boxplot(input(1:end1,position_1(1)), 'position', position_1(1), 'widths', 0.65, 'color', 'b');
hold on
boxplot(input(1:end2,position_1(2)), 'position', position_1(2), 'widths', 0.65, 'color', 'b');
hold on
boxplot(input(1:end3,position_1(3)), 'position', position_1(3), 'widths', 0.65, 'color', 'b');
ylim([0 1000]);
ylabel('Makespan (min)')

yyaxis right
position_2 = [2 3 4 5 6 8 9 10 11 12 14 15 16 17 18]; 
hold on
boxplot(input(1:end1,position_2(1):position_2(5)), 'position', positions(:,position_2(1):position_2(5)), 'widths', 0.65, 'color', 'k'); 
hold on
boxplot(input(1:end2,position_2(6):position_2(10)), 'position', positions(:,position_2(6):position_2(10)), 'widths', 0.65, 'color', 'k');
hold on
boxplot(input(1:end3,position_2(11):position_2(15)), 'position', positions(:,position_2(11):position_2(15)), 'widths', 0.65, 'color', 'k');
ylim([-10 400]);

ylabel('Discrepancy (t)')

xlim([0 19]);

set(gca,'xtick',[mean(positions(1:6)) mean(positions(7:12)) mean(positions(13:18)) ]);
set(gca,'TickLabelInterpreter','latex');
set(gca,'xticklabel',{'MOEA/D', 'MOEA/D + New Mutation', 'MOEA/D + New Elitism'});
xlabel('MOEA/D with Proposed Genetic Operators')

%color = ['c','g', 'm', 'y', [255/255, 159/255, 128/255] ];
color = ['y','', 'm', 'y', [255/255, 159/255, 128/255] ];

color1 ='m';
color2 =[147/255,112/255,219/255];
color3 ='g';
color4 ='c';
color5 ='y';

colordegree = 1;

 h = findobj(gca,'Tag','Box');
 patch(get(h(1),'XData'),get(h(1),'YData'), color1,'FaceAlpha',colordegree);
 patch(get(h(2),'XData'),get(h(2),'YData'), color2,'FaceAlpha',colordegree);
 patch(get(h(3),'XData'),get(h(3),'YData'), color3,'FaceAlpha',colordegree);
 patch(get(h(4),'XData'),get(h(4),'YData'), color4,'FaceAlpha',colordegree);
 patch(get(h(5),'XData'),get(h(5),'YData'), color5,'FaceAlpha',colordegree);
 patch(get(h(6),'XData'),get(h(6),'YData'), color1,'FaceAlpha',colordegree);
 patch(get(h(7),'XData'),get(h(7),'YData'), color2,'FaceAlpha',colordegree);
 patch(get(h(8),'XData'),get(h(8),'YData'), color3,'FaceAlpha',colordegree);
 patch(get(h(9),'XData'),get(h(9),'YData'), color4,'FaceAlpha',colordegree);
 patch(get(h(10),'XData'),get(h(10),'YData'), color5,'FaceAlpha',colordegree);
 patch(get(h(11),'XData'),get(h(11),'YData'), color1,'FaceAlpha',colordegree);
 patch(get(h(12),'XData'),get(h(12),'YData'), color2,'FaceAlpha',colordegree);
 patch(get(h(13),'XData'),get(h(13),'YData'), color3,'FaceAlpha',colordegree);
 patch(get(h(14),'XData'),get(h(14),'YData'), color4,'FaceAlpha',colordegree);
 patch(get(h(15),'XData'),get(h(15),'YData'), color5,'FaceAlpha',colordegree);
 
patch(get(h(18),'XData'),get(h(18),'YData'),'w','FaceAlpha',colordegree);

c = get(gca, 'Children');
hleg1 = legend(c(1:6),'Makespan', 'Std Weiss', 'Weiss Matt', 'Super Weiss', 'Weiss Basis', 'Sum');


lines = findobj(gcf, 'type', 'line', 'Tag', 'Box');
set(lines, 'color', 'b');

lines = findobj(gcf, 'type', 'line', 'Tag', 'Median');
set(lines, 'Color', 'r');



% for j=1:length(h)
%    patch(get(h(j),'XData'),get(h(j),'YData'),'m','FaceAlpha',.5);
% end
