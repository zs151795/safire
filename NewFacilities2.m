close all;
f=figure('Position', [100, 100, 800, 300]);
positions = 1:18;
position_1 = [1 7 13]; 

end1 = 200;
end2 = 203;
end3 = 18;

yyaxis left
boxplot(input(1:end1,position_1(1)), 'position', position_1(1), 'widths', 0.65, 'color', 'b');
hold on
boxplot(input(1:end2,position_1(2)), 'position', position_1(2), 'widths', 0.65, 'color', 'b');
hold on
boxplot(input(1:end3,position_1(3)), 'position', position_1(3), 'widths', 0.65, 'color', 'b');
ylim([0 1000]);
ylabel('Makespan')

yyaxis right
position_2 = [2 3 4 5 6 8 9 10 11 12 14 15 16 17 18]; 
hold on
boxplot(input(1:end1,position_2(1):position_2(5)), 'position', positions(:,position_2(1):position_2(5)), 'widths', 0.65, 'color', 'r'); 
hold on
boxplot(input(1:end2,position_2(6):position_2(10)), 'position', positions(:,position_2(6):position_2(10)), 'widths', 0.65, 'color', 'r');
hold on
boxplot(input(1:end3,position_2(11):position_2(15)), 'position', positions(:,position_2(11):position_2(15)), 'widths', 0.65, 'color', 'r');
ylim([-10 400]);


xlim([0 19]);

set(gca,'xtick',[mean(positions(1:6)) mean(positions(7:12)) mean(positions(13:18))]);
set(gca,'TickLabelInterpreter','latex');
set(gca,'xticklabel',{'MOEA/D', 'MOEA/D + New Improve', 'MOEA/D + New Elitism'});
ylabel('Discrepancy')

color = ['w','c', 'y', 'm', 'g', 'w','w','c', 'y', 'm', 'g', 'w','w','c', 'y', 'm', 'g', 'w'];
colordegree = .5
 h = findobj(gca,'Tag','Box');
 patch(get(h(1),'XData'),get(h(1),'YData'),'c','FaceAlpha',colordegree);
 patch(get(h(2),'XData'),get(h(2),'YData'),'g','FaceAlpha',colordegree);
 patch(get(h(3),'XData'),get(h(3),'YData'),'m','FaceAlpha',colordegree);
 patch(get(h(4),'XData'),get(h(4),'YData'),'y','FaceAlpha',colordegree);
 patch(get(h(5),'XData'),get(h(5),'YData'),[255/255, 159/255, 128/255],'FaceAlpha',colordegree);
patch(get(h(6),'XData'),get(h(6),'YData'),'c','FaceAlpha',colordegree);
patch(get(h(7),'XData'),get(h(7),'YData'),'g','FaceAlpha',colordegree);
patch(get(h(8),'XData'),get(h(8),'YData'),'m','FaceAlpha',colordegree);
patch(get(h(9),'XData'),get(h(9),'YData'),'y','FaceAlpha',colordegree);
 patch(get(h(10),'XData'),get(h(10),'YData'),[255/255, 159/255, 128/255],'FaceAlpha',colordegree);
patch(get(h(11),'XData'),get(h(11),'YData'),'c','FaceAlpha',colordegree);
patch(get(h(12),'XData'),get(h(12),'YData'),'g','FaceAlpha',colordegree);
patch(get(h(13),'XData'),get(h(13),'YData'),'m','FaceAlpha',colordegree);
patch(get(h(14),'XData'),get(h(14),'YData'),'y','FaceAlpha',colordegree);
 patch(get(h(15),'XData'),get(h(15),'YData'),[255/255, 159/255, 128/255],'FaceAlpha',colordegree);
patch(get(h(17),'XData'),get(h(17),'YData'),'w','FaceAlpha',colordegree);

c = get(gca, 'Children');
hleg1 = legend(c(1:6),'Makespan', 'Std Weiss', 'Weiss Matt', 'Super Glanz', 'Weiss Basis', 'Sum');


lines = findobj(gcf, 'type', 'line', 'Tag', 'Box');
set(lines, 'color', 'k');

lines = findobj(gcf, 'type', 'line', 'Tag', 'Median');
set(lines, 'Color', 'k');



% for j=1:length(h)
%    patch(get(h(j),'XData'),get(h(j),'YData'),'m','FaceAlpha',.5);
% end
