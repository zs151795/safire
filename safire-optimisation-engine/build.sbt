lazy val akkaHttpVersion = "10.0.9"
lazy val akkaVersion    = "2.5.3"

// From:
// The repository to which the image is pushed when the docker:publish task is run. 
// This should be of the form [repository.host[:repository.port]] (assumes use of the index.docker.io repository) 
// or [repository.host[:repository.port]][/username] (discouraged, but available for backwards compatibilty.).
// dockerRepository := Some("dockerbuntu.atb-bremen.de:2376")
// Sebastian has emailed certificates for access to ATB's docker server
// certificates (on windows) are located in the .docker folder in the current user directory.
// docker --help command will show the exact path detail

// From: https://github.com/sbt/sbt-native-packager/issues/412
// I think your best bet is to make sure your docker daemon is configured correctly so sbt can run as-is.

// retrieveManaged := true
scalacOptions += "-Ypartial-unification"

// settings for sbt-assembly 
// see https://github.com/sbt/sbt-assembly

test in assembly := {}
// mainClass in assembly := Some("uk.ac.york.safire.optimisation.mitm.atb.UoYEarlyPrototypeDemo")
// mainClass in assembly := Some("uk.ac.york.safire.optimisation.HttpRemoteOptimisationEngineServer")
mainClass in assembly := Some("uk.ac.york.safire.optimisation.HttpRemoteObjectiveFunctionServer")

// lazy val app = (project in file("app")).
// //  settings(assemblySettings: _*)
//   settings(mainClass in assembly := Some("uk.ac.york.safire.optimisation.mitm.atb.UoYEarlyPrototypeDemo"))

// End -- settings for sbt-assembly 

////////////////////////////////////////

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "uk.ac.york.safire"
      , version := "0.4-snapshot"
      , scalaVersion    := "2.12.2"
    )),
    name := "safire-optimisation-engine",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"         % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"     % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"       % akkaVersion,
	  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion, // "10.0.10" 
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test
//      , "org.scalatest"     %% "scalatest"         % "3.0.1"         % Test
      , "org.apache.httpcomponents" % "httpclient" % "4.5.3"
      , "org.apache.httpcomponents" % "httpcore" % "4.4.6"
	  , "io.vavr" % "vavr" % "0.9.2"
      , "com.google.code.gson" % "gson" % "2.8.2"
      , "com.google.guava" % "guava" % "23.0"
      , "org.choco-solver" % "choco-solver" % "4.0.6"
	  , "com.novocode" % "junit-interface" % "0.11" % "test"
	  , "org.uma.jmetal" % "jmetal-core" % "5.4"
	  , "org.uma.jmetal" % "jmetal-algorithm" % "5.4"
	  , "org.uma.jmetal" % "jmetal-exec" % "5.4"
	  , "org.uma.jmetal" % "jmetal" % "5.4"
//	  , "org.typelevel" %% "spire" % "0.14.1"
      , "org.apache.kafka" %% "kafka" % "1.1.0"
      // , "org.typelevel" %% "cats-core" % "1.0.1"
      // , "com.github.docker-java" % "docker-java" % "3.0.14"
      , "com.codepoetics" % "protonpack" % "1.15"
//      , "joda-time" % "joda-time" % "2.10"
      , "org.slf4j" % "slf4j-nop" % "1.7.25" % Test
      , "commons-cli" % "commons-cli" % "1.4"
    )
  )

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)
enablePlugins(AshScriptPlugin)

// http://jkinkead.blogspot.co.uk/2015/04/running-with-alternate-jvm-args-in-sbt.html
fork in run := true
// Set flags for java. Memory, GC settings, properties, etc.
javaOptions ++= Seq("-Xmx6g")
// End -- http://jkinkead.blogspot.co.uk/2015/04/running-with-alternate-jvm-args-in-sbt.html

 mainClass in (Compile, run) := Some("uk.ac.york.safire.optimisation.HttpRemoteOptimisationEngineClient" )
// mainClass in (Compile, run) := Some("uk.ac.york.safire.optimisation.maxplusplant.TestPlant")
// mainClass in (Compile, run) := Some("uk.ac.york.safire.optimisation.HttpRemoteOptimisationEngineServer" )
// mainClass in Compile := Some("uk.ac.york.safire.optimisation.HttpRemoteOptimisationEngineServer" )
// mainClass in Compile := Some("uk.ac.york.safire.optimisation.HttpRemoteObjectiveFunctionServer" )
// mainClass in (Compile, run) := Some("uk.ac.york.safire.optimisation.mitm.atb.ATBSimulatorKafkaProducer" )
// mainClass in (Compile, run) := Some("uk.ac.york.safire.optimisation.mitm.atb.UoYEarlyPrototypeDemo" )
// ^ for some reason, having (Compile, run) in the line above interferes with docker deployment

dockerBaseImage       := "openjdk:jre-alpine"

// End ///////////////////////////////////////////////////////////////
