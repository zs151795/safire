package uk.ac.york.safire.optimisation.ia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.util.Pair;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.google.common.primitives.Doubles;

import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.OptimisationResult;
import uk.ac.york.safire.metrics.Utility;
import uk.ac.york.safire.metrics.ValueVisitor;
import uk.ac.york.safire.optimisation.ObjectiveFunction;
import uk.ac.york.safire.optimisation.OptmisationUtility;
import uk.ac.york.safire.optimisation.mitm.atb.ATBSimulatorKafkaProducer;
import uk.ac.york.safire.optimisation.mitm.atb.OnaHardwiredConfigurationType;
import uk.ac.york.safire.optimisation.mitm.atb.OnaSingleObjectiveFunction;
import uk.ac.york.safire.optimisation.mitm.atb.UoYEarlyPrototypeDemo;
import uk.ac.york.safire.optimisation.mitm.atb.ATBSimulatorKafkaProducer.RecipeInfo;

///////////////////////////////////

public class TestOnaObjectiveFunction {

	@Test
	public void testWeightedMultiobjectives() {
		final Random rng = new java.util.Random( 0xDEADBEEF );

		final int percentageAvailability = 100;		
		
		final int JMAX = 11;

		// final ConfigurationType ct = OnaHardwiredConfigurationType.hardwiredConfigurationType(percentageAvailability, rng);
		final boolean isMultiObjective = false;
		final ConfigurationType ct = tinyHardwiredConfigurationType(percentageAvailability, isMultiObjective, rng);
		final Configuration config = Utility.randomConfiguration(ct, rng);

		final ObjectiveFunction.LocalObjectiveFunction of = new OnaSingleObjectiveFunction();			

		///////////////////////////
		
		final int numOEInvocations = 30;
		for( int j=0; j<JMAX; ++j ) {
				
			OnaSingleObjectiveFunction.W1_$eq( jeep.math.LinearInterpolation.apply(j, 0, JMAX - 1, 0.0, 1.0) );
			OnaSingleObjectiveFunction.W2_$eq( 1.0 - OnaSingleObjectiveFunction.W1() );

		//	jeep.lang.Diag.println( "W1: " + OnaObjectiveFunction.W1() + " " + OnaObjectiveFunction.W2() );
			
//			final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config,of,rng);
//			final Configuration finalConfig = or.getReconfigurations().get(0);
		
//			ValueVisitor< Number > visitor = new ValueVisitor.NumberVisitor();
//			final double makespan = visitor.visit( finalConfig.getKeyObjectives().get("makespan") ).doubleValue();
//			final double cost = visitor.visit( finalConfig.getKeyObjectives().get("totalcost") ).doubleValue();
//			final double normalisedMakespan = visitor.visit( finalConfig.getKeyObjectives().get("makespan-normalised") ).doubleValue();
//			final double normalisedCost = visitor.visit( finalConfig.getKeyObjectives().get("totalcost-normalised") ).doubleValue();
//			
//			System.out.printf( "makespan: %f, cost: %f\n", makespan, cost );
//			System.out.printf( "makespan-normalised: %f, cost-normalised: %f\n", normalisedMakespan, normalisedCost );			
			averageOEResults(numOEInvocations, config, of, rng);			
		}
	}
	
	///////////////////////////////
	
	public static ConfigurationType 
	tinyHardwiredConfigurationType(int percentageAvailability, boolean isMultiobjective, Random random) {

		final String [] resourceNames = { 
			"Medium 1", "Large 1", "Medium 2", "Large 2",
			"Medium 3", "Large 3", "Medium 4", "Large 4"				
		};

		// Convention: resource names prefixes (up to the first space) denote the same resource
		// hence they are mutually exclusive:
		final BiPredicate< String, String > mutex = (String resource1,String resource2) -> {
			final String [] s1 = resource1.split( " " );
			final String [] s2 = resource2.split( " " );
			return ( s1.length == 0 && s2.length == 0 ) || ( s1[0].equals( s2[0] ) );
		};

		final java.util.function.Function< String [], List< Integer > > resourceIndices = ( String [] names ) -> {
			List< Integer > result = Arrays.asList(names).stream().map( 
				(String nm) -> Arrays.asList(resourceNames).indexOf( nm ) ).collect(Collectors.toList());
			if( result.contains(-1) )
				jeep.lang.Diag.println( Arrays.toString(names) + " contains bad string:\n" + result );
			
			return result;
		};
		
		final Map< Pair< String, String >, Boolean > mutices = new HashMap<>();
		for( String r1: resourceNames )
			for( String r2: resourceNames )
				mutices.put( Pair.create(r1,r2), mutex.test(r1,r2 ) );
		
		final Map< String, List< RecipeInfo > > recipeInfo = new HashMap<>();
		
		recipeInfo.put( "P12", Lists.newArrayList( new RecipeInfo( "P12", 1, 
			// resourceIndices.apply(new String [] { "Large 1", "Large 2", "Large 3", "Large 4" } )
				resourceIndices.apply(resourceNames )				
		) ) );
		
		
		recipeInfo.put( "P13", Lists.newArrayList( new RecipeInfo( "P13", 1, 
				// resourceIndices.apply(new String [] { "Large 1", "Large 2", "Large 3", "Large 4" } )
				resourceIndices.apply( resourceNames )				
		) ) );
		
		
		recipeInfo.put( "P14", Lists.newArrayList( new RecipeInfo( "P14", 1, 
			// resourceIndices.apply(new String [] { "Large 1", "Large 2", "Large 3", "Large 4" } )
			resourceIndices.apply( resourceNames )				
		) ) );

		
		final Map< Pair< String, String >, Double > recipeAndResourceNameToCost = new HashMap<>();
		
		recipeAndResourceNameToCost.put( Pair.create( "P12", "Large 1" ), 2058.4 );
		recipeAndResourceNameToCost.put( Pair.create( "P12", "Large 2" ), 2908.8 );
		recipeAndResourceNameToCost.put( Pair.create( "P12", "Large 3" ), 1883.1 );
		recipeAndResourceNameToCost.put( Pair.create( "P12", "Large 4" ), 1816.1 );
		
		recipeAndResourceNameToCost.put( Pair.create( "P13", "Large 1" ), 4651.4 );
		recipeAndResourceNameToCost.put( Pair.create( "P13", "Large 2" ), 6573.1 );
		recipeAndResourceNameToCost.put( Pair.create( "P13", "Large 3" ), 4255.4 );
		recipeAndResourceNameToCost.put( Pair.create( "P13", "Large 4" ), 3566.2 );
		
		recipeAndResourceNameToCost.put( Pair.create( "P14", "Large 1" ), 6201.9 );
		recipeAndResourceNameToCost.put( Pair.create( "P14", "Large 2" ), 8764.1 );
		recipeAndResourceNameToCost.put( Pair.create( "P14", "Large 3" ), 5673.8 );
		recipeAndResourceNameToCost.put( Pair.create( "P14", "Large 4" ), 4754.9 );

		recipeAndResourceNameToCost.put( Pair.create( "P12", "Medium 1" ), 2058.4 / 2.0 );
		recipeAndResourceNameToCost.put( Pair.create( "P12", "Medium 2" ), 2908.8 / 2.0 );
		recipeAndResourceNameToCost.put( Pair.create( "P12", "Medium 3" ), 1883.1 / 2.0 );
		recipeAndResourceNameToCost.put( Pair.create( "P12", "Medium 4" ), 1816.1 / 2.0 );
		
		recipeAndResourceNameToCost.put( Pair.create( "P13", "Medium 1" ), 4651.4 / 2.0 );
		recipeAndResourceNameToCost.put( Pair.create( "P13", "Medium 2" ), 6573.1 / 2.0 );
		recipeAndResourceNameToCost.put( Pair.create( "P13", "Medium 3" ), 4255.4 / 2.0 );
		recipeAndResourceNameToCost.put( Pair.create( "P13", "Medium 4" ), 3566.2 / 2.0 );
		
		recipeAndResourceNameToCost.put( Pair.create( "P14", "Medium 1" ), 6201.9 / 2.0 );
		recipeAndResourceNameToCost.put( Pair.create( "P14", "Medium 2" ), 8764.1 / 2.0 );
		recipeAndResourceNameToCost.put( Pair.create( "P14", "Medium 3" ), 5673.8 / 2.0 );
		recipeAndResourceNameToCost.put( Pair.create( "P14", "Medium 4" ), 4754.9 / 2.0 );
		
		///////////////////////////

		final Map< Pair< String, String >, Integer > recipeAndResourceNameToCuttingTime = new HashMap<>();
		
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P12", "Large 1" ), (int)(60 * 3337.9 ) );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P12", "Large 2" ), (int)(60 * 4768.5 ) );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P12", "Large 3" ), (int)(60 * 2953.9 ) );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P12", "Large 4" ), (int)(60 * 2688.9 ) );
		
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P13", "Large 1" ), (int)(60 * 7542.9 ) );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P13", "Large 2" ), (int)(60 * 10775.5 ) );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P13", "Large 3" ), (int)(60 * 6675.1 ) );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P13", "Large 4" ), (int)(60 * 5280.0 ) );
		
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P14", "Large 1" ), (int)(60 * 10057.1 ) );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P14", "Large 2" ), (int)(60 * 14367.3 ) );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P14", "Large 3" ), (int)(60 * 8900.1 ) );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P14", "Large 4" ), (int)(60 * 7040.0 ) );

		recipeAndResourceNameToCuttingTime.put( Pair.create( "P12", "Medium 1" ), (int)(60 * 3337.9 ) * 2 );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P12", "Medium 2" ), (int)(60 * 4768.5 ) * 2 );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P12", "Medium 3" ), (int)(60 * 2953.9 ) * 2 );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P12", "Medium 4" ), (int)(60 * 2688.9 ) * 2 );
		
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P13", "Medium 1" ), (int)(60 * 7542.9 ) * 2 );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P13", "Medium 2" ), (int)(60 * 10775.5 ) * 2 );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P13", "Medium 3" ), (int)(60 * 6675.1 ) * 2 );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P13", "Medium 4" ), (int)(60 * 5280.0 ) * 2 );
		
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P14", "Medium 1" ), (int)(60 * 10057.1 ) * 2 );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P14", "Medium 2" ), (int)(60 * 14367.3 ) * 2 );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P14", "Medium 3" ), (int)(60 * 8900.1 ) * 2 );
		recipeAndResourceNameToCuttingTime.put( Pair.create( "P14", "Medium 4" ), (int)(60 * 7040.0 ) * 2 );
		
		///////////////////////////

	   return OnaHardwiredConfigurationType.makeConfigurationType( 
			  recipeInfo, resourceNames, 
			  recipeAndResourceNameToCost,
			  percentageAvailability,  
			  recipeAndResourceNameToCuttingTime, 
			  mutices,
			  isMultiobjective,
			  random ); 
	}
	
	///////////////////////////////
	
	private static void averageOEResults(int numInvocations, Configuration config, ObjectiveFunction.LocalObjectiveFunction of, Random rng) {

		final List< Double > costs = new ArrayList<>();
		final List< Double > makespans = new ArrayList<>();		
		
		for( int i=0; i<numInvocations; ++i ) {
		
			final int urgency = 50;
			final int iterations = 100;			
			final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config,of,urgency,iterations,rng);
			final Configuration finalConfig = or.getReconfigurations().get(0);
		
			ValueVisitor< Number > visitor = new ValueVisitor.NumberVisitor();
			final double makespan = visitor.visit( finalConfig.getKeyObjectives().get("makespan") ).doubleValue();
			final double cost = visitor.visit( finalConfig.getKeyObjectives().get("totalcost") ).doubleValue();
			
			costs.add(cost);
			makespans.add(makespan);			
		}

		System.out.printf( "W1: %f, W2: %f\n", OnaSingleObjectiveFunction.W1(), OnaSingleObjectiveFunction.W2() );
		
		{
			final Double mean = StatUtils.mean(Doubles.toArray(costs));
			final Double sd = Math.sqrt(StatUtils.populationVariance(Doubles.toArray(costs)));
			final Double min = StatUtils.min(Doubles.toArray(costs));
			final Double max = StatUtils.max(Doubles.toArray(costs));
			System.out.printf( "cost -- min: %f, max: %f, mean: %f, sd: %f\n", min, max, mean, sd );
		}
		{
			final Double mean = StatUtils.mean(Doubles.toArray(makespans));
			final Double sd = Math.sqrt(StatUtils.populationVariance(Doubles.toArray(makespans)));
			final Double min = StatUtils.min(Doubles.toArray(makespans));
			final Double max = StatUtils.max(Doubles.toArray(makespans));
			System.out.printf( "makespan -- min: %f, max: %f, mean: %f, sd: %f\n", min, max, mean, sd );
		}
	}
	
	///////////////////////////////

	private static List< Pair< Double, Double > > 
	makespansAndCosts(OptimisationResult or) {
		
		List< Pair< Double, Double > > result = new ArrayList<>();
		for( Configuration config: or.getReconfigurations()) {
		
			ValueVisitor< Number > visitor = new ValueVisitor.NumberVisitor();
			final Double makespan = visitor.visit( config.getKeyObjectives().get("makespan") ).doubleValue();
			final Double cost = visitor.visit( config.getKeyObjectives().get("totalcost") ).doubleValue();
			result.add(Pair.create(makespan,cost) );
		}
		
		return result;
	}
	
	///////////////////////////////	
	
	@Test
	public void test() {
		final Random rng = new java.util.Random( 0xDEADBEEF );

		final int percentageAvailability = 70;		
jeep.lang.Diag.println( "percentageAvailability: " + percentageAvailability );

		final boolean isMultiObjective = false;
		final ConfigurationType ct = tinyHardwiredConfigurationType(percentageAvailability, isMultiObjective, rng);
		final Configuration config = Utility.randomConfiguration(ct, rng);
		
		OnaSingleObjectiveFunction.parseUnavailability(config);
		
		final ObjectiveFunction.LocalObjectiveFunction of = new OnaSingleObjectiveFunction();
		final int urgency = 50;
		
		// final int numOEInvocations = 30;		
		
		List< Pair< Double, Double > > mc1, mc2;	
		
		{
			OnaSingleObjectiveFunction.W1_$eq( 0.0 );
			OnaSingleObjectiveFunction.W2_$eq( 1.0 - OnaSingleObjectiveFunction.W1() );

			jeep.lang.Diag.println( "OF weightings: W1: " + OnaSingleObjectiveFunction.W1() + ", W2: " + OnaSingleObjectiveFunction.W2() );

			final int iterations = 500;
			final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config,of,urgency,iterations, rng);
			// final Configuration finalConfig = or.getReconfigurations().get(0);
			// jeep.lang.Diag.println( ATBSimulatorKafkaProducer.getStatusString(finalConfig) );
			
			// System.out.printf( "makespan: %f, cost: %f\n", makespan, cost );			
			// averageOEResults(numOEInvocations, config, of, rng);
			mc1 = makespansAndCosts(or);
			// System.out.println( "makespans and costs: " + mc1.size() );			
			System.out.println( "makespans and costs: " + mc1 );
			jeep.lang.Diag.println( OptmisationUtility.keyObjectivesHistogram(or.getReconfigurations() ) );			
		}
		{
			OnaSingleObjectiveFunction.W1_$eq( 1.0 );
			OnaSingleObjectiveFunction.W2_$eq( 1.0 - OnaSingleObjectiveFunction.W1() );
			jeep.lang.Diag.println( "=========================================================" );			
			jeep.lang.Diag.println( "OF weightings: W1: " + OnaSingleObjectiveFunction.W1() + ", W2: " + OnaSingleObjectiveFunction.W2() );
			final int iterations = 100;
			final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config,of,urgency,iterations,rng);
			mc2 = makespansAndCosts(or);
			System.out.println( "makespans and costs: " + mc2 );
			
			jeep.lang.Diag.println( OptmisationUtility.keyObjectivesHistogram(or.getReconfigurations() ) );			
			// System.out.println( "makespans and costs: " + makespansAndCosts(or) );
			
//			final Configuration finalConfig = or.getReconfigurations().get(0);
//			// jeep.lang.Diag.println( ATBSimulatorKafkaProducer.getStatusString(finalConfig) );
//			
//			ValueVisitor< Number > visitor = new ValueVisitor.NumberVisitor();
//			final Double makespan = visitor.visit( finalConfig.getKeyObjectives().get("makespan") ).doubleValue();
//			final Double cost = visitor.visit( finalConfig.getKeyObjectives().get("totalcost") ).doubleValue();
//			
//			System.out.printf( "makespan: %f, cost: %f\n", makespan, cost );			
//			// averageOEResults(numOEInvocations, config, of, rng);			
		}
		// System.out.println( mc1.equals(mc2));
	}	
}

// End ///////////////////////////////////////////////////////////////
