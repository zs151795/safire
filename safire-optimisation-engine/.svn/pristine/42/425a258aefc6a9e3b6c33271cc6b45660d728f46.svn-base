package uk.ac.york.safire.optimisation.maxplusplant;

import java.util.List;
import java.util.Random;

import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.optimisation.JMetalLocalOptimisationEngine;
import uk.ac.york.safire.optimisation.MockObjectiveFunction;
import uk.ac.york.safire.optimisation.OEClientCommon;
import uk.ac.york.safire.optimisation.OptimisationEngine;

///////////////////////////////////

public final class TuneCostParameters {

	private static final double energyCostWeighting = 1.0;
	private static final double makespanCostWeighting = 1.0;
	private static final double computeTimeCostWeighting = 1.0;
    
    ///////////////////////////
	
	private static final double urgency = 0.5;
	private static final double quality = 0.5;

	private static final int numGenerations = 1000;	    
	private static final int populationSize = 100;
    
	private static final int minEvaluations = populationSize * numGenerations;
	private static final int maxEvaluations = minEvaluations;	    

    ///////////////////////////////
    
	public static void run(MockObjectiveFunction objectiveFunction, long seed) {
		
	    // JMetal single-objective GA:
//		final OptimisationEngine oe =  JMetalLocalOptimisationEngine.soga(
//    	minEvaluations, maxEvaluations, objectiveFunction, new Random(seed));	    	
		final OptimisationEngine oe =  JMetalLocalOptimisationEngine.nsga3(
			minEvaluations, maxEvaluations, objectiveFunction, new Random(seed));	    	
	    
	    System.out.println("Optimum fitness: " + objectiveFunction.optimum() );	    
	    System.out.println( "Applying Optimisation Engine: " + oe );
	    
	    final scala.Tuple2< List< scala.Tuple2<Configuration,Double> >, Long > populationWithFitnessAndComputeTime = 
	    	OEClientCommon.run(oe, objectiveFunction, populationSize, urgency, quality, seed);
	    
	    // final List< scala.Tuple2<Configuration,Double> > populationWithFitness = populationWithFitnessAndComputeTime._1();
	    final List< Configuration > population = populationWithFitnessAndComputeTime._1().stream().map( x -> x._1() ).collect(java.util.stream.Collectors.toList());	    
	    final Long computeTime = populationWithFitnessAndComputeTime._2();
	    
	    System.out.println( "Compute time (secs): " + ( computeTime / 1000.0 ) );
	    System.out.println( "relativeErrorStats: " + objectiveFunction.relativeErrorStats(population) );
	}
	
	///////////////////////////////
	
	public static void main( String [] args ) {

	    System.out.println( "Parameters -- populationSize: " + populationSize + 
	    	", minEvaluations: " + minEvaluations +
	    	", maxEvaluations: " + maxEvaluations + 
	    	", urgency: " + urgency + 
	    	", quality: " + quality +
	    	", energyCostWeighting: " + energyCostWeighting + 
	    	", makespanCostWeighting: " + makespanCostWeighting +
	    	", computeTimeCostWeighting: " + computeTimeCostWeighting   	
	    );
	    	
	    System.out.println();

	    final long seed = 0xDEADBEEF;
	    final Random rng = new Random(seed);
	    
	    ///////////////////////////
	    
	    //	    final MockObjectiveFunction objectiveFunction =
	    //	// MockObjectiveFunction.smallPlant(energyCostWeighting,makespanCostWeighting,rng);	    		
	    // MockObjectiveFunction.largerPlant(energyCostWeighting,makespanCostWeighting);
	    final int numBenchmarks = 30;
	    final long [] seeds = rng.longs(numBenchmarks).toArray();  
	    
	    final List< MockObjectiveFunction > benchmarks = 
	    	ScaledPlantGenerator.generateBenchmarkInstances(energyCostWeighting,makespanCostWeighting, numBenchmarks,seed);

	    for(int i=0; i<benchmarks.size(); ++i) {
	    	System.out.println( "Running JMetal GA on instance " + (i+1) + " of " + benchmarks.size() );
	    	final long startTime = System.currentTimeMillis();
	    	run( benchmarks.get(i), seeds[i] );	    	
	    	final long endTime = System.currentTimeMillis();
	    	System.out.println( "elapsed (secs): " + ( ( endTime - startTime ) / 1000.0 ) );
	    }
	}
}

// End ///////////////////////////////////////////////////////////////
