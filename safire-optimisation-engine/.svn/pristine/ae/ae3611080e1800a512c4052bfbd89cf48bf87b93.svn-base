package uk.ac.york.safire.optimisation.ia

case class TaskId(name: String)
case class Resource(id: String)

///////////////////////////////////

case class Product(name: String, quantity: Int) {
  require(quantity > 0)
}

case class Task(
    id: TaskId,
    resourceExecutionTime: Map[Resource, Int],
    relations: Map[TaskId, IntervalRelation],
    // parents: Set[TaskId],  
    releaseTime: Int,
    // mutex: Resource => Set[Resource] = r => Set(),
    mutexMap: Map[Resource, Set[Resource]] = Map.empty,
    // antiAffinity: Resource => Set[Resource] = r => Set(),
    antiAffinityMap: Map[Resource, Set[Resource]] = Map.empty,
    finalProduct: Option[Product] = None
) {

  require(releaseTime >= 0)

  /////////////////////////////////

  def mutex(r: Resource): Set[Resource] = {
    //  jeep.lang.Diag.println( s"mutex($id,$r): ${mutexMap.getOrElse(r, Set())}" )  
    mutexMap.getOrElse(r, Set())
  }

  def antiAffinity(r: Resource): Set[Resource] = antiAffinityMap.getOrElse(r, Set())

  /////////////////////////////////

  // lazy 
  val immediatePrecessors: Set[TaskId] =
    relations.filter {
      case (taskId, ir) =>
        ir == IntervalRelation.PreceededImmediatelyBy
    }.map { _._1 }.toSet

  // lazy 
  val generalPredecessors: Set[TaskId] =
    relations.filter {
      case (taskId, ir) =>
        ir == IntervalRelation.PreceededGenerallyBy || ir == IntervalRelation.PreceededImmediatelyBy
    }.map { _._1 }.toSet

  //  def availableResources(r: Resource): Set[Resource] = {
  //    require( isCompatibleWithResource( r ) )
  //    compatibleResources.diff( mutex( r ) )  
  //  }

  def isImmediateSuccessorOf(other: Task): Boolean =
    immediatePrecessors.contains(other.id)

  def compatibleResources: Set[Resource] =
    resourceExecutionTime.keySet

  def isCompatibleWithResource(r: Resource): Boolean =
    resourceExecutionTime.keySet.contains(r)

  def duration(r: Resource): Int = {
    require(isCompatibleWithResource(r))
    resourceExecutionTime(r)
  }

  def notBefore(time: Int): Task =
    copy(releaseTime = math.max(releaseTime, time))

  override def toString() = {
    import org.apache.commons.lang3.builder._
    ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE)
  }
}

///////////////////////////////////

object Task {

  def single(id: TaskId, resourceExecutionTime: Map[Resource, Int], releaseTime: Int,
    mutex: Map[Resource, Set[Resource]] = Map()): Task =
    Task(id, resourceExecutionTime, Map(), releaseTime, mutex)

  def sequenceDependentFromJavaHelper(
    id: TaskId,
    resourceExecutionTime: java.util.Map[Resource, Integer]
  ): Task = {
    import scala.collection.JavaConversions._
    val ret: Map[Resource, Int] = resourceExecutionTime.toMap.map { case (k, v) => (k, v.toInt) }
    Task(id, ret, Map(), releaseTime = 0)
  }
}

// End ///////////////////////////////////////////////////////////////
