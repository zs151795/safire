package uk.ac.york.safire.optimisation.ia

import scala.collection.mutable.{ Map => MutableMap }

///////////////////////////////////

trait ProductReporter {
  def apply(r: Resource, schedule: List[(Task, Interval)]): Option[Product]
}

///////////////////////////////////

case class Schedule(byResource: Map[Resource, List[(Task, Interval)]]) {

  require(Schedule.topologicallySorted(this))
  require(Schedule.compatibleResources(this))
  require(Schedule.uniqueTaskIds(this))
  require(Schedule.resourcewiseMonotoneIncreasingIntervals(this))

  /////////////////////////////////

  def makespan: Int =
    if (byResource.isEmpty) 0 else byResource.values.map { _.last._2.upper }.max

  def idleTime(r: Resource, mutex: Set[Resource]): Int = {
    def impl(r: Resource): Int = byResource.get(r) match {
      case Some(l) => l.last._2.upper
      case None => 0
    }

    byResource.get(r) match {
      case Some(l) => if (mutex.isEmpty) l.last._2.upper else math.max(l.last._2.upper, mutex.map { impl }.max)
      case None => if (mutex.isEmpty) 0 else math.max(0, mutex.map { r2 => impl(r2) }.max)
    }
  }

  def taskIds: Set[TaskId] =
    byResource.values.toList.flatten.map { _._1.id }.toSet

  def containsTaskId(tId: TaskId): Boolean =
    taskIds.contains(tId)

  def generalPredecesorsScheduled(t: Task): Boolean =
    t.generalPredecessors.forall { case taskId => containsTaskId(taskId) }

  def nonImmediatePredecesorsScheduled(t: Task): Boolean =
    t.generalPredecessors.forall { case taskId => if (t.immediatePrecessors.contains(taskId)) true else containsTaskId(taskId) }

  def entry(t: TaskId): Option[(Task, Interval)] =
    byResource.values.flatten.find { case (task, interval) => task.id == t }

  def completionTime(t: TaskId): Option[Int] =
    entry(t).map { _._2.upper }

  def maxCompletionTime(taskIds: Set[TaskId]): Int = {
    if (taskIds.isEmpty) 0 else {
      val times = taskIds.map { completionTime(_) }.flatten
      if (times.isEmpty) 0 else times.max
    }
  }

  /////////////////////////////////

  def append(r: Resource, t: Task, requireImmediatePredecesors: Boolean = true, notBeforeTime: Int = 0): Option[Schedule] = {
    require(t.isCompatibleWithResource(r))
    // require( generalPredecesorsScheduled(t) )
    require(!containsTaskId(t.id), s"${t.id} already present in ${taskIds}")
    require(notBeforeTime >= 0)

    val predecessorsScheduled = if (requireImmediatePredecesors) generalPredecesorsScheduled(t) else nonImmediatePredecesorsScheduled(t)
    if (predecessorsScheduled) {

      val lowerByDependencies = maxCompletionTime(t.generalPredecessors)
      val lowerByResource = if (!byResource.contains(r) || byResource(r).isEmpty) 0 else byResource(r).last._2.upper
      val lower = math.max(lowerByDependencies, math.max(lowerByResource, notBeforeTime))
      val entries = byResource.getOrElse(r, Nil)
      Some(Schedule(byResource.updated(r, entries :+ (t, Interval(lower, lower + t.duration(r))))))
    } else
      None
  }

  //  def append(seq: List[(Resource,Task)]): Schedule = {
  //    var result = this
  //    seq.foreach { case (r,t) => result = result.append(r,t) }
  //    result
  //  }

  def concat(schedule: Schedule): Option[Schedule] = {
    var result = this
    schedule.byResource.foreach {
      case (resource, tasks) =>
        tasks.foreach {
          case (task, interval) =>
            result.append(resource, task, requireImmediatePredecesors = true) match {
              case Some(newSchedule) => result = newSchedule
              case None => return None
            }
        }
    }
    Some(result)
  }

  def postponeBy(time: Int): Schedule = {
    require(time >= 0)
    Schedule(byResource.map { case (k, tasks) => k -> tasks.map { case (t, i) => (t, i.translate(time)) } })
  }

  /////////////////////////////////

  override def toString(): String = {
    def formatTasks(l: List[(Task, Interval)]): String =
      l.map { case (task, interval) => (task.id.name, interval) }.mkString("[", ",", "]")

    val byResourceStr = byResource.map { case (k, v) => s"${k.id} -> ${formatTasks(v)}" }.mkString(" ", "\n ", "")
    s"Schedule(\n${byResourceStr}\n)"
  }
}

/////////////////////////////////

object Schedule {

  def apply(): Schedule = Schedule(Map.empty[Resource, List[(Task, Interval)]])
  def apply(r: Resource, t: Task): Schedule =
    Schedule(Map(r -> List((t, Interval(t.releaseTime, t.duration(r))))))

  /////////////////////////////////

  def compatibleResources(schedule: Schedule): Boolean =
    schedule.byResource.forall {
      case (r, l) =>
        l.forall {
          case (t, i) =>
            t.isCompatibleWithResource(r)
        }
    }

  def uniqueTaskIds(ordered: List[Task]): Boolean =
    ordered.map { _.id }.toSet.size == ordered.size

  def uniqueTaskIds(schedule: Schedule): Boolean =
    schedule.taskIds.size == schedule.byResource.values.flatten.size

  def topologicallySorted(ordered: List[Task]): Boolean = {
    val mapTaskIdToIndex = Map() ++ ordered.map { _.id }.zipWithIndex

    ordered.forall { task =>
      val taskIndex = mapTaskIdToIndex(task.id)
      task.generalPredecessors.forall { parent =>
        mapTaskIdToIndex.contains(parent) &&
          mapTaskIdToIndex(parent) < taskIndex
      }
    }
  }

  def topologicallySorted(schedule: Schedule): Boolean =
    schedule.byResource.values.flatten.forall {
      case (t, i) =>
        schedule.maxCompletionTime(t.generalPredecessors) <= i.lower
    }

  def disjointIntervals(seq: List[Interval]): Boolean = {
    for (i <- 0 until seq.length; j <- i + 1 until seq.length)
      if (seq(i).overlaps(seq(j)))
        return false

    true
  }

  def monotoneIncreasingIntervals(seq: List[Interval]): Boolean =
    seq.zip(seq.tail).forall { case (a, b) => a.upper <= b.lower }

  def resourcewiseMonotoneIncreasingIntervals(schedule: Schedule): Boolean =
    schedule.byResource.values.forall { l => monotoneIncreasingIntervals(l.map { _._2 }) }

  def productReport(schedule: Schedule, reporter: ProductReporter): List[Option[Product]] =
    schedule.byResource.toList.map { case (res, tasks) => reporter(res, tasks) }

  def consolodatedProductReport(schedule: Schedule, reporter: ProductReporter): List[Product] = {
    val removeNones = productReport(schedule, reporter).flatten
    val mapNameToProducts = removeNones.groupBy { _.name }
    mapNameToProducts.map { case (name, products) => Product(name, products.map { _.quantity }.sum) }.toList
  }
}

// End ///////////////////////////////////////////////////////////////
