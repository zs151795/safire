package uk.ac.york.safire.optimisation.maxplusplant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;

import com.google.common.collect.ImmutableList;

import akka.stream.javadsl.Partition;
import scala.Tuple2;
import uk.ac.york.safire.metrics.*;
import uk.ac.york.safire.optimisation.*;

///////////////////////////////////

public final class GECCOGranularityExperimentsMain {

	private static final double energyCostWeighting = 1.0;
	private static final double makespanCostWeighting = 1.0;
	private static final double computeTimeCostWeighting = 1.0;

	private static final int numPartitions = 10;
    
    ///////////////////////////

	private static final int numBenchmarks = 30;

    private static final double urgency = 0.5;
	private static final double quality = 0.5;

	static final int maxStages = 100;
	// ^ maximum number of times the OE can be applied to a partition

	static final int maxUnimprovingStages = 20;
	
    ///////////////////////////////
	
	private static List< scala.Tuple2< Integer,List<Configuration > >> 
	partition(List<Configuration> population, int numPartitions) {
		final List< List< Configuration > > partitions = 
			com.google.common.collect.Lists.partition(population, population.size() / numPartitions);
		// Copying is necessary, since com.google.common.collect.Lists.partition 
		// returns an immutable list...
		List< scala.Tuple2< Integer,List<Configuration > > > result = new ArrayList<>();
		// result.addAll(partitions);		
		for( int i=0; i<partitions.size(); ++i )
			result.add( scala.Tuple2.apply(i, partitions.get(i)) );

		return result;
	}
	
    ///////////////////////////////	
	
	private static boolean allStopped(boolean [] stopConditions) {
		for( boolean b: stopConditions ) 
			if( !b ) 
				return false;
		return true;
	}
			
	public static StoppingConditionResult
	runUntilStoppingCondition(MockObjectiveFunction objectiveFunction, 
			List< Configuration > population,
			int numGAGenerations,
			Predicate< StoppingConditionFeatures > stoppingCondition,
		int numPartitions, long seed) {
		
		final int minEvaluations = numGAGenerations * population.size();
		final int maxEvaluations = minEvaluations;		
		
		final Random rng = new Random(seed);
		// JMetal single-objective GA:
//		final OptimisationEngine oe =  JMetalLocalOptimisationEngine.soga(
//		minEvaluations, maxEvaluations, objectiveFunction, rng);
		final OptimisationEngine oe =  JMetalLocalOptimisationEngine.nsga3(
				minEvaluations, maxEvaluations, objectiveFunction, rng);
		
		///////////////////////////
		
		final List< scala.Tuple2< Integer, List< Configuration > > > partitions = partition(population, numPartitions);
		
		final boolean [] shouldStopProcessingPartition = new boolean [numPartitions];
		
		final List< List< HistoryInfo > > partitionHistory = new ArrayList<>();
		for( int i=0; i<partitions.size(); ++i )
			partitionHistory.add( new ArrayList<>() );
		
		while( !allStopped(shouldStopProcessingPartition) ) {
		
			// for( int i=0; i<partitions.size(); ++i ) {
			// for( scala.Tuple2< Integer, List< Configuration > > partitionAndIndex: partitions )
			partitions.parallelStream().forEach( partitionAndIndex -> {
				
				final int i = partitionAndIndex._1();
				final List< Configuration > partition = partitionAndIndex._2();
				
				if( !shouldStopProcessingPartition[ i ] ) {
	    
					final long oeStartTime = System.currentTimeMillis(); 
					final OptimisationResult result = oe.optimise(new OptimisationArguments( partition, urgency, quality ) );
					final long oeEndTime = System.currentTimeMillis();
					final long oeElapsed = oeEndTime - oeStartTime;
				
					partitions.set( i, scala.Tuple2.apply(i, result.getReconfigurations() ) );
				
					// final Stats relativeErrorStats = objectiveFunction.relativeErrorStats( result.getReconfigurations() );
					final Stats flatCostStats = objectiveFunction.soFitnessStats( result.getReconfigurations() );

					double min = flatCostStats.min();
					// FIXME: sort this out
					if( !partitionHistory.get(i).isEmpty() )
						min = Math.min( min, StoppingConditionFeatures.calcLowestConfigurationCost( partitionHistory.get(i) ) );
//					for(int j=0; j<partitionHistory.get(i).size(); ++j ) {
//						final double previousMin = partitionHistory.get(i).get(j).min();
////						if( min > previousMin ) {
////							System.out.println( "WARNING: partition " + i + " is nonmonotonic: " + min + " versus " + partitionHistory.get(i) );
////						}
//						min = previousMin;
//					}
					
					final HistoryInfo info = HistoryInfo.apply(flatCostStats.withMin(min), oeElapsed );
					partitionHistory.get(i).add( info );
					final StoppingConditionFeatures features = StoppingConditionFeatures.apply( partitionHistory.get(i) );
					shouldStopProcessingPartition[i] = stoppingCondition.test( features );
				}
			} );
		}
		
		// final double lowestFlatCost = HistoryInfo.lowestConfigurationCost( history );
		return new StoppingConditionResult( partitionHistory ); // lowestFlatCost, totalElapsed, numIterations );
	}
	
	///////////////////////////////
	
	private static boolean defaultStoppingCondition(StoppingConditionFeatures features) {
		return features.sd() <= 1.e-3 || features.numStages() > maxStages || features.numUnimprovingStages() >= maxUnimprovingStages;
	}

	///////////////////////////////
	
	static List<StoppingConditionResult> 
	run(List< MockObjectiveFunction > benchmarks,
		int populationSize, int numGAGenerations,			
		Predicate< StoppingConditionFeatures > stoppingCondition, long seed) {

		final Random masterRng = new Random(seed);
	    
	    ///////////////////////////
	    
	    final long [] seeds = masterRng.longs(benchmarks.size()).toArray();  
	    
	   // final jeep.util.ProgressDisplay progress = new jeep.util.ProgressDisplay(benchmarks.size());
	    List< StoppingConditionResult > result = new ArrayList<>();
	    for(int i=0; i<benchmarks.size(); ++i) {
	    	final Random rng = new Random(seeds[i]);
	    	final MockObjectiveFunction objectiveFunction = benchmarks.get(i);
	    	
	    	final Configuration parentConfig = Utility.randomConfiguration(objectiveFunction.configurationType(), rng); 
	    	
		    final List< Configuration > population = 
		    	OptmisationUtility.simplePlantMkPopulation(parentConfig, objectiveFunction, populationSize, objectiveFunction.configurationType(), rng);
	    	
	    	// System.out.println( "Running JMetal SOGA on instance " + (i+1) + " of " + benchmarks.size() );
	    	final StoppingConditionResult scr = runUntilStoppingCondition( 
	    		benchmarks.get(i), population, numGAGenerations, stoppingCondition, numPartitions, seeds[i] );	    	
	    	result.add( scr );
	    	
	    	// progress.increment();
	    }
	    return result;
	}
	
	///////////////////////////////	
	
	public static void main( String [] args ) {

		final long seed = 0xDEADBEEF;
	    // final long seed = 0xBABEDEAF;
		
	    System.out.println( "Parameters -- " +
	    	"numBenchmarks: " + numBenchmarks +	    
//		    ", populationSize: " + populationSize + 
//	    	", minEvaluations: " + minEvaluations +
//	    	", maxEvaluations: " + maxEvaluations + ",\n" +
//	    	"urgency: " + urgency + 
//	    	", quality: " + quality +
	    	", maxStages: " + maxStages +
	    	", maxUnimprovingStages: " + maxUnimprovingStages +
	    	", energyCostWeighting: " + energyCostWeighting + 
	    	", makespanCostWeighting: " + makespanCostWeighting +
	    	", computeTimeCostWeighting: " + computeTimeCostWeighting +
	    	", seed: " + seed	    	
	    );
	    	
	    System.out.println();

		final int benchmarkIndex = 29;
		// final int benchmarkIndex = 29;
	    
		System.out.println( "benchmarkIndex: " + benchmarkIndex );
	    
	    final List< MockObjectiveFunction > benchmarks =
	    	java.util.Collections.singletonList(	    		
		    	ScaledPlantGenerator.generateBenchmarkInstances(energyCostWeighting,makespanCostWeighting, numBenchmarks, seed ).get(benchmarkIndex));
	    jeep.lang.Diag.println( "Vertices: " + benchmarks.get(0).plant().getGraph().getNoOfVertices() );
	    jeep.lang.Diag.println( "Edges: " + benchmarks.get(0).plant().getGraph().getNoOfEdges() );
	    jeep.lang.Diag.println( "Levels: " + benchmarks.get(0).plant().getGraph().getMaxLevel() );
	    
//	    final List< MockObjectiveFunction > benchmarks = java.util.Collections.singletonList( 
//	    		MockObjectiveFunction.largerPlant( energyCostWeighting, makespanCostWeighting ) );
//	    final List< MockObjectiveFunction > benchmarks = java.util.Collections.singletonList( 
//	    	MockObjectiveFunction.smallPlant( energyCostWeighting, makespanCostWeighting, new java.util.Random(seed ) ) );
	    
	    final long startTime = System.currentTimeMillis();
	    
	    System.out.println( "Running granularity experiments..." );

	    final List< Integer > genValues = ImmutableList.of( 100, 200, 500 );
	    final List< Integer > popValues = ImmutableList.of( 100, 200, 500 );	    
//	    final List< Integer > popValues = ImmutableList.of( // 100, 200, 
//		500, 
//		// 700, 
//		1000 );
	    // final List< Integer > genValues = ImmutableList.of( 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 );
//	    final List< Integer > genValues = ImmutableList.of( // 100, 200, 
//	    		500, 1000 );
	    
	    for( int pop=0; pop<popValues.size(); ++pop ) {
		    for( int gen=0; gen<genValues.size(); ++gen ) {	    	
		    	
		    	final int population = popValues.get(pop);
		    	final int generations = genValues.get(gen);
		    	
		    	System.out.println( "populationSize: " + population + ", generations: " + generations );
		    	
			    final long runStartTime = System.currentTimeMillis();		    	
		    	final List< StoppingConditionResult > results = run(benchmarks, population, generations, 
		    			features -> defaultStoppingCondition(features), seed);
			    final long runEndTime = System.currentTimeMillis();
			    
		    	final StoppingConditionResult result0 = results.get(0);
		    	
		    	assert( results.size() == 1 );
		    	
		    	System.out.println( "S.D. per-stage: " + result0.sdByStage().mkString("[", ",", "]") );
		    	
		    	// (a) final value returned by GA after each stage (! - needed for VC creation) together with the value predictions
		    	System.out.println( "Min values per-stage: " + result0.minValuesByStage().mkString("[", ",", "]") );
		    	
		    	System.out.println( "Predicted min values per-stage: " + result0.predictedMinValuesByStage().mkString("[", ",", "]") );		    	
		    	
		    	// (b) an average execution time of a single stage with regard to both the controlled variables
		    	System.out.println( "Average execution time per-stage:" + result0.averageProcessingTimeMillisByStage().mkString("[", ",", "]") );
		    	// (c) an average no. of stages with regard to both the controlled variables
		    	System.out.println( "Average number of stages: " + result0.averageNumStages() + " +/- " + result0.sdNumStages() );		    	

		    	// jeep.lang.Diag.println( result0 );
		    	
		    	System.out.println( "% min value prediction error by stage: " + 
		    		result0.minValuePercentErrorByStage().mkString("[", ",", "]") );
		    	
			    System.out.println( "elapsed (populationSize: " + population + ", generations: " + generations + "): " + ( runEndTime - runStartTime ) / 1000.0 );
		    	System.out.println( "========================================================" );
		    	System.out.println();
		    }
	    }
	    
	    final long endTime = System.currentTimeMillis();
	    System.out.println( "total elapsed: " + ( endTime - startTime ) / 1000.0 );	    
	    System.out.println( "All done." );		
	}
}

// End ///////////////////////////////////////////////////////////////
