package uk.ac.york.safire.optimisation.mitm.atb;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.function.Function;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import jeep.util.ProgressDisplay;
import uk.ac.york.aura.Operators;
import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.OptimisationArguments;
import uk.ac.york.safire.metrics.OptimisationResult;
import uk.ac.york.safire.optimisation.AuraLocalOptimisationEngine;
import uk.ac.york.safire.optimisation.ConfigurationUtilities;
import uk.ac.york.safire.optimisation.ObjectiveFunction;
import uk.ac.york.safire.optimisation.OptimisationEngine;
import uk.ac.york.safire.optimisation.RandomSearchLocalOptimisationEngine;

///////////////////////////////////

/**
 * From:
 * https://docs.hortonworks.com/HDPDocuments/HDP2/HDP-2.6.4/bk_kafka-component-guide/content/ch_kafka-development.html
 */

public final class UoYEarlyPrototypeDemo {

	public static OptimisationResult 
	invokeOE(Configuration config, ObjectiveFunction.LocalObjectiveFunction of, int urgency, int iterations, Random rng) {
	    // final IAObjectiveFunction of = new OasObjectiveFunction();

		// final OptimisationEngine oe = new OptimisationEngine.DoNothingOptimisationEngine();
		// final int populationSize = 10000;
		final int populationSize = 50;
		jeep.lang.Diag.println( "populationSize: " + populationSize );		
	    // final int minEvaluations = iterations * populationSize; // 10;
	    // final int maxEvaluations = iterations * populationSize; // 10;	    

		final OptimisationEngine oe = 
//				new RandomSearchLocalOptimisationEngine(of,minEvaluations,maxEvaluations,rng);
//				 JMetalLocalOptimisationEngine.nsga3(minEvaluations, maxEvaluations,of,rng);
//		final OptimisationEngine oe = new ExhaustiveSearchLocalOptimisationEngine(of,rng);	  
				new AuraLocalOptimisationEngine(of,iterations,rng);				
	    
	    // config = IAObjectiveFunction.makeAllResourcesAvailabile(config);
	       
	    // final List< Configuration > configs = Collections.singletonList( config );
		final List< Configuration > configs = new ArrayList<>(); // Collections.singletonList( config );
		configs.add(config);
		final Function< Configuration, Configuration > mutation = Operators.hyperMutation(1.0, rng);
		for( int i=0; i<populationSize - 1; ++i ) {
			// jeep.lang.Diag.println(i);			
			configs.add(mutation.apply(config));	
		}
	    
	    final double urgencyNormalised = jeep.math.LinearInterpolation.apply(urgency,0.0, 100.0, 0.0, 1.0 );
	    OnaSingleObjectiveFunction.W1_$eq(urgencyNormalised);
	    OnaSingleObjectiveFunction.W2_$eq(1.0 - urgencyNormalised);	    
	    
	    final long startTime = System.currentTimeMillis();
	    final OptimisationResult or = oe.optimise(new OptimisationArguments(configs,0.5,0.5) );
	    final long endTime = System.currentTimeMillis();
	    System.out.println( "Optmisation took: " + ( (endTime - startTime ) / 1000.0 ) + " seconds" );
	    return or;
	}

	///////////////////////////////
	
	private static long memoryUsageInMB() {
		System.gc();
	    final Runtime rt = Runtime.getRuntime();
	    final long usedMB = (rt.totalMemory() - rt.freeMemory()) / ( 1024 * 1024 );
	    return usedMB;
	}
	    
	///////////////////////////////
	
	private static void	run(KafkaConsumer<String, String> uoyConsumer, 
		Producer< String, String > uoyProducer, long maxTopicUpdates, 
		ObjectiveFunction.LocalObjectiveFunction of,		
		int urgency,
		int iterations,
		Random rng) {
	
		long maxMemoryUsage = memoryUsageInMB();
		for( int i=0; i<maxTopicUpdates; ++i ) {

			final ConsumerRecords<String, String> records = uoyConsumer.poll(5000);
			for (ConsumerRecord<String, String> record : records) {
				// System.out.printf("Received Message topic =%s, partition =%s, offset = %d, key = %s, value = %s\n", record.topic(), record.partition(), record.offset(), record.key(), record.value());
				
				final Configuration config = new uk.ac.york.safire.metrics.JsonConverter().fromJson(record.value(), Configuration.class);
				
				// jeep.lang.Diag.println( "objectives before:" + config.getKeyObjectives() ); 
				OptimisationResult or = invokeOE( config, of, urgency, iterations, rng );
			    final Configuration optimisedConfig = or.getReconfigurations().get(0);
			    // TODO: display status string for all reconfigurations 
			    System.out.println( ConfigurationUtilities.getStatusString(optimisedConfig) );
			    
			    final long memoryUsage = memoryUsageInMB();
			    maxMemoryUsage = Math.max(memoryUsage,maxMemoryUsage); 
			    System.out.println("Maximum memory usage: " + maxMemoryUsage + "MB" );
			    
				// jeep.lang.Diag.println( "objectives after:" + optimisedConfig.getKeyObjectives() );
				
				final String configJSON = new uk.ac.york.safire.metrics.JsonConverter().toJson(optimisedConfig);			    
				final Instant now = Instant.now(); // get time in UTC				
				final ProducerRecord<String, String> data = new ProducerRecord<String, String>("OptimisationEngineResult", "timestamp: " + now, configJSON );
				uoyProducer.send(data);
			}

			uoyConsumer.commitSync();
		}
	}
	
	///////////////////////////////
	
	private static KafkaConsumer<String, String> 
	createUoYConsumer(String bootstrapServersConfig) {
		final Properties consumerConfig = new Properties();
		consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig);
		consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "my-group");
		// consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName() );
		consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName() );

		return new KafkaConsumer<>(consumerConfig);
	}

	private static Producer<String, String>
	createUoYProducer(String bootstrapServersConfig) {
		final Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig);
       
		// props.put(ProducerConfig.CLIENT_ID_CONFIG, "test.app.producer");
		props.put(ProducerConfig.ACKS_CONFIG, "all");
		props.put(ProducerConfig.RETRIES_CONFIG, 0);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName() );
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName() );
	
		return new KafkaProducer<String, String>(props);
	}
	
	///////////////////////////////
	
	static void runUoY(long maxTopicUpdates, String bootstrapServersConfig, BusinessCase bc, int urgency, int iterations, Random rng) {

		final KafkaConsumer<String, String> uoyConsumer = createUoYConsumer(bootstrapServersConfig);
		final Producer<String, String> uoyProducer = createUoYProducer(bootstrapServersConfig);

	     final ObjectiveFunction.LocalObjectiveFunction of = bc.getObjectiveFunction();
		
		try {
			uoyConsumer.subscribe( Collections.singletonList("ResourceAvailability") );
		    run(uoyConsumer, uoyProducer, maxTopicUpdates, of, urgency, iterations, rng);
		}
		finally {
			uoyConsumer.close();
			uoyProducer.close();
			ATBSimulatorKafkaProducer.isFinished = true;
		}
	}
	
	///////////////////////////////
	
	static void runUoYandATB(long maxTopicUpdates, 
			String bootstrapServersConfig, 
			int finalPercentAvailability,
			long updateTopicStringEveryMillis,
			BusinessCase bc,
			int urgency,
			int iterations,
			Random rng) {

		final Producer<String, String
			> atbProducer = ATBSimulatorKafkaProducer.createATBProducer(bootstrapServersConfig);

		 final Thread atbProducerThread = new Thread("atbProducerThread"){
			 @Override   
			 public void run(){
				 try {
					 // final ConfigurationType configurationType = ATBSimulatorKafkaProducer.hardwiredConfigurationType(percentAvailability, rng);
					 ATBSimulatorKafkaProducer.run(finalPercentAvailability, atbProducer, updateTopicStringEveryMillis, maxTopicUpdates, bc, rng );
				 } finally {
					 atbProducer.close();			
				 }
			 }
		 };

		 //////////////////////////

		 atbProducerThread.start();
		 runUoY(maxTopicUpdates, bootstrapServersConfig, bc, urgency, iterations, rng);
	}
	
	///////////////////////////////	

	private static String makeBanner(String arg ) {
		final int imageWidth = 80; // 144;
		final int imageHeight = 32;		
		BufferedImage image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
		Graphics g = image.getGraphics();
		final int fontSize = 10; // 24
		g.setFont(new Font("Dialog", Font.PLAIN, fontSize));
		Graphics2D graphics = (Graphics2D) g;
		graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		// graphics.drawString( "Hello World!", 6, 24);
		graphics.drawString( arg, 6, fontSize ); // 24);
		// ImageIO.write(image, "png", new File("text.png"));

		StringBuilder result = new StringBuilder();
		for (int y = 0; y < imageHeight; ++y ) {
			StringBuilder sb = new StringBuilder();
			for (int x = 0; x < imageWidth; ++x ) {
				// sb.append(image.getRGB(x, y) == -16777216 ? " " : image.getRGB(x, y) == -1 ? "#" : "*");
				sb.append(image.getRGB(x, y) == -16777216 ? " " : "*");
			}
		    
			if (sb.toString().trim().isEmpty()) 
				continue;
			result.append(sb + "\n");
		}
		return result.toString();
	}
	
	///////////////////////////////
	
	private final static class Params {
		
		final int finalAvailabilityPercent;
		final int topicUpdateEverySeconds;
		final int numUpdates;
		final long randomSeed;
		final boolean useOnaObjectiveFunction;
		final int urgency;
		final int iterations;		
		
		///////////////////////////
		
		Params(int finalAvailabilityPercent, int topicUpdateEverySeconds, int numUpdates, long randomSeed,boolean useOnaObjectiveFunction, int urgency,int iterations) {
			this.finalAvailabilityPercent = finalAvailabilityPercent;
			this.topicUpdateEverySeconds = topicUpdateEverySeconds;
			this.numUpdates = numUpdates;
			this.randomSeed = randomSeed; 
			this.useOnaObjectiveFunction = useOnaObjectiveFunction;
			this.urgency = urgency;
			this.iterations = iterations;			
		}
	}
	
	private static Params parseCommandLine(String [] args) {
		
		int finalAvailabilityPercent = 90;
		int topicUpdateEverySeconds = 30;
		int numUpdates = 100;
		long randomSeed = System.currentTimeMillis();		
		boolean useOnaObjectiveFunction = false;
		int urgency = 50;
		int iterations = 10;		
		
		///////////////////////////
		
		final Options options = new Options();
		final Option availOpt = new Option("a", "avail", true, "Final mixer availability percentage");
		availOpt.setRequired(false);
		options.addOption(availOpt);

		final Option topicUpdateEveryOpt = new Option("u", "updateperiod", true, "ATB simulator - topic update period (seconds)");
		topicUpdateEveryOpt.setRequired(false);
		options.addOption(topicUpdateEveryOpt);

		final Option numUpdatesOpt = new Option("n", "numupdates", true, "ATB simulator - total number of updates ");
		numUpdatesOpt.setRequired(false);
		options.addOption(numUpdatesOpt);

		final Option randomSeedOpt = new Option("r", "randomseed", true, "Seed for random number generator");
		randomSeedOpt.setRequired(false);
		options.addOption(randomSeedOpt);

		final Option onaOpt = new Option("ona", "ona", false, "Use ONA objective function");
		onaOpt.setRequired(false);
		options.addOption(onaOpt);

		final Option urgencyOpt = new Option("urgency", "urgency", true, "Schedule urgency");
		urgencyOpt.setRequired(false);
		options.addOption(urgencyOpt);

		final Option iterOpt = new Option("i", "iterations", true, "Optimiser iterations");
		iterOpt.setRequired(false);
		options.addOption(iterOpt);
		
		final CommandLineParser parser = new DefaultParser();
		
		///////////////////////////

	     try {
	    	 final CommandLine cmd = parser.parse(options, args);
	    	 
	    	 {
	    		 String s = cmd.getOptionValue("avail", "" + finalAvailabilityPercent );
			     final int val = Integer.valueOf(s);
			     if( val >= 0 && val <= 100 )
			    	 finalAvailabilityPercent = val;
	    	 }
		     
	    	 {
	    		 String s = cmd.getOptionValue("updateperiod", "" + topicUpdateEverySeconds );
	    		 final int val = Integer.valueOf(s);
	    		 if( val > 0 )
	    			 topicUpdateEverySeconds = val;
	    	 }

	    	 {
	    		 String s = cmd.getOptionValue("numupdates", "" + numUpdates );
	    		 final int val = Integer.valueOf(s);
	    		 if( val > 0 )
	    			 numUpdates = val;
	    	 }

	    	 {
	    		 String s = cmd.getOptionValue("randomseed", "" + randomSeed );
	    		 final long val = Long.valueOf(s);
	    		 if( val > 0 )
	    			 randomSeed = val;
	    	 }

	    	 {
	    		 String s = cmd.getOptionValue("urgency", "" + urgency );
	    		 final double val = Double.valueOf(s);
	    		 if( val >= 0 && val <= 100 )
	    			 urgency = (int)val;
	    	 }
	    	 {
	    		 String s = cmd.getOptionValue("iterations", "" + iterations );
	    		 final int val = Integer.valueOf(s);
	    		 if( val >= 0 )
	    			 iterations = (int)val;
	    	 }

	    	 useOnaObjectiveFunction = cmd.hasOption("ona" );
	    	 
	     } catch (ParseException | NumberFormatException e ) {
	    	 System.out.println(e.getClass().getSimpleName() + " " + e.getMessage());
	    	 final HelpFormatter formatter = new HelpFormatter();
	    	 formatter.printHelp( UoYEarlyPrototypeDemo.class.getSimpleName(), options);

	    	 System.exit(1);
	     }

	     //////////////////////////	     
	     
	     return new Params(finalAvailabilityPercent, topicUpdateEverySeconds, numUpdates, randomSeed,useOnaObjectiveFunction,urgency,iterations);
	}
	
	///////////////////////////////	
	
	public static void main(String[] args) {

	     System.out.println( makeBanner("OE Demo") );
	     if( CompilationTime.get().isPresent() )
	    	 System.out.println( "build time: " + CompilationTime.get().get() );
		
	     System.out.println();
	     
	     ///////////////////////////	     
		
	 	final Params params = parseCommandLine(args);

	     ///////////////////////////
		
	     final Random rng = new Random(0xDEADBEEF);
	     final String bootstrapServersConfig = "localhost:9092";
	     final long updateTopicStringEveryMillis = params.topicUpdateEverySeconds * 1000;

	     ///////////////////////////
		
	     System.out.println( "PARAMETERS:" );		
	     System.out.println( "Final mixer availability probability: " + params.finalAvailabilityPercent + "%" );
	     System.out.println( "ATB simulator posts mixer status topic updates every " + params.topicUpdateEverySeconds + " seconds");
	     System.out.println( "Num ATB simulator topic updates: " + params.numUpdates );
	     System.out.println( "Random seed: " + params.randomSeed );
	     System.out.println( "OE invocations: " + params.iterations );	     
	     System.out.println( "Use ONA objective function: " + params.useOnaObjectiveFunction );
	     System.out.println( "Schedule urgency: " + params.urgency + "%" );
	     System.out.println( "End PARAMETERS" );		
	     System.out.println();
		
	     System.out.println( "Configuring..." );		
	     final long maxCount = 1L << 32;
	     final ProgressDisplay progress = new ProgressDisplay(maxCount); 
	     for( long i=0; i<maxCount; ++i )
	    	 progress.increment();
		
	     ///////////////////////////
		
	     final BusinessCase bc = params.useOnaObjectiveFunction ? BusinessCase.ONA : BusinessCase.OAS;
	     
	    runUoYandATB(
			params.numUpdates, 
			bootstrapServersConfig, 
			params.finalAvailabilityPercent,
			updateTopicStringEveryMillis,
			bc,
			params.urgency,
			params.iterations,
			rng);
		
	     ///////////////////////////
	    
		System.out.println( "All done." );
	}
}

// End ///////////////////////////////////////////////////////////////
