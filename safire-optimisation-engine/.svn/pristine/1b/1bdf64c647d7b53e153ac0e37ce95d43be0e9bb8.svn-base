package uk.ac.york.safire.optimisation

import org.uma.jmetal.algorithm._
import org.uma.jmetal.algorithm.impl._
import org.uma.jmetal.algorithm.multiobjective._
import org.uma.jmetal.algorithm.multiobjective.mocell._
import org.uma.jmetal.algorithm.multiobjective.nsgaii._
import org.uma.jmetal.algorithm.multiobjective.nsgaiii._
import org.uma.jmetal.algorithm.multiobjective.pesa2._
import org.uma.jmetal.algorithm.multiobjective.spea2._
import org.uma.jmetal.algorithm.multiobjective.smsemoa._
import org.uma.jmetal.operator._
import org.uma.jmetal.operator.impl.crossover._
import org.uma.jmetal.operator.impl.mutation._
import org.uma.jmetal.operator.impl.selection._
import org.uma.jmetal.problem._
import org.uma.jmetal.solution._
import org.uma.jmetal.solution.impl._
import org.uma.jmetal.util.AlgorithmRunner
import org.uma.jmetal.util.evaluator._
import org.uma.jmetal.util.evaluator.impl._
import org.uma.jmetal.util._

import uk.ac.york.safire.metrics._
import scala.collection.JavaConversions._

///////////////////////////////////

class RandomSearchLocalOptimisationEngine(of: ObjectiveFunction.LocalObjectiveFunction, minEvaluations: Int, maxEvaluations: Int, rng: java.util.Random)
    extends OptimisationEngine.LocalOptimisationEngine(of) {

  require(minEvaluations > 0 && minEvaluations <= maxEvaluations)

  private val comparator = new org.uma.jmetal.util.comparator.DominanceComparator[ConfigurationSolution]()

  ///////////////////////////		

  private def impl(
    problem: JMetalLocalOptimisationEngine.ProblemType,
    population: List[Configuration]
  ): List[Configuration] = {
    val newPop = population.map { parentConfig =>

      val randomControlledMetrics =
        Map() ++ parentConfig.getConfigurationType().getControlledMetrics().map { t =>
          t.name -> uk.ac.york.safire.metrics.Utility.randomValue(t.valueType, rng)
        }

      val parentSol = new ConfigurationSolution(parentConfig, problem)

      //      val proposedConfig = Configuration.update(parentConfig, randomControlledMetrics,
      //        of.predictKeyObjectives(parentConfig, randomControlledMetrics).keyObjectiveMetrics)

      val configAndEval = ObjectiveFunction.evaluateNowHelper(of, new ObjectiveFunctionArguments(parentConfig, randomControlledMetrics))
      val proposedConfig = configAndEval.getConfiguration

      val proposedSol = new ConfigurationSolution(proposedConfig, problem)

      val cmp = comparator.compare(parentSol, proposedSol)
      // From:
      // https://github.com/jMetal/jMetal/blob/master/jmetal-core/src/main/java/org/uma/jmetal/util/comparator/DominanceComparator.java
      // DimonanceCmparator returns -1, or 0, or 1 if parentSol dominates proposedSol, both are
      // non-dominated, or parentSol  is dominated by proposedSol, respectively.

      if (cmp >= 0) {
        // jeep.lang.Diag.println( "accepted proposed" )        
        proposedConfig
      } else {
        // jeep.lang.Diag.println( "rejected proposed" )        
        // since there is now a requirement for the objective function to be able to modify observable metrics, 
        // we need to propagate those even if the proposedConfiguration is rejected:
        new Configuration(
          proposedConfig.getConfigurationType(),
          parentConfig.getControlledMetrics(), proposedConfig.getObservableMetrics(), parentConfig.getKeyObjectives()
        )
        // parentConfig
      }
    }
    newPop
  }

  /////////////////////////////////

  override def optimise(args: OptimisationArguments): OptimisationResult = {

    if (args.getConfigurations().isEmpty)
      new OptimisationResult(args.getConfigurations())
    else {

      val problem = JMetalLocalOptimisationEngine.createProblem(args.getConfigurations().head, of, rng)

      val maxEval = OptmisationUtility.maxEvaluations(
        args.getUrgency,
        args.getQuality, minEvaluations, maxEvaluations
      )

      import scala.collection.JavaConversions._
      var result = args.getConfigurations().toList
      for (i <- 0 until maxEval)
        result = impl(problem, result)

      new OptimisationResult(result)
    }
  }
}

// End ///////////////////////////////////////////////////////////////
