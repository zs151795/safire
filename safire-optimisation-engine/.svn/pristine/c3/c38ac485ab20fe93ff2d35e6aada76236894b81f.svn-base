package uk.ac.york.safire.optimisation.mitm.atb;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.math3.util.Pair;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;

import jeep.math.LinearInterpolation;
import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.ControlledMetricType;
import uk.ac.york.safire.metrics.KeyObjectiveType;
import uk.ac.york.safire.metrics.ObservableMetricType;
import uk.ac.york.safire.metrics.SampleRate;
import uk.ac.york.safire.metrics.SearchDirection;
import uk.ac.york.safire.metrics.Utility;
import uk.ac.york.safire.metrics.Value;
import uk.ac.york.safire.metrics.ValueType;
import uk.ac.york.safire.metrics.ValueVisitor.ValueStringVisitor;
import uk.ac.york.safire.optimisation.ia.Interval;

import com.codepoetics.protonpack.StreamUtils;

/**
 * From:
 * https://docs.hortonworks.com/HDPDocuments/HDP2/HDP-2.6.4/bk_kafka-component-guide/content/ch_kafka-development.html
 */

public final class ATBSimulatorKafkaProducer {
	
	public static volatile boolean isFinished = false;
	
	///////////////////////////////
	
	public static class RecipeInfo {
		public final String name;
		public final List< Integer > compatibleResources;
		public final int instances;		
		// final int commodityProduced; 
		// final int executionTime;	
		
		///////////////////////////
		
		public RecipeInfo(
			String name,
			int instances,			
			List< Integer > compatibleResources
			// , int commodityProduced
			// , int executionTime
			) {
			this.name = name;
			this.instances = instances;
			this.compatibleResources = Collections.unmodifiableList(compatibleResources);
			// this.commodityProduced = commodityProduced; 
			// this.executionTime = executionTime;			
		}
	}
	
	///////////////////////////
	
	
	static void run(int finalPercentAvailability, Producer<String, String> producer, 
		long updateTopicStringEveryMillis, long numTopicStringUpdates, BusinessCase bc, Random random) {
		
		for( long i=0; !isFinished && i<numTopicStringUpdates; ++i ) {
			final Instant now = Instant.now(); // get time in UTC

			final int percentAvailability = (int) LinearInterpolation.apply(i, 0, numTopicStringUpdates, 100, finalPercentAvailability );
			// final ConfigurationType ct = hardwiredConfigurationType(percentAvailability, random);
			final ConfigurationType ct = bc.getConfigurationType(percentAvailability, random);
			Configuration config = Utility.randomConfiguration(ct, random);
			final String configJSON = new uk.ac.york.safire.metrics.JsonConverter().toJson(config);
			final ProducerRecord<String, String> data = new ProducerRecord<String, String>("ResourceAvailability", "timestamp: " + now, configJSON );
			producer.send(data);
			com.google.common.util.concurrent.Uninterruptibles.sleepUninterruptibly(updateTopicStringEveryMillis, TimeUnit.MILLISECONDS);
		}
	}
	
	///////////////////////////////	
	
	static Producer<String, String> 
	createATBProducer(String bootstrapServersConfig) {
		final Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig);
	       
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "test.app.producer");
		props.put(ProducerConfig.ACKS_CONFIG, "all");
		props.put(ProducerConfig.RETRIES_CONFIG, 0);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName() );
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName() );

		///////////////////////////
		
		return new KafkaProducer<String, String>(props);
	}
	
	///////////////////////////////	
	
	public static void main(String[] args) {

		final Random random = new Random(0xDEABBEEF);
		final long updateTopicStringEveryMillis = 30 * 1000;
		final long numTopicStringUpdates = 1000;
		final int percentAvailability = 100;		
		
		final String bootstrapServersConfig = "localhost:9092";
		
		///////////////////////////

		final Producer<String, String> atbProducer = createATBProducer(bootstrapServersConfig);

		try {
			final BusinessCase bc = BusinessCase.OAS;
			run(percentAvailability, atbProducer, updateTopicStringEveryMillis, numTopicStringUpdates, bc, random );
		} finally {
			atbProducer.close();			
		}
	}
}

// End ///////////////////////////////////////////////////////////////
