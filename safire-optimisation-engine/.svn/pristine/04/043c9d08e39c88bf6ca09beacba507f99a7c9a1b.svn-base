package uk.ac.york.safire.optimisation.mitm.atb

import uk.ac.york.safire.metrics._

import uk.ac.york.safire.optimisation._

import uk.ac.york.safire.optimisation.ia._
import scala.collection.mutable.PriorityQueue

import uk.ac.york.safire.metrics.ValueVisitor

import uk.ac.york.safire.optimisation.ObjectiveFunction
import uk.ac.york.safire.optimisation.ObjectiveFunctionResult

import java.util.concurrent._

///////////////////////////////////

/**
 * Minimise over cost, makespan and number of violations of user priority ordering
 * with possibility of pre-emption over subtasks
 */

class PreemptionIAObjectiveFunction(sequenceDependentTasks: java.util.List[SequenceDependentTaskInfo]) extends ObjectiveFunction.LocalObjectiveFunction {

  import uk.ac.york.safire.optimisation.ObjectiveFunction.Result

  /////////////////////////////////  

  override def predictKeyObjectives(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result =
    predictKeyObjectivesImpl(originalConfig, proposedControlMetrics)

  /////////////////////////////////

  // Some alternative priorities previously considered:

  //        def compareAllenPrecedence(a: Task, b: Task): Int = {
  //          val aRb = a.relations.get(b.id)
  //          if (aRb == Some(IntervalRelation.PreceededGenerallyBy) || aRb == Some(IntervalRelation.PreceededImmediatelyBy))
  //            -1
  //          else {
  //            val bRa = b.relations.get(a.id)
  //            if (bRa == Some(IntervalRelation.PreceededGenerallyBy) || bRa == Some(IntervalRelation.PreceededImmediatelyBy))
  //              1
  //            else
  //              0
  //          }
  //        }
  //
  //        def compareByPrecedenceThenPriority(a: Task, b: Task): Int = {
  //          val cmp1 = compareAllenPrecedence(a, b)
  //          if (cmp1 == 0) java.lang.Double.compare(priority(a), priority(b)) else cmp1
  //        }
  //
  //        def compareByReleaseTimeThenPriority(a: Task, b: Task): Int = {
  //          val cmp1 = java.lang.Double.compare(a.releaseTime, b.releaseTime)
  //          if (cmp1 == 0) java.lang.Double.compare(priority(a), priority(b)) else cmp1
  //        }

  // End -- Some alternative priorities previously considered:

  /////////////////////////////////    

  private def predictKeyObjectivesImpl(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result = {

    import scala.collection.JavaConversions._

    val config = Configuration.update(originalConfig, proposedControlMetrics, originalConfig.getKeyObjectives)
    val (recipes, resources) = IAObjectiveFunction.reverseEngineerRecipesFromConfig(config)

    ///////////////////////////////

    PreemptionIAObjectiveFunction.makeTasks(recipes, resources, config) match {
      case m @ Left(errorString) => new Result(config.getKeyObjectives, errorString)
      case Right(tasks) => {
        val scheduler = new FIFOScheduler()

        def parentTaskPriority(t: Task): Int =
          IAObjectiveFunction.priority(PreemptionIAObjectiveFunction.parentTask(t.id).name, config)

        def preemptionPriorityIncrement(t: Task): Double = {
          val inc = PreemptionIAObjectiveFunction.preemptionPoint(t.id).get / 10000.0
          (1.0 - inc)
        }

        def endUserPriority(t: Task): Double =
          parentTaskPriority(t) + preemptionPriorityIncrement(t)

        // get permutation of priorities from controlled metrics:
        val perm = PreemptionIAObjectiveFunction.searchBasedPriorityPermutation(config)
        // jeep.lang.Diag.println( perm )

        val tasksByEndUserPriority = tasks.groupBy { parentTaskPriority }

        val mapTaskToConsequtiveEndUserPriorityIndex = Map() ++
          tasksByEndUserPriority.toList.sortBy {
            _._1
          }.zipWithIndex.map {
            case ((userPriority, tasks), consequtive) =>
              tasks.map { t => t -> consequtive }
          }.flatten

        def searchBasedPriority(t: Task): Double = {
          val endUserIndex = mapTaskToConsequtiveEndUserPriorityIndex(t)
          perm(endUserIndex) + preemptionPriorityIncrement(t)
        }

        // val taskQueue: PriorityQueue[Task] = PriorityQueue(tasks: _*)(Ordering.by { endUserPriority })
        val taskQueue: PriorityQueue[Task] = PriorityQueue(tasks: _*)(Ordering.by { searchBasedPriority })

        //        import scala.math.Ordering.comparatorToOrdering        
        //        val taskQueue: PriorityQueue[Task] = PriorityQueue(tasks: _*) { compareByReleaseTimeThenPriority }
        // jeep.lang.Diag.println("#tasks: " + taskQueue.length)
        //        jeep.lang.Diag.println("tasks: " + tasks.map { _.id }  )        
        // jeep.lang.Diag.println("queue: " + toList(taskQueue).map { _.id } )
        //        jeep.lang.Diag.println("priorities: " + toList(taskQueue).map { t => (t.id, priority(t)) } )        
        //        System.exit(0)
        // jeep.lang.Diag.println("priorities: " + taskQueue.map { priority })

        val sequenceDependentSetup = SequenceDependentTaskInfo.makeSequenceDependantSetup(sequenceDependentTasks)
        scheduler.schedule(taskQueue, resources.toSet, sequenceDependentSetup) match {
          case None => new Result(config.getKeyObjectives, "Scheduler cannot find a valid schedule")
          case Some(schedule) =>
            // jeep.lang.Diag.println( schedule )
            // val newKeyObjectives = Map("makespan" ->
            // Value.realValue(makespan, config.getKeyObjectives().get("makespan").getType.asInstanceOf[ValueType.Real]))

            val newKeyObjectives = doUpdatedKeyObjectives(schedule, config)
            val reportStr = "Status: Succeeded.\n" +
              PreemptionIAObjectiveFunction.toReportString(schedule, newKeyObjectives) +
              doReportStringSuffix(schedule, newKeyObjectives)
            new Result(newKeyObjectives, reportStr)
        }
      }
    }
  }

  // protected def doUpdatedKeyObjectives(schedule: Schedule, config: Configuration): Map[String, Value]

  /////////////////////////////////

  //  override def objectiveValues(result: Result): java.util.List[java.lang.Double] = {
  //    val value = result.keyObjectiveMetrics.get("makespan")
  //    val visitor = new ValueVisitor.NumberVisitor()
  //
  //    import scala.collection.JavaConversions._
  //    List(IAObjectiveFunction.doubleValue(value).asInstanceOf[java.lang.Double])
  //  }

  /////////////////////////////////  

  override def evaluate(args: ObjectiveFunctionArguments): Callable[ObjectiveFunctionResult] =
    new Callable[ObjectiveFunctionResult]() {
      override def call(): ObjectiveFunctionResult = {
        val result = predictKeyObjectivesImpl(args.getConfiguration, args.getProposedControlMetrics)
        // jeep.lang.Diag.println("statusString: " + result.objectiveFunctionStatusReport)
        val newConfig = IAObjectiveFunction.addOptimisationStatusToConfiguration(
          result.objectiveFunctionStatusReport,
          Configuration.update(args.getConfiguration, args.getProposedControlMetrics, result.keyObjectiveMetrics)
        )
        // org.apache.commons.math3.util.Pair.create(newConfig, objectiveValues(result))
        new ObjectiveFunctionResult(newConfig, objectiveValues(result))
      }
    }

  /////////////////////////////////

  // protected def doReportStringSuffix(s: Schedule, newKeyObjectives: Map[String, Value]): String = ???

  override def numObjectives: Int = 3

  override def objectiveValues(result: Result): java.util.List[java.lang.Double] = {
    import scala.collection.JavaConversions._
    val cost = IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("totalcost")).asInstanceOf[java.lang.Double]
    val makespan = IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("makespan")).asInstanceOf[java.lang.Double]
    val userPreferenceViolations = IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("user preference violations"))

    // List(IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("totalcost")).asInstanceOf[java.lang.Double])
    // val xx = (OnaObjectiveFunction.W1*makespan)/(50*111917.3) + (OnaObjectiveFunction.W2*cost)/68616.1
    // val xx = (OnaObjectiveFunction.W1 * makespan) / (50 * 111917.3) + (OnaObjectiveFunction.W2 * cost) / 68616.1
    List[java.lang.Double](cost, makespan, userPreferenceViolations)
  }

  /////////////////////////////////

  protected def doUpdatedKeyObjectives(schedule: Schedule, config: Configuration): Map[String, Value] = {

    import scala.collection.JavaConversions._

    // jeep.lang.Diag.println( config.getObservableMetrics().filter { o => o._1.contains("cost" )} )

    //      if (config.getObservableMetrics().get(recipeAndResourceName + " cost") == null)
    //        jeep.lang.Diag.println("cost not found for: " + recipeAndResourceName)
    //      IAObjectiveFunction.doubleValue(config.getObservableMetrics().get(recipeAndResourceName + " cost"))

    def stripUid(taskId: String): String = {
      val lo = taskId.indexOf("[UID")
      val hi = taskId.indexOf("]")
      if (lo == -1 || hi == -1) taskId else { taskId.substring(0, lo) + taskId.substring(hi + 1, taskId.length) }
    }

    val totalcost = (for ((resource, tasks) <- schedule.byResource; task <- tasks) yield {

      val recipeAndResourceName = PreemptionIAObjectiveFunction.parentTask(task._1.id).name + " " + resource.id

      val cost = if (recipeAndResourceName.startsWith("idle")) {
        //jeep.lang.Diag.println( recipeAndResourceName )
        //jeep.lang.Diag.println( prefix( recipeAndResourceName ) )
        sequenceDependentTasks.find { t => t.id + " " + t.resource == stripUid(recipeAndResourceName) }.map { _.cost }
      } else {
        val costValue = config.getObservableMetrics().get(recipeAndResourceName + " cost")
        if (costValue == null) None else Some(IAObjectiveFunction.doubleValue(costValue))
      }
      if (!cost.isDefined)
        jeep.lang.Diag.println(s"WARNING: cannot find cost for ${recipeAndResourceName}")
      cost
    }).flatten.sum

    ///////////////////////////////

    def numDescents(perm: List[Int]): Int =
      perm.zip(perm.tail).count { case (permi, permiplusone) => permi > permiplusone }

    // get permutation of priorities from controlled metrics:
    val perm = PreemptionIAObjectiveFunction.searchBasedPriorityPermutation(config)
    val userPreferenceViolations = numDescents(perm)

    val makespan = schedule.makespan

    ///////////////////////////////    

    Map(
      "totalcost" -> Value.realValue(totalcost, ValueType.realType(0.0, Double.MaxValue)),
      "makespan" -> Value.realValue(makespan, config.getKeyObjectives().get("makespan").getType.asInstanceOf[ValueType.Real]),
      "user preference violations" -> Value.intValue(userPreferenceViolations, ValueType.intType(0, Integer.MAX_VALUE))
    )
  }

  /////////////////////////////////

  protected def doReportStringSuffix(s: Schedule, newKeyObjectives: Map[String, Value]): String = {
    s"\nmakespan: ${new jeep.util.HMS(s.makespan)}\n" ++
      s"\ncost: ${IAObjectiveFunction.doubleValue(newKeyObjectives("totalcost"))}\n"
  }
}

///////////////////////////////////

object PreemptionIAObjectiveFunction {

  import IAObjectiveFunction.{ InstanceName => InstanceName }

  // Drop the "preempt N" suffix from synthetic 'preempt' tasks:  
  def parentTask(tid: TaskId): TaskId =
    if (tid.name.contains("preempt"))
      TaskId(tid.name.split(" ").dropRight(2).mkString(" "))
    else
      tid

  def preemptionPoint(tid: TaskId): Option[Int] =
    if (tid.name.contains("preempt"))
      Some(tid.name.split(" ").last.toInt)
    else
      None

  /////////////////////////////////      

  def searchBasedPriorityPermutation(config: Configuration): List[Int] = {
    val priorityPermutationIndex = config.getControlledMetrics().get("priority-permutation-index")
    val upperBound = new ValueTypeVisitor.NumberRangeVisitor().visit(priorityPermutationIndex.getType()).getRight.intValue()
    val n = ComeauBijectivePermutationHash.inverseFactorial(upperBound)
    val index = IAObjectiveFunction.intValue(config.getControlledMetrics().get("priority-permutation-index"))
    val perm = ComeauBijectivePermutationHash.permutationOfIndex(index, n)
    perm.toList
  }

  /////////////////////////////////

  def preemptionPoints(instanceName: InstanceName, allocatedResource: Resource, config: Configuration): Int = {

    val recipeAndResourceNamePrefix = instanceName + " " + allocatedResource.id
    val value = config.getObservableMetrics().get(recipeAndResourceNamePrefix + " preemption-points")
    if (value == null)
      jeep.lang.Diag.println("No preemption-points for " + recipeAndResourceNamePrefix)

    IAObjectiveFunction.intValue(config.getObservableMetrics().get(recipeAndResourceNamePrefix + " preemption-points"))
  }

  /////////////////////////////////  

  def makeTasks(recipes: Map[String, List[RecipeInfo]], resources: List[Resource], config: Configuration): Either[String, List[Task]] = {

    val mutex = IAObjectiveFunction.parseMutexes(resources, config)

    //    def mkTask(instanceName: InstanceName, allocatedResource: Resource, preemptionPointIndex: Int, numPreemptionPoints: Int): Task = {
    //      // require( isCompatibleResource(recipes, resources)(infoName + in, allocatedResource) )      
    //      val executionInterval = IAObjectiveFunction.interval(instanceName, allocatedResource, config)
    //      val startTime = jeep.math.LinearInterpolation.apply(preemptionPointIndex, 
    //        0, numPreemptionPoints-1, executionInterval.lower(), executionInterval.upper() )
    //        
    //      val executionLength = executionInterval.length / numPreemptionPoints  
    //      
    //      Task.single(
    //        TaskId(instanceName.toString()),
    //        Map(allocatedResource -> executionLength),
    //        releaseTime = executionInterval.lower,
    //        mutex
    //      )
    //    }

    // def sequenceDependentSetup: resource: Resource, last: Task, next: Task => Option[Task]

    def mkPreemptionTaskList(instanceName: InstanceName, allocatedResource: Resource, numPreemptionPoints: Int): List[Task] = {

      val overallExecutionInterval = IAObjectiveFunction.interval(instanceName, allocatedResource, config)
      val individualExecutionTime = overallExecutionInterval.length / numPreemptionPoints

      val tasks = (0 until numPreemptionPoints).map { preemptionPointIndex =>

        def mkTaskId(preemptionPoint: Int) = TaskId(instanceName.toString() + " preempt " + preemptionPoint)

        if (preemptionPointIndex == 0) {
          Task.single(
            mkTaskId(preemptionPointIndex),
            Map(allocatedResource -> individualExecutionTime),
            releaseTime = overallExecutionInterval.lower,
            mutex
          )
        } else {
          val t = Task.apply(
            mkTaskId(preemptionPointIndex),
            Map(allocatedResource -> individualExecutionTime),
            relations = Map(mkTaskId(preemptionPointIndex - 1) -> IntervalRelation.PreceededGenerallyBy),
            releaseTime = jeep.math.LinearInterpolation.apply(preemptionPointIndex, 0, numPreemptionPoints - 1, overallExecutionInterval.lower, overallExecutionInterval.upper - individualExecutionTime).toInt,
            mutex
          ) // , antiAffinityMap, finalProduct)
          // jeep.lang.Diag.println( s"generalPredecessors: " + t.id + ": " + t.generalPredecessors )
          t
        }
      }
      tasks.toList
    }

    val tasks = (for ((recipeName, infos) <- recipes; info <- infos; instanceId <- 0 until info.instances) yield {
      // val instanceName = info.name + " " + instanceId
      val instanceName = InstanceName(info, instanceId)
      val allocatedResource = IAObjectiveFunction.allocation(instanceName, config)
      val allocationAvailable = IAObjectiveFunction.isResourceAvailable(allocatedResource, config)
      if (!allocationAvailable) {

        val compatible = IAObjectiveFunction.compatibleResources(info.name, recipes, resources)
        val availability = Map() ++ compatible.map { r => r -> config.getObservableMetrics().get(r.id + " availability") }
        compatible.find { r => IAObjectiveFunction.isResourceAvailable(r, config) } match {
          // case None => Left(s"${allocatedResource.id} is not available for task $instanceName (and no compatible alternative resource is available)")
          case None => Left(s"${allocatedResource.id} is not available for task $instanceName") //  (compatible resources: ${compatible})")          
          case Some(allocatedResource) => {
            val numPreemptionPoints = preemptionPoints(instanceName, allocatedResource, config)

            //            val tasks = (0 until numPreemptionPoints).map { preemptionPointIndex => 
            //              mkTask(instanceName, allocatedResource,preemptionPointIndex,numPreemptionPoints) 
            //            }
            Right(mkPreemptionTaskList(instanceName, allocatedResource, numPreemptionPoints))
            // Right(mkTask(instanceName, allocatedResource))            
          }
        }
      } else {
        val numPreemptionPoints = preemptionPoints(instanceName, allocatedResource, config)
        //        val tasks = (0 until numPreemptionPoints).map { preemptionPointIndex => 
        //          mkTask(instanceName, allocatedResource,preemptionPointIndex,numPreemptionPoints) 
        //        }
        // Right(mkTask(instanceName, allocatedResource))        
        // Right(tasks)
        Right(mkPreemptionTaskList(instanceName, allocatedResource, numPreemptionPoints))
      }
    }).toList

    // if all tasks could be allocated to resources, return them in a list
    // otherwise, provide an descriptive string for the unallocated ones
    Either.cond(
      tasks.forall { e => e.isRight },
      tasks.map { case Right(x) => x }.flatten,
      tasks.map { case Left(descriptiveStr) => descriptiveStr + "\n"; case Right(task) => "" }.mkString
    )
  }

  /////////////////////////////////

  def toReportString(s: Schedule, newKeyObjectives: Map[String, Value]): String = {

    // object oasProductReporter extends ProductReporter {
    //   override def apply(r: Resource, schedule: List[(Task, Interval)]): Option[Product] =
    //   schedule.last._1.finalProduct
    // }
    // val report = Schedule.consolodatedProductReport(schedule.get, oasProductReporter)

    // val now = org.joda.time.DateTime.now()
    // def toJodaInterval(i: Interval) = {
    // val start: org.joda.time.ReadableInstant = now.plus(org.joda.time.Seconds.seconds(i.lower))
    // val duration: org.joda.time.ReadableDuration = org.joda.time.Duration.standardSeconds(i.length) 
    // new org.joda.time.Interval(start,duration)
    // }

    import java.time._
    val now = java.time.LocalDateTime.now()

    def toTimeString(i: Interval): String = {
      val start = now.plusSeconds(i.lower)
      val duration = Duration.ofSeconds(i.length)
      val end = start.plus(duration)

      val fmt = java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME
      s"from ${start.format(fmt)} to ${end.format(fmt)}"
    }

    // val cmp = new NaturalOrderComparator()

    val cmp = new java.util.Comparator[Interval]() {
      override def compare(a: Interval, b: Interval): Int =
        a.lower - b.lower
    }

    val m = s.byResource.map {
      case (resource, assignments) =>
        resource.id -> assignments.map {
          case (task, interval) =>
            (task.id.name, interval)
        }
    }.map {
      case (resource, tasks) =>
        (resource, tasks.sortWith {
          case (a, b) =>
            cmp.compare(a._2, b._2) <= 0
        })
    }.map {
      case (resource, tasks) => (resource, tasks.map {
        case (task, interval) =>
          (task, interval)
      })
    }
    // (task,toTimeString(interval)) } ) } 

    // import scala.collection.JavaConverters._
    // val typeOfMap=new com.google.gson.reflect.TypeToken[java.util.Map[String,java.util.List[ org.apache.commons.lang3.tuple.Pair[ String, org.apache.commons.lang3.tuple.Pair[ LocalDateTime, LocalDateTime ] ] ] ] ](){}.getType()              
    // new JsonConverter().toJson(m.asJava,typeOfMap)
    // new JsonConverter().toJson(m.asJava)

    val result = (m.map {
      case (resource, tasks) => s"$resource -> " +
        tasks.map { case (product, interval) => s"$product $interval" }.mkString("[", ",\n", "]")
    }).mkString("\n")

    result
  }
}

// End ///////////////////////////////////////////////////////////////
