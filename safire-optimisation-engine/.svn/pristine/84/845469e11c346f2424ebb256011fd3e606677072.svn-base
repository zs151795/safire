package uk.ac.york.safire.optimisation.maxplusplant;


import com.google.gson.Gson;

import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.Value;
import java.util.Map;
import java.util.Set;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public final class Graph {

	private List<Vertex> vertexList = new ArrayList<Vertex>();
	private List<Edge> edgeList = new ArrayList<Edge>();
	private transient Map<String, List<Edge> > outgoingEdgesMap = new HashMap<String, List<Edge>>();
	private transient Map<String, List<Edge> > ingoingEdgesMap = new HashMap<String, List<Edge>>();
	private String root;
	private String sink;
	private transient boolean sorted;
	

	///////////////////////////////
	
    public Graph() {
    	sorted=false;
    }

	///////////////////////////////
    
	public int getNoOfEdges() { return edgeList.size(); }
	public int getNoOfVertices() { return vertexList.size(); }	
	
	///////////////////////////////	    
    
	public void addVertex(Vertex vertex) {
    	sorted=false;
		this.vertexList.add(vertex);
	}

	
	private void addEdge(Edge edge) {
    	sorted=false;
    	this.edgeList.add(edge);
	}

	public List<Edge> getOutgoingEdgesOfVertex(Vertex vertex) {
		if(!outgoingEdgesMap.containsKey(vertex.getName())) {
			throw new InvalidParameterException();
		}
		return outgoingEdgesMap.get(vertex.getName());
	}
	
	public List<Edge> getIngoingEdgesOfVertex(Vertex vertex) {
		return ingoingEdgesMap.get(vertex.getName());
	}	

	public boolean hasOutgoingEdges(Vertex vertex) {
		return outgoingEdgesMap.containsKey(vertex.getName());
	}

	
	public boolean hasIngoingEdges(Vertex vertex) {
		return ingoingEdgesMap.containsKey(vertex.getName());
	}
	
	
	public void addOutgoingEdge(Vertex vertex, Edge edge) {
    	sorted=false;
    	

    	if(!edgeList.contains(edge)) {
    		this.addEdge(edge);
    	}    	
    	
    	
    	
    	if(hasOutgoingEdges(vertex)) {
        	getOutgoingEdgesOfVertex(vertex).add(edge);
    	}
    	else {
    		List<Edge> listEdge = new ArrayList<Edge>();
    		listEdge.add(edge);
    		outgoingEdgesMap.put(vertex.getName(), listEdge);
    		
    	}
	}

	
	public void addIngoingEdge(Vertex vertex, Edge edge) {
    	sorted=false;
    	

    	if(!edgeList.contains(edge)) {
    		this.addEdge(edge);
    	}    	
    	
    	
    	
    	if(hasIngoingEdges(vertex)) {
        	getIngoingEdgesOfVertex(vertex).add(edge);
    	}
    	else {
    		List<Edge> listEdge = new ArrayList<Edge>();
    		listEdge.add(edge);
    		ingoingEdgesMap.put(vertex.getName(), listEdge);
    	}
	}
	
	

	public String getRootName() { return root; }

	public String getSinkName() { return sink; }

	///////////////////////////////
	
	public void setRootName(String vertexName) {
		root = vertexName;
	}

	public void setSinkName(String vertexName) {
		sink = vertexName;
	}
	
	
	///////////////////////////////
	
	public boolean HasLoop() {
		// TO DO: check if the graph has any loops
		return false;
	}
	



	
	int getMaxLevel() {
		int maxLevel = 0;

		for(Vertex vertex: vertexList) {
			if(vertex.getLevel()>maxLevel) {
				maxLevel=vertex.getLevel();
			}
		}
		
		return maxLevel;
	}
	
	
	private int getLatestEndingTime() {
		int maxLevel = getMaxLevel();
		Map<Integer,List<Vertex> > levelVertexMap = new HashMap<Integer,List<Vertex> >();
		Map<String,Integer> vertexNameToStartingTimeMap = new HashMap<String,Integer>();
		
		for(Vertex vertex: vertexList) {
			Integer level=vertex.getLevel();
			if(!levelVertexMap.containsKey(level)) {
				List<Vertex> listVertexSameLevel = new ArrayList<Vertex>();
				levelVertexMap.put(level, listVertexSameLevel);
			}
			levelVertexMap.get(level).add(vertex);
		}
		
		vertexNameToStartingTimeMap.put(getRootName(), 0);
		
		
		for(Integer level=0;level < maxLevel ; level++) {
			for(Vertex vertex: levelVertexMap.get(level)) {
				for(Edge outgoingEdge: outgoingEdgesMap.get(vertex.getName())) {
					Vertex vertexDest=getVertexByName(outgoingEdge.getDestVertex());
					int arg1=MaxPlusAlgebra.getNeutralAdd();
					if(vertexNameToStartingTimeMap.containsKey(vertexDest.getName())) {
						arg1=vertexNameToStartingTimeMap.get(vertexDest.getName());
					}
					
									
					int sourceStartingTime = vertexNameToStartingTimeMap.get(vertex.getName());
					int arg2=MaxPlusAlgebra.mult(sourceStartingTime,outgoingEdge.getRealProcessingTime());
					if(MaxPlusAlgebra.add(arg1,arg2)==arg2) {
						vertexNameToStartingTimeMap.put(vertexDest.getName(),arg2);
					}
				}						
			}
		}
		
		return(vertexNameToStartingTimeMap.get(getSinkName()));
	}
	

	
	double getTotalEnergy() {
		if(HasLoop()) {
			throw new InvalidParameterException();
		}


		double totalEnergy=0.0;
		for(Edge edge: edgeList) {
			totalEnergy+=(edge.getRealEnergy()*edge.getRealProcessingTime());
		}

		return totalEnergy;
	}
	
	public int getEndingTime() {
		if(HasLoop()) {
			throw new InvalidParameterException();
		}
		return getLatestEndingTime();
	}
	
	public boolean isSorted() {	return sorted; }


	public boolean allValidControlMetricNames(Map< String,Value> controlledMetrics) {
		return controlledMetrics.keySet().containsAll(getListControlledMetrics()); 
	}
	
    public void setControlledMetrics(Map< String,Value> controlledMetrics) {
    	if( !allValidControlMetricNames(controlledMetrics) ) {
    		Set< String > namesRequiredButNotPresent = new HashSet<String>(getListControlledMetrics());
    		namesRequiredButNotPresent.removeAll( controlledMetrics.keySet() );    		
    		throw new IllegalArgumentException( "The following edge names required by graph are not present in the proposedcontrol metrics: " + namesRequiredButNotPresent );
    	}
    	// Map< String, Value > controlledMetrics = config.getControlledMetrics();
    	
    	for(Edge edge: edgeList) {
    		String metricName=edge.getControlMetricsName();
    		Value.Nominal MyMode = (Value.Nominal)controlledMetrics.get(metricName);
    		// int indexMode=MyMode.index;
      		// String strMyMode=MyMode.typ.getValue(indexMode);
    		String strMyMode=MyMode.getValue();
    		edge.setCurrentModeByName(strMyMode);
    	}
    	
    	
    }
    
    public void serializeGraph() {
    	String json = new Gson().toJson(this);
    	System.out.println("Serialised to:");
    	System.out.println(json);
    }
    
    public Vertex getVertexByName(String name) {
    	Vertex result = null;

        for (Iterator<Vertex> it = vertexList.iterator(); it.hasNext(); ) {
            Vertex vertex = it.next();
            if (vertex.getName().equals(name)) {
                result = vertex;
            }
        }
        if(result==null) {
			throw new InvalidParameterException();
        }
        return result;
    }
    
    
    public void addOutgoingEdges() {
    	for (Edge edge: edgeList) {
    		String sourceVertexName=edge.getSourceVertex(); 
    		Vertex sourceVertex=getVertexByName(sourceVertexName);
        	addOutgoingEdge(sourceVertex, edge);
    	}
    	
    }

    
    
	public List<String> getListControlledMetrics() {
		List<String> listControlledMetrics = new ArrayList<String>();
		for(Edge edge: edgeList) {
			listControlledMetrics.add(edge.getControlMetricsName());
		}
		
		return listControlledMetrics;
	}
    
	

	
	
	public List<Edge> getListEdge() {
		List<Edge> listEdge = new ArrayList<Edge>();
		for(Edge edge: edgeList) {
			listEdge.add(edge);
		}		
		return listEdge;
	}

	

    public int getTotalNoOfModeValues() {
    	int result=0;
    	
    	for(Edge edge: edgeList) {
    		result+=edge.getNoOfModes();
    	}
    	return result;
    }
    
    

    public int getTotalNoOfIngoingEdgesAbove(int threshold) {
    	int result=0;

    	for(Vertex vertex: vertexList) {
    		if(hasIngoingEdges(vertex)) {
    			int inDegree=getIngoingEdgesOfVertex(vertex).size();
    			inDegree-=threshold;
    			if (inDegree>0) {
    				result+=inDegree;
    			}
    		}
    	}
    	return result;
    	
    }

	int getWCET() {
		int result=0;
	  
		for(Edge edge: edgeList) {
			result+=(int)Math.ceil(edge.getNominalDuration()*edge.getHighestDurationMultiplier());
		}
		return result;
	}
	
	double getWCEn() {
		double result=0.0;
		for(Edge edge: edgeList) {
			result+=Math.ceil(edge.getNominalDuration()*edge.getHighestDurationMultiplier()*edge.getHighestEnergyMultiplier()*edge.getNominalEnergyPerTimeUnit());
		}
		return result;
	}
    
    
    
}

// End ///////////////////////////////////////////////////////////////

