sbt package
sbt docker:publishLocal
# sbt docker:publish

# From: https://medium.com/jeroen-rosenberg/lightweight-docker-containers-for-scala-apps-11b99cf1a666
# docker run --rm -p8080:8080 safire-optimisation-engine:0.1-SNAPSHOT
docker run --rm -p8080:8080  safire-optimisation-engine:0.4-snapshot

#########################################################################

## Swan: Everything below represents an (unsuccessful) attempt to publish with some supplied certificates (e.g. for access to the ATB Kafka server)

# From https://gitter.im/sbt/sbt-native-packager/archives/2015/08/17 :
# It [docker:publish?] just assumes thedocker command works. 
# It�s a thin wrapper around docker build and docker push, so you can always run those commands manually.

# From: https://stackoverflow.com/questions/24835831/what-does-sbt-native-packagers-dockerpublishlocal-do
# In practice, it [docker:publish] maps to
# docker build -t "${dockerRepository}/${projectName}:${version}" . // working directory at `target/docker/stage`
# docker push "${dockerRepository}/${projectName}:${version}"
# You can then use this image from your local machine, or other machines, with docker run "${dockerRepository}/${projectName}:${version}".

# ???
#docker run -d \
#  --restart=always \
#  --name registry \
#  -v `pwd`/certs:/certs \
#  -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
#  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
#  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
#  -p 443:443 \
#  registry:2
