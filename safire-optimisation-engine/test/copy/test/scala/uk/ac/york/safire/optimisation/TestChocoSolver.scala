package uk.ac.york.safire.optimisation

import uk.ac.york.safire.optimisation.maxplusplant._

import org.junit.Assert._
import org.junit.Test

///////////////////////////////////

class TestChocoSolver {

  val rng = new java.util.Random(0xDEADBEEF)
  val smallPlant = PlantGenerator.generatePlant(
    2, // noOfInternalLevels
    1, //int minNoOfVerticesPerInternalLevel 
    3, //int maxNoOfVerticesPerInternalLevel 
    3, //int noOfMachines
    4, //int maxNoOfModesPerMachine
    1, //int minNominalDuration
    20, //int maxNominalDuration 
    1.0, //double minEnergyPerTimeUnit 
    10.0, //double maxEnergyPerTimeUnit
    0.1, //double minEnergyMultiplier
    2.0, //double maxEnergyMultiplier
    0.2, //double minDurationMultiplier
    3.0, //double maxDurationMultiplier
    rng
  )

  /////////////////////////////////

  @Test
  def test1: Unit = {

    //    val parentConfig = uk.ac.york.safire.metrics.Utility.randomConfiguration(smallPlant.getConfigurationType(), rng )
    //    val of = new MockObjectiveFunction(smallPlant)
    //    val solutions = JMetalLocalOptimisationEngine.optimalSolutions(parentConfig, of)
    //    jeep.lang.Diag.println( solutions )

    val xx = smallPlant.cpGetParetoFront()
    println(smallPlant.cpGetParetoFront())

  }

  /////////////////////////////////
}

// End ///////////////////////////////////////////////////////////////
