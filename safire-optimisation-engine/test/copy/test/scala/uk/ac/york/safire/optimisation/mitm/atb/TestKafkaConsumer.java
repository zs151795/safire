package uk.ac.york.safire.optimisation.mitm.atb;

import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

///////////////////////////////////

public final class TestKafkaConsumer {

	private void run(KafkaConsumer<String, String> consumer) {
		while ( true ) {
			final ConsumerRecords<String, String> records = consumer.poll(1000);
	    	for( ConsumerRecord<String, String> record : records ) {
	    		   
	    	System.out.printf("Received Message topic =%s, partition =%s, offset = %d, key = %s, value = %s\n", record.topic(), record.partition(), record.offset(), record.key(), record.value());
	    	consumer.commitSync();
	       }
	   }
	}
	
	///////////////////////////////
	
	public static void main(String [] args) {
		Properties consumerConfig = new Properties();
	    // consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:2181");
		
		// consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "dockerbuntu.atb-bremen.de:2376");
		
	    consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "my-group");
	    consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
	    consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName() );
	    consumerConfig.put( ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName() );

	    // System.out.println( "constructing consumer..." );	    
	    final KafkaConsumer<String, String> consumer = new KafkaConsumer<>(consumerConfig);
	    // System.out.println( "constructed consumer..." );
	    System.out.println( "querying topics..." );	    
	    System.out.println( "topics: " + consumer.listTopics() );
	    consumer.subscribe(Collections.singletonList("ResourceAvailability") );
	    
	    consumer.close();
	}
}

// End ///////////////////////////////////////////////////////////////
