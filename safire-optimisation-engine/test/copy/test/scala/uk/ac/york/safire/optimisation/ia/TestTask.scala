package uk.ac.york.safire.optimisation.ia

import org.junit._
import org.junit.Assert._

import uk.ac.york.safire.optimisation.ia._
import scala.collection.mutable.PriorityQueue

//////////////////////////////////////////////////////////////////////

final class TestTask {

  /**
   * Test numbers refer to the equation numbering from
   * "An Interval Algebra for Multiprocessor Resource Allocation" (IAMRA)
   * by Indrusiak and Dziurzanski
   */

  @Test
  def testIAMRA6(): Unit = {

    val resource = Resource("R1")

    val taskResourceExecutionTimes = Map(resource -> 40)
    val task = Task.single(TaskId("A"), taskResourceExecutionTimes, releaseTime = 0)

    val scheduler = new FIFOScheduler()
    val taskQueue = PriorityQueue(task)(Ordering.by { (t: Task) => t.releaseTime })
    val schedule = scheduler.schedule(taskQueue, Set(resource))

    assertEquals(Some(Schedule(resource, task)), schedule)
  }

  /////////////////////////////////

  @Test
  def testIAMRA7(): Unit = {

    val resource = Resource("R1")

    val task1ResourceExecutionTimes = Map(resource -> 40)
    val task1 = Task.single(TaskId("A"), task1ResourceExecutionTimes, releaseTime = 0)

    val task2ResourceExecutionTimes = Map(resource -> 50)
    val task2 = Task.single(TaskId("B"), task2ResourceExecutionTimes, releaseTime = 0)

    val scheduler = new FIFOScheduler()
    val taskQueue = PriorityQueue(task1, task2)(Ordering.by { (t: Task) => t.releaseTime })
    val schedule = scheduler.schedule(taskQueue, Set(resource))

    val start1 = 0
    val completion1 = task1ResourceExecutionTimes(resource)
    val start2 = completion1
    val completion2 = completion1 + task2ResourceExecutionTimes(resource)

    assertEquals(Some(Schedule(Map(resource -> List((task1, Interval(start1, completion1)), (task2, Interval(start2, completion2)))))), schedule)
  }

  /////////////////////////////////

  @Test
  def testFIFOMultipleResources(): Unit = {

    val resource = Resource("R1")
    val resource2 = Resource("R2")

    val taskResourceExecutionTimes = Map(resource -> 40, resource2 -> 30)
    val task1 = Task.single(TaskId("A"), taskResourceExecutionTimes, releaseTime = 0)
    val task2 = Task.single(TaskId("B"), taskResourceExecutionTimes, releaseTime = 0)

    val scheduler = new FIFOScheduler()

    {
      val taskQueue = PriorityQueue(task1, task2)(Ordering.by { (t: Task) => t.releaseTime })
      val schedule = scheduler.schedule(taskQueue, Set(resource, resource2))
      assertEquals(Some(40), schedule.map { _.makespan })
    }
    {
      val taskQueue = PriorityQueue(task1, task2)(Ordering.by { (t: Task) => t.releaseTime })
      val schedule = scheduler.schedule(taskQueue, Set(resource))
      assertEquals(Some(80), schedule.map { _.makespan })
    }
  }

  /////////////////////////////////

  @Test
  def testPriorityFIFO(): Unit = {

    val resource = Resource("R1")

    val taskResourceExecutionTimes = Map(resource -> 40)
    val LowPriorityParent = Task.single(TaskId("Parent"), taskResourceExecutionTimes, releaseTime = 0)
    val HighPriorityChild = Task(TaskId("Child"), taskResourceExecutionTimes,
      Map(LowPriorityParent.id -> IntervalRelation.PreceededGenerallyBy), releaseTime = 0,0,0,1)

    val scheduler = new FIFOScheduler()

    val priorityOrdering: Ordering[Task] = Ordering.by {
      // Hard-coded ordering:      
      (t: Task) =>
        t match {
          case HighPriorityChild => 0
          case LowPriorityParent => 1
          case _ => 2
        }
    }

    {
      val taskQueue = PriorityQueue(LowPriorityParent, HighPriorityChild)(priorityOrdering)
      val schedule = scheduler.schedule(taskQueue, Set(resource))
      assertEquals(Some(80), schedule.map { _.makespan })
    }
    {
      val taskQueue = PriorityQueue(HighPriorityChild, LowPriorityParent)(priorityOrdering)
      val schedule = scheduler.schedule(taskQueue, Set(resource))
      assertEquals(Some(80), schedule.map { _.makespan })
    }
  }

  /////////////////////////////////

  def safireFormatMutex(allResources: Set[Resource])(r: Resource): Set[Resource] = {
    def machine(r: Resource): String = {
      import scala.util.matching.Regex._
      val pattern = "([a-zA-Z_0-9]+)_mode_([0-9]+)".r
      val pattern(machineStr, modeStr) = r.id
      machineStr
    }

    val byMachine = allResources.groupBy(machine)
    byMachine(machine(r)).diff(Set(r))
  }

  def safireFormatMutex2(mutexMap: Map[Resource, Set[Resource]])(r: Resource): Set[Resource] = {
    mutexMap(r)
  }

  /////////////////////////////////	

  def makeMutexMap(mutexResources: List[Resource]): Map[Resource, Set[Resource]] =
    Map() ++ (0 until mutexResources.size).toList.map { i =>
      mutexResources(i) -> mutexResources.to[Set].diff(Set(mutexResources(i)))
    }

  @Test
  def testFIFOMultipleResourcesMutex(): Unit = {

    val machine0mode0 = Resource("machine_0_mode_0")
    val machine0mode1 = Resource("machine_0_mode_1")
    val machine0mode2 = Resource("machine_0_mode_2")
    val machine0AllModes = List(machine0mode0, machine0mode1)

    val taskResourceExecutionTimes = Map(machine0mode0 -> 40, machine0mode1 -> 30)

    val task1 = Task.single(TaskId("A"), taskResourceExecutionTimes, releaseTime = 0, makeMutexMap(machine0AllModes))
    val task2 = Task.single(TaskId("B"), taskResourceExecutionTimes, releaseTime = 0, makeMutexMap(machine0AllModes))

    val scheduler = new FIFOScheduler()

    {
      val taskQueue = PriorityQueue(task1, task2)(Ordering.by { (t: Task) => t.releaseTime })
      val schedule = scheduler.schedule(taskQueue, machine0AllModes.toSet)
      assertEquals(Some(60), schedule.map { _.makespan })
    }
  }
}

// End ///////////////////////////////////////////////////////////////
