package uk.ac.york.safire.optimisation.ia

import org.junit._
import org.junit.Assert._

import uk.ac.york.safire.optimisation.ia._
import scala.collection.mutable.PriorityQueue

///////////////////////////////////

class TestOAS {

  def makeMutexMap(mutexResources: List[List[Resource]]): Map[Resource, Set[Resource]] = {
    def mkMutexList(l: List[Resource]): List[(Resource, Set[Resource])] =
      (0 until l.size).toList.map { i => l(i) -> l.to[Set].diff(Set(l(i)))
      }

    Map() ++ mutexResources.map { mkMutexList }.flatten
  }

  /////////////////////////////////

  @Test
  def testSimplifiedOASUseCase(): Unit = {

    val silo1_Scale1_Conveyor1_Mixer1 = Resource("silo1_Scale1_Conveyor1_Mixer1")
    val silo1_Scale1_Conveyor1_Mixer2 = Resource("silo1_Scale1_Conveyor1_Mixer2")
    val silo1_Scale1_Conveyor1_Mixer3 = Resource("silo1_Scale1_Conveyor1_Mixer3")

    val siloWater_Mixer1 = Resource("siloWater_Mixer1")
    val siloWater_Mixer2 = Resource("siloWater_Mixer2")
    val siloWater_Mixer3 = Resource("siloWater_Mixer3")

    val mixer1 = Resource("mixer1")
    val mixer2 = Resource("mixer2")
    val mixer3 = Resource("mixer3")

    val mixer1_Pipeline1_Tank1 = Resource("mixer1_Pipeline1_Tank1")
    val mixer1_Pipeline2_Tank2 = Resource("mixer1_Pipeline2_Tank2")
    val mixer2_Pipeline1_Tank1 = Resource("mixer2_Pipeline1_Tank1")
    val mixer2_Pipeline2_Tank2 = Resource("mixer2_Pipeline2_Tank2")
    val mixer3_Pipeline1_Tank1 = Resource("mixer3_Pipeline1_Tank1")
    val mixer3_Pipeline2_Tank2 = Resource("mixer3_Pipeline2_Tank2")

    val mixer1Mutex = List(silo1_Scale1_Conveyor1_Mixer1, siloWater_Mixer1, mixer1, mixer1_Pipeline1_Tank1, mixer1_Pipeline2_Tank2)
    val mixer2Mutex = List(silo1_Scale1_Conveyor1_Mixer2, siloWater_Mixer2, mixer2, mixer2_Pipeline1_Tank1, mixer2_Pipeline2_Tank2)
    val mixer3Mutex = List(silo1_Scale1_Conveyor1_Mixer3, siloWater_Mixer3, mixer3, mixer3_Pipeline1_Tank1, mixer3_Pipeline2_Tank2)
    val tank1Mutex = List(mixer1_Pipeline1_Tank1, mixer2_Pipeline1_Tank1, mixer3_Pipeline1_Tank1)
    val tank2Mutex = List(mixer1_Pipeline2_Tank2, mixer2_Pipeline2_Tank2, mixer3_Pipeline2_Tank2)

    //    val mixer1Mutex = List(silo1_Scale1_Conveyor1_Mixer1,siloWater_Mixer1,mixer1,mixer1_Pipeline1_Tank1)
    //    val mixer2Mutex = List(silo1_Scale1_Conveyor1_Mixer2,siloWater_Mixer2,mixer2,mixer2_Pipeline1_Tank1,mixer2_Pipeline1_Tank1,mixer2_Pipeline2_Tank2)
    //    val mixer3Mutex = List(silo1_Scale1_Conveyor1_Mixer3,siloWater_Mixer3,mixer3,mixer2_Pipeline1_Tank1,mixer3_Pipeline1_Tank1,mixer3_Pipeline2_Tank2)

    /*    
    val stage1ExecutionTimes = Map( silo1_Scale1_Conveyor1_Mixer1 -> 40, silo1_Scale1_Conveyor1_Mixer2 -> 30, silo1_Scale1_Conveyor1_Mixer3 -> 40)
    val stage2ExecutionTimes = Map( siloWater_Mixer1 -> 10, siloWater_Mixer2 -> 10, siloWater_Mixer3 -> 10)
    val stage3ExecutionTimes = Map( mixer1 -> 100, mixer2 -> 100, mixer3 -> 120)
    val stage4ExecutionTimes = Map( mixer1_Pipeline1_Tank1 -> 40, mixer1_Pipeline2_Tank2 -> 30, mixer2_Pipeline1_Tank1 -> 40, 
        mixer2_Pipeline2_Tank2 -> 40, mixer3_Pipeline1_Tank1 -> 30, mixer3_Pipeline2_Tank2 -> 40)

*/
    //decision from OE: paint1: mixer2, tank1

    //note: only certain paths are allowed, so the execution times must be finetuned somehow to reflect this

    val paint1_Stage1ExecutionTimes = Map(silo1_Scale1_Conveyor1_Mixer2 -> 30)
    val paint1_Stage2ExecutionTimes = Map(siloWater_Mixer2 -> 10)
    val paint1_Stage3ExecutionTimes = Map(mixer2 -> 100)
    val paint1_Stage4ExecutionTimes = Map(mixer2_Pipeline1_Tank1 -> 40)

    val mutex = makeMutexMap(List()) // mixer1Mutex,mixer2Mutex,mixer3Mutex,tank1Mutex,tank2Mutex))
    val antiAffinity: Map[Resource, Set[Resource]] = Map()

    val paint1_Stage1 = Task.single(TaskId("paint1_Stage1"), paint1_Stage1ExecutionTimes, releaseTime = 0, mutex)
    val paint1_Stage2 = Task(TaskId("paint1_Stage2"), paint1_Stage2ExecutionTimes, Map(paint1_Stage1.id -> IntervalRelation.PreceededGenerallyBy), releaseTime = 0,0,0,1, mutex, antiAffinity, Some(Product("WeiB Glanz", 20)))
    val paint1_Stage3 = Task(TaskId("paint1_Stage3"), paint1_Stage3ExecutionTimes, Map(paint1_Stage2.id -> IntervalRelation.PreceededGenerallyBy), releaseTime = 0,0,0,1, mutex)
    val paint1_Stage4 = Task(TaskId("paint1_Stage4"), paint1_Stage4ExecutionTimes, Map(paint1_Stage3.id -> IntervalRelation.PreceededGenerallyBy), releaseTime = 0,0,0,1, mutex, antiAffinity, Some(Product("WeiB Glanz", 30)))
    //note: probably we would need to add more mutexes, not just one per stage

    //    val paint1_Start1 = 0    
    //    val paint1_Completion1 = paint1_Stage1ExecutionTimes(silo1_Scale1_Conveyor1_Mixer2)
    //    val paint1_Start2 = paint1_Completion1
    //    val paint1_Completion2 = paint1_Completion1 + paint1_Stage2ExecutionTimes(siloWater_Mixer2)
    //    val paint1_Start3 = paint1_Completion2
    //    val paint1_Completion3 = paint1_Completion2 + paint1_Stage3ExecutionTimes(mixer2)
    //    val paint1_Start4 = paint1_Completion3
    //    val paint1_Completion4 = paint1_Completion3 + paint1_Stage4ExecutionTimes(mixer2_Pipeline1_Tank1)

    //    println("paint1_Completion4: " + paint1_Completion4)

    object oasProductReporter extends ProductReporter {
      override def apply(r: Resource, schedule: List[(Task, Interval)]): Option[Product] = {
        schedule.last._1.finalProduct
      }
    }

    val scheduler = new FIFOScheduler()

    {
      val taskQueue = PriorityQueue(paint1_Stage1, paint1_Stage2, paint1_Stage3, paint1_Stage4)(Ordering.by { (t: Task) => t.releaseTime })
      val schedule = scheduler.schedule(taskQueue, mixer2Mutex.toSet)

      assertEquals(Some(180), schedule.map { _.makespan })
      assertEquals(List(Product("WeiB Glanz", 50)), Schedule.consolodatedProductReport(schedule.get, oasProductReporter))
    }
  }
}

// End ///////////////////////////////////////////////////////////////
