package uk.ac.york.safire.optimisation.ia

import org.junit._
import org.junit.Assert._

import spray.json._

import uk.ac.york.safire.optimisation.ia._

//////////////////////////////////////////////////////////////////////

final class TestJsonSerialisation {

  @Test
  def testInterval(): Unit = {

    import uk.ac.york.safire.optimisation.ia.IAJSonProtocol._
    // import DefaultJsonProtocol._

    // implicit val intervalFormat: JsonFormat[Interval] = DefaultJsonProtocol.jsonFormat2(uk.ac.york.safire.optimisation.ia.Interval)
    implicit val intervalFormat = DefaultJsonProtocol.jsonFormat[Int, Int, Interval](Interval, "lower", "upper")

    val interval = Interval(0, 10)
    val toJson = interval.toJson
    val fromJson = toJson.convertTo[uk.ac.york.safire.optimisation.ia.Interval]

    assertEquals(interval, fromJson)
  }

  /////////////////////////////////

  @Test
  def testTask(): Unit = {

    import uk.ac.york.safire.optimisation.ia.IAJSonProtocol._

    val resource = Resource("R1")

    val taskResourceExecutionTimes = Map(resource -> 40)
    val task = Task.single(TaskId("A"), taskResourceExecutionTimes, releaseTime = 0)

    val toJson = task.toJson
    val fromJson = toJson.convertTo[Task]

    assertEquals(task, fromJson)
  }
}

// End ///////////////////////////////////////////////////////////////
