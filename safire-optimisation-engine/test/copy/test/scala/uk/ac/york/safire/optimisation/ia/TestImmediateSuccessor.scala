package uk.ac.york.safire.optimisation.ia

import org.junit._
import org.junit.Assert._

import uk.ac.york.safire.optimisation.ia._
import scala.collection.mutable.PriorityQueue

//////////////////////////////////////////////////////////////////////

final class TestImmediateSuccessor {

  @Test
  def testProceedGenerally(): Unit = {

    val resource1 = Resource("R1")
    val resource2 = Resource("R2")

    val taskA = Task(TaskId("A"), resourceExecutionTime = Map(resource1 -> 50), relations = Map(), releaseTime = 0,0,0,1)
    val taskB = Task(TaskId("B"), resourceExecutionTime = Map(resource2 -> 50), relations = Map(TaskId("A") -> IntervalRelation.PreceededGenerallyBy), releaseTime = 0,0,0,1)
    val taskC = Task(TaskId("C"), resourceExecutionTime = Map(resource2 -> 20), relations = Map(), releaseTime = 0,0,0,1)
    val taskD = Task(TaskId("D"), resourceExecutionTime = Map(resource1 -> 30), relations = Map(TaskId("C") -> IntervalRelation.PreceededGenerallyBy), releaseTime = 0,0,0,1)

    val scheduler = new FIFOScheduler()
    val taskQueue = PriorityQueue(taskA, taskB, taskC, taskD)(Ordering.by { (t: Task) => t.releaseTime })
    val schedule = scheduler.schedule(taskQueue, Set(resource1, resource2))

    // assertEquals( Schedule(resource, task), schedule )
  }

  /////////////////////////////////

  @Test
  def testIsImmediateSuccessorOf(): Unit = {

    val resource1 = Resource("R1")
    val resource2 = Resource("R2")

    val taskA = Task(TaskId("A"), resourceExecutionTime = Map(), relations = Map(), releaseTime = 0,0,0,1)
    val taskB = Task(TaskId("B"), resourceExecutionTime = Map(), relations = Map(TaskId("A") -> IntervalRelation.PreceededImmediatelyBy), releaseTime = 0,0,0,1)

    assertTrue(taskB.isImmediateSuccessorOf(taskA))
  }

  /////////////////////////////////

  @Test
  def testProceedImmediately1(): Unit = {

    val resource1 = Resource("R1")
    val resource2 = Resource("R2")

    val taskA = Task(TaskId("A"), resourceExecutionTime = Map(resource1 -> 50), relations = Map(), releaseTime = 0,0,0,1)
    val taskB = Task(TaskId("B"), resourceExecutionTime = Map(resource2 -> 50), relations = Map(TaskId("A") -> IntervalRelation.PreceededImmediatelyBy), releaseTime = 0,0,0,1)
    val taskC = Task(TaskId("C"), resourceExecutionTime = Map(resource2 -> 20), relations = Map(), releaseTime = 0,0,0,1)
    val taskD = Task(TaskId("D"), resourceExecutionTime = Map(resource1 -> 30), relations = Map(TaskId("C") -> IntervalRelation.PreceededImmediatelyBy), releaseTime = 0,0,0,1)

    val scheduler = new FIFOScheduler()
    val taskQueue = PriorityQueue(taskA, taskB, taskC, taskD)(Ordering.by { (t: Task) => t.releaseTime })
    val schedule = scheduler.schedule(taskQueue, Set(resource1, resource2))

    assertEquals(Some(Schedule(Map(
      resource1 -> List((taskA, Interval(0, 50)), (taskD, Interval(120, 150))),
      resource2 -> List((taskB, Interval(50, 100)), (taskC, Interval(100, 120)))
    ))), schedule)
  }

  /////////////////////////////////

  @Test
  def testProceedImmediately2(): Unit = {

    val resource1 = Resource("R1")
    val resource2 = Resource("R2")

    val taskA = Task(TaskId("A"), resourceExecutionTime = Map(resource1 -> 50), relations = Map(), releaseTime = 0,0,0,1)
    val taskB = Task(TaskId("B"), resourceExecutionTime = Map(resource2 -> 50), relations = Map(TaskId("A") -> IntervalRelation.PreceededImmediatelyBy), releaseTime = 0,0,0,1)
    val taskC = Task(TaskId("C"), resourceExecutionTime = Map(resource2 -> 20), relations = Map(), releaseTime = 0,0,0,1)
    val taskD = Task(TaskId("D"), resourceExecutionTime = Map(resource1 -> 30), relations = Map(TaskId("C") -> IntervalRelation.PreceededImmediatelyBy), releaseTime = 0,0,0,1)
    val taskE = Task(TaskId("E"), resourceExecutionTime = Map(resource2 -> 9), relations = Map(), releaseTime = 0,0,0,1)

    val scheduler = new FIFOScheduler()
    val taskQueue = PriorityQueue(taskE, taskA, taskB, taskC, taskD)(Ordering.by { (t: Task) => t.releaseTime })
    val schedule = scheduler.schedule(taskQueue, Set(resource1, resource2))

    assertEquals(Some(Schedule(Map(
      resource1 -> List((taskA, Interval(0, 50)), (taskD, Interval(120, 150))),
      resource2 -> List((taskE, Interval(0, 9)), (taskB, Interval(50, 100)), (taskC, Interval(100, 120)))
    ))), schedule)
  }
}

// End ///////////////////////////////////////////////////////////////
