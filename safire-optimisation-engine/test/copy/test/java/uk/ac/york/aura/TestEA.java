package uk.ac.york.aura;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.math3.util.Pair;
import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.primitives.Doubles;

import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.ControlledMetricType;
import uk.ac.york.safire.metrics.KeyObjectiveType;
import uk.ac.york.safire.metrics.ObservableMetricType;
import uk.ac.york.safire.metrics.SearchDirection;
import uk.ac.york.safire.metrics.Value;
import uk.ac.york.safire.metrics.ValueType;
import uk.ac.york.safire.metrics.ValueVisitor;
import uk.ac.york.safire.optimisation.ObjectiveFunction;

///////////////////////////////////

public final class TestEA {
	
	public static ConfigurationType 
	onemaxConfigurationType(int numBits) {
		final List< KeyObjectiveType > keyObjectiveMetricTypes = ImmutableList.of( 
			new KeyObjectiveType( "ones", ValueType.realType( 0, Integer.MAX_VALUE ), "scalar", SearchDirection.MAXIMIZING ) 
		); 
			
		final List< ControlledMetricType > controlledMetricTypes = new ArrayList<>();		
		for( int i=0; i<numBits; ++i )
			controlledMetricTypes.add( new ControlledMetricType( "bit " + i, ValueType.intType( 0, 1 ), "scalar" ) );			
			
		final List< ObservableMetricType > observableMetricTypes = ImmutableList.of();

		return new ConfigurationType.Explicit(keyObjectiveMetricTypes,controlledMetricTypes,observableMetricTypes);
	}

	///////////////////////////////
	
	@Test
	public void testOnemax() {

		final int populationSize = 100;
		final Random rng = new java.util.Random(0xDEADBEEF);
		final int numBits = 32;
		final int maxIterations = 1000;		
		
		///////////////////////////
		
		final ConfigurationType ct = onemaxConfigurationType(numBits);
		
		final List< Configuration > initialPopulation = Operators.initialPopulation(populationSize, ct, rng);
			
		final ObjectiveFunction.LocalObjectiveFunction of = new ObjectiveFunction.LocalObjectiveFunction() {

			@Override
			public Result predictKeyObjectives(Configuration current, Map<String, Value> proposedControlMetrics) {
				final Value trueValue = Value.intValue(1, ValueType.intType(0, 1));
				final long setBits = proposedControlMetrics.entrySet().stream().filter( e -> e.getKey().contains("bit") && e.getValue().equals( trueValue ) ).count();
				final Map< String, Value > keyObjectives = new HashMap<>();
				keyObjectives.put( "ones", 	Value.realValue(setBits, ValueType.realType(0, Integer.MAX_VALUE)) );
				return new Result(keyObjectives, "Status: OK");
			}

			@Override
			public List<Double> objectiveValues(Result result) {
				final double value = new ValueVisitor.NumberVisitor().visit( result.keyObjectiveMetrics.get("ones") ).doubleValue(); 
				return Doubles.asList( value );
			}

			@Override
			public int numObjectives() { return 1; }
			
		};
		
		///////////////////////////
		
		final Comparator< List< Double > > compareMultiObjective  = Operators.compareMultiObjective(SearchDirection.MAXIMIZING); 
		final BiFunction< List< PopulationEntry >, Random, PopulationEntry > select  = Operators.proportionalSelection;
		final BiFunction< Configuration, Configuration, Configuration > crossover  = Operators.onePointCrossover(rng);
		final Function< Configuration, Configuration > mutation = Operators.uniformMutation(rng);
		
		final Predicate< List< List< PopulationEntry > > 
			> isFinished  = Operators.maxIterTermination(maxIterations);   
		
		final Pair< List< PopulationEntry >, PopulationEntry > p = EA.apply(initialPopulation, 
				of,
				compareMultiObjective,
				select,
				crossover,
				mutation,
				isFinished, rng );
		
		jeep.lang.Diag.println(p.getSecond());
		jeep.lang.Diag.println(p.getSecond().getObjectives());		
	}
}

// End ///////////////////////////////////////////////////////////////
