package uk.ac.york.aura;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;
import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.primitives.Doubles;

import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.ControlledMetricType;
import uk.ac.york.safire.metrics.KeyObjectiveType;
import uk.ac.york.safire.metrics.ObservableMetricType;
import uk.ac.york.safire.metrics.SearchDirection;
import uk.ac.york.safire.metrics.Value;
import uk.ac.york.safire.metrics.ValueType;
import uk.ac.york.safire.metrics.ValueVisitor;
import uk.ac.york.safire.optimisation.ObjectiveFunction;

///////////////////////////////////

public final class TestNSGA2 {

	@Test
	public void testParetoDominates() {
		assertTrue( NSGA2.paretoDominates( Doubles.asList(0.0), Doubles.asList(1.0), SearchDirection.MINIMIZING ));
		assertFalse( NSGA2.paretoDominates( Doubles.asList(1.0), Doubles.asList(0.0), SearchDirection.MINIMIZING ));
		assertTrue( NSGA2.paretoDominates( Doubles.asList(1.0), Doubles.asList(0.0), SearchDirection.MAXIMIZING ));
		assertFalse( NSGA2.paretoDominates( Doubles.asList(0.0), Doubles.asList(1.0), SearchDirection.MAXIMIZING ));
		
		assertTrue( NSGA2.paretoDominates( Doubles.asList(0.0,0.0), Doubles.asList(0.0,1.0), SearchDirection.MINIMIZING ));		
		assertTrue( NSGA2.paretoDominates( Doubles.asList(0.0,0.0), Doubles.asList(1.0,1.0), SearchDirection.MINIMIZING ));
		assertTrue( NSGA2.paretoDominates( Doubles.asList(0.0,0.0), Doubles.asList(1.0,0.0), SearchDirection.MINIMIZING ));		

		
		assertFalse( NSGA2.paretoDominates( Doubles.asList(0.0,1.0), Doubles.asList(0.0,0.0), SearchDirection.MINIMIZING ));		
		assertFalse( NSGA2.paretoDominates( Doubles.asList(1.0,1.0), Doubles.asList(0.0,0.0), SearchDirection.MINIMIZING ));
		assertFalse( NSGA2.paretoDominates( Doubles.asList(1.0,0.0), Doubles.asList(0.0,0.0), SearchDirection.MINIMIZING ));		
		
		assertFalse( NSGA2.paretoDominates( Doubles.asList(0.0,0.0), Doubles.asList(0.0,1.0), SearchDirection.MAXIMIZING ));		
		assertFalse( NSGA2.paretoDominates( Doubles.asList(0.0,0.0), Doubles.asList(1.0,1.0), SearchDirection.MAXIMIZING ));
		assertFalse( NSGA2.paretoDominates( Doubles.asList(0.0,0.0), Doubles.asList(1.0,0.0), SearchDirection.MAXIMIZING ));
		
		assertFalse( NSGA2.paretoDominates( Doubles.asList(0.66879439607494, 0.22736165743227382), Doubles.asList(0.42645325162547487, 5.286351146774817), SearchDirection.MAXIMIZING ));
		assertFalse( NSGA2.paretoDominates( Doubles.asList(0.66879439607494, 0.22736165743227382), Doubles.asList(0.42645325162547487, 5.286351146774817), SearchDirection.MINIMIZING ));		

		assertFalse( NSGA2.paretoDominates( Doubles.asList(0.42645325162547487, 5.286351146774817), Doubles.asList(0.66879439607494, 0.22736165743227382), SearchDirection.MAXIMIZING ));
		assertFalse( NSGA2.paretoDominates( Doubles.asList(0.42645325162547487, 5.286351146774817), Doubles.asList(0.66879439607494, 0.22736165743227382), SearchDirection.MINIMIZING ));		
	}

	///////////////////////////////

	private static final double minZDT1Control = 0.0;
	private static final double maxZDT1Control = 1.0;		

	private static final double minZDT1Objective = -1000.0;
	private static final double maxZDT1Objective = 1000.0;		
	
	public static ConfigurationType 
	zdt1ConfigurationType(int numDimensions) {
		
		final SearchDirection dir = SearchDirection.MINIMIZING;
		
		final List< KeyObjectiveType > keyObjectiveMetricTypes = ImmutableList.of( 
			new KeyObjectiveType( "objective1", ValueType.realType( minZDT1Objective, maxZDT1Objective ), "scalar", dir ),
			new KeyObjectiveType( "objective2", ValueType.realType( minZDT1Objective, maxZDT1Objective ), "scalar", dir )				
		); 
			
		final List< ControlledMetricType > controlledMetricTypes = new ArrayList<>();		
		for( int i=0; i<numDimensions; ++i )
			controlledMetricTypes.add( new ControlledMetricType( "var " + i, ValueType.realType( minZDT1Control, maxZDT1Control ), "scalar" ) );			
			
		final List< ObservableMetricType > observableMetricTypes = ImmutableList.of();

		return new ConfigurationType.Explicit(keyObjectiveMetricTypes,controlledMetricTypes,observableMetricTypes);
	}
	
	///////////////////////////////
	
	private static double 
	zdt1_gx(List<Double> x) {
		final int n = x.size();
		
		double sigma = 0.0;
		for( int i=1; i<n; ++i )
			sigma += x.get(i);
			
		final double gx = 1 + ( ( 9 * sigma ) / ( n - 1 ) );
		return gx;
	}
	
	private static Pair< Double, Double > 
	zdt1(List<Double> x) {
		
		final double x1 = x.get(0);
		
		final double gx = zdt1_gx(x);

		final double f1 = x1;
		final double f2 = gx * (1 - Math.sqrt(x1 / gx ) );
		
		return Pair.create(f1,f2);
	}
	
	///////////////////////////////
	
	/**
	 * @article{zdt2000a,
	 *  author={E. Zitzler and K. Deb and L. Thiele},
	 * 	title={{Comparison of Multiobjective Evolutionary Algorithms: Empirical Results}},
	 * 	journal={Evolutionary Computation},
	 * 	year={2000},
	 * 	volume={8},
	 * 	pages={173--195},
	 * 	number={2}
	 * }
	 */ 
	
	@Test
	public void testZDT1Multiobjective() {
		
		// final Random rng = new java.util.Random(0xDEADBEEF);
		final Random rng = new java.util.Random(0xDEAFBABE);
		
		final int populationSize = 100;
		final int numDimensions = 2;
		final int maxIterations = 5000;
		// Timings:
		// final int populationSize = 100;
		// final int numDimensions = 2;
		// final int maxIterations = 5000;
		// Concurrent: 237s
		// Sequential: 335s
		 
		///////////////////////////
		
		final ConfigurationType ct = zdt1ConfigurationType(numDimensions);
		
		final List< Configuration > initialPopulation = Operators.initialPopulation(populationSize, ct, rng);

		final Function< Value, Double > valueToDouble = (v) ->  new ValueVisitor.NumberVisitor().visit(v).doubleValue();
		
		final ObjectiveFunction.LocalObjectiveFunction of = new ObjectiveFunction.LocalObjectiveFunction() {

			@Override
			public Result predictKeyObjectives(Configuration current, Map<String, Value> proposedControlMetrics) {

				final List< Double > vars = proposedControlMetrics.entrySet().stream().filter( e -> e.getKey().contains("var") ).map( 
						(e) -> valueToDouble.apply(e.getValue()) ).collect(Collectors.toList());
				
				final Pair< Double, Double > p = zdt1(vars);
				
				final Map< String, Value > keyObjectives = new HashMap<>();
				
				final ValueType.Real objectiveType = ValueType.realType( minZDT1Objective, maxZDT1Objective );
				keyObjectives.put( "f1", Value.realValue(p.getFirst(), objectiveType ) );
				keyObjectives.put( "f2", Value.realValue(p.getSecond(), objectiveType ) );				
				return new Result(keyObjectives, "Status: OK");
			}

			@Override
			public List<Double> objectiveValues(Result result) {
				final double f1 = new ValueVisitor.NumberVisitor().visit( result.keyObjectiveMetrics.get("f1") ).doubleValue();
				final double f2 = new ValueVisitor.NumberVisitor().visit( result.keyObjectiveMetrics.get("f2") ).doubleValue();				
				return Doubles.asList( f1, f2 );
			}

			@Override
			public int numObjectives() { return 2; }
			
		};
		
		///////////////////////////

		final List< Double > objectiveValueRanges = Doubles.asList( maxZDT1Objective - minZDT1Objective, maxZDT1Objective - minZDT1Objective );
		final int tournamentSize = 7;
		final SearchDirection dir = SearchDirection.MINIMIZING;				
		
		final Comparator< List< Double > > compareMultiObjective  = Operators.compareMultiObjective(dir); 
		final BiFunction< Configuration, Configuration, Configuration > crossover  = Operators.onePointCrossover(rng);
		final Function< Configuration, Configuration > mutation = Operators.uniformMutation(rng);
		
		final Predicate< List< List< PopulationEntry > > 
			> isFinished  = Operators.maxIterTermination(maxIterations);   

		final Function< Configuration, List< Double > > getControls = ( c ) ->  c.getControlledMetrics().entrySet().stream().filter( e -> e.getKey().contains("var") ).map( 
				(e) -> valueToDouble.apply(e.getValue()) ).collect(Collectors.toList());
		
		///////////////////////////

		final long startTime = System.currentTimeMillis();
		
		final List< PopulationEntry > bestFront = NSGA2.apply(initialPopulation, 
			of,
			compareMultiObjective,
			crossover,
			mutation,
			isFinished, 
			objectiveValueRanges,
			tournamentSize,
			dir,				
			rng );
		
		final long endTime = System.currentTimeMillis();
		
		final double elapsed = ( endTime - startTime ) / 1000.0;
		
		///////////////////////////
		
		jeep.lang.Diag.println( bestFront.size() );
		jeep.lang.Diag.println( "elapsed (s) " + elapsed );		
		
		final List< List< Double > > controls = bestFront.stream().map( p -> getControls.apply( p.getConfiguration() ) ).collect(Collectors.toList()); 
		final List< Double > gControls = controls.stream().map( cntrl -> zdt1_gx( cntrl ) ).collect(Collectors.toList());
	
		// zdt1_gxAccording to Zitzler et al, "Comparison of Multiobjective Evolutionary Algorithms: Empirical Results"
		// zdt1_gxthe Pareto-optimal front has zdt1_gx == 1.0 :
		
		double [] gExpected = new double [ gControls.size() ];
		Arrays.fill( gExpected, 1.0 );
		assertArrayEquals(gExpected, Doubles.toArray(gControls), 0.001 );
	}
}

// End ///////////////////////////////////////////////////////////////
