package uk.ac.york.aura;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import uk.ac.york.aura.Operators.PerturbationVisitor;
import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.SearchDirection;
import uk.ac.york.safire.metrics.Value;
import uk.ac.york.safire.metrics.Value.Nominal;
import uk.ac.york.safire.metrics.ValueType;
import uk.ac.york.safire.optimisation.ObjectiveFunction.LocalObjectiveFunction;
import uk.ac.york.safire.optimisation.ObjectiveFunctionArguments;
import uk.ac.york.safire.optimisation.ObjectiveFunctionResult;
import uk.ac.york.safire.optimisation.mitm.atb.OasProductionLineConfigurationType;

/**
 * This class implements the MOEA/D algorithm proposed by the following article,
 * with a novel mutation operation designed specially for the recipe Allocation
 * issue for factories.
 * 
 * Li, H. and Zhang, Q. "Multiobjective Optimization problems with Complicated
 * Pareto Sets, MOEA/D and NSGA-II." IEEE Transactions on Evolutionary
 * Computation, 13(2):284-302, 2009.
 */

public class MOEAD_RS extends MOEADAbstract {

	/**
	 * The factor that controls mutation approach.
	 */
	public static double F = 0.3;
	public static double M = 0.8;
	public static boolean enableNewCrossover = true;
	public static boolean enableNewMutation = true;
	public static boolean enableElitism = true;
	public static boolean enableImrpove = false;

	public List<Integer> productIndex = new ArrayList<Integer>();

	BiFunction<Configuration, Configuration, List<Configuration>> newCrossover;
	Function<Configuration, Configuration> newMutation;

	public MOEAD_RS(List<Configuration> initialPopulation, LocalObjectiveFunction of,
			BiFunction<Configuration, Configuration, Configuration> crossover,
			Function<Configuration, Configuration> mutation, Predicate<List<List<PopulationEntry>>> isFinished,
			List<Double> objectiveValueRanges, int tournamentSize, SearchDirection dir, Random rng) {
		super(initialPopulation, of, crossover, mutation, isFinished, objectiveValueRanges, tournamentSize, dir, rng);

		newMutation = allocationSwtichMutator(rng);
		newCrossover = productSwitchCrossover(rng);

		Configuration config = initialPopulation.get(0);
		final Map<String, Value> controls = config.getControlledMetrics();
		final List<MutablePair<String, Value>> controlsList = controls.entrySet().stream()
				.map(e -> MutablePair.of(e.getKey(), e.getValue())).collect(Collectors.toList());
		controlsList.sort((p1, p2) -> p1.getKey().compareTo(p2.getKey()));

		String[] key = controlsList.get(0).getKey().split(" ");
		String productName = key[0] + " " + key[1];

		for (int i = 1; i < controlsList.size(); i++) {
			if (controlsList.get(i).getKey().contains(productName)) {
				continue;
			} else {
				key = controlsList.get(i).getKey().split(" ");
				productName = key[0] + " " + key[1];
				productIndex.add(i);
			}
		}

		System.out.println(productIndex.toString());
	}

	/**
	 * A novel mutation approach designed for the recipe allocation problem for
	 * smart factories.
	 */
	public Function<Configuration, Configuration> allocationSwtichMutator(Random rng) {
		if (MOEAD_RS.M < 0.0 || MOEAD_RS.M > 1.0)
			throw new IllegalArgumentException();

		return (c) -> {
			final Map<String, Value> controls = c.getControlledMetrics();
			final List<MutablePair<String, Value>> controlsList = controls.entrySet().stream()
					.map(e -> MutablePair.of(e.getKey(), e.getValue())).collect(Collectors.toList());

			controlsList.sort((p1, p2) -> p1.getKey().compareTo(p2.getKey()));

			for (int i = 0; i < controlsList.size(); ++i) {

				if (rng.nextDouble() < MOEAD_RS.M) {
					MutablePair<String, Value> p = controlsList.get(i);

					if (p.getKey().contains("allocation")) {
						String newAllocation = "";
						Nominal allocValue = (Nominal) p.getValue();
						String currentAllocation = allocValue.getValue();
						ValueType.Nominal allocType = (ValueType.Nominal) allocValue.getType();
						List<String> vaildAllocations = new ArrayList<String>();

						for (int j = 0; j < allocType.numValues(); j++) {
							if (!allocType.getValue(j).contains("No"))
								vaildAllocations.add(allocType.getValue(j));
						}

						if (rng.nextDouble() < F) {
							if (currentAllocation.contains("No"))
								newAllocation = vaildAllocations.get(rng.nextInt(vaildAllocations.size()));
							else
								newAllocation = allocType.getValue(allocType.numValues() - 1);
						} else {
							if (!currentAllocation.contains("No")) {
								int currentIndex = vaildAllocations.indexOf(currentAllocation);
								int newIndex = rng.nextInt(vaildAllocations.size());
								while (newIndex == currentIndex) {
									newIndex = rng.nextInt(vaildAllocations.size());
								}
								newAllocation = vaildAllocations.get(newIndex);
							} else
								newAllocation = allocType.getValue(allocType.numValues() - 1);
						}

						assert (newAllocation.length() > 0);
						Value newAllocValue = new Nominal(newAllocation, allocType);
						p.setValue(newAllocValue);
					} else {
						p.setValue(new PerturbationVisitor(rng).visit(p.getValue()));
					}

				}
			}

			Map<String, Value> proposedControls = controlsList.stream()
					.collect(Collectors.toMap(Pair::getKey, Pair::getValue));

			return new Configuration(c.getConfigurationType(), proposedControls, c.getObservableMetrics(),
					c.getKeyObjectives());
		};
	}

	public BiFunction<Configuration, Configuration, List<Configuration>> productSwitchCrossover(Random rng) {
		return (c1, c2) -> {

			List<Configuration> newIndividuals = new ArrayList<Configuration>();

			List<Map.Entry<String, Value>> linear1 = c1.getControlledMetrics().entrySet().stream()
					.collect(Collectors.toList());
			List<Map.Entry<String, Value>> linear2 = c2.getControlledMetrics().entrySet().stream()
					.collect(Collectors.toList());
			assert (linear1.size() == linear2.size());

			linear1.sort((p1, p2) -> p1.getKey().compareTo(p2.getKey()));
			linear2.sort((p1, p2) -> p1.getKey().compareTo(p2.getKey()));

			int index = -1;
			List<Map.Entry<String, Value>> crossed = new ArrayList<Map.Entry<String, Value>>();
			Map<String, Value> crossedControlledMetrics = null;

			/** The normal crossover operator. */
			index = rng.nextInt(linear1.size());
			crossed = linear1.subList(0, index);
			crossed.addAll(linear2.subList(index, linear2.size()));
			crossedControlledMetrics = crossed.stream().collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
			newIndividuals.add(new Configuration(c1.getConfigurationType(), crossedControlledMetrics,
					c1.getObservableMetrics(), c1.getKeyObjectives()));

			/** The new crossover method */
			index = productIndex.get(rng.nextInt(productIndex.size()));
			crossed = linear1.subList(0, index);
			crossed.addAll(linear2.subList(index, linear2.size()));
			crossedControlledMetrics = crossed.stream().collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
			newIndividuals.add(new Configuration(c1.getConfigurationType(), crossedControlledMetrics,
					c1.getObservableMetrics(), c1.getKeyObjectives()));

			return newIndividuals;
		};
	}

	public PopulationEntry improve(Configuration individual) {
		List<Configuration> newIndividuals = new ArrayList<Configuration>();
		newIndividuals.add(individual);

		final Map<String, Value> controls = individual.getControlledMetrics();
		final List<MutablePair<String, Value>> controlsList = controls.entrySet().stream()
				.map(e -> MutablePair.of(e.getKey(), e.getValue())).collect(Collectors.toList());

		controlsList.sort((p1, p2) -> p1.getKey().compareTo(p2.getKey()));
		List<MutablePair<String, Value>> priorities = new ArrayList<MutablePair<String, Value>>();

		List<List<MutablePair<String, Value>>> SWallocated = new ArrayList<List<MutablePair<String, Value>>>();
		List<List<MutablePair<String, Value>>> SWunAllocated = new ArrayList<List<MutablePair<String, Value>>>();

		List<List<MutablePair<String, Value>>> WMallocated = new ArrayList<List<MutablePair<String, Value>>>();
		List<List<MutablePair<String, Value>>> WMunAllocated = new ArrayList<List<MutablePair<String, Value>>>();

		List<List<MutablePair<String, Value>>> SGallocated = new ArrayList<List<MutablePair<String, Value>>>();
		List<List<MutablePair<String, Value>>> SGunAllocated = new ArrayList<List<MutablePair<String, Value>>>();

		List<List<MutablePair<String, Value>>> WBallocated = new ArrayList<List<MutablePair<String, Value>>>();
		List<List<MutablePair<String, Value>>> WBunAllocated = new ArrayList<List<MutablePair<String, Value>>>();

		for (int i = 0; i < 4; i++) {
			SWallocated.add(new ArrayList<MutablePair<String, Value>>());
			SWunAllocated.add(new ArrayList<MutablePair<String, Value>>());

			WMallocated.add(new ArrayList<MutablePair<String, Value>>());
			WMunAllocated.add(new ArrayList<MutablePair<String, Value>>());

			SGallocated.add(new ArrayList<MutablePair<String, Value>>());
			SGunAllocated.add(new ArrayList<MutablePair<String, Value>>());

			WBallocated.add(new ArrayList<MutablePair<String, Value>>());
			WBunAllocated.add(new ArrayList<MutablePair<String, Value>>());
		}

		for (int i = 0; i < controlsList.size(); ++i) {
			MutablePair<String, Value> p = controlsList.get(i);
			if (p.getKey().contains("allocation")) {

				Nominal allocValue = (Nominal) p.getValue();
				if (p.getKey().contains("Std Weiss A")) {
					if (!allocValue.value.contains("No"))
						SWallocated.get(0).add(p);
					else
						SWunAllocated.get(0).add(p);
				}
				if (p.getKey().contains("Std Weiss B")) {
					if (!allocValue.value.contains("No"))
						SWallocated.get(1).add(p);
					else
						SWunAllocated.get(1).add(p);
				}
				if (p.getKey().contains("Std Weiss C")) {
					if (!allocValue.value.contains("No"))
						SWallocated.get(2).add(p);
					else
						SWunAllocated.get(2).add(p);
				}
				if (p.getKey().contains("Std Weiss D")) {
					if (!allocValue.value.contains("No"))
						SWallocated.get(3).add(p);
					else
						SWunAllocated.get(3).add(p);
				}

				if (p.getKey().contains("Weiss Matt A")) {
					if (!allocValue.value.contains("No"))
						WMallocated.get(0).add(p);
					else
						WMunAllocated.get(0).add(p);
				}
				if (p.getKey().contains("Weiss Matt B")) {
					if (!allocValue.value.contains("No"))
						WMallocated.get(1).add(p);
					else
						WMunAllocated.get(1).add(p);
				}
				if (p.getKey().contains("Weiss Matt C")) {
					if (!allocValue.value.contains("No"))
						WMallocated.get(2).add(p);
					else
						WMunAllocated.get(2).add(p);
				}
				if (p.getKey().contains("Weiss Matt D")) {
					if (!allocValue.value.contains("No"))
						WMallocated.get(3).add(p);
					else
						WMunAllocated.get(3).add(p);
				}

				if (p.getKey().contains("Super Glanz A")) {
					if (!allocValue.value.contains("No"))
						SGallocated.get(0).add(p);
					else
						SGunAllocated.get(0).add(p);
				}
				if (p.getKey().contains("Super Glanz B")) {
					if (!allocValue.value.contains("No"))
						SGallocated.get(1).add(p);
					else
						SGunAllocated.get(1).add(p);
				}
				if (p.getKey().contains("Super Glanz C")) {
					if (!allocValue.value.contains("No"))
						SGallocated.get(2).add(p);
					else
						SGunAllocated.get(2).add(p);
				}
				if (p.getKey().contains("Super Glanz D")) {
					if (!allocValue.value.contains("No"))
						SGallocated.get(3).add(p);
					else
						SGunAllocated.get(3).add(p);
				}

				if (p.getKey().contains("Weiss Basis A")) {
					if (!allocValue.value.contains("No"))
						WBallocated.get(0).add(p);
					else
						WBunAllocated.get(0).add(p);
				}
				if (p.getKey().contains("Weiss Basis B")) {
					if (!allocValue.value.contains("No"))
						WBallocated.get(1).add(p);
					else
						WBunAllocated.get(1).add(p);
				}
				if (p.getKey().contains("Weiss Basis C")) {
					if (!allocValue.value.contains("No"))
						WBallocated.get(2).add(p);
					else
						WBunAllocated.get(2).add(p);
				}
				if (p.getKey().contains("Weiss Basis D")) {
					if (!allocValue.value.contains("No"))
						WBallocated.get(3).add(p);
					else
						WBunAllocated.get(3).add(p);
				}

			} else {
				priorities.add(p);
			}
		}

		int totalsize = 0;
		for (int i = 0; i < 4; i++) {
			totalsize += SWallocated.get(i).size();
			totalsize += SWunAllocated.get(i).size();

			totalsize += WMallocated.get(i).size();
			totalsize += WMunAllocated.get(i).size();

			totalsize += SGallocated.get(i).size();
			totalsize += SGunAllocated.get(i).size();

			totalsize += WBallocated.get(i).size();
			totalsize += WBunAllocated.get(i).size();
		}
		assert (totalsize == priorities.size());

		improveForOneProduct(individual, controlsList, SWallocated, SWunAllocated, newIndividuals);
		improveForOneProduct(individual, controlsList, WMallocated, WMunAllocated, newIndividuals);
		improveForOneProduct(individual, controlsList, SGallocated, SGunAllocated, newIndividuals);
		improveForOneProduct(individual, controlsList, WBallocated, WBunAllocated, newIndividuals);

		List<PopulationEntry> newIndividualEntrys = new ArrayList<PopulationEntry>();
		for (Configuration c : newIndividuals) {
			ObjectiveFunctionResult evaled = null;
			try {
				evaled = of.evaluate(new ObjectiveFunctionArguments(c, c.getControlledMetrics())).call();
			} catch (Exception e) {
				e.printStackTrace();
			}
			PopulationEntry newIndividualEntry = new PopulationEntry(evaled.getConfiguration(),
					evaled.getObjectiveValues());
			newIndividualEntrys.add(newIndividualEntry);
		}

		List<Double> paints = new ArrayList<Double>();
		for (PopulationEntry entry : newIndividualEntrys) {
			double sum = entry.getObjectives().get(1) + entry.getObjectives().get(2) + entry.getObjectives().get(3)
					+ entry.getObjectives().get(4);
			paints.add(sum);
		}

		for (int i = 0; i < paints.size() - 1; i++) {
			if (Double.compare(paints.get(i), paints.get(i + 1)) != 0) {
				System.err.println("Improvement Mehtod ERROR with different amount of paints produced! : index: " + i
						+ " and " + (i + 1) + " paints: " + paints.get(i) + " and: " + paints.get(i + 1));
				System.out.println(paints.toString());
				for (PopulationEntry entry : newIndividualEntrys)
					System.out.println(entry.getObjectives().toString());
				System.exit(-1);
			}
		}

		// double makespanorigin = newIndividualEntrys.get(0).getObjectives().get(0);
		Collections.sort(newIndividualEntrys,
				(i1, i2) -> i1.getObjectives().get(0).compareTo(i2.getObjectives().get(0)));
		// double makespannew = newIndividualEntrys.get(0).getObjectives().get(0);
		// System.out.println(
		// "Improve method returns: original makespan: " + makespanorigin + " new make
		// span: " + makespannew);
		return newIndividualEntrys.get(0);
	}

	private void improveForOneProduct(Configuration individual, List<MutablePair<String, Value>> controlsList,
			List<List<MutablePair<String, Value>>> allocated, List<List<MutablePair<String, Value>>> unAllocated,
			List<Configuration> newIndividuals) {

		for (int recipeType = 0; recipeType < 4; recipeType++) {
			/** Check whether the current recipe type is suitable for improvement. */
			if (recipeType == 0 ? allocated.get(recipeType).size() > 1 : allocated.get(recipeType).size() > 0) {

				ValueType.Nominal allocType = (ValueType.Nominal) allocated.get(recipeType).get(0).getValue().getType();

				for (int recipeNo = 0; recipeNo < 4; recipeNo++) {
					if (recipeType == recipeNo)
						continue;

					List<MutablePair<String, Value>> newGenes = new ArrayList<MutablePair<String, Value>>();

					if (recipeNo == 0 ? unAllocated.get(recipeNo).size() > 1 : unAllocated.get(recipeNo).size() > 0) {
						if (recipeType == 0) {
							MutablePair<String, Value> a0 = new MutablePair<String, Value>(
									allocated.get(recipeType).get(0).left, new Nominal("No allocation", allocType));
							MutablePair<String, Value> a1 = new MutablePair<String, Value>(
									allocated.get(recipeType).get(1).left, new Nominal("No allocation", allocType));
							newGenes.add(a0);
							newGenes.add(a1);
						} else {
							MutablePair<String, Value> a0 = new MutablePair<String, Value>(
									allocated.get(recipeType).get(0).left, new Nominal("No allocation", allocType));
							newGenes.add(a0);
						}

						ValueType.Nominal unallocType = (ValueType.Nominal) unAllocated.get(recipeNo).get(0).getValue()
								.getType();
						if (recipeNo == 0) {
							MutablePair<String, Value> ua0 = new MutablePair<String, Value>(
									unAllocated.get(recipeNo).get(0).left,
									new Nominal(unallocType.getValue(rng.nextInt(unallocType.numValues() - 1)),
											unallocType));
							MutablePair<String, Value> ua1 = new MutablePair<String, Value>(
									unAllocated.get(recipeNo).get(1).left,
									new Nominal(unallocType.getValue(rng.nextInt(unallocType.numValues() - 1)),
											unallocType));
							newGenes.add(ua0);
							newGenes.add(ua1);
						} else {
							MutablePair<String, Value> ua0 = new MutablePair<String, Value>(
									unAllocated.get(recipeNo).get(0).left,
									new Nominal(unallocType.getValue(rng.nextInt(unallocType.numValues() - 1)),
											unallocType));
							newGenes.add(ua0);
						}
					}

					List<MutablePair<String, Value>> newControls = new ArrayList<MutablePair<String, Value>>(
							controlsList);
					int counter = 0;
					for (MutablePair<String, Value> gene : newGenes) {
						for (int i = 0; i < newControls.size(); i++) {
							if (newControls.get(i).getKey().equals(gene.getKey())) {
								newControls.set(i, gene);
								counter++;
								break;
							}
						}
					}

					assert (counter == newGenes.size());
					Map<String, Value> proposedControls = newControls.stream()
							.collect(Collectors.toMap(Pair::getKey, Pair::getValue));

					Configuration newConfig = new Configuration(individual.getConfigurationType(), proposedControls,
							individual.getObservableMetrics(), individual.getKeyObjectives());
					newIndividuals.add(newConfig);
				}

			}

		}
	}

	public Function<List<PopulationEntry>, Configuration> productSwitchCrossover() {
		return (c) -> {

			if (c.size() == 1)
				return c.get(0).getConfiguration();

			List<String> commidiesOrder = OasProductionLineConfigurationType.getProducts();
			assert (commidiesOrder.size() == c.size());

			Map<String, Map<String, Value>> productMapping = new HashMap<>();
			for (int i = 0; i < c.size(); i++) {
				productMapping.put(commidiesOrder.get(i), c.get(i).getConfiguration().getControlledMetrics());
			}

			List<Map.Entry<String, Value>> crossed = new ArrayList<>();
			for (Entry<String, Map<String, Value>> entry : productMapping.entrySet()) {
				String key = entry.getKey();
				List<Entry<String, Value>> controlConfig = entry.getValue().entrySet().stream()
						.collect(Collectors.toList());

				List<Entry<String, Value>> controlConfigOneProduct = controlConfig.stream()
						.filter(config -> config.getKey().contains(key)).collect(Collectors.toList());
				crossed.addAll(controlConfigOneProduct);
			}

			// List<List<Map.Entry<String, Value>>> linears = new
			// ArrayList<List<Map.Entry<String, Value>>>();
			// for (int i = 0; i < c.size(); i++) {
			// linears.add(c.get(i).getConfiguration().getControlledMetrics().entrySet().stream()
			// .collect(Collectors.toList()));
			// }
			//
			// for (List<Map.Entry<String, Value>> linear : linears) {
			// linear.sort((p1, p2) -> p1.getKey().compareTo(p2.getKey()));
			// }

			// List<Map.Entry<String, Value>> crossed = linears.get(0).subList(0,
			// productIndex.get(0));
			// crossed.addAll(linears.get(1).subList(productIndex.get(2),
			// linears.get(1).size()));
			// crossed.addAll(linears.get(2).subList(productIndex.get(0),
			// productIndex.get(1)));
			// crossed.addAll(linears.get(3).subList(productIndex.get(1),
			// productIndex.get(2)));

			Map<String, Value> crossedControlledMetrics = crossed.stream()
					.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
			Configuration newConfig =  new Configuration(c.get(0).getConfiguration().getConfigurationType(), crossedControlledMetrics,
					c.get(0).getConfiguration().getObservableMetrics(), c.get(0).getConfiguration().getKeyObjectives());
			
			
			
			return newConfig;

		};
	}

	public void evolve() {

		/* for each individual in the current population */
		for (int i = 0; i < populationSize; i++) {
			/*
			 * Step 2.1 Reproduction. Get two random neighbors of individual i and let them
			 * crossover and mutate.
			 */
			int[] neighborIndexes = neighborhood.get(i);

			/* The randomly selection applied in the original MOEA/D algorithm. */
			PopulationEntry corssover1 = currentPopulation.get(neighborIndexes[rng.nextInt(neighborIndexes.length)]);
			PopulationEntry corssover2 = currentPopulation.get(neighborIndexes[rng.nextInt(neighborIndexes.length)]);

			List<Configuration> crossoverResults = new ArrayList<Configuration>();
			if (enableNewCrossover) {
				crossoverResults
						.addAll(newCrossover.apply(corssover1.getConfiguration(), corssover2.getConfiguration()));
			} else {
				crossoverResults.add(crossover.apply(corssover1.getConfiguration(), corssover2.getConfiguration()));
				crossoverResults.add(crossover.apply(corssover1.getConfiguration(), corssover2.getConfiguration()));
			}

			List<Configuration> offSpring = new ArrayList<Configuration>();

			for (int j = 0; j < crossoverResults.size(); j++) {
				if (enableNewMutation)
					offSpring.add(newMutation.apply(crossoverResults.get(j)));
				else
					offSpring.add(mutation.apply(crossoverResults.get(j)));
			}

			for (Configuration newIndividual : offSpring) {
				/*
				 * Step 2.2 Repair and Improvement. Apply the problem-specific heuristic to
				 * improve the new individual. For our application, this should check whether
				 * the solution is schedulable.
				 */
				PopulationEntry newIndividualEntry = null;

				if (enableImrpove && i % 25 == 0) {
					newIndividualEntry = improve(newIndividual);
				} else {
					ObjectiveFunctionResult evaled = null;
					try {
						evaled = of.evaluate(
								new ObjectiveFunctionArguments(newIndividual, newIndividual.getControlledMetrics()))
								.call();
					} catch (Exception e) {
						e.printStackTrace();
					}
					newIndividualEntry = new PopulationEntry(evaled.getConfiguration(), evaled.getObjectiveValues());
				}
				/*
				 * Step 2.3 Update of Z. Update the Z vector if the new individual contains
				 * better values for any subproblems.
				 */
				updateIdealPoint(newIndividualEntry);

				/*
				 * Step 2.4 Update of Neighboring Solution. Iterates through all neighbors of
				 * individual i and replaces the neighbors with the new individual if they have
				 * a lower fitness.
				 */
				for (int j = 0; j < neighborhood.get(i).length; j++) {
					double neighborFitness = getFitness(currentPopulation.get(neighborIndexes[j]), neighborIndexes[j]);
					double newFitness = getFitness(newIndividualEntry, neighborIndexes[j]);
					if (newFitness <= neighborFitness) {
						currentPopulation.set(neighborIndexes[j], newIndividualEntry);
						fitness.set(neighborIndexes[j], newFitness);

					}
				}

				/*
				 * Step 2.5 Update of EP. (1) Remove from EP all the individuals dominated by
				 * the new individual; (2) Add new individual to EP if no individuals in EP
				 * dominates it.
				 */
				updateExternalPopulation(newIndividualEntry);
			}

		}
		if (enableElitism && bestSolutions.size() > 1) {
			Configuration best = productSwitchCrossover().apply(bestSolutions);
			PopulationEntry newIndividualEntry = null;
			if (enableImrpove)
				newIndividualEntry = improve(best);
			else {
				ObjectiveFunctionResult evaled = null;
				try {
					evaled = of.evaluate(new ObjectiveFunctionArguments(best, best.getControlledMetrics())).call();
				} catch (Exception e) {
					e.printStackTrace();
				}
				newIndividualEntry = new PopulationEntry(evaled.getConfiguration(), evaled.getObjectiveValues());
			}

			boolean isInserted = false;
			for (int i = 0; i < currentPopulation.size(); i++) {
				PopulationEntry individual = currentPopulation.get(i);
				if (dominate(newIndividualEntry, individual)) {
					if (!isInserted) {
						currentPopulation.set(i, newIndividualEntry);
						isInserted = true;
					}
					updateIdealPoint(newIndividualEntry);
					updateExternalPopulation(newIndividualEntry);
					break;
				}
			}
		}

	}
}
