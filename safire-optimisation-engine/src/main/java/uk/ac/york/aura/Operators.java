package uk.ac.york.aura;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import jeep.math.ClosedInterval;
import jeep.math.LinearInterpolation;
import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.SearchDirection;
import uk.ac.york.safire.metrics.Utility;
import uk.ac.york.safire.metrics.Value;
import uk.ac.york.safire.metrics.Value.Int;
import uk.ac.york.safire.metrics.Value.Nominal;
import uk.ac.york.safire.metrics.Value.Real;
import uk.ac.york.safire.metrics.ValueType;
import uk.ac.york.safire.metrics.ValueTypeVisitor;
import uk.ac.york.safire.metrics.ValueVisitor;

public class Operators {

	static final List< Double > 
	getControlsNamedVar( Configuration c)  {
		final Function< Value, Double > valueToDouble = (v) ->  new ValueVisitor.NumberVisitor().visit(v).doubleValue();
		return c.getControlledMetrics().entrySet().stream().filter( e -> e.getKey().contains("var") ).map( 
				(e) -> valueToDouble.apply(e.getValue()) ).collect(Collectors.toList());
	}
	
	public static List< Configuration > 
	initialPopulation(int populationSize, ConfigurationType ct, Random rng) { 
		final List< Configuration > initialPopulation  = new ArrayList<>();
		for( int i=0; i<populationSize; ++i )
			initialPopulation.add( Utility.randomConfiguration(ct, rng) );
		return initialPopulation;
	}

	///////////////////////////////
	
	public static Comparator< List< Double > > compareMultiObjective(SearchDirection direction) {  
		return new Comparator< List< Double > >() {
			@Override
			public int compare(List<Double> o1, List<Double> o2) {
				if( o1.size() != o2.size() )
					throw new IllegalArgumentException();
				
				int acc = 0;
				for( int i=0; i<o1.size(); ++i ) {
					final int cmp = direction == SearchDirection.MINIMIZING ? -Double.compare(o1.get(i), o2.get(i)) : Double.compare(o1.get(i), o2.get(i));
					if( cmp < 0 )
						return -1;
					else {
						acc = Math.min(acc,cmp);
					}
				}
				
				return acc;
			}
		};
	}
		
	///////////////////////////////
		
	public static BiFunction< List< PopulationEntry >, Random, PopulationEntry > proportionalSelection  = (pop,rnd) -> {
		if( pop.isEmpty() ) {
			throw new IllegalArgumentException();
		}
		if( pop.get(0).getObjectives().size() != 1 ) {
			throw new IllegalArgumentException();
		}
		

		final int index = ProportionalSelectionUtil.select( pop, (pe) -> pe.getObjectives().get(0), rnd );
		return pop.get( index );
	};
	

	public static BiFunction< List< PopulationEntry >, Random, PopulationEntry > proportionalSelectionFirstObjective  = (pop,rnd) -> {
		if( pop.isEmpty() ) {
			throw new IllegalArgumentException();
		}
		

		final int index = ProportionalSelectionUtil.select( pop, (pe) -> pe.getObjectives().get(0), rnd );
		return pop.get( index );
	};	
	
	
	
	public static BiFunction< Configuration, Configuration, Configuration > onePointCrossover(Random rng)  { 
		return  (c1,c2) -> {
			//if( !( c1.getConfigurationType().equals( c2.getConfigurationType() ) ) )
			//	throw new IllegalArgumentException();
		
			List< Map.Entry< String, Value > > linear1 = c1.getControlledMetrics().entrySet().stream().collect(Collectors.toList());
			List< Map.Entry< String, Value > > linear2 = c2.getControlledMetrics().entrySet().stream().collect(Collectors.toList());
			assert( linear1.size() == linear2.size() );
			
			linear1.sort((p1, p2) -> p1.getKey().compareTo(p2.getKey()));
			linear2.sort((p1, p2) -> p1.getKey().compareTo(p2.getKey()));
			
		
			final int index = rng.nextInt(linear1.size());

			final List< Map.Entry< String, Value > > crossed = linear1.subList(0, index);
			crossed.addAll( linear2.subList( index, linear2.size() ) );
			


			
			final Map< String, Value > crossedControlledMetrics = 
					 crossed.stream().collect(Collectors.toMap( e -> e.getKey(), e -> e.getValue() ));					
			return new Configuration( c1.getConfigurationType(), crossedControlledMetrics, c1.getObservableMetrics(),c1.getKeyObjectives() );
		};
	}
	
	public static Function< Configuration, Configuration > 
	hyperMutation(Random rng) { 
		return (c) -> { return Utility.randomConfiguration(c.getConfigurationType(), rng); };
	}

	///////////////////////////////
	
	static final class PerturbationVisitor extends ValueVisitor< Value > {

		private final Random rng;

		///////////////////////////
		
		public PerturbationVisitor(Random rng) {
			this.rng = rng;
		}
		
		///////////////////////////
		
		private static List< Boolean > 
		mutateUniform(List< Boolean > bv, Random rng) {
			for( int i=0; i<bv.size(); ++i )
				if( rng.nextDouble() < 1.0 / bv.size()  )
					bv.set(i, !bv.get(i) );
				
				return bv;
		}

		///////////////////////////
		
		/*
		 * From http://stackoverflow.com/questions/29526985/java-from-biginteger-to-bitset-and-back 
		 * BigInteger is big-endian andBitSet is little-endian, so the bytes will be reversed when 
		 * trying to convert directly from one to the other via toByteArray(). 
		 * The simplest solution is to just reverse them again. 
		 */

		private static byte [] reverse( byte [] bytes) {
		    for(int i = 0; i < bytes.length/2; i++) {
		    	byte temp = bytes[i];
		        bytes[i] = bytes[bytes.length-i-1];
		        bytes[bytes.length-i-1] = temp;
		    }
		    return bytes;
		}	
				
		
		private static String toBinaryString(List< Boolean > bv) {
			StringBuffer result = new StringBuffer();
			for( int i=0; i<bv.size(); ++i )
				result.append( bv.get(i) ? "1" : "0" );
			
			return result.toString();
		}

		private static List< Boolean >
		fromBinaryString(String s) {
			List< Boolean > result = new ArrayList<>();
			for( int i=0; i<s.length(); ++i ) {
				if( s.charAt(i) == '0' )
					result.add( false );
				else {
					if( s.charAt(i) == '1' )
						result.add( true );
					else
						throw new IllegalArgumentException();
				}
			}
			
			return result;
		}
		
		private static List< Boolean > toList(BigInteger bi) {
			return fromBinaryString(bi.toString(2));
		}
		
		private static BigInteger toBigInteger(List< Boolean > bv) {
			return new BigInteger( toBinaryString(bv), 2 );
		}
		
		private static BigInteger binaryToGrey( BigInteger n ) { return n.xor(n.shiftRight(1));	}
		private static BigInteger greyToBinary( BigInteger n ) {
			for( BigInteger i= n.shiftRight(1); i.compareTo(BigInteger.ZERO) != 0; n = n.xor(i), i=i.shiftRight(1) )
				;
			
			return n;
		}
		
		private static List< Boolean > 
		grayEncodeFromReal(double value, int numBits, ClosedInterval range) {
			if( numBits % 8 != 0 )
				throw new IllegalArgumentException();			
			if( !range.contains( value ) )
				throw new IllegalArgumentException();
			
			final double maxOutputValue = Math.pow( 2.0, numBits ) - 1; 
			BigInteger result = new BigInteger( new byte [ numBits / 8 ] );		
			long discreteValue = (long)LinearInterpolation.apply( value, 
					range.getLower(), range.getUpper(), 0, maxOutputValue );

			int bitIndex = 0;		
			for( int j=0; j<numBits; ++j, ++bitIndex )
				if( ( discreteValue & ( 1L << j ) ) != 0 )
					result = result.setBit( bitIndex );
				else
					result = result.clearBit( bitIndex );					

			return toList( binaryToGrey( result ) ); // , numBits );
		}

		///////////////////////////
		
		private static double grayDecodeToReal(List< Boolean > encoded, int numBits, ClosedInterval range) {
			final double maxInputValue = Math.pow( 2.0, numBits ) - 1;
			
			BigInteger bi = greyToBinary( toBigInteger( encoded ) );
			double dValue = bi.doubleValue();
			return LinearInterpolation.apply( dValue, 0, maxInputValue, range.getLower(), range.getUpper() );
		}
		
		///////////////////////////
		
		@Override
		protected Value onInt(Int x) {
			final Pair< Number, Number > range = new ValueTypeVisitor.NumberRangeVisitor().visit( x.getType() );
			final int lower = range.getLeft().intValue();
			final int upper = range.getRight().intValue();			
//			final ClosedInterval interval = lower == upper ? ClosedInterval.create(lower, lower + 1) : ClosedInterval.create(lower, upper);
//			
//			final int numBits = 32;
//			final List< Boolean > mutated = mutateUniform(grayEncodeFromReal(x.value, numBits, interval ), rng);
//			final double value = grayDecodeToReal( mutated, numBits, interval );
//			return Value.intValue( (int)value, (ValueType.Integer)x.getType());
			// FIXME: put the above back in
			final int currentValue = new ValueVisitor.NumberVisitor().visit( x ).intValue();			
			int newValue = currentValue + rng.nextInt(100);
			if( newValue < lower )
				newValue = lower;
			else if( newValue > upper )
				newValue = lower;;
				
			return Value.intValue( newValue, (ValueType.Integer)x.getType());			
		}

		@Override
		protected Value onNominal(Nominal x) {
			final ValueType.Nominal vtn = (ValueType.Nominal) x.getType();
			final int index = rng.nextInt( vtn.numValues() );
			return Value.nominalValue( index, vtn );
		}

		@Override
		protected Value onReal(Real x) {
			final Pair< Number, Number > range = new ValueTypeVisitor.NumberRangeVisitor().visit( x.getType() );
			final double lower = range.getLeft().doubleValue();
			final double upper = range.getRight().doubleValue();			
			final ClosedInterval interval = lower == upper ? ClosedInterval.create(lower, lower + 1) : ClosedInterval.create(lower, upper);
			final int numBits = 64;
			final List< Boolean > encoded = grayEncodeFromReal(x.value, numBits, interval );
			final double value = grayDecodeToReal( mutateUniform(encoded, rng), numBits, interval );
			return Value.realValue(value, (ValueType.Real)x.getType());
		}
	}
	
	///////////////////////////////
	
	public static Function< Configuration, Configuration > 
	onePointMutation(Random rng) { 
		return (c) -> { 
			final Map< String, Value > controls = c.getControlledMetrics();
			final List< MutablePair<String, Value> > controlsList = controls.entrySet().stream().map(e -> MutablePair.of(e.getKey(),e.getValue()) ).collect(Collectors.toList());
			final int index = rng.nextInt( controlsList.size() );
			MutablePair< String, Value> p = controlsList.get(index); 
			// p.setValue( new ValueTypeVisitor.RandomValueVisitor(rng).visit( p.getValue().getType() ));
			p.setValue( new PerturbationVisitor(rng).visit( p.getValue() ));			

			Map< String, Value > proposedControls = controlsList.stream().collect(Collectors.toMap( Pair::getKey, Pair::getValue ));
			
			return new Configuration(c.getConfigurationType(), 
				proposedControls,
				c.getObservableMetrics(),
				c.getKeyObjectives() ); 
		};
	}

	public static Function< Configuration, Configuration > 
	uniformMutation(Random rng) { 
		return (c) -> { 
			final Map< String, Value > controls = c.getControlledMetrics();
			final List< MutablePair<String, Value> > controlsList = controls.entrySet().stream().map(e -> MutablePair.of(e.getKey(),e.getValue()) ).collect(Collectors.toList());
			
			for( int i=0; i<controlsList.size(); ++i ) {
				if( rng.nextDouble() < 1.0 / controlsList.size() ) {
					MutablePair< String, Value> p = controlsList.get(i); 
					p.setValue( new PerturbationVisitor(rng).visit( p.getValue() ));
				}
			}
			
			Map< String, Value > proposedControls = controlsList.stream().collect(Collectors.toMap( Pair::getKey, Pair::getValue ));
			
			return new Configuration(c.getConfigurationType(), 
				proposedControls,
				c.getObservableMetrics(),
				c.getKeyObjectives() ); 
		};
	}

	public static Function< Configuration, Configuration > 
	hyperMutation(double pm, Random rng) {
		if( pm < 0.0 || pm > 1.0 )
			throw new IllegalArgumentException();
		
		return (c) -> { 
			final Map< String, Value > controls = c.getControlledMetrics();
			final List< MutablePair<String, Value> > controlsList = controls.entrySet().stream().map(e -> MutablePair.of(e.getKey(),e.getValue()) ).collect(Collectors.toList());
			
			for( int i=0; i<controlsList.size(); ++i ) {
				if( rng.nextDouble() < pm ) {
					MutablePair< String, Value> p = controlsList.get(i); 
					p.setValue( new PerturbationVisitor(rng).visit( p.getValue() ));
				}
			}
			
			Map< String, Value > proposedControls = controlsList.stream().collect(Collectors.toMap( Pair::getKey, Pair::getValue ));
			
			return new Configuration(c.getConfigurationType(), 
				proposedControls,
				c.getObservableMetrics(),
				c.getKeyObjectives() ); 
		};
	}
	
	public static Predicate< List< List< PopulationEntry > > > 
	maxIterTermination(int maxIter) { 
		return (l) -> l.size() >= maxIter;   
	}
}

// End ///////////////////////////////////////////////////////////////
