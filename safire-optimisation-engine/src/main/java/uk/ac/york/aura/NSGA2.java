package uk.ac.york.aura;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.MutablePair;

import jeep.lang.Logic;
import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.SearchDirection;
import uk.ac.york.safire.optimisation.ObjectiveFunction;
import uk.ac.york.safire.optimisation.ObjectiveFunctionArguments;
import uk.ac.york.safire.optimisation.ObjectiveFunctionResult;

///////////////////////////////////

public final class NSGA2 {

	static int compareObjectives(double o1, double o2, SearchDirection dir) {
		return dir == SearchDirection.MINIMIZING ? -Double.compare(o1, o2) : Double.compare(o1, o2);		
	}
	
	///////////////////////////////
	
	/**
	 * Algorithm 98 from "Essentials of Metaheuristics", 2nd Edition, p137.
	 */
	static boolean paretoDominates(List< Double > o1, List< Double > o2, SearchDirection dir) {
		if( o1.size() != o2.size() )
			throw new IllegalArgumentException("Unequal sizes: " + o1.size() + " and " + o2.size() );
		
		boolean result = false;
		for( int i=0; i<o1.size(); ++i ) {
			final int cmp = compareObjectives(o1.get(i), o2.get(i), dir);
			if( cmp > 0 )
				result = true;
			else {
				if( cmp < 0 )
					return false;					
			}
		}
		
		assert( Logic.implies( o1.equals(o2), !result ) );
		return result;
	}
	
	///////////////////////////////
	
	public static boolean isNondominated(List< List< Double > > objectivesList, SearchDirection dir) {
		for(int i=0; i<objectivesList.size(); ++i )
			for(int j=0; j<objectivesList.size(); ++j )
				if( j != i && paretoDominates(objectivesList.get(i), objectivesList.get(j), dir) )
					return false;
		
		return true;
	}
	
	///////////////////////////////	

//	public static boolean isInParetoFrontOf(List< Double > f, List< List< Double > > front, List< List< Double > > pop, SearchDirection dir) {
//		return front.contains(f) && pop.stream().anyMatch( g -> front.contains(g) ? false : paretoDominates(g, f, dir ) );
//	}
	
	public static boolean isParetoFrontOf(List< List< Double > > front, List< List< Double > > pop, SearchDirection dir) {
		return isNondominated( front, dir ) &&
			pop.stream().allMatch( g -> front.contains(g) || 
				!front.stream().anyMatch( f -> paretoDominates(g, f, dir ) ) );
	}
	
//	public static void isParetoFrontOfReport(List< List< Double > > front, List< List< Double > > pop, final SearchDirection dir ) {
//		if( !isNondominated( front, dir ) )
//			jeep.lang.Diag.println("Front is internally dominated!!!");
//
//		final List< String > dd = front.stream().filter( f ->
//					!isInParetoFrontOf(f, front, pop, dir)				
//		).map( Object::toString ).collect(Collectors.toList());
//		
//		final Stream< Pair< List< Double >, List< Double  > > > cartesianProduct = 
//				front.stream().flatMap( f -> 
//					pop.stream().map( g -> 
//						new Pair< List<Double>, List<Double> >(f,g) 
//					) 
//				);
//		
//		final Stream< String > dominated =  			
//			cartesianProduct.filter( p -> 
//				front.contains( p.getSecond() ) ? false : paretoDominates( p.getSecond(), p.getFirst(), dir ) 
//			).map( p -> 
//				"dominatedBy( " + p.getFirst() + "  " + p.getSecond() + ")" 
//			);
//
//		jeep.lang.Diag.println( front.size() );
//		jeep.lang.Diag.println( pop.size() );		
//		jeep.lang.Diag.println( "Front: " + front.stream().map(Object::toString).collect(Collectors.joining("\n")) );		
//		jeep.lang.Diag.println( "Dominated: " + dominated.collect(Collectors.joining("\n")) );
//		
//		jeep.lang.Diag.println( dd.size() );		
//		jeep.lang.Diag.println( dd.stream().collect(Collectors.joining("\n")) );		
//	}
	
	///////////////////////////////	

	static List< PopulationEntry > 
	paretoFrontNaive(List< PopulationEntry > G, SearchDirection dir) {
		
		// naive O(n^2) algorithm

		List< PopulationEntry > F = new ArrayList<>();
		
		for( int i=0; i<G.size(); ++i ) {
			final PopulationEntry G_i = G.get(i);
			
			boolean dominated = false;
			for( int j=0; !dominated && j<G.size(); ++j ) {
				if( j == i )
					continue;
				
				final PopulationEntry G_j = G.get(j);
				
				dominated = paretoDominates(G_j.getObjectives(),G_i.getObjectives(), dir);
			}
			if( !dominated && !F.stream().anyMatch( F_j -> F_j.getObjectives().equals( G_i.getObjectives() ) ) )
				F.add( G_i );
		}

		assert( isParetoFrontOf(F.stream().map( x -> x.getObjectives() ).collect(Collectors.toList()), 
				G.stream().map( x -> x.getObjectives() ).collect(Collectors.toList()) , dir));		
		
		return F;
	}

	/**
	 * Adapted from Algorithm 100 from "Essentials of Metaheuristics", 2nd Edition, p137.
	 */
	
	static List< PopulationEntry > 
	paretoFront(List< PopulationEntry > G, SearchDirection dir) {
		
		
		List< PopulationEntry > F = new ArrayList<>();
		for( int i=0; i<G.size(); ++i ) {

			final PopulationEntry G_i = G.get(i);
			F = F.stream().filter( F_j -> !paretoDominates(G_i.getObjectives(), F_j.getObjectives(), dir) ).collect(Collectors.toList());
			if( !F.stream().anyMatch( F_j -> 
				F_j.getObjectives().equals( G_i.getObjectives() ) || 
				paretoDominates(F_j.getObjectives(), G_i.getObjectives(), dir) ) )
				F.add(G_i);
		}
		
		assert( F.stream().collect(Collectors.toSet()).equals( paretoFrontNaive(G, dir).stream().collect(Collectors.toSet()) ));		
		// return F.stream().collect(Collectors.toSet()).stream().collect(Collectors.toList());
		return F;
	}	
	
	///////////////////////////////

	/**
	 * Algorithm 101 from "Essentials of Metaheuristics", 2nd Edition, p137.
	 */
	
	static List< List< PopulationEntry > >
	paretoFrontRanks( List< PopulationEntry > P, SearchDirection dir) {
		
		List< PopulationEntry > Pdash = new ArrayList<>();
		Pdash.addAll(P);
		List< List< PopulationEntry >  > ranks = new ArrayList<>();
		do {
			final List< PopulationEntry > R_i = paretoFront( Pdash, dir );
			ranks.add( R_i );
			Pdash.removeAll(R_i);
			
		} while( !Pdash.isEmpty() );
		
		return ranks;		
	}
	
	///////////////////////////////

	static final class PopulationEntryRankAndSparsity {
		final PopulationEntry populationEntry ;
		final int rank;
		final double sparsity;
		
		///////////////////////////
		
		public PopulationEntryRankAndSparsity(PopulationEntry pe, int rank, double sparsity) {
			this.populationEntry = pe;
			this.rank = rank;
			this.sparsity = sparsity;
		}
	}
	
	///////////////////////////////
	
	/**
	 * Algorithm 102 from "Essentials of Metaheuristics", 2nd Edition, p137.
	 */
	
	static List< PopulationEntryRankAndSparsity >
	multiobjectiveSparsityAssignment( List< PopulationEntry > F, int rank, 
		List< Double > objectiveValueRange, 
		SearchDirection dir) {

		if( objectiveValueRange.stream().anyMatch( value -> value <= 0.0 ) )
			throw new IllegalArgumentException();
		if( F.isEmpty() )
			throw new IllegalArgumentException();
		if( F.stream().anyMatch( pe -> pe.getObjectives().size() != objectiveValueRange.size() ) )
			throw new IllegalArgumentException("Expected #objectives: " + objectiveValueRange.size() + ", found " + F.stream().map( pe -> pe.getObjectives().size() ).collect(Collectors.toList())  );
		
		///////////////////////////
		
		final List< MutablePair< PopulationEntry, Double > 
			> Fdash = F.stream().map( pe -> MutablePair.of(pe,0.0) ).collect(Collectors.toList());
		
		// final int numObjectives = F.get(0).getObjectives().size();

		for( int i=0; i<objectiveValueRange.size(); ++i ) {
			final int iFinal = i;
			Fdash.sort( (p1,p2) -> compareObjectives( p1.getLeft().getObjectives().get(iFinal), p2.getLeft().getObjectives().get(iFinal), dir ) );
			Fdash.get(0).setRight(Double.POSITIVE_INFINITY);
			Fdash.get(Fdash.size() - 1).setRight(Double.POSITIVE_INFINITY);
			
			for( int j=1; j<Fdash.size() - 1; ++j ) {
				final double inc = Fdash.get(j+1).getLeft().getObjectives().get(i) - Fdash.get(j-1).getLeft().getObjectives().get(i);
				Fdash.get(j).setRight( Fdash.get(j).getRight() + ( inc / objectiveValueRange.get(i) ) );
			}
		}
		
		return Fdash.stream().map( p -> new PopulationEntryRankAndSparsity(p.getLeft(), rank, p.getRight() ) ).collect(Collectors.toList());
	}
	
	///////////////////////////////

	/**
	 * Algorithm 103 from "Essentials of Metaheuristics", 2nd Edition.
	 */
	
	static PopulationEntry 
	nondominatedLexicographicTournamentSelectionWithSparsity(int tournamentSize, List< PopulationEntryRankAndSparsity > P, Random rng) {
		if( tournamentSize < 1 )
			throw new IllegalArgumentException();
		
		PopulationEntryRankAndSparsity best = P.get( rng.nextInt( P.size()) );
		for( int i=2; i<tournamentSize; ++i ) {
			final PopulationEntryRankAndSparsity next = P.get( rng.nextInt( P.size() ) );			
			if( next.rank < best.rank || ( next.rank == best.rank && next.sparsity > best.sparsity ) )
				best = next;						
		}
		
		return best.populationEntry;
	}
	
	///////////////////////////////
	
	private static List< PopulationEntry > 
	breedSequential(final List< PopulationEntryRankAndSparsity > currentPopulation,
		ObjectiveFunction.LocalObjectiveFunction of,			
		BiFunction< List< PopulationEntryRankAndSparsity >, Random, PopulationEntry > select,
		BiFunction< Configuration, Configuration, Configuration > crossover,
		Function< Configuration, Configuration > mutation, Random rng ) {
		
		final List< PopulationEntry > newPopulation = new ArrayList<>();
	
		// IntStream.range(0, initialPopulation.size() / 2).parallel().forEach( i -> {			
		IntStream.range(0, currentPopulation.size() / 2).forEach( i -> {
			final PopulationEntry p1 = select.apply( currentPopulation, rng );
			final PopulationEntry p2 = select.apply( currentPopulation, rng );
			final Configuration c1 = mutation.apply( crossover.apply( p1.getConfiguration(), p2.getConfiguration() ) );
			final Configuration c2 = mutation.apply( crossover.apply( p1.getConfiguration(), p2.getConfiguration() ) );
			final ObjectiveFunctionResult evaled1 = ObjectiveFunction.evaluateNowHelper(of, new ObjectiveFunctionArguments(p1.getConfiguration(), c1.getControlledMetrics()) );
			final ObjectiveFunctionResult evaled2 = ObjectiveFunction.evaluateNowHelper(of,new ObjectiveFunctionArguments(p2.getConfiguration(), c2.getControlledMetrics()) );			
			final PopulationEntry e1 = new PopulationEntry(evaled1.getConfiguration(), evaled1.getObjectiveValues() );
			final PopulationEntry e2 = new PopulationEntry(evaled2.getConfiguration(), evaled2.getObjectiveValues() );
			newPopulation.add(e1);
			newPopulation.add(e2);			
		} );
		
		return newPopulation;
	}

	///////////////////////////////
	
	private static ObjectiveFunctionResult 
	futureGetHelper(Future< ObjectiveFunctionResult > future) {
		try {
			return future.get();
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException( e.getCause() );
		}
	}
	
	private static List< PopulationEntry > 
	breedConcurrent(final List< PopulationEntryRankAndSparsity > currentPopulation,
		ObjectiveFunction.LocalObjectiveFunction of,			
		BiFunction< List< PopulationEntryRankAndSparsity >, Random, PopulationEntry > select,
		BiFunction< Configuration, Configuration, Configuration > crossover,
		Function< Configuration, Configuration > mutation, Random rng ) {

		final ExecutorService executor = Executors.newWorkStealingPool();
		
		// Batch together arguments for objective function invocations:
		final List< Callable< ObjectiveFunctionResult > > callables = new ArrayList<>();
		IntStream.range(0, currentPopulation.size() / 2).forEach( i -> {
			final PopulationEntry p1 = select.apply( currentPopulation, rng );
			final PopulationEntry p2 = select.apply( currentPopulation, rng );
			final Configuration c1 = mutation.apply( crossover.apply( p1.getConfiguration(), p2.getConfiguration() ) );
			final Configuration c2 = mutation.apply( crossover.apply( p1.getConfiguration(), p2.getConfiguration() ) );
			
			callables.add(of.evaluate( new ObjectiveFunctionArguments( p1.getConfiguration(), c1.getControlledMetrics() ) ));
			callables.add(of.evaluate( new ObjectiveFunctionArguments( p2.getConfiguration(), c2.getControlledMetrics() ) ));			
		} );

		///////////////////////////
		
		// Now call them in parallel:
		try {
			return executor.invokeAll(callables).stream().map(
				future -> {
					final ObjectiveFunctionResult ofr = futureGetHelper( future );
					return new PopulationEntry(ofr.getConfiguration(), ofr.getObjectiveValues() );
				} 
			).collect(Collectors.toList());
		} catch (InterruptedException e) {
			throw new RuntimeException( e.getCause() );
		}		
	
//		// IntStream.range(0, initialPopulation.size() / 2).parallel().forEach( i -> {			
//		IntStream.range(0, currentPopulation.size() / 2).forEach( i -> {
//			final PopulationEntry p1 = select.apply( currentPopulation, rng );
//			final PopulationEntry p2 = select.apply( currentPopulation, rng );
//			final Configuration c1 = mutation.apply( crossover.apply( p1.getConfiguration(), p2.getConfiguration() ) );
//			final Configuration c2 = mutation.apply( crossover.apply( p1.getConfiguration(), p2.getConfiguration() ) );
//			final ObjectiveFunctionResult evaled1 = ObjectiveFunction.evaluateNowHelper(of,p1.getConfiguration(), c1.getControlledMetrics() );
//			final ObjectiveFunctionResult evaled2 = ObjectiveFunction.evaluateNowHelper(of,p2.getConfiguration(), c2.getControlledMetrics() );
//			
//			final PopulationEntry e1 = new PopulationEntry(evaled1.getConfiguration(), evaled1.getObjectiveValues() );
//			final PopulationEntry e2 = new PopulationEntry(evaled2.getConfiguration(), evaled2.getObjectiveValues() );
//			newPopulation.add(e1);
//			newPopulation.add(e2);			
//		} );
	}
	
	///////////////////////////////	
	
	/**
	 * Algorithm 104 from "Essentials of Metaheuristics", 2nd Edition.
	 */
	
	public static List< PopulationEntry > 
	apply(List< Configuration > initialPopulation, 
		ObjectiveFunction.LocalObjectiveFunction of,
		Comparator< List< Double > > compareMultiObjective,
		BiFunction< Configuration, Configuration, Configuration > crossover,
		Function< Configuration, Configuration > mutation,
		Predicate< List< List< PopulationEntry > > > isFinished, 
		List< Double > objectiveValueRanges,
		int tournamentSize,
		SearchDirection dir, Random rng ) {
		
		if( initialPopulation.size() < 2 )
			throw new IllegalArgumentException();
		
		///////////////////////////

		final int maxArchiveSize = initialPopulation.size();
		
		// final Stream< PopulationEntry > population = initialPopulation.parallelStream().map(
		final Stream< PopulationEntry > population = initialPopulation.stream().map(		
			(Configuration c) -> {
				ObjectiveFunctionResult evaled = ObjectiveFunction.evaluateNowHelper(of,new ObjectiveFunctionArguments( c, c.getControlledMetrics() ) );
				return new PopulationEntry(evaled.getConfiguration(), evaled.getObjectiveValues() );					
			}
		); 
		
		List< PopulationEntry > currentPopulation = population.collect(Collectors.toList());
		final List< PopulationEntryRankAndSparsity > archive = new ArrayList<>();
		
		///////////////////////////		
		
		final List< Integer > bestFrontSize = new ArrayList<>();
		List< PopulationEntry > bestFront = new ArrayList<>(); 
		final List< List< PopulationEntry > > history = new ArrayList<>();
		history.add( currentPopulation );
		int iter = 0;
		while( !isFinished.test( history ) ) {
			
			int current = history.size();
			if (current % 10 == 0)
				System.out.println("now " + current + " generations");

			currentPopulation.addAll( archive.stream().map(p -> p.populationEntry).collect(Collectors.toList()) );
			
			bestFront = paretoFront( currentPopulation, dir );
			if( false && iter > 0 )
				jeep.lang.Diag.println( bestFront );

			bestFrontSize.add( bestFront.size() );
 //jeep.lang.Diag.println( bestFront.size() );

			final List< List< PopulationEntry > > R = paretoFrontRanks( bestFront, dir );
			
			archive.clear();
			for( int i=0; i<R.size(); ++i ) {
				final List< PopulationEntryRankAndSparsity 
					> withSparsity = multiobjectiveSparsityAssignment( R.get(i), i, objectiveValueRanges, dir);
				
				if( R.get(i).size() + archive.size() >= maxArchiveSize ) {

					withSparsity.sort( (p1,p2) -> Double.compare( p1.sparsity, p2.sparsity ) );					
					final List< PopulationEntryRankAndSparsity 
						> sparsest = withSparsity.subList( 0, maxArchiveSize - archive.size() );
					archive.addAll( sparsest );
					
					break;
				}
				else {
					archive.addAll( withSparsity );
				}
			}
			
			final BiFunction< List< PopulationEntryRankAndSparsity >, Random, PopulationEntry > select = (pop, r) ->
				nondominatedLexicographicTournamentSelectionWithSparsity(tournamentSize, pop, r);

				
			final boolean concurrent = true;
			if( !concurrent )
				jeep.lang.Diag.println( "WARNING: concurrency is disabled in " + NSGA2.class.getName() );
				
			currentPopulation = concurrent ? breedConcurrent(archive, of, select, crossover, mutation, rng ) : breedSequential(archive, of, select, crossover, mutation, rng );
			// currentPopulation = breedConcurrent(archive, of, select, crossover, mutation, rng );				
			history.add(currentPopulation);
			
			++iter;
		}

//jeep.lang.Diag.println( "best front size: " + bestFrontSize );
		
		// return bestFront.stream().collect(Collectors.toSet()).stream().collect(Collectors.toList());
	//assert( isParetoFrontOf(bestFront.stream().map( x -> x.getObjectives() ).collect(Collectors.toList()), 
	//	currentPopulation.stream().map( x -> x.getObjectives() ).collect(Collectors.toList()) , dir));		
		
		return bestFront;
	}
}

// End ///////////////////////////////////////////////////////////////
