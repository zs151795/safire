package uk.ac.york.safire.optimisation.maxplusplant;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import com.google.gson.Gson;

import jeep.math.ClosedOpenInterval;
import uk.ac.york.safire.optimisation.AlgType;
import uk.ac.york.safire.optimisation.JMetalLocalOptimisationEngine;
import uk.ac.york.safire.optimisation.MockObjectiveFunction;
import uk.ac.york.safire.optimisation.ObjectiveFunction;
import uk.ac.york.safire.optimisation.OptimisationEngine;

// import scala.collection.JavaConverters.*;

///////////////////////////////////

public class RunPlant {

//	private static Configuration
//	simulatePlant(Plant plant, 
//		Configuration config,
//		OptimisationEngine optimiser,
//		// ObjectiveFunction objectiveFunction,
//		int numIterations) {
//		
//		final double urgency = 0.5;
//		final double quality = 0.5;
//		
//		for( int i=0; i<numIterations; ++i ) {
////			jeep.lang.Diag.println( "iteration " + i + " ( of " + numIterations + ")" );
////			jeep.lang.Diag.println( config );
//			List< Configuration > population = Collections.singletonList( config );
//		OptimisationResult or = optimiser.optimise(new OptimisationArguments(population,urgency,quality));
//	//		System.out.println( "value of proposed metrics: " + java.util.Arrays.toString( objectiveFunction.value(config, or) ) );			
////		scala.Option<scala.collection.immutable.List< scala.Tuple2< ConfigurationSolution, double [] > > > opt = JMetalLocalOptimisationEngine.optimalSolutions(config, objectiveFunction);
////		System.out.println( "value of proposed metrics: " + java.util.Arrays.toString(opt.get().apply(0)._2 ) );
////			jeep.lang.Diag.println( opt );
//			config = plant.update(config, or);
//		}
//		
//		return config;
//	}
	
	///////////////////////////////

	static OptimisationEngine 
	createHttpOptimisationEngineClient(String [] args, ObjectiveFunction objective) throws MalformedURLException {
		String args0 = null;
		
		if( args.length == 0 ) {
			java.net.InetSocketAddress addr = new java.net.InetSocketAddress("127.0.0.1", 8080); 
			args0 = "http:/" + addr;
			System.out.println( args0 );
		}
		else {
			if( args.length == 1 ) 
				args0 = args[ 0 ];
			else {
				System.err.println( "Usage: " + RunPlant.class.getSimpleName() + " http://optimization-engine-url" );
				System.exit( 0 );			
			}
		}
		
		jeep.lang.Diag.println( args0 );

		URL optimizationEngineURL = new URL(args0);
		return new OptimisationEngine.RemoteHttpJsonOptimisationEngine(optimizationEngineURL);
	}
	
	static OptimisationEngine 
	createJMetalLocalOptimisationEngine(int minEvaluations, int maxEvaluations, MockObjectiveFunction objectiveFunction, Random rng) {
		return new JMetalLocalOptimisationEngine( AlgType.NSGA3,
			minEvaluations, maxEvaluations, objectiveFunction, rng);
	}
	
	///////////////////////////////
	
//	public static String runOneIteration(String jsonIn) {
//		JsonConverter jsonConverter = new JsonConverter();
//		Configuration currentConfig = jsonConverter.fromJson(jsonIn, Configuration.class );
//
//		Graph graph=deserializeGraph();
//		SimplePlant plant = new SimplePlant(graph,currentConfig.getConfigurationType());
////		double a[]=plant.fitness(currentConfig);	
//		currentConfig = plant.update(currentConfig);
//		
//		String result= jsonConverter.toJson(currentConfig);
//		return result;	
//	}
	
	///////////////////////////////

    private static Graph deserializeGraph() {
//    	String json = "{\"verticeList\":[{\"name\":\"1\"},{\"name\":\"2\"},{\"name\":\"3\"},{\"name\":\"4\"},{\"name\":\"5\"}],\"edgeSet\":[{\"name\":\"e\",\"sourceVertex\":\"3\",\"destVertex\":\"4\",\"nominalDuration\":10,\"nominalEnergyPerTimeUnit\":1,\"controlMetricsName\":\"mode E\",\"modeSet\":[{\"name\":\"highSpeed\",\"durationMultiplier\":1.25,\"energyMultiplier\":1.5},{\"name\":\"normal\",\"durationMultiplier\":1,\"energyMultiplier\":1},{\"name\":\"ecoMode\",\"durationMultiplier\":0.75,\"energyMultiplier\":0.5}]},{\"name\":\"c\",\"sourceVertex\":\"2\",\"destVertex\":\"3\",\"nominalDuration\":10,\"nominalEnergyPerTimeUnit\":1,\"controlMetricsName\":\"mode C\",\"modeSet\":[{\"name\":\"highSpeed\",\"durationMultiplier\":1.25,\"energyMultiplier\":1.5},{\"name\":\"normal\",\"durationMultiplier\":1,\"energyMultiplier\":1},{\"name\":\"ecoMode\",\"durationMultiplier\":0.75,\"energyMultiplier\":0.5}]},{\"name\":\"b\",\"sourceVertex\":\"2\",\"destVertex\":\"4\",\"nominalDuration\":10,\"nominalEnergyPerTimeUnit\":1,\"controlMetricsName\":\"mode B\",\"modeSet\":[{\"name\":\"highSpeed\",\"durationMultiplier\":1.25,\"energyMultiplier\":1.5},{\"name\":\"normal\",\"durationMultiplier\":1,\"energyMultiplier\":1},{\"name\":\"ecoMode\",\"durationMultiplier\":0.75,\"energyMultiplier\":0.5}]},{\"name\":\"a\",\"sourceVertex\":\"1\",\"destVertex\":\"2\",\"nominalDuration\":10,\"nominalEnergyPerTimeUnit\":1,\"controlMetricsName\":\"mode A\",\"modeSet\":[{\"name\":\"highSpeed\",\"durationMultiplier\":1.25,\"energyMultiplier\":1.5},{\"name\":\"normal\",\"durationMultiplier\":1,\"energyMultiplier\":1},{\"name\":\"ecoMode\",\"durationMultiplier\":0.75,\"energyMultiplier\":0.5}]},{\"name\":\"d\",\"sourceVertex\":\"4\",\"destVertex\":\"5\",\"nominalDuration\":10,\"nominalEnergyPerTimeUnit\":1,\"controlMetricsName\":\"mode D\",\"modeSet\":[{\"name\":\"highSpeed\",\"durationMultiplier\":1.25,\"energyMultiplier\":1.5},{\"name\":\"normal\",\"durationMultiplier\":1,\"energyMultiplier\":1},{\"name\":\"ecoMode\",\"durationMultiplier\":0.75,\"energyMultiplier\":0.5}]}],\"root\":\"1\"}";

    	
//generated plant    	
    	String json="{\"verticeList\":[{\"name\":\"v0\"},{\"name\":\"v1\"},{\"name\":\"v2\"},{\"name\":\"v3\"},{\"name\":\"v4\"},{\"name\":\"v5\"},{\"name\":\"v6\"}],\"edgeSet\":[{\"name\":\"v0-v1\",\"sourceVertex\":\"v0\",\"destVertex\":\"v1\",\"nominalDuration\":17,\"nominalEnergyPerTimeUnit\":1.9675218649917645,\"controlMetricsName\":\"Mode v0-v1\",\"modeSet\":[{\"name\":\"_machine_0_mode_0\",\"durationMultiplier\":2.5516017758565486,\"energyMultiplier\":1.4064928924211904},{\"name\":\"_machine_0_mode_1\",\"durationMultiplier\":2.5516017758565486,\"energyMultiplier\":1.4064928924211904},{\"name\":\"_machine_0_mode_2\",\"durationMultiplier\":2.5516017758565486,\"energyMultiplier\":1.4064928924211904}]},{\"name\":\"v2-v3\",\"sourceVertex\":\"v2\",\"destVertex\":\"v3\",\"nominalDuration\":11,\"nominalEnergyPerTimeUnit\":8.338763912656852,\"controlMetricsName\":\"Mode v2-v3\",\"modeSet\":[{\"name\":\"_machine_1_mode_0\",\"durationMultiplier\":2.6589231725308253,\"energyMultiplier\":1.9780702698898924}]},{\"name\":\"v5-v6\",\"sourceVertex\":\"v5\",\"destVertex\":\"v6\",\"nominalDuration\":16,\"nominalEnergyPerTimeUnit\":7.974308442140751,\"controlMetricsName\":\"Mode v5-v6\",\"modeSet\":[{\"name\":\"_machine_2_mode_0\",\"durationMultiplier\":2.691299409024743,\"energyMultiplier\":0.17915686817789883},{\"name\":\"_machine_2_mode_1\",\"durationMultiplier\":2.691299409024743,\"energyMultiplier\":0.17915686817789883}]},{\"name\":\"v3-v6\",\"sourceVertex\":\"v3\",\"destVertex\":\"v6\",\"nominalDuration\":5,\"nominalEnergyPerTimeUnit\":5.4727774206316235,\"controlMetricsName\":\"Mode v3-v6\",\"modeSet\":[{\"name\":\"_machine_2_mode_0\",\"durationMultiplier\":2.691299409024743,\"energyMultiplier\":0.17915686817789883},{\"name\":\"_machine_2_mode_1\",\"durationMultiplier\":2.691299409024743,\"energyMultiplier\":0.17915686817789883}]},{\"name\":\"v1-v3\",\"sourceVertex\":\"v1\",\"destVertex\":\"v3\",\"nominalDuration\":8,\"nominalEnergyPerTimeUnit\":8.262704568773577,\"controlMetricsName\":\"Mode v1-v3\",\"modeSet\":[{\"name\":\"_machine_1_mode_0\",\"durationMultiplier\":2.6589231725308253,\"energyMultiplier\":1.9780702698898924}]},{\"name\":\"v4-v6\",\"sourceVertex\":\"v4\",\"destVertex\":\"v6\",\"nominalDuration\":6,\"nominalEnergyPerTimeUnit\":3.467210592487516,\"controlMetricsName\":\"Mode v4-v6\",\"modeSet\":[{\"name\":\"_machine_2_mode_0\",\"durationMultiplier\":2.691299409024743,\"energyMultiplier\":0.17915686817789883},{\"name\":\"_machine_2_mode_1\",\"durationMultiplier\":2.691299409024743,\"energyMultiplier\":0.17915686817789883}]},{\"name\":\"v1-v4\",\"sourceVertex\":\"v1\",\"destVertex\":\"v4\",\"nominalDuration\":3,\"nominalEnergyPerTimeUnit\":3.7331740552862387,\"controlMetricsName\":\"Mode v1-v4\",\"modeSet\":[{\"name\":\"_machine_1_mode_0\",\"durationMultiplier\":2.6589231725308253,\"energyMultiplier\":1.9780702698898924}]},{\"name\":\"v0-v2\",\"sourceVertex\":\"v0\",\"destVertex\":\"v2\",\"nominalDuration\":15,\"nominalEnergyPerTimeUnit\":3.316829561606236,\"controlMetricsName\":\"Mode v0-v2\",\"modeSet\":[{\"name\":\"_machine_2_mode_0\",\"durationMultiplier\":2.691299409024743,\"energyMultiplier\":0.17915686817789883},{\"name\":\"_machine_2_mode_1\",\"durationMultiplier\":2.691299409024743,\"energyMultiplier\":0.17915686817789883}]},{\"name\":\"v1-v5\",\"sourceVertex\":\"v1\",\"destVertex\":\"v5\",\"nominalDuration\":16,\"nominalEnergyPerTimeUnit\":5.237352942758269,\"controlMetricsName\":\"Mode v1-v5\",\"modeSet\":[{\"name\":\"_machine_0_mode_0\",\"durationMultiplier\":2.5516017758565486,\"energyMultiplier\":1.4064928924211904},{\"name\":\"_machine_0_mode_1\",\"durationMultiplier\":2.5516017758565486,\"energyMultiplier\":1.4064928924211904},{\"name\":\"_machine_0_mode_2\",\"durationMultiplier\":2.5516017758565486,\"energyMultiplier\":1.4064928924211904}]}],\"root\":\"v0\"}";  
    			
    	
    	Graph graph=new Gson().fromJson(json,Graph.class);
    	graph.addOutgoingEdges();
    	return graph;

    }
	
	///////////////////////////////
    
    public static SimplePlant generatePlant( 
    	int noOfInternalLevels,
        ClosedOpenInterval< Integer > verticesPerInternalLevel,
    	int noOfMachines,
    	int maxNoOfModesPerMachine,
    	ClosedOpenInterval< Integer > nominalDuration,
    	ClosedOpenInterval< Double > energyPerTimeUnit,
    	ClosedOpenInterval< Double > energyMultiplier,
    	ClosedOpenInterval< Double > durationMultiplier,			
    	Random rng) {

		return PlantGenerator.generatePlant(
			noOfInternalLevels,
			verticesPerInternalLevel.getLower(), verticesPerInternalLevel.getUpper(), 
			noOfMachines, maxNoOfModesPerMachine,
			nominalDuration.getLower(), nominalDuration.getUpper(),				
			energyPerTimeUnit.getLower(), energyPerTimeUnit.getUpper(),
			energyMultiplier.getLower(), energyMultiplier.getUpper(),
			durationMultiplier.getLower(), durationMultiplier.getUpper(), rng);
    }
    
	///////////////////////////////
    
//    static double run(SimplePlant plant, OptimisationEngine optimiser, int numIterations, Random rng) 
//    		// throws IOException 
//    {
//
//		final Configuration config = Utility.randomConfiguration(plant.getConfigurationType(), rng);
//		
//		final long startTime = System.currentTimeMillis();
//		simulatePlant(plant, config, optimiser, numIterations);
//		final long endTime = System.currentTimeMillis();
//		final double elapsed = (endTime - startTime ) / 1000.0;
//		// System.out.println( "elapsed: " + elapsed );
//		return elapsed;
//    }
    
	///////////////////////////////    

    static final class Result {
    	
    	final int numMachines;    	
    	final int numVertices;
    	final int numEdges;    	
    	final double elapsedInSeconds;
    	
    	Result(int numMachines, int numVertices, int numEdges, double elapsedInSeconds) {
    		this.numMachines = numMachines;
    		this.numVertices = numVertices;
    		this.numEdges = numEdges;
    		this.elapsedInSeconds = elapsedInSeconds;    		
    	}

    	@Override
    	public String toString() {
			return org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString(this);			
    	}
    }
    
	///////////////////////////////
    
    /**********
    static void testScalability(Random rng) throws IOException {

		final int numIterations = 1;
		final int numRuns = 2;
		
		List< Result > results = new ArrayList<>();
		for( int i=0; i<numRuns; ++i ) {
jeep.lang.Diag.println("**" + i + "*************************************************************************");			
			
			// final int numMachines = 10 * (i + 1);
 			final int numMachines = 12;
			final SimplePlant plant = generatePlant( 
                3 * (i + 1), // noOfInternalLevels
				new ClosedOpenInterval<Integer>(1,3+i), // verticesPerInternalLevel
				numMachines, // 3 + i, // noOfMachines
	    		4 + i, // maxNoOfModesPerMachine
	    		new ClosedOpenInterval<Integer>(1,20 + i), // nominalDuration
	    		new ClosedOpenInterval<Double>(1.0,10.0 + i), // energyPerTimeUnit
	    		new ClosedOpenInterval<Double>(0.1,2.0 + i), // energyMultiplier	    		
	    		new ClosedOpenInterval<Double>(0.2,3.0 + i), // durationMultiplier			
	    		rng);
			
			
			
			final double elapsed = run(plant,numIterations,rng);
//			final int V = plant.getGraph().getNoOfVertices();
//			final double edgeDensity = plant.getGraph().getNoOfEdges() / ((double)V * V);
//			jeep.lang.Diag.println( "#machines: " + numMachines + ", #V: " + V + ", #E: " + plant.getGraph().getNoOfEdges() + ", edgeDensity:" + edgeDensity + ", elapsed:" + elapsed );
			results.add( new Result( numMachines, plant.getGraph().getNoOfVertices(), plant.getGraph().getNoOfEdges(), elapsed ) );
			List< List< ObjectiveValue > > result=plant.cpSolve();
			for(List< ObjectiveValue > solution : result) {
				System.out.println(solution.toString());
			}
		}
		
		StringBuffer data = new StringBuffer( "{" );
		for( int i=0; i<results.size(); ++i ) {
			final Result r = results.get(i);
			data.append( "{" + r.numMachines + "," + r.numEdges + "," + r.elapsedInSeconds + "}" );	
			if( i < results.size() - 1 )
				data.append( "," );				
		}
		data.append( "}" );		

//		System.out.print( "elements = {" );
//		for( int i=0; i<results.size(); ++i ) {
//			final Result r = results.get(i);
//			if( i < results.size() - 1 )
//				System.out.print( "," );				
//		}
//		System.out.println( "};" );		
		
		// System.out.println( "ListPlot[MapThread[Labeled, {data, elements}]]" );
		System.out.println( "ListPlot3D[" + data + ", AxesLabel -> {\"|V|\", \"|E|\", \"t (secs)\"} ]" );
		// exact for 0-th entry:
		// value of proposed metrics: [1161.6118603811553, 71.0]						
	}
    **********/
    
	///////////////////////////////	
    /****	
	public static void main(String [] args) throws IOException {
		
		System.out.println( new java.util.Date() );
		
		final int seed = 0xDEADBEEF;
		final Random rng = new Random(seed);

	    	
		final SimplePlant plant = PlantGenerator.generatePlant(
    	        2, // noOfInternalLevels
    	        1, //int minNoOfVerticesPerInternalLevel 
    	        3, //int maxNoOfVerticesPerInternalLevel 
    	        3, //int noOfMachines
    	        4, //int maxNoOfModesPerMachine
    	        1, //int minNominalDuration
    	        20, //int maxNominalDuration 
    	        1.0, //double minEnergyPerTimeUnit 
    	        10.0, //double maxEnergyPerTimeUnit
    	        0.1, //double minEnergyMultiplier
    	        2.0, //double maxEnergyMultiplier
    	        0.2, //double minDurationMultiplier
    	        3.0, //double maxDurationMultiplier
    	        rng);

    	final ObjectiveFunction objectiveFunction = new ObjectiveFunction() {
			@Override
			public int numObjectives() { return 2; }
			
			@Override
			public double [] value(Configuration current, Map< String,Value> controlledMetrics) {
				double [] result = plant.fitness(Configuration.update(current,controlledMetrics ));
				assert result.length == numObjectives();
				return result;
			}
		};
		
		// final int populationSize = 100;
		
		final int minEvaluations = 5000;
		final int maxEvaluations = 5000;
		
//		List< Configuration > population = new ArrayList<>();
//		for( int i=0; i<populationSize; ++i )
//			population.add( Utility.randomConfiguration(plant.getConfigurationType(), rng) );

		final OptimisationEngine optimiser = 
			createJMetalLocalOptimisationEngine(minEvaluations, maxEvaluations, objectiveFunction, rng);
		
		final int numIterations = 1;
	    run(plant, optimiser, numIterations, rng);
	    // testScalability(rng);
	    	        
		// System.out.println( plant.cpSolve() ); 
				
		///////////////////////////
		
		System.out.println( "All done." );
	}
****/
}

// End ///////////////////////////////////////////////////////////////
