package uk.ac.york.safire.optimisation.maxplusplant;

public class ConfigurationPG {
	private final int noOfInternalLevels;  
	private final int minNoOfVerticesPerInternalLevel;
	private final int maxNoOfVerticesPerInternalLevel;  
	private final int noOfMachines;
	private final int maxNoOfModesPerMachine;
	private final double minEnergyMultiplier;
	private final double maxEnergyMultiplier;
	private final double minDurationMultiplier;
	private final double maxDurationMultiplier;
	private final int minNominalDuration;
	private final int maxNominalDuration;
	private final double minEnergyPerTimeUnit;
	private final double maxEnergyPerTimeUnit;
	
	ConfigurationPG(
			int noOfInternalLevels,
			int minNoOfVerticesPerInternalLevel, 
			int maxNoOfVerticesPerInternalLevel, 
			int noOfMachines,
			int maxNoOfModesPerMachine,
			int minNominalDuration,
			int maxNominalDuration, 
			double minEnergyPerTimeUnit, 
			double maxEnergyPerTimeUnit,
			double minEnergyMultiplier,
			double maxEnergyMultiplier,
			double minDurationMultiplier,
			double maxDurationMultiplier
			){
		this.noOfInternalLevels=noOfInternalLevels;  
		this.minNoOfVerticesPerInternalLevel=minNoOfVerticesPerInternalLevel;
		this.maxNoOfVerticesPerInternalLevel=maxNoOfVerticesPerInternalLevel;  
		this.noOfMachines=noOfMachines;
		this.maxNoOfModesPerMachine=maxNoOfModesPerMachine;
		this.minEnergyMultiplier=minEnergyMultiplier;
		this.maxEnergyMultiplier=maxEnergyMultiplier;
		this.minDurationMultiplier=minDurationMultiplier;
		this.maxDurationMultiplier=maxDurationMultiplier;
		this.minNominalDuration=minNominalDuration;
		this.maxNominalDuration=maxNominalDuration;
		this.minEnergyPerTimeUnit=minEnergyPerTimeUnit;
		this.maxEnergyPerTimeUnit=maxEnergyPerTimeUnit;
		}
			
	public final double getMinEnergyMultiplier() {
		return minEnergyMultiplier;
	}
	
	
	public final double getMinDurationMultiplier() {
		return minDurationMultiplier;
	}
	
	
	public final double getMaxDurationMultiplier() {
		return maxDurationMultiplier;
	}
	

	public final double getMaxEnergyMultiplier() {
		return maxEnergyMultiplier;
	}


	public final int getMaxNoOfModesPerMachine() {
		return maxNoOfModesPerMachine;
	}


	public final int getNoOfMachines() {
		return noOfMachines;
	}


	public final int getMinNominalDuration() {
		return minNominalDuration;
	}


	public final int getMaxNominalDuration() {
		return maxNominalDuration;
	}


	public final double getMinEnergyPerTimeUnit() {
		return minEnergyPerTimeUnit;
	}


	public final double getMaxEnergyPerTimeUnit() {
		return maxEnergyPerTimeUnit;
	}

	public final int getNoOfInternalLevels() {
		return noOfInternalLevels;
	}


	public final int getMinNoOfVerticesPerInternalLevel() {
		return minNoOfVerticesPerInternalLevel;
	}


	public final int getMaxNoOfVerticesPerInternalLevel() {
		return maxNoOfVerticesPerInternalLevel;
	}

}
