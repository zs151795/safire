package uk.ac.york.safire.optimisation;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.google.common.primitives.Doubles;

import io.vavr.control.Either;
import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.JsonConverter;
import uk.ac.york.safire.metrics.Value;

///////////////////////////////////

public abstract class ObjectiveFunction {

	public static final class Result {

		public final String objectiveFunctionStatusReport;
		public final Map<String, Value> keyObjectiveMetrics;

		///////////////////////////

		public Result(Map<String, Value> keyObjectiveMetrics, String objectiveFunctionStatusReport) {
			this.keyObjectiveMetrics = Collections.unmodifiableMap(keyObjectiveMetrics);
			this.objectiveFunctionStatusReport = objectiveFunctionStatusReport;
		}

		///////////////////////////

		@Override
		public boolean equals(Object rhs) {
			return org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals(this, rhs);
		}

		@Override
		public int hashCode() {
			return org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode(this);
		}

		@Override
		public String toString() {
			return org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString(this,
					ToStringStyle.MULTI_LINE_STYLE);
		}
	}

	///////////////////////////////

	public abstract int numObjectives();

	// public abstract Result predictKeyObjectives(Configuration current, Map<
	// String, Value > proposedControlMetrics);
	// public abstract List<Double> objectiveValues(Result result);
	// ensure: result.size() == numObjectives()

	// public abstract Triple< Configuration, List<Double>, String >
	// public abstract Pair< Configuration, List<Double> >
	// public abstract Future< ObjectiveFunctionResult >
	public abstract Callable<ObjectiveFunctionResult> evaluate(ObjectiveFunctionArguments args);
	// public abstract List<Double> value(Configuration current, Map< String, Value
	// > proposedControlMetrics);

	// public List<Double> value(Configuration current, Map< String, Value >
	// proposedControlMetrics) {
	// return objectiveValues(predictKeyObjectives(current,proposedControlMetrics));
	// }

	public static ObjectiveFunctionResult evaluateNowHelper(ObjectiveFunction of, ObjectiveFunctionArguments args) {

		final ExecutorService executor = Executors.newSingleThreadExecutor();

		try {
			final Future<ObjectiveFunctionResult> future = executor.submit(of.evaluate(args));
			return future.get();
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e.getCause()));
		}
	}

	///////////////////////////////

	public static abstract class LocalObjectiveFunction extends ObjectiveFunction {

		public abstract Result predictKeyObjectives(Configuration current, Map<String, Value> proposedControlMetrics);

		public abstract List<Double> objectiveValues(Result result);
		// ensure: result.size() == numObjectives()

		@Override
		public // Pair< Configuration, List<Double> >
		Callable<ObjectiveFunctionResult> evaluate(ObjectiveFunctionArguments args) {
			jeep.lang.Diag.println(getClass().getName());
			System.exit(0);

			return new Callable<ObjectiveFunctionResult>() {
				@Override
				public ObjectiveFunctionResult call() {
					jeep.lang.Diag.println(LocalObjectiveFunction.this.getClass().getName());
					System.exit(0);
					final Result r = predictKeyObjectives(args.getConfiguration(), args.getProposedControlMetrics());
					final Configuration newConfig = Configuration.update(args.getConfiguration(),
							args.getProposedControlMetrics(), r.keyObjectiveMetrics);
					return new ObjectiveFunctionResult(newConfig, objectiveValues(r));
				}
			};
			// return Pair.create(newConfig, objectiveValues(r) ); //
			// ,r.objectiveFunctionStatusReport);
		}
	}

	///////////////////////////////

	public static final class RemoteHttpJsonObjectiveFunction extends ObjectiveFunction {

		private JsonConverter jsonConverter = new JsonConverter();
		private URL url;
		private final ObjectiveFunction.LocalObjectiveFunction impl;

		///////////////////////////

		private static Either<String, String> httpRequest(URL url, String body) throws IOException {

			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(url.toString());
			post.setHeader("Accept", "application/json");
			post.setHeader("Content-type", "application/json");
			HttpEntity requestEntity = new StringEntity(body, ContentType.APPLICATION_JSON);
			post.setEntity(requestEntity);
			HttpResponse response = client.execute(post);
			HttpEntity responseEntity = response.getEntity();
			String responseStr = responseEntity == null ? response.getStatusLine().toString()
					: EntityUtils.toString(responseEntity);

			final int status = response.getStatusLine().getStatusCode();
			if (status >= 200 && status < 300) {
				return Either.right(responseStr);
			} else {
				return Either.left(responseStr);
			}
		}

		///////////////////////////

		public RemoteHttpJsonObjectiveFunction(URL url, ObjectiveFunction.LocalObjectiveFunction impl) {
			this.url = url;
			this.impl = impl;
		}

		///////////////////////////

		public int numObjectives() {
			return impl.numObjectives();
		};

		@Override
		public // Pair< Configuration, List<Double> >
		Callable<ObjectiveFunctionResult> evaluate(ObjectiveFunctionArguments args) {
			return new Callable<ObjectiveFunctionResult>() {
				@Override
				public ObjectiveFunctionResult call() {
					// if( !Configuration.equivalentConfigurations( pop ) ) throw new
					// IllegalArgumentException();

					// final long serialisationStartTime = System.currentTimeMillis();
					String json = jsonConverter.toJson(args);
					// final long serialisationEndTime = System.currentTimeMillis();
					// jeep.lang.Diag.println( "JSON serialisation time (millis):" +
					// ((serialisationEndTime - serialisationStartTime)) );

					try {
						Either<String, String> response = httpRequest(new URL(url.toString() + "/apply/"), json);
						if (response.isLeft()) {
							// TODO: proper error handling
							jeep.lang.Diag.println("Objective function response: <<" + response + ">>");
							return new ObjectiveFunctionResult(args.getConfiguration(),
									Doubles.asList(new double[numObjectives()]));
						} else {
							String responseStr = response.get();
							// jeep.lang.Diag.println( responseStr );
							final long startTime = System.currentTimeMillis();
							// double [] result = jsonConverter.fromJson(responseStr, double [].class );
							final ObjectiveFunctionResult result = jsonConverter.fromJson(responseStr,
									ObjectiveFunctionResult.class);
							final long endTime = System.currentTimeMillis();
							// jeep.lang.Diag.println( "JSON deserialisation time (millis):" + ((endTime -
							// startTime)) );
							// jeep.lang.Diag.println( result );
							// jeep.lang.Diag.println( result.getControlledMetrics().equals(
							// currentConfig.getControlledMetrics() ) );
							// return new ObjectiveFunctionResult( args.getConfiguration(), Doubles.asList(
							// result ) );
							return result;
						}
					} catch (IOException ioe) {
						// TODO: proper error handling
						jeep.lang.Diag.println("Error: <<" + ioe.getMessage() + ">>");
						// Do nothing, i.e. just return the existing controlled metrics
						// return new OptimisationResult( config.getControlledMetrics() );
						// return new ObjectiveFunctionResult( args.getConfiguration(), new
						// OptimisationResult( args.getConfigurations() ) );
						final double[] dummyObjectiveValues = new double[numObjectives()];
						Arrays.fill(dummyObjectiveValues, Double.NaN);
						return new ObjectiveFunctionResult(args.getConfiguration(),
								Doubles.asList(dummyObjectiveValues));
					}
				}
			};
		}
	}
}

// End ///////////////////////////////////////////////////////////////
