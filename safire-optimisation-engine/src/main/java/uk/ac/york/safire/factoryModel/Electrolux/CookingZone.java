package uk.ac.york.safire.factoryModel.Electrolux;

import uk.ac.york.safire.factoryModel.Electrolux.ElectroluxFactoryModel.DeviceType;

public class CookingZone extends Device{

	int energyLevel;

	public CookingZone(int id, DeviceType type, int availability, String notavailbaileTime, int level) {
		super(id, type, availability, notavailbaileTime);
		this.energyLevel = level;
	}


	public int getEnergyLevel() {
		return energyLevel;
	}

	public void setEnergyLevel(int energyLevel) {
		this.energyLevel = energyLevel;
	}

}
