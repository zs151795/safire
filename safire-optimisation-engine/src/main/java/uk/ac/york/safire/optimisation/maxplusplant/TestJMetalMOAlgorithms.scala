package uk.ac.york.safire.optimisation.maxplusplant

///////////////////////////////////

import uk.ac.york.safire.optimisation._

import org.uma.jmetal.algorithm._
import org.uma.jmetal.algorithm.impl._
import org.uma.jmetal.algorithm.multiobjective._
import org.uma.jmetal.algorithm.multiobjective.mocell._
import org.uma.jmetal.algorithm.multiobjective.nsgaii._
import org.uma.jmetal.algorithm.multiobjective.nsgaiii._
import org.uma.jmetal.algorithm.multiobjective.pesa2._
import org.uma.jmetal.algorithm.multiobjective.spea2._
import org.uma.jmetal.algorithm.multiobjective.smsemoa._
import org.uma.jmetal.operator._
import org.uma.jmetal.operator.impl.crossover._
import org.uma.jmetal.operator.impl.mutation._
import org.uma.jmetal.operator.impl.selection._
import org.uma.jmetal.problem._
import org.uma.jmetal.solution._
import org.uma.jmetal.solution.impl._
import org.uma.jmetal.util._
import org.uma.jmetal.util.evaluator._
import org.uma.jmetal.util.evaluator.impl._
import uk.ac.york.safire.metrics._
import scala.collection.JavaConversions._
import scala.util.Random.javaRandomToRandom
import uk.ac.york.safire.optimisation.JMetalLocalOptimisationEngine
import uk.ac.york.safire.optimisation.ObjectiveFunction

import jeep.math.ClosedOpenInterval

///////////////////////////////////

object TestJMetalLocalOptimisationEngine {

  def run(
    algorithm: AbstractGeneticAlgorithm[ConfigurationSolution, java.util.List[ConfigurationSolution]],
    parentConfig: Configuration,
    objectiveFunction: ObjectiveFunction,
    rng: java.util.Random
  ): (Double, List[ConfigurationSolution]) = {

    // val problem = JMetalLocalOptimisationEngine.createProblem(parentConfig, objectiveFunction, rng)
    val startTime = System.currentTimeMillis()
    new AlgorithmRunner.Executor(algorithm).execute()
    val endTime = System.currentTimeMillis()

    val solutions = algorithm.getResult()
    // val solution = algorithm.getResult().get(0) // FIXME 

    import scala.collection.JavaConversions._
    ((endTime - startTime) / 1000.0, solutions.toList)
  }

  /////////////////////////////////

  def generateScaledPlant(i: Int, numMachines: Int, rng: java.util.Random): SimplePlant = {
    RunPlant.generatePlant(
      3 * (i + 1), // noOfInternalLevels
      new ClosedOpenInterval(1, 3 + i), // verticesPerInternalLevel
      numMachines, // 3 + i, // noOfMachines
      4 + i, // maxNoOfModesPerMachine
      new ClosedOpenInterval(1, 20 + i), // nominalDuration
      new ClosedOpenInterval(1.0, 10.0 + i), // energyPerTimeUnit
      new ClosedOpenInterval(0.1, 2.0 + i), // energyMultiplier	    		
      new ClosedOpenInterval(0.2, 3.0 + i), // durationMultiplier			
      rng
    )
  }

  /////////////////////////////////
  /**
   * *****************
   * def main(args: Array[String]): Unit = {
   *
   * jeep.lang.Diag.println(new java.util.Date());
   *
   * val seed = 0xDEADBEEF
   * val populationSize = 100
   * val maxEvaluations = 5000
   * val numMachines = 10
   * val plantScaleFactor = 10
   *
   * val numRuns = 1
   *
   * val rng = new java.util.Random(seed)
   * // final int numIterations = 1;
   *
   * ///////////////////////////
   *
   * case class MeanAndSD(mean: Double, sd: Double) {
   * override def toString(): String = s"$mean +/- $sd"
   * }
   *
   * object MeanAndSD {
   * def apply(x: Seq[Double]): MeanAndSD =
   * MeanAndSD(
   * org.apache.commons.math3.stat.StatUtils.mean(x.toArray),
   * math.sqrt(org.apache.commons.math3.stat.StatUtils.variance(x.toArray))
   * )
   * }
   *
   * val startTime = System.currentTimeMillis()
   *
   * val problemSeeds = (0 until numRuns).map { _ => rng.nextLong() }
   * val solutionSeeds = (0 until numRuns).map { _ => rng.nextLong() }
   *
   * case class Result(algType: AlgType, elapsed: Double, objectiveValues: List[Array[Double]])
   *
   * val runResults = for (i <- 0 until numRuns) yield {
   *
   * val problemRNG = new java.util.Random(problemSeeds(i))
   * val solutionRNG = new java.util.Random(solutionSeeds(i))
   *
   * val plant = generateScaledPlant(plantScaleFactor, numMachines, problemRNG)
   * //      val plant = PlantGenerator.generatePlant(
   * //        2, // noOfInternalLevels
   * //        1, //int minNoOfVerticesPerInternalLevel
   * //        3, //int maxNoOfVerticesPerInternalLevel
   * //        3, //int noOfMachines
   * //        4, //int maxNoOfModesPerMachine
   * //        1, //int minNominalDuration
   * //        20, //int maxNominalDuration
   * //        1.0, //double minEnergyPerTimeUnit
   * //        10.0, //double maxEnergyPerTimeUnit
   * //        0.1, //double minEnergyMultiplier
   * //        2.0, //double maxEnergyMultiplier
   * //        0.2, //double minDurationMultiplier
   * //        3.0, //double maxDurationMultiplier
   * //        rng
   * //      );
   *
   * jeep.lang.Diag.println("#machines: " + numMachines + ", #V: " + plant.getGraph().getNoOfVertices() + ", #E: " + plant.getGraph().getNoOfEdges())
   *
   * val objectiveFunction = new MockObjectiveFunction(plant)
   * val parentConfig = uk.ac.york.safire.metrics.Utility.randomConfiguration(plant.getConfigurationType(), problemRNG)
   * val initialPopulation = List.fill(populationSize) { parentConfig }
   * val problem = JMetalLocalOptimisationEngine.createProblem(parentConfig, objectiveFunction, problemRNG)
   *
   * val algorithms = JMetalLocalOptimisationEngine.createAlgorithmsGA(
   * initialPopulation,
   * problem,
   * parentConfig.getConfigurationType(),
   * maxEvaluations,
   * rng
   * )
   *
   * val result = algorithms.toList.map {
   * case (algType, alg) =>
   * val (elapsed, solutions) = run(alg, parentConfig, objectiveFunction, solutionRNG)
   * val objectiveValues = solutions.map { sol => objectiveFunction.value(parentConfig, sol.controlMetrics) }
   * Map(algType -> (elapsed, objectiveValues))
   * }
   * result
   * }
   *
   * val endTime = System.currentTimeMillis()
   *
   * ///////////////////////////////
   *
   * val accResults = scala.collection.mutable.Map.empty[AlgType, (List[Double], List[Array[Double]])]
   * for (m <- runResults.flatten; algType <- m.keySet) {
   *
   * val (elapsed, objectiveValues) = m(algType)
   * if (accResults.contains(algType)) {
   * val existing = accResults(algType)
   * accResults.put(algType, (existing._1 :+ elapsed, existing._2 ++ objectiveValues))
   * } else {
   * accResults.put(algType, (List(elapsed), objectiveValues))
   * }
   * }
   *
   * val stats = accResults.map {
   * case (algType, (elapsedTimes, objectiveValuesList)) =>
   * val elapsedStats = MeanAndSD(elapsedTimes)
   * val objectiveValuesByColumn = for (i <- 0 until objectiveValuesList.head.length) yield objectiveValuesList.map { a => a(i) }
   * (algType, elapsedStats, objectiveValuesByColumn.map { MeanAndSD(_) })
   * }
   *
   * jeep.lang.Diag.println(stats.mkString("\n"))
   *
   * ///////////////////////////////
   *
   * //		    val runResults = ( 0 until numRuns ).toList.map { _ =>  }
   * //		    val elapsedStats = MeanAndSD( runResults.map { x => x._1.toDouble } )
   * //		    val moResults = runResults.map { x => x._2 }.flatten.map { s => objectiveFunction.value(parentConfig, new OptimisationResult(s.controlMetrics)) }
   * //		    val moResultsByColumn = for( i <- 0 until moResults.head.length ) yield moResults.map { a => a(i) }
   * //		    val moStats = moResultsByColumn.map { MeanAndSD(_) }
   *
   * //    jeep.lang.Diag.println( results )
   *
   * println(s"total elapsed ${(endTime - startTime) / 1000.0}")
   *
   * ///////////////////////////////
   *
   * //		val data = new StringBuffer( "{" );
   * //		for( i <- 0 until results.size() ) {
   * //			val r = results.get(i);
   * //			data.append( "{" + r.numMachines + "," + r.numEdges + "," + r.elapsedInSeconds + "}" );
   * //			if( i < results.size() - 1 )
   * //				data.append( "," );
   * //		}
   * //		data.append( "}" );
   *
   * //		System.out.print( "elements = {" );
   * //		for( int i=0; i<results.size(); ++i ) {
   * //			final Result r = results.get(i);
   * //			if( i < results.size() - 1 )
   * //				System.out.print( "," );
   * //		}
   * //		System.out.println( "};" );
   *
   * // System.out.println( "ListPlot[MapThread[Labeled, {data, elements}]]" );
   * //		println( "ListPlot3D[" + data + ", AxesLabel -> {\"|V|\", \"|E|\", \"t (secs)\"} ]" );
   * // exact for 0-th entry:
   * // value of proposed metrics: [1161.6118603811553, 71.0]
   * println("All done.");
   * }
   * *****************
   */
}

// End ///////////////////////////////////////////////////////////////
