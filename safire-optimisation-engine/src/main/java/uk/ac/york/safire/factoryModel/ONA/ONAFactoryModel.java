package uk.ac.york.safire.factoryModel.ONA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.math3.util.Pair;

import uk.ac.york.safire.optimisation.mitm.atb.SequenceDependentTaskInfo;

public class ONAFactoryModel {

	/********************************************************************************************************************/

	public static boolean runWith14 = true;

	public static Random ran = new Random();

	public static enum DeviceType {
		Small, Medium, Large
	}

	public static enum AllenOperator {
		M, UnDefined/* , LT, S, F, EQ, O, D */
	}

	public static int[] NUMBER_OF_DEVICES = { 4, 4, 4 };

	public static String[] PRODUCTIONS = null;

	public static int numberOfCuts = 3;

	public static int setupProcessingTime = 10;
	public static int setupEnergy = 10;
	public static int setupMonetary = 1000;

	public static String[] objectives = { "makespan", "energy", "montary"/* , "urgency" */ };

	public static String getDefaultNotAvailableTime() {
		return "";
	}

	public static void main(String args[]) {
		ONAFactoryModel ona = new ONAFactoryModel();
		ona.getONAConfiguration();
		//System.out.println("done");
	}

	/********************************************************************************************************************/

	public List<List<Device>> devices;
	public List<ProductionProcess> processes;
	public List<SubProcessRelation> relations;
	public List<SequenceDependentTaskInfo> setups;

	public void getONAConfiguration() {
		getONAConfiguration(14);
	}

	public void getONAConfiguration(int numberOfParts) {
		int NoP = numberOfParts;

		PRODUCTIONS = new String[NoP];
		for (int i = 0; i < NoP; i++) {
			PRODUCTIONS[i] = "P" + (i + 1);
		}

		/**
		 * Devices
		 */
		devices = new ArrayList<List<Device>>();

		for (int deviceType = 0; deviceType < DeviceType.values().length; deviceType++) {
			List<Device> devicesForOneType = new ArrayList<Device>();

			for (int i = 0; i < NUMBER_OF_DEVICES[deviceType]; i++) {
				int id = i + 1;

				devicesForOneType.add(new Device(id, DeviceType.values()[deviceType], 1, getDefaultNotAvailableTime()));
			}

			devices.add(devicesForOneType);

		}

		//System.out.println("----------------------------- Devices -----------------------------");
		for (List<Device> oneType : devices) {
			for (Device d : oneType) {
				//System.out.println(d.toString());
			}
			//System.out.println();
		}
		//System.out.println("--------------------------------------------------------------------");

		/**
		 * Production Processes
		 */
		processes = new ArrayList<>();

		Map<Pair<String, String>, Integer> processingTimes = generateProcessingTimes();
		Map<Pair<String, String>, Double> montarys = generateMontarys();

		for (int productionNumber = 0; productionNumber < PRODUCTIONS.length; productionNumber++) {
			String productionProcessName = PRODUCTIONS[productionNumber];

			List<Device> compitables = generateCompitables(productionProcessName);
			List<Integer> processingTime = new ArrayList<>();
			List<Integer> montary = new ArrayList<>();
			List<Integer> energy = new ArrayList<>();

			for (Device d : compitables) {
				Pair<String, String> key = Pair.create(productionProcessName, d.getName());
				int processTimeGet = processingTimes.get(key);
				int energyGet = 0;
				int montaryGet = (int) montarys.get(key).doubleValue();

				switch (d.getName().split(" ")[0]) {
				case "Small":
					energyGet = 60;
					break;
				case "Medium":
					energyGet = 90;
					break;
				case "Large":

					energyGet = 120;
					break;

				default:
					break;
				}

				processingTime.add(processTimeGet);
				montary.add(montaryGet);
				energy.add(energyGet);
			}

			ProductionProcess process = new ProductionProcess(productionProcessName, compitables, processingTime,
					energy, montary, numberOfCuts, productionNumber + 1, 1);
			processes.add(process);
		}

		//System.out.println("\n----------------------------- Production Processes-----------------------------");
		for (ProductionProcess process : processes) {
			//System.out.println(process.toString());
		}
		//System.out.println("----------------------------------------------------------------------------------");

		/**
		 * Subprocess Relations
		 */

		relations = new ArrayList<SubProcessRelation>();

		for (ProductionProcess production : processes) {

			for (int i = 0; i < production.getSubProcesses().size() - 1; i++) {
				SubProcess source = production.getSubProcesses().get(i);
				SubProcess target = production.getSubProcesses().get(i + 1);

				SubProcessRelation relation = new SubProcessRelation(source, target, AllenOperator.M);
				relations.add(relation);
			}
		}

		//System.out.println("\n----------------------------- Subprocess Relation -----------------------------");
		for (SubProcessRelation relation : relations) {
			//System.out.println(relation.toString());
		}
		//System.out.println("----------------------------------------------------------------------------------");

		/**
		 * Dependent Setup
		 */

		Map<String, List<Device>> resourcesUsedBySubProcesses = new HashMap<>();
		for (int i = 0; i < processes.size(); i++) {
			ProductionProcess process = processes.get(i);
			resourcesUsedBySubProcesses.put(process.getName(), process.compitableResource);
		}

		final List<Triple<String, String, Set<String>>> sharedResources = new ArrayList<>();

		for (int i = 0; i < processes.size(); i++) {
			for (int j = 0; j < processes.size(); j++) {

				String source = processes.get(i).getName();
				String target = processes.get(j).getName();

				if (j == i)
					continue;

				final List<String> sourcecompatNestedList = resourcesUsedBySubProcesses.get(source).stream()
						.map(c -> c.getName()).collect(Collectors.toList());
				final Set<String> sourcecompat = sourcecompatNestedList.stream().collect(Collectors.toSet());

				final List<String> targetcompatNestedList = resourcesUsedBySubProcesses.get(target).stream()
						.map(c -> c.getName()).collect(Collectors.toList());
				final Set<String> targetcompat = targetcompatNestedList.stream().collect(Collectors.toSet());

				sourcecompat.retainAll(targetcompat);

				if (!sourcecompat.isEmpty())
					sharedResources.add(Triple.of(source, target, sourcecompat));
			}
		}

		setups = new ArrayList<SequenceDependentTaskInfo>();
		for (int i = 0; i < sharedResources.size(); ++i) {
			final String source = sharedResources.get(i).getLeft();
			final String target = sharedResources.get(i).getMiddle();
			final Set<String> compatibleResources = sharedResources.get(i).getRight();

			for (String resource : compatibleResources) {
				final String taskId = "DependentSetUp from " + source + " to " + target;
				setups.add(new SequenceDependentTaskInfo(setupProcessingTime, setupEnergy, setupMonetary, resource,
						source, target, taskId));
			}
		}

		//System.out.println("\n----------------------------- Dependent SetUp -----------------------------");
		for (SequenceDependentTaskInfo setUp : setups) {

			//System.out.println(setUp.getFullInfo());
		}
		//System.out.println("----------------------------------------------------------------------------------");

	}

	private List<Device> generateCompitables(String productionName) {

		List<Device> comptiables = new ArrayList<>();

		switch (productionName) {
		case "P1":
			comptiables.addAll(devices.get(0));
			comptiables.addAll(devices.get(1));
			comptiables.addAll(devices.get(2));
			break;
		case "P2":
			comptiables.addAll(devices.get(0));
			comptiables.addAll(devices.get(1));
			comptiables.addAll(devices.get(2));
			break;
		case "P3":
			comptiables.addAll(devices.get(1));
			comptiables.addAll(devices.get(2));
			break;
		case "P4":
			comptiables.addAll(devices.get(1));
			comptiables.addAll(devices.get(2));
			break;
		case "P5":
			comptiables.addAll(devices.get(1));
			comptiables.addAll(devices.get(2));
			break;
		case "P6":
			comptiables.addAll(devices.get(1));
			comptiables.addAll(devices.get(2));
			break;
		case "P7":
			comptiables.addAll(devices.get(2));
			break;
		case "P8":
			comptiables.addAll(devices.get(2));
			break;
		case "P9":
			comptiables.addAll(devices.get(2));
			break;
		case "P10":
			comptiables.addAll(devices.get(2));
			break;
		case "P11":
			comptiables.addAll(devices.get(2));
			break;
		case "P12":
			comptiables.addAll(devices.get(2));
			break;
		case "P13":
			comptiables.addAll(devices.get(2));
			break;
		case "P14":
			comptiables.addAll(devices.get(2));
			break;
		case "P15":
			comptiables.addAll(devices.get(2));
			break;
		case "P16":
			comptiables.addAll(devices.get(2));
			break;
		case "P17":
			comptiables.addAll(devices.get(0));
			comptiables.addAll(devices.get(1));
			comptiables.addAll(devices.get(2));
			break;
		case "P18":
			comptiables.addAll(devices.get(0));
			comptiables.addAll(devices.get(1));
			comptiables.addAll(devices.get(2));
			break;
		case "P19":
			comptiables.addAll(devices.get(1));
			comptiables.addAll(devices.get(2));
			break;
		case "P20":
			comptiables.addAll(devices.get(1));
			comptiables.addAll(devices.get(2));
			break;
		default:
			break;
		}

		return comptiables;
	}

	private Map<Pair<String, String>, Double> generateMontarys() {
		Map<Pair<String, String>, Double> recipeAndResourceNameToCost = new HashMap<>();

		recipeAndResourceNameToCost.put(Pair.create("P1", "Small 2"), 192.1);
		recipeAndResourceNameToCost.put(Pair.create("P1", "Small 1"), 168.4);
		recipeAndResourceNameToCost.put(Pair.create("P1", "Small 4"), 175.9);
		recipeAndResourceNameToCost.put(Pair.create("P1", "Small 3"), 167.0);

		recipeAndResourceNameToCost.put(Pair.create("P1", "Medium 2"), 273.1);
		recipeAndResourceNameToCost.put(Pair.create("P1", "Medium 1"), 238.6);
		recipeAndResourceNameToCost.put(Pair.create("P1", "Medium 4"), 237.1);
		recipeAndResourceNameToCost.put(Pair.create("P1", "Medium 3"), 230.0);

		recipeAndResourceNameToCost.put(Pair.create("P1", "Large 2"), 596.9);
		recipeAndResourceNameToCost.put(Pair.create("P1", "Large 1"), 519.3);
		recipeAndResourceNameToCost.put(Pair.create("P1", "Large 4"), 481.6);
		recipeAndResourceNameToCost.put(Pair.create("P1", "Large 3"), 462.1);

		recipeAndResourceNameToCost.put(Pair.create("P2", "Small 2"), 305.5);
		recipeAndResourceNameToCost.put(Pair.create("P2", "Small 1"), 265.6);
		recipeAndResourceNameToCost.put(Pair.create("P2", "Small 4"), 283.1);
		recipeAndResourceNameToCost.put(Pair.create("P2", "Small 3"), 262.2);

		recipeAndResourceNameToCost.put(Pair.create("P2", "Medium 2"), 434.2);
		recipeAndResourceNameToCost.put(Pair.create("P2", "Medium 1"), 376.3);
		recipeAndResourceNameToCost.put(Pair.create("P2", "Medium 4"), 381.5);
		recipeAndResourceNameToCost.put(Pair.create("P2", "Medium 3"), 361.1);

		recipeAndResourceNameToCost.put(Pair.create("P2", "Large 2"), 949.1);
		recipeAndResourceNameToCost.put(Pair.create("P2", "Large 1"), 819.0);
		recipeAndResourceNameToCost.put(Pair.create("P2", "Large 4"), 775.0);
		recipeAndResourceNameToCost.put(Pair.create("P2", "Large 3"), 756.9);

		recipeAndResourceNameToCost.put(Pair.create("P3", "Medium 2"), 1096.3);
		recipeAndResourceNameToCost.put(Pair.create("P3", "Medium 1"), 903.8);
		recipeAndResourceNameToCost.put(Pair.create("P3", "Medium 4"), 902.8);
		recipeAndResourceNameToCost.put(Pair.create("P3", "Medium 3"), 867.0);

		recipeAndResourceNameToCost.put(Pair.create("P3", "Large 2"), 2396.2);
		recipeAndResourceNameToCost.put(Pair.create("P3", "Large 1"), 1967.2);
		recipeAndResourceNameToCost.put(Pair.create("P3", "Large 4"), 1833.9);
		recipeAndResourceNameToCost.put(Pair.create("P3", "Large 3"), 1817.1);

		recipeAndResourceNameToCost.put(Pair.create("P4", "Medium 2"), 1224.4);
		recipeAndResourceNameToCost.put(Pair.create("P4", "Medium 1"), 1004.3);
		recipeAndResourceNameToCost.put(Pair.create("P4", "Medium 4"), 915.4);
		recipeAndResourceNameToCost.put(Pair.create("P4", "Medium 3"), 856.4);

		recipeAndResourceNameToCost.put(Pair.create("P4", "Large 2"), 2676.2);
		recipeAndResourceNameToCost.put(Pair.create("P4", "Large 1"), 2185.7);
		recipeAndResourceNameToCost.put(Pair.create("P4", "Large 4"), 2062.6);
		recipeAndResourceNameToCost.put(Pair.create("P4", "Large 3"), 1904.6);

		recipeAndResourceNameToCost.put(Pair.create("P5", "Medium 2"), 1737.7);
		recipeAndResourceNameToCost.put(Pair.create("P5", "Medium 1"), 1599.2);
		recipeAndResourceNameToCost.put(Pair.create("P5", "Medium 4"), 1370.1);
		recipeAndResourceNameToCost.put(Pair.create("P5", "Medium 3"), 1244.3);

		recipeAndResourceNameToCost.put(Pair.create("P5", "Large 2"), 3798.3);
		recipeAndResourceNameToCost.put(Pair.create("P5", "Large 1"), 3045.3);
		recipeAndResourceNameToCost.put(Pair.create("P5", "Large 4"), 2783.1);
		recipeAndResourceNameToCost.put(Pair.create("P5", "Large 3"), 2617.6);

		recipeAndResourceNameToCost.put(Pair.create("P6", "Medium 2"), 2225.9);
		recipeAndResourceNameToCost.put(Pair.create("P6", "Medium 1"), 1770.8);
		recipeAndResourceNameToCost.put(Pair.create("P6", "Medium 4"), 1587.2);
		recipeAndResourceNameToCost.put(Pair.create("P6", "Medium 3"), 1303.0);

		recipeAndResourceNameToCost.put(Pair.create("P6", "Large 2"), 4865.4);
		recipeAndResourceNameToCost.put(Pair.create("P6", "Large 1"), 3854.2);
		recipeAndResourceNameToCost.put(Pair.create("P6", "Large 4"), 3453.0);
		recipeAndResourceNameToCost.put(Pair.create("P6", "Large 3"), 3269.4);

		recipeAndResourceNameToCost.put(Pair.create("P7", "Large 2"), 4560.0);
		recipeAndResourceNameToCost.put(Pair.create("P7", "Large 1"), 3826.5);
		recipeAndResourceNameToCost.put(Pair.create("P7", "Large 4"), 3614.3);
		recipeAndResourceNameToCost.put(Pair.create("P7", "Large 3"), 3532.6);

		recipeAndResourceNameToCost.put(Pair.create("P8", "Large 2"), 5175.0);
		recipeAndResourceNameToCost.put(Pair.create("P8", "Large 1"), 4325.6);
		recipeAndResourceNameToCost.put(Pair.create("P8", "Large 4"), 4274.2);
		recipeAndResourceNameToCost.put(Pair.create("P8", "Large 3"), 3982.4);

		recipeAndResourceNameToCost.put(Pair.create("P9", "Large 2"), 10027.4);
		recipeAndResourceNameToCost.put(Pair.create("P9", "Large 1"), 8039.5);
		recipeAndResourceNameToCost.put(Pair.create("P9", "Large 4"), 7347.4);
		recipeAndResourceNameToCost.put(Pair.create("P9", "Large 3"), 7138.4);

		recipeAndResourceNameToCost.put(Pair.create("P10", "Large 2"), 12844.6);
		recipeAndResourceNameToCost.put(Pair.create("P10", "Large 1"), 10175.0);
		recipeAndResourceNameToCost.put(Pair.create("P10", "Large 4"), 9116.0);
		recipeAndResourceNameToCost.put(Pair.create("P10", "Large 3"), 8823.1);

		recipeAndResourceNameToCost.put(Pair.create("P11", "Large 2"), 2481.0);
		recipeAndResourceNameToCost.put(Pair.create("P11", "Large 1"), 1755.7);
		recipeAndResourceNameToCost.put(Pair.create("P11", "Large 4"), 1452.9);
		recipeAndResourceNameToCost.put(Pair.create("P11", "Large 3"), 1206.2);

		recipeAndResourceNameToCost.put(Pair.create("P12", "Large 2"), 2908.8);
		recipeAndResourceNameToCost.put(Pair.create("P12", "Large 1"), 2058.4);
		recipeAndResourceNameToCost.put(Pair.create("P12", "Large 4"), 1816.1);
		recipeAndResourceNameToCost.put(Pair.create("P12", "Large 3"), 1583.1);

		recipeAndResourceNameToCost.put(Pair.create("P13", "Large 2"), 6573.1);
		recipeAndResourceNameToCost.put(Pair.create("P13", "Large 1"), 4651.4);
		recipeAndResourceNameToCost.put(Pair.create("P13", "Large 4"), 3566.2);
		recipeAndResourceNameToCost.put(Pair.create("P13", "Large 3"), 3255.4);

		recipeAndResourceNameToCost.put(Pair.create("P14", "Large 2"), 8764.1);
		recipeAndResourceNameToCost.put(Pair.create("P14", "Large 1"), 6201.9);
		recipeAndResourceNameToCost.put(Pair.create("P14", "Large 4"), 5673.8);
		recipeAndResourceNameToCost.put(Pair.create("P14", "Large 3"), 4754.9);

		recipeAndResourceNameToCost.put(Pair.create("P15", "Large 1"), 4651.4);
		recipeAndResourceNameToCost.put(Pair.create("P15", "Large 2"), 6573.1);
		recipeAndResourceNameToCost.put(Pair.create("P15", "Large 3"), 3566.2);
		recipeAndResourceNameToCost.put(Pair.create("P15", "Large 4"), 4255.4);

		recipeAndResourceNameToCost.put(Pair.create("P16", "Large 1"), 6201.9);
		recipeAndResourceNameToCost.put(Pair.create("P16", "Large 2"), 8764.1);
		recipeAndResourceNameToCost.put(Pair.create("P16", "Large 3"), 4754.9);
		recipeAndResourceNameToCost.put(Pair.create("P16", "Large 4"), 5673.8);

		recipeAndResourceNameToCost.put(Pair.create("P17", "Small 2"), 192.1);
		recipeAndResourceNameToCost.put(Pair.create("P17", "Small 1"), 168.4);
		recipeAndResourceNameToCost.put(Pair.create("P17", "Small 4"), 175.9);
		recipeAndResourceNameToCost.put(Pair.create("P17", "Small 3"), 167.0);

		recipeAndResourceNameToCost.put(Pair.create("P17", "Medium 2"), 273.1);
		recipeAndResourceNameToCost.put(Pair.create("P17", "Medium 1"), 238.6);
		recipeAndResourceNameToCost.put(Pair.create("P17", "Medium 4"), 237.1);
		recipeAndResourceNameToCost.put(Pair.create("P17", "Medium 3"), 230.0);

		recipeAndResourceNameToCost.put(Pair.create("P17", "Large 2"), 596.9);
		recipeAndResourceNameToCost.put(Pair.create("P17", "Large 1"), 519.3);
		recipeAndResourceNameToCost.put(Pair.create("P17", "Large 4"), 481.6);
		recipeAndResourceNameToCost.put(Pair.create("P17", "Large 3"), 462.1);

		recipeAndResourceNameToCost.put(Pair.create("P18", "Small 2"), 192.1);
		recipeAndResourceNameToCost.put(Pair.create("P18", "Small 1"), 168.4);
		recipeAndResourceNameToCost.put(Pair.create("P18", "Small 4"), 175.9);
		recipeAndResourceNameToCost.put(Pair.create("P18", "Small 3"), 167.0);

		recipeAndResourceNameToCost.put(Pair.create("P18", "Medium 2"), 273.1);
		recipeAndResourceNameToCost.put(Pair.create("P18", "Medium 1"), 238.6);
		recipeAndResourceNameToCost.put(Pair.create("P18", "Medium 4"), 237.1);
		recipeAndResourceNameToCost.put(Pair.create("P18", "Medium 3"), 230.0);

		recipeAndResourceNameToCost.put(Pair.create("P18", "Large 2"), 596.9);
		recipeAndResourceNameToCost.put(Pair.create("P18", "Large 1"), 519.3);
		recipeAndResourceNameToCost.put(Pair.create("P18", "Large 4"), 481.6);
		recipeAndResourceNameToCost.put(Pair.create("P18", "Large 3"), 462.1);

		recipeAndResourceNameToCost.put(Pair.create("P19", "Medium 2"), 2225.9);
		recipeAndResourceNameToCost.put(Pair.create("P19", "Medium 1"), 1770.8);
		recipeAndResourceNameToCost.put(Pair.create("P19", "Medium 4"), 1587.2);
		recipeAndResourceNameToCost.put(Pair.create("P19", "Medium 3"), 1303.0);

		recipeAndResourceNameToCost.put(Pair.create("P19", "Large 2"), 4865.4);
		recipeAndResourceNameToCost.put(Pair.create("P19", "Large 1"), 3854.2);
		recipeAndResourceNameToCost.put(Pair.create("P19", "Large 4"), 3453.0);
		recipeAndResourceNameToCost.put(Pair.create("P19", "Large 3"), 3269.4);

		recipeAndResourceNameToCost.put(Pair.create("P20", "Medium 2"), 2225.9);
		recipeAndResourceNameToCost.put(Pair.create("P20", "Medium 1"), 1770.8);
		recipeAndResourceNameToCost.put(Pair.create("P20", "Medium 4"), 1587.2);
		recipeAndResourceNameToCost.put(Pair.create("P20", "Medium 3"), 1303.0);

		recipeAndResourceNameToCost.put(Pair.create("P20", "Large 2"), 4865.4);
		recipeAndResourceNameToCost.put(Pair.create("P20", "Large 1"), 3854.2);
		recipeAndResourceNameToCost.put(Pair.create("P20", "Large 4"), 3453.0);
		recipeAndResourceNameToCost.put(Pair.create("P20", "Large 3"), 3269.4);

		return recipeAndResourceNameToCost;
	}

	private Map<Pair<String, String>, Integer> generateProcessingTimes() {
		final Map<Pair<String, String>, Integer> recipeAndResourceNameToCuttingTime = new HashMap<>();

		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Small 2"), (int) (2833.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Small 1"), (int) (2956.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Small 4"), (int) (3042.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Small 3"), (int) (3174.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Medium 2"), (int) (2033.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Medium 1"), (int) (2156.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Medium 4"), (int) (2242.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Medium 3"), (int) (2674.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Large 2"), (int) (1256.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Large 1"), (int) (1633.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Large 4"), (int) (1842.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Large 3"), (int) (1974.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Small 2"), (int) (2833.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Small 1"), (int) (2956.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Small 4"), (int) (3042.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Small 3"), (int) (3174.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Medium 2"), (int) (2033.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Medium 1"), (int) (2156.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Medium 4"), (int) (2242.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Medium 3"), (int) (2674.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Large 2"), (int) (1256.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Large 1"), (int) (1633.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Large 4"), (int) (1842.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Large 3"), (int) (1974.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Medium 2"), (int) (2899.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Medium 1"), (int) (2990.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Medium 4"), (int) (3093.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Medium 3"), (int) (3250.4));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Large 2"), (int) (2099.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Large 1"), (int) (2290.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Large 4"), (int) (2493.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Large 3"), (int) (2650.4));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Medium 2"), (int) (2899.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Medium 1"), (int) (2990.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Medium 4"), (int) (3093.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Medium 3"), (int) (3250.4));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Large 2"), (int) (2099.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Large 1"), (int) (2290.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Large 4"), (int) (2493.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Large 3"), (int) (2650.4));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Medium 2"), (int) (2899.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Medium 1"), (int) (2990.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Medium 4"), (int) (3093.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Medium 3"), (int) (3250.4));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Large 2"), (int) (2099.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Large 1"), (int) (2290.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Large 4"), (int) (2493.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Large 3"), (int) (2650.4));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Medium 2"), (int) (2899.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Medium 1"), (int) (2990.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Medium 4"), (int) (3093.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Medium 3"), (int) (3250.4));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Large 2"), (int) (2099.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Large 1"), (int) (2290.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Large 4"), (int) (2493.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Large 3"), (int) (2650.4));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P7", "Large 2"), (int) (5341.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P7", "Large 1"), (int) (5505.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P7", "Large 4"), (int) (6205.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P7", "Large 3"), (int) (7421.3));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P8", "Large 2"), (int) (5341.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P8", "Large 1"), (int) (5505.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P8", "Large 4"), (int) (6205.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P8", "Large 3"), (int) (7421.3));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P9", "Large 2"), (int) (5341.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P9", "Large 1"), (int) (5505.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P9", "Large 4"), (int) (6205.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P9", "Large 3"), (int) (7421.3));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P10", "Large 2"), (int) (5341.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P10", "Large 1"), (int) (5505.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P10", "Large 4"), (int) (6205.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P10", "Large 3"), (int) (7421.3));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P11", "Large 2"), (int) (5341.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P11", "Large 1"), (int) (5505.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P11", "Large 4"), (int) (6205.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P11", "Large 3"), (int) (7421.3));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P12", "Large 2"), (int) (5341.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P12", "Large 1"), (int) (5505.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P12", "Large 4"), (int) (6205.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P12", "Large 3"), (int) (7421.3));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P13", "Large 2"), (int) (5341.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P13", "Large 1"), (int) (5505.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P13", "Large 4"), (int) (6205.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P13", "Large 3"), (int) (7421.3));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P14", "Large 2"), (int) (5341.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P14", "Large 1"), (int) (5505.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P14", "Large 4"), (int) (6205.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P14", "Large 3"), (int) (7421.3));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P15", "Large 2"), (int) (5341.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P15", "Large 1"), (int) (5505.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P15", "Large 4"), (int) (6205.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P15", "Large 3"), (int) (7421.3));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P16", "Large 2"), (int) (5341.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P16", "Large 1"), (int) (5505.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P16", "Large 4"), (int) (6205.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P16", "Large 3"), (int) (7421.3));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Small 1"), (int) (2833.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Small 2"), (int) (2956.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Small 3"), (int) (3042.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Small 4"), (int) (3174.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Medium 1"), (int) (2033.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Medium 2"), (int) (2156.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Medium 3"), (int) (2242.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Medium 4"), (int) (2674.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Large 1"), (int) (1256.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Large 2"), (int) (1633.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Large 3"), (int) (1842.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P17", "Large 4"), (int) (1974.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Small 1"), (int) (2833.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Small 2"), (int) (2956.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Small 3"), (int) (3042.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Small 4"), (int) (3174.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Medium 1"), (int) (2033.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Medium 2"), (int) (2156.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Medium 3"), (int) (2242.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Medium 4"), (int) (2674.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Large 1"), (int) (1256.2));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Large 2"), (int) (1633.5));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Large 3"), (int) (1842.1));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P18", "Large 4"), (int) (1974.1));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P19", "Medium 1"), (int) (2899.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P19", "Medium 2"), (int) (2990.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P19", "Medium 3"), (int) (3093.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P19", "Medium 4"), (int) (3250.4));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P19", "Large 1"), (int) (2099.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P19", "Large 2"), (int) (2290.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P19", "Large 3"), (int) (2493.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P19", "Large 4"), (int) (2650.4));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P20", "Medium 1"), (int) (2899.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P20", "Medium 2"), (int) (2990.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P20", "Medium 3"), (int) (3093.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P20", "Medium 4"), (int) (3250.4));

		recipeAndResourceNameToCuttingTime.put(Pair.create("P20", "Large 1"), (int) (2099.7));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P20", "Large 2"), (int) (2290.0));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P20", "Large 3"), (int) (2493.3));
		recipeAndResourceNameToCuttingTime.put(Pair.create("P20", "Large 4"), (int) (2650.4));

		return recipeAndResourceNameToCuttingTime;
	}

	static boolean compareSubProcessTypes(DeviceType[] dt1, DeviceType[] dt2) {
		if (dt1.length != dt2.length)
			return false;

		for (int i = 0; i < dt1.length; i++) {
			if (dt1[i].toString() != dt2[i].toString())
				return false;
		}
		return true;

	}

	//
	// private Map<Pair<String, String>, Double> generateMontarys() {
	// Map<Pair<String, String>, Double> recipeAndResourceNameToCost = new
	// HashMap<>();
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Small 1"), 168.4);
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Medium 1"), 238.6);
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Large 1"), 519.3);
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Small 2"), 192.1);
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Medium 2"), 273.1);
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Large 2"), 596.9);
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Small 3"), 167.0);
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Medium 3"), 230.0);
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Large 3"), 482.1);
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Small 4"), 175.9);
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Medium 4"), 237.1);
	// recipeAndResourceNameToCost.put(Pair.create("P1", "Large 4"), 481.6);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Small 1"), 265.6);
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Medium 1"), 376.3);
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Large 1"), 819.0);
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Small 2"), 305.5);
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Medium 2"), 434.2);
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Large 2"), 949.1);
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Small 3"), 262.2);
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Medium 3"), 361.1);
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Large 3"), 756.9);
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Small 4"), 283.1);
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Medium 4"), 381.5);
	// recipeAndResourceNameToCost.put(Pair.create("P2", "Large 4"), 775.0);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P3", "Medium 1"), 903.8);
	// recipeAndResourceNameToCost.put(Pair.create("P3", "Large 1"), 1967.2);
	// recipeAndResourceNameToCost.put(Pair.create("P3", "Medium 2"), 1096.3);
	// recipeAndResourceNameToCost.put(Pair.create("P3", "Large 2"), 2396.2);
	// recipeAndResourceNameToCost.put(Pair.create("P3", "Medium 3"), 867.0);
	// recipeAndResourceNameToCost.put(Pair.create("P3", "Large 3"), 1817.1);
	// recipeAndResourceNameToCost.put(Pair.create("P3", "Medium 4"), 902.8);
	// recipeAndResourceNameToCost.put(Pair.create("P3", "Large 4"), 1833.9);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P4", "Medium 1"), 1004.3);
	// recipeAndResourceNameToCost.put(Pair.create("P4", "Large 1"), 2185.7);
	// recipeAndResourceNameToCost.put(Pair.create("P4", "Medium 2"), 1224.4);
	// recipeAndResourceNameToCost.put(Pair.create("P4", "Large 2"), 2676.2);
	// recipeAndResourceNameToCost.put(Pair.create("P4", "Medium 3"), 956.4);
	// recipeAndResourceNameToCost.put(Pair.create("P4", "Large 3"), 2004.6);
	// recipeAndResourceNameToCost.put(Pair.create("P4", "Medium 4"), 1015.4);
	// recipeAndResourceNameToCost.put(Pair.create("P4", "Large 4"), 2062.6);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P5", "Medium 1"), 1399.2);
	// recipeAndResourceNameToCost.put(Pair.create("P5", "Large 1"), 3045.3);
	// recipeAndResourceNameToCost.put(Pair.create("P5", "Medium 2"), 1737.7);
	// recipeAndResourceNameToCost.put(Pair.create("P5", "Large 2"), 3798.3);
	// recipeAndResourceNameToCost.put(Pair.create("P5", "Medium 3"), 1344.3);
	// recipeAndResourceNameToCost.put(Pair.create("P5", "Large 3"), 2817.6);
	// recipeAndResourceNameToCost.put(Pair.create("P5", "Medium 4"), 1370.1);
	// recipeAndResourceNameToCost.put(Pair.create("P5", "Large 4"), 2783.1);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P6", "Medium 1"), 1770.8);
	// recipeAndResourceNameToCost.put(Pair.create("P6", "Large 1"), 3854.2);
	// recipeAndResourceNameToCost.put(Pair.create("P6", "Medium 2"), 2225.9);
	// recipeAndResourceNameToCost.put(Pair.create("P6", "Large 2"), 4865.4);
	// recipeAndResourceNameToCost.put(Pair.create("P6", "Medium 3"), 1703.0);
	// recipeAndResourceNameToCost.put(Pair.create("P6", "Large 3"), 3569.4);
	// recipeAndResourceNameToCost.put(Pair.create("P6", "Medium 4"), 1687.2);
	// recipeAndResourceNameToCost.put(Pair.create("P6", "Large 4"), 3453.0);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P7", "Large 1"), 3826.5);
	// recipeAndResourceNameToCost.put(Pair.create("P7", "Large 2"), 4560.0);
	// recipeAndResourceNameToCost.put(Pair.create("P7", "Large 3"), 3532.6);
	// recipeAndResourceNameToCost.put(Pair.create("P7", "Large 4"), 3614.3);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P8", "Large 1"), 4325.6);
	// recipeAndResourceNameToCost.put(Pair.create("P8", "Large 2"), 5175.0);
	// recipeAndResourceNameToCost.put(Pair.create("P8", "Large 3"), 3982.4);
	// recipeAndResourceNameToCost.put(Pair.create("P8", "Large 4"), 4274.2);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P9", "Large 1"), 8039.5);
	// recipeAndResourceNameToCost.put(Pair.create("P9", "Large 2"), 10027.4);
	// recipeAndResourceNameToCost.put(Pair.create("P9", "Large 3"), 7438.4);
	// recipeAndResourceNameToCost.put(Pair.create("P9", "Large 4"), 7347.4);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P10", "Large 1"), 10175.0);
	// recipeAndResourceNameToCost.put(Pair.create("P10", "Large 2"), 12844.6);
	// recipeAndResourceNameToCost.put(Pair.create("P10", "Large 3"), 9423.1);
	// recipeAndResourceNameToCost.put(Pair.create("P10", "Large 4"), 9116.0);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P11", "Large 1"), 1755.7);
	// recipeAndResourceNameToCost.put(Pair.create("P11", "Large 2"), 2481.0);
	// recipeAndResourceNameToCost.put(Pair.create("P11", "Large 3"), 1606.2);
	// recipeAndResourceNameToCost.put(Pair.create("P11", "Large 4"), 1452.9);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P12", "Large 1"), 2058.4);
	// recipeAndResourceNameToCost.put(Pair.create("P12", "Large 2"), 2908.8);
	// recipeAndResourceNameToCost.put(Pair.create("P12", "Large 3"), 1883.1);
	// recipeAndResourceNameToCost.put(Pair.create("P12", "Large 4"), 1816.1);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P13", "Large 1"), 4651.4);
	// recipeAndResourceNameToCost.put(Pair.create("P13", "Large 2"), 6573.1);
	// recipeAndResourceNameToCost.put(Pair.create("P13", "Large 3"), 4255.4);
	// recipeAndResourceNameToCost.put(Pair.create("P13", "Large 4"), 3566.2);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P14", "Large 1"), 6201.9);
	// recipeAndResourceNameToCost.put(Pair.create("P14", "Large 2"), 8764.1);
	// recipeAndResourceNameToCost.put(Pair.create("P14", "Large 3"), 5673.8);
	// recipeAndResourceNameToCost.put(Pair.create("P14", "Large 4"), 4754.9);
	//
	// if (!runWith14) {
	// recipeAndResourceNameToCost.put(Pair.create("P15", "Large 1"), 4651.4);
	// recipeAndResourceNameToCost.put(Pair.create("P15", "Large 2"), 6573.1);
	// recipeAndResourceNameToCost.put(Pair.create("P15", "Large 3"), 4255.4);
	// recipeAndResourceNameToCost.put(Pair.create("P15", "Large 4"), 3566.2);
	//
	// recipeAndResourceNameToCost.put(Pair.create("P16", "Large 1"), 6201.9);
	// recipeAndResourceNameToCost.put(Pair.create("P16", "Large 2"), 8764.1);
	// recipeAndResourceNameToCost.put(Pair.create("P16", "Large 3"), 5673.8);
	// recipeAndResourceNameToCost.put(Pair.create("P16", "Large 4"), 4754.9);
	// }
	//
	// return recipeAndResourceNameToCost;
	// }
	//
	// private Map<Pair<String, String>, Integer> generateProcessingTimes() {
	// final Map<Pair<String, String>, Integer> recipeAndResourceNameToCuttingTime =
	// new HashMap<>();
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Small 1"), (int)
	// (842.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Medium 1"), (int)
	// (842.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Large 1"), (int)
	// (842.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Small 2"), (int)
	// (974.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Medium 2"), (int)
	// (974.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Large 2"), (int)
	// (974.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Small 3"), (int)
	// (756.2));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Medium 3"), (int)
	// (756.2));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Large 3"), (int)
	// (756.2));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Small 4"), (int)
	// (733.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Medium 4"), (int)
	// (733.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P1", "Large 4"), (int)
	// (733.5));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Small 1"), (int)
	// (1328.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Medium 1"), (int)
	// (1328.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Large 1"), (int)
	// (1328.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Small 2"), (int)
	// (1544.6));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Medium 2"), (int)
	// (1544.6));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Large 2"), (int)
	// (1544.6));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Small 3"), (int)
	// (1187.3));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Medium 3"), (int)
	// (1187.3));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Large 3"), (int)
	// (1187.3));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Small 4"), (int)
	// (1180.4));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Medium 4"), (int)
	// (1180.4));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P2", "Large 4"), (int)
	// (1180.4));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Medium 1"), (int)
	// (3190.0));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Large 1"), (int)
	// (3190.0));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Medium 2"), (int)
	// (3899.7));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Large 2"), (int)
	// (3899.7));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Medium 3"), (int)
	// (2850.4));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Large 3"), (int)
	// (2850.4));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Medium 4"), (int)
	// (2793.3));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P3", "Large 4"), (int)
	// (2793.3));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Medium 1"), (int)
	// (3544.4));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Large 1"), (int)
	// (3544.4));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Medium 2"), (int)
	// (4355.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Large 2"), (int)
	// (4355.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Medium 3"), (int)
	// (3144.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Large 3"), (int)
	// (3144.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Medium 4"), (int)
	// (3141.7));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P4", "Large 4"), (int)
	// (3141.7));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Medium 1"), (int)
	// (4938.3));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Large 1"), (int)
	// (4938.3));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Medium 2"), (int)
	// (6181.6));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Large 2"), (int)
	// (6181.6));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Medium 3"), (int)
	// (4419.7));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Large 3"), (int)
	// (4419.7));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Medium 4"), (int)
	// (4239.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P5", "Large 4"), (int)
	// (4239.1));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Medium 1"), (int)
	// (6250.0));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Large 1"), (int)
	// (6250.0));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Medium 2"), (int)
	// (7918.4));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Large 2"), (int)
	// (7918.4));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Medium 3"), (int)
	// (5599.0));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Large 3"), (int)
	// (5599.0));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Medium 4"), (int)
	// (5297.7));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P6", "Large 4"), (int)
	// (5297.7));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P7", "Large 1"), (int)
	// (6205.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P7", "Large 2"), (int)
	// (7421.3));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P7", "Large 3"), (int)
	// (5541.3));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P7", "Large 4"), (int)
	// (5505.1));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P8", "Large 1"), (int)
	// (7014.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P8", "Large 2"), (int)
	// (8422.3));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P8", "Large 3"), (int)
	// (6247.0));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P8", "Large 4"), (int)
	// (6510.3));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P9", "Large 1"), (int)
	// (13037.0));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P9", "Large 2"), (int)
	// (16319.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P9", "Large 3"), (int)
	// (11668.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P9", "Large 4"), (int)
	// (11191.3));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P10", "Large 1"), (int)
	// (16500.0));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P10", "Large 2"), (int)
	// (20904.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P10", "Large 3"), (int)
	// (14781.4));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P10", "Large 4"), (int)
	// (13985.8));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P11", "Large 1"), (int)
	// (2847.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P11", "Large 2"), (int)
	// (4067.2));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P11", "Large 3"), (int)
	// (2519.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P11", "Large 4"), (int)
	// (2151.1));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P12", "Large 1"), (int)
	// (3337.9));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P12", "Large 2"), (int)
	// (4768.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P12", "Large 3"), (int)
	// (2953.9));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P12", "Large 4"), (int)
	// (2688.9));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P13", "Large 1"), (int)
	// (7542.9));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P13", "Large 2"), (int)
	// (10775.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P13", "Large 3"), (int)
	// (6675.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P13", "Large 4"), (int)
	// (5280.0));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P14", "Large 1"), (int)
	// (10057.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P14", "Large 2"), (int)
	// (14367.3));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P14", "Large 3"), (int)
	// (8900.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P14", "Large 4"), (int)
	// (7040.0));
	//
	// if (!runWith14) {
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P15", "Large 1"), (int)
	// (7542.9));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P15", "Large 2"), (int)
	// (10775.5));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P15", "Large 3"), (int)
	// (6675.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P15", "Large 4"), (int)
	// (5280.0));
	//
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P16", "Large 1"), (int)
	// (10057.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P16", "Large 2"), (int)
	// (14367.3));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P16", "Large 3"), (int)
	// (8900.1));
	// recipeAndResourceNameToCuttingTime.put(Pair.create("P16", "Large 4"), (int)
	// (7040.0));
	// }
	//
	// return recipeAndResourceNameToCuttingTime;
	// }

}
