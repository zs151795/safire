package uk.ac.york.safire.optimisation.maxplusplant

import uk.ac.york.safire.metrics._
import uk.ac.york.safire.optimisation._
import uk.ac.york.safire.optimisation.maxplusplant._

import uk.ac.york.safire.metrics._
import scala.collection.JavaConversions._
import scala.util.Random.javaRandomToRandom
import uk.ac.york.safire.optimisation.JMetalLocalOptimisationEngine
import uk.ac.york.safire.optimisation.ObjectiveFunction

import jeep.math.ClosedOpenInterval
import org.apache.commons.lang3.builder._

///////////////////////////////////

case class GeneratorParams(scaleFactor: Double, energyCostWeighting: Double, makespanCostWeighting: Double, seed: Long) {
  override def toString(): String =
    ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE)
}

///////////////////////////////////

object ScaledPlantGenerator {

  def generateScaledPlant(scaleFactor: Double, seed: Long): SimplePlant = {
    require(scaleFactor > 0)

    val numMachines: Int = 3 + scaleFactor.toInt
    val rng = new java.util.Random(seed)
    RunPlant.generatePlant(
      math.ceil(scaleFactor).toInt, // noOfInternalLevels
      new ClosedOpenInterval(1, 3 + scaleFactor.toInt), // verticesPerInternalLevel
      numMachines, // 3 + i, // noOfMachines
      4 + math.ceil(scaleFactor).toInt, // maxNoOfModesPerMachine
      new ClosedOpenInterval(1, 20), // nominalDuration
      new ClosedOpenInterval(1.0, 10.0), // energyPerTimeUnit
      new ClosedOpenInterval(0.1, 2.0), // energyMultiplier	    		
      new ClosedOpenInterval(0.2, 3.0), // durationMultiplier			
      rng
    )
  }

  /////////////////////////////////

  def generateBenchmarkInstances(
    energyCostWeighting: Double,
    makespanCostWeighting: Double, numBenchmarks: Int, seed: Long
  ): java.util.List[MockObjectiveFunction] = {

    val rng = new java.util.Random(seed)
    val problemSeeds = (1 to numBenchmarks) map { _ => rng.nextLong() }

    val scaleFactorMin = 1.0
    val scaleFactorMax = 5.0

    ///////////////////////////////    

    // generate plant and objective function for increasing scale factors...
    val ofs = (0 until numBenchmarks) map { i =>
      val scaleFactor = jeep.math.LinearInterpolation.apply(i, 0, numBenchmarks - 1, scaleFactorMin, scaleFactorMax)
      val of = new MockObjectiveFunction(
        energyCostWeighting,
        makespanCostWeighting,
        generateScaledPlant(scaleFactor, problemSeeds(i))
      )
      (of, GeneratorParams(scaleFactor, energyCostWeighting, makespanCostWeighting, problemSeeds(i)))
    }

    ///////////////////////////////

    println("Caclulating optima (using Choco solver)...")
    val progress = new jeep.util.ProgressDisplay(numBenchmarks)
    val startTime = System.currentTimeMillis()
    ofs.zipWithIndex.foreach {
      case ((of, generatorParams), index) =>
        // println(s"${1 + index} (of ${numBenchmarks})")
        val startTime = System.currentTimeMillis()
        val opt = of.optimum
        val endTime = System.currentTimeMillis()
        // println(s"$generatorParams, optimum: ${opt}, elapsed (secs): ${(endTime - startTime) / 1000.0}")
        progress.increment()
    }
    val endTime = System.currentTimeMillis()

    println(s"total elapsed (secs): ${(endTime - startTime) / 1000.0}")
    ofs.map { _._1 }.toList
  }

  /////////////////////////////////  

  def main(args: Array[String]): Unit = {

    val EnergyCostWeighting = 1.0
    val MakespanCostWeighting = 1.0
    val NumBenchmarks = 30

    generateBenchmarkInstances(EnergyCostWeighting, MakespanCostWeighting, NumBenchmarks, seed = 0xDEADBEEF)
  }
}

// End ///////////////////////////////////////////////////////////////
