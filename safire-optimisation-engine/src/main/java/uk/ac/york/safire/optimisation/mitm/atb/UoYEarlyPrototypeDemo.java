package uk.ac.york.safire.optimisation.mitm.atb;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import jeep.util.ProgressDisplay;
import uk.ac.york.aura.Operators;
import uk.ac.york.aura.PopulationEntry;
import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.OptimisationArguments;
import uk.ac.york.safire.metrics.OptimisationResult;
import uk.ac.york.safire.metrics.Utility;
import uk.ac.york.safire.metrics.Value;
import uk.ac.york.safire.metrics.Value.Real;
import uk.ac.york.safire.optimisation.AuraLocalOptimisationEngine;
import uk.ac.york.safire.optimisation.ConfigurationUtilities;
import uk.ac.york.safire.optimisation.ObjectiveFunction;
import uk.ac.york.safire.optimisation.OptimisationEngine;

///////////////////////////////////

/**
 * From:
 * https://docs.hortonworks.com/HDPDocuments/HDP2/HDP-2.6.4/bk_kafka-component-guide/content/ch_kafka-development.html
 */

public final class UoYEarlyPrototypeDemo {

	public static List<PopulationEntry> lastGeneration = new ArrayList<>();

	public static OptimisationResult invokeOE(Configuration config, ObjectiveFunction.LocalObjectiveFunction of,
			int urgency, int iterations, int populationSize, Random rng, int OE) {

		final OptimisationEngine oe = new AuraLocalOptimisationEngine(of, iterations, rng);

		final List<Configuration> configs = new ArrayList<>(); // Collections.singletonList( config );
		configs.add(config);
		final Function<Configuration, Configuration> mutation = Operators.hyperMutation(1.0, rng);
		for (int i = 0; i < populationSize - 1; ++i) {
			// jeep.lang.Diag.println(i);
			configs.add(mutation.apply(config));
		}

		List<Map.Entry<String, Value>> linear1 = config.getControlledMetrics().entrySet().stream()
				.collect(Collectors.toList());
		linear1.sort((p1, p2) -> p1.getKey().compareTo(p2.getKey()));

		// final double urgencyNormalised = jeep.math.LinearInterpolation.apply(urgency,
		// 0.0, 100.0, 0.0, 1.0);
		// OnaSingleObjectiveFunction.W1_$eq(urgencyNormalised);
		// OnaSingleObjectiveFunction.W2_$eq(1.0 - urgencyNormalised);

		final long startTime = System.currentTimeMillis();
		final OptimisationResult or = oe.optimise(new OptimisationArguments(configs, 0.5, 0.5, OE));
		lastGeneration = OptimisationEngine.lastgeneration;
		final long endTime = System.currentTimeMillis();
		System.out.println("Optmisation took: " + ((endTime - startTime) / 1000.0) + " seconds");
		return or;
	}

	///////////////////////////////

	private static long memoryUsageInMB() {
		System.gc();
		final Runtime rt = Runtime.getRuntime();
		final long usedMB = (rt.totalMemory() - rt.freeMemory()) / (1024 * 1024);
		return usedMB;
	}

	///////////////////////////////

	private static void run(KafkaConsumer<String, String> uoyConsumer, Producer<String, String> uoyProducer,
			long maxTopicUpdates, ObjectiveFunction.LocalObjectiveFunction of, int urgency, int iterations,
			BusinessCase bc, Random rng) {

		long maxMemoryUsage = memoryUsageInMB();
		for (int i = 0; i < maxTopicUpdates; ++i) {
			// try {
			// Thread.sleep(5000);
			// } catch (InterruptedException e1) {
			// e1.printStackTrace();
			// }

			final ConsumerRecords<String, String> records = uoyConsumer.poll(5000);
			if (!records.isEmpty())
				System.out.println("Json received!");

			for (ConsumerRecord<String, String> record : records) {
				String received = record.value();
				String factory = received.substring(0, 3);
				String receivedConfig = received.substring(3);

				String bcMode = "";
				switch (bc) {
				case OAS:
					bcMode = "OAS";
					break;
				case ONA:
					bcMode = "ONA";
					break;
				case Electrolux:
					bcMode = "ELE";
					break;

				default:
					break;
				}

				if (!bcMode.equalsIgnoreCase(factory)) {
					System.out.println("Wrong Json, try again or clear kafka queue.");
					continue;
				}

				final Configuration config = new uk.ac.york.safire.metrics.JsonConverter().fromJson(receivedConfig,
						Configuration.class);

				OptimisationResult or = invokeOE(config, of, urgency, iterations, populationSize, rng, 1);

				final Configuration optimisedConfig = or.getReconfigurations().get(0);
				System.out.println(ConfigurationUtilities.getStatusString(optimisedConfig));

				final long memoryUsage = memoryUsageInMB();
				maxMemoryUsage = Math.max(memoryUsage, maxMemoryUsage);
				System.out.println("Maximum memory usage: " + maxMemoryUsage + "MB");

				String schedInfo = ConfigurationUtilities.getStatusString(optimisedConfig);
				String[] taskInfo = schedInfo.split("\n");
				String resultsJson = "[\n\n";

				int idCounter = 0;
				List<FormattedResultOAS> res = new ArrayList<FormattedResultOAS>();
				for (int s = 0; s < taskInfo.length; s++) {
					String[] toArray = taskInfo[s].split("_");
					if (toArray.length == 12) {
						String year = toArray[0];
						String month = toArray[1];
						String day = toArray[2];
						String startTime = toArray[3];
						String endTime = toArray[4];

						String idRaw = idCounter + "";
						String id = "";
						if (idRaw.length() < 4) {
							int blanklenth = 4 - idRaw.length();

							for (int b = 0; b < blanklenth; b++) {
								id += "0";
							}
							id += idRaw;
						} else {
							id += idRaw;
						}

						String productName = toArray[5];
						String recipeType = toArray[6];
						String instanceNumber = toArray[7];
						String amountProduced = toArray[8];
						String mixerID = toArray[9];
						String mixerName = toArray[10];
						String mixerStatus = toArray[11];

						FormattedResultOAS oneRes = new FormattedResultOAS(year, month, day, startTime, endTime, id,
								productName, recipeType, instanceNumber, amountProduced, mixerID, mixerName,
								mixerStatus);
						if (s == taskInfo.length - 2)
							resultsJson += oneRes.formattedOut(true);
						else
							resultsJson += oneRes.formattedOut(false);
						res.add(oneRes);
						idCounter++;

					}
				}
				resultsJson.substring(0, resultsJson.length() - 1);
				resultsJson += "\n]";

				final String configJSON = resultsJson;
				final Instant now = Instant.now(); // get time in UTC
				final ProducerRecord<String, String> data = new ProducerRecord<String, String>(
						"OptimisationEngineResult", "timestamp: " + now, configJSON);

				UoYEarlyPrototypeDemo.writeSystem("outputJSON", configJSON);

				jeep.lang.Diag.println("#front: " + or.getReconfigurations().size());

				List<List<Value>> results = new ArrayList<List<Value>>();

				for (Configuration c : or.getReconfigurations()) {
					List<Value> result = new ArrayList<Value>();

					if (c.getKeyObjectives().get("makespan") != null)
						result.add(c.getKeyObjectives().get("makespan"));

					if (c.getKeyObjectives().get("energy") != null)
						result.add(c.getKeyObjectives().get("energy"));

					if (c.getKeyObjectives().get("montary") != null)
						result.add(c.getKeyObjectives().get("montary"));

					if (c.getKeyObjectives().get("urgency") != null)
						result.add(c.getKeyObjectives().get("urgency"));

					if (c.getKeyObjectives().get("quality") != null)
						result.add(c.getKeyObjectives().get("quality"));

					if (c.getKeyObjectives().get("Std Weiss amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Std Weiss amount discrepancy score"));

					if (c.getKeyObjectives().get("Weiss Matt amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Weiss Matt amount discrepancy score"));

					if (c.getKeyObjectives().get("Super Glanz amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Super Glanz amount discrepancy score"));

					if (c.getKeyObjectives().get("Weiss Basis amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Weiss Basis amount discrepancy score"));

					if (c.getKeyObjectives().get("Boiled Water amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Boiled Water amount discrepancy score"));

					if (c.getKeyObjectives().get("Pasta amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Pasta amount discrepancy score"));

					if (c.getKeyObjectives().get("Rice amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Rice amount discrepancy score"));

					if (c.getKeyObjectives().get("Beef amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Beef amount discrepancy score"));

					if (c.getKeyObjectives().get("Potato amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Potato amount discrepancy score"));

					if (c.getKeyObjectives().get("Mushroom amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Mushroom amount discrepancy score"));

					results.add(result);
				}
				List<List<Double>> analysableResults = new ArrayList<List<Double>>();
				for (int j = 0; j < results.get(0).size(); j++) {
					analysableResults.add(new ArrayList<Double>());
				}

				String finalResult = "Generations: " + iterations + " Populations: " + iterations
						+ " Optimization Engine: " + 1 + "\n";

				for (int j = 0; j < results.size(); j++) {
					for (int k = 0; k < results.get(j).size(); k++) {
						Real objective = (Real) results.get(j).get(k);
						finalResult += objective.value + " ";
						analysableResults.get(k).add((double) objective.value);
					}

					finalResult += "\n";
				}
				finalResult += "\n";
				System.out.println(finalResult);

				DecimalFormat df = new DecimalFormat("0.00");

				for (int j = 0; j < analysableResults.size(); j++) {
					DoubleSummaryStatistics stats = analysableResults.get(j).stream().mapToDouble((x) -> x)
							.summaryStatistics();
					System.out.println("Average: " + df.format(stats.getAverage()) + " Min: "
							+ df.format(stats.getMin()) + " Max: " + df.format(stats.getMax()));
				}

				// System.out.println(ct.getControlledMetrics().size() / 2 + " down.");
				System.out.println(data.value());
				uoyProducer.send(data);
			}

			// uoyConsumer.commitSync();
			try {
				uoyConsumer.commitSync();
			} catch (Exception e) {
				// System.out.println(" catch it! optimisation finished.");
			}
		}
	}

	private static void runLocal(ObjectiveFunction.LocalObjectiveFunction of, ConfigurationType ct, Random rng,
			Params params) {

		final Configuration config = Utility.randomConfiguration(ct, rng);
		UoYEarlyPrototypeDemo.writeSystem("EleJSONExample", new uk.ac.york.safire.metrics.JsonConverter().toJson(config));
		final int engine = 1;
		final int urgency = params.urgency;
		final int iterations = params.iterations;
		final int populationSize = 50;
		jeep.lang.Diag.println(ct.getControlledMetrics().size() / 2 + " optimisation started: engine: " + engine
				+ " population: " + populationSize + " generation: " + iterations);
		final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config, of, urgency, iterations, populationSize,
				rng, engine);
		///////////////////////

		final Configuration optimisedConfig = or.getReconfigurations().get(0);
		System.out.println(ConfigurationUtilities.getStatusString(optimisedConfig));

		String schedInfo = ConfigurationUtilities.getStatusString(optimisedConfig);
		String[] taskInfo = schedInfo.split("\n");
		String resultsJson = "[\n\n";

		int idCounter = 0;
		List<FormattedResultOAS> res = new ArrayList<FormattedResultOAS>();
		for (int s = 0; s < taskInfo.length; s++) {
			String[] toArray = taskInfo[s].split("_");
			if (toArray.length == 12) {
				String year = toArray[0];
				String month = toArray[1];
				String day = toArray[2];
				String startTime = toArray[3];
				String endTime = toArray[4];

				String idRaw = idCounter + "";
				String id = "";
				if (idRaw.length() < 4) {
					int blanklenth = 4 - idRaw.length();

					for (int b = 0; b < blanklenth; b++) {
						id += "0";
					}
					id += idRaw;
				} else {
					id += idRaw;
				}

				String productName = toArray[5];
				String recipeType = toArray[6];
				String instanceNumber = toArray[7];
				String amountProduced = toArray[8];
				String mixerID = toArray[9];
				String mixerName = toArray[10];
				String mixerStatus = toArray[11];

				FormattedResultOAS oneRes = new FormattedResultOAS(year, month, day, startTime, endTime, id,
						productName, recipeType, instanceNumber, amountProduced, mixerID, mixerName, mixerStatus);
				if (s == taskInfo.length - 2)
					resultsJson += oneRes.formattedOut(true);
				else
					resultsJson += oneRes.formattedOut(false);
				res.add(oneRes);
				idCounter++;

			}
		}
		resultsJson.substring(0, resultsJson.length() - 1);
		resultsJson += "\n]";

		final String configJSON = resultsJson;
		final Instant now = Instant.now(); // get time in UTC
		final ProducerRecord<String, String> data = new ProducerRecord<String, String>("OptimisationEngineResult",
				"timestamp: " + now, configJSON);

		UoYEarlyPrototypeDemo.writeSystem("outputJSON", configJSON);

		jeep.lang.Diag.println("#front: " + or.getReconfigurations().size());

		List<List<Value>> results = new ArrayList<List<Value>>();

		for (Configuration c : or.getReconfigurations()) {
			List<Value> result = new ArrayList<Value>();

			if (c.getKeyObjectives().get("makespan") != null)
				result.add(c.getKeyObjectives().get("makespan"));

			if (c.getKeyObjectives().get("energy") != null)
				result.add(c.getKeyObjectives().get("energy"));

			if (c.getKeyObjectives().get("montary") != null)
				result.add(c.getKeyObjectives().get("montary"));

			if (c.getKeyObjectives().get("urgency") != null)
				result.add(c.getKeyObjectives().get("urgency"));

			if (c.getKeyObjectives().get("quality") != null)
				result.add(c.getKeyObjectives().get("quality"));

			if (c.getKeyObjectives().get("Std Weiss amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Std Weiss amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Matt amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Matt amount discrepancy score"));

			if (c.getKeyObjectives().get("Super Glanz amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Super Glanz amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Basis amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Basis amount discrepancy score"));

			if (c.getKeyObjectives().get("Boiled Water amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Boiled Water amount discrepancy score"));

			if (c.getKeyObjectives().get("Pasta amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Pasta amount discrepancy score"));

			if (c.getKeyObjectives().get("Rice amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Rice amount discrepancy score"));

			if (c.getKeyObjectives().get("Beef amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Beef amount discrepancy score"));

			if (c.getKeyObjectives().get("Potato amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Potato amount discrepancy score"));

			if (c.getKeyObjectives().get("Mushroom amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Mushroom amount discrepancy score"));

			results.add(result);
		}
		List<List<Double>> analysableResults = new ArrayList<List<Double>>();
		for (int j = 0; j < results.get(0).size(); j++) {
			analysableResults.add(new ArrayList<Double>());
		}

		String finalResult = "Generations: " + iterations + " Populations: " + iterations + " Optimization Engine: " + 1
				+ "\n";

		for (int j = 0; j < results.size(); j++) {
			for (int k = 0; k < results.get(j).size(); k++) {
				Real objective = (Real) results.get(j).get(k);
				finalResult += objective.value + " ";
				analysableResults.get(k).add((double) objective.value);
			}

			finalResult += "\n";
		}
		finalResult += "\n";
		System.out.println(finalResult);

		DecimalFormat df = new DecimalFormat("0.00");

		for (int j = 0; j < analysableResults.size(); j++) {
			DoubleSummaryStatistics stats = analysableResults.get(j).stream().mapToDouble((x) -> x).summaryStatistics();
			System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
					+ " Max: " + df.format(stats.getMax()));
		}

		// System.out.println(ct.getControlledMetrics().size() / 2 + " down.");
		System.out.println(data.value());

	}

	///////////////////////////////

	private static KafkaConsumer<String, String> createUoYConsumer(String bootstrapServersConfig) {
		final Properties consumerConfig = new Properties();
		consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig);
		consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "my-group");
		// consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

		// consumerConfig.put("fetch.message.max.bytes", "" + 1024 * 1024 * 42);

		// consumerConfig.put("max.request.size", "" + 1024 * 1024 * 20);
		// consumerConfig.put("message.max.bytes", "" + 1024 * 1024 * 20);
		// consumerConfig.put("replica.fetch.max.bytes", "" + 1024 * 1024 * 20);
		consumerConfig.put("max.message.bytes", "" + 52428800);
		// consumerConfig.put("session.timeout.ms", "" + 25000000);

		return new KafkaConsumer<>(consumerConfig);
	}

	private static Producer<String, String> createUoYProducer(String bootstrapServersConfig) {
		final Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig);

		// props.put(ProducerConfig.CLIENT_ID_CONFIG, "test.app.producer");
		props.put(ProducerConfig.ACKS_CONFIG, "all");
		props.put(ProducerConfig.RETRIES_CONFIG, 0);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

		// props.put("message.max.bytes", "" + 1024 * 1024 * 40);
		// props.put("replica.fetch.max.bytes", "" + 1024 * 1024 * 41);

		// props.put("key.serializer",
		// "org.apache.kafka.common.serialization.StringSerializer");
		// props.put("value.serializer",
		// "org.apache.kafka.common.serialization.ByteArraySerializer");
		//// props.put("bootstrap.servers", brokerList);
		// props.put("compression.type", "gzip");

		// props.put("max.request.size", "" + 1024 * 1024 * 40);
		// props.put("fetch.message.max.bytes", "" + 1024 * 1024 * 40);
		props.put("max.message.bytes", "" + 52428800);
		return new KafkaProducer<String, String>(props);
	}

	///////////////////////////////

	static void runUoY(long maxTopicUpdates, String bootstrapServersConfig, BusinessCase bc, int urgency,
			int iterations, Random rng) {

		final KafkaConsumer<String, String> uoyConsumer = createUoYConsumer(bootstrapServersConfig);
		final Producer<String, String> uoyProducer = createUoYProducer(bootstrapServersConfig);

		final ObjectiveFunction.LocalObjectiveFunction of = bc.getObjectiveFunction();

		try {
			uoyConsumer.subscribe(Collections.singletonList("ResourceAvailability"));
			run(uoyConsumer, uoyProducer, maxTopicUpdates, of, urgency, iterations, bc, rng);
		} finally {
			uoyConsumer.close();
			uoyProducer.close();
			ATBSimulatorKafkaProducer.isFinished = true;
		}
	}

	///////////////////////////////

	static void runUoYandATB(long maxTopicUpdates, String bootstrapServersConfig, int finalPercentAvailability,
			long updateTopicStringEveryMillis, BusinessCase bc, int urgency, int iterations, Random rng) {

		final Producer<String, String> atbProducer = ATBSimulatorKafkaProducer
				.createATBProducer(bootstrapServersConfig);

		final Thread atbProducerThread = new Thread("atbProducerThread") {
			@Override
			public void run() {
				try {
					ATBSimulatorKafkaProducer.run(finalPercentAvailability, atbProducer, updateTopicStringEveryMillis,
							maxTopicUpdates, bc, rng);
				} finally {
					atbProducer.close();
				}
			}
		};

		//////////////////////////

		atbProducerThread.start();
		try {
			System.out.println("wait for atb producer input.");
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		runUoY(maxTopicUpdates, bootstrapServersConfig, bc, urgency, iterations, rng);
	}

	///////////////////////////////

	private static String makeBanner(String arg) {
		final int imageWidth = 80; // 144;
		final int imageHeight = 32;
		BufferedImage image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
		Graphics g = image.getGraphics();
		final int fontSize = 10; // 24
		g.setFont(new Font("Dialog", Font.PLAIN, fontSize));
		Graphics2D graphics = (Graphics2D) g;
		graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		// graphics.drawString( "Hello World!", 6, 24);
		graphics.drawString(arg, 6, fontSize); // 24);
		// ImageIO.write(image, "png", new File("text.png"));

		StringBuilder result = new StringBuilder();
		for (int y = 0; y < imageHeight; ++y) {
			StringBuilder sb = new StringBuilder();
			for (int x = 0; x < imageWidth; ++x) {
				// sb.append(image.getRGB(x, y) == -16777216 ? " " : image.getRGB(x, y) == -1 ?
				// "#" : "*");
				sb.append(image.getRGB(x, y) == -16777216 ? " " : "*");
			}

			if (sb.toString().trim().isEmpty())
				continue;
			result.append(sb + "\n");
		}
		return result.toString();
	}

	///////////////////////////////

	private final static class Params {

		final int finalAvailabilityPercent;
		final int topicUpdateEverySeconds;
		final int numUpdates;
		final long randomSeed;
		final int factory;
		final int urgency;
		final int iterations;

		///////////////////////////

		Params(int finalAvailabilityPercent, int topicUpdateEverySeconds, int numUpdates, long randomSeed, int factory,
				int urgency, int iterations) {
			this.finalAvailabilityPercent = finalAvailabilityPercent;
			this.topicUpdateEverySeconds = topicUpdateEverySeconds;
			this.numUpdates = numUpdates;
			this.randomSeed = randomSeed;
			this.factory = factory;
			this.urgency = urgency;
			this.iterations = iterations;
		}
	}

	private static Params parseCommandLine(String[] args) {

		int finalAvailabilityPercent = 100;
		int topicUpdateEverySeconds = 30;
		int numUpdates = 1;
		long randomSeed = System.currentTimeMillis();
		int factory = 0;
		int urgency = 50;
		int iterations = generationSize;

		assert (args.length > 0);

		String factoryName = args[0];

		if (factoryName.equalsIgnoreCase("OAS"))
			factory = 1;
		if (factoryName.equalsIgnoreCase("ONA"))
			factory = 2;
		if (factoryName.equalsIgnoreCase("Electrolux") || factoryName.equalsIgnoreCase("ele"))
			factory = 3;

		assert (factory != 0);

		///////////////////////////

		final Options options = new Options();
		final Option availOpt = new Option("a", "avail", true, "Final mixer availability percentage");
		availOpt.setRequired(false);
		options.addOption(availOpt);

		final Option topicUpdateEveryOpt = new Option("u", "updateperiod", true,
				"ATB simulator - topic update period (seconds)");
		topicUpdateEveryOpt.setRequired(false);
		options.addOption(topicUpdateEveryOpt);

		final Option numUpdatesOpt = new Option("n", "numupdates", true, "ATB simulator - total number of updates ");
		numUpdatesOpt.setRequired(false);
		options.addOption(numUpdatesOpt);

		final Option randomSeedOpt = new Option("r", "randomseed", true, "Seed for random number generator");
		randomSeedOpt.setRequired(false);
		options.addOption(randomSeedOpt);

		final Option onaOpt = new Option("ona", "ona", false, "Use ONA objective function");
		onaOpt.setRequired(false);
		options.addOption(onaOpt);

		final Option urgencyOpt = new Option("urgency", "urgency", true, "Schedule urgency");
		urgencyOpt.setRequired(false);
		options.addOption(urgencyOpt);

		final Option iterOpt = new Option("i", "iterations", true, "Optimiser iterations");
		iterOpt.setRequired(false);
		options.addOption(iterOpt);

		final CommandLineParser parser = new DefaultParser();

		///////////////////////////

		try {
			final CommandLine cmd = parser.parse(options, args);

			{
				String s = cmd.getOptionValue("avail", "" + finalAvailabilityPercent);
				final int val = Integer.valueOf(s);
				if (val >= 0 && val <= 100)
					finalAvailabilityPercent = val;
			}

			{
				String s = cmd.getOptionValue("updateperiod", "" + topicUpdateEverySeconds);
				final int val = Integer.valueOf(s);
				if (val > 0)
					topicUpdateEverySeconds = val;
			}

			{
				String s = cmd.getOptionValue("numupdates", "" + numUpdates);
				final int val = Integer.valueOf(s);
				if (val > 0)
					numUpdates = val;
			}

			{
				String s = cmd.getOptionValue("randomseed", "" + randomSeed);
				final long val = Long.valueOf(s);
				if (val > 0)
					randomSeed = val;
			}

			{
				String s = cmd.getOptionValue("urgency", "" + urgency);
				final double val = Double.valueOf(s);
				if (val >= 0 && val <= 100)
					urgency = (int) val;
			}
			{
				String s = cmd.getOptionValue("iterations", "" + iterations);
				final int val = Integer.valueOf(s);
				if (val >= 0)
					iterations = (int) val;
			}

			// useOnaObjectiveFunction = cmd.hasOption("ona");

		} catch (ParseException | NumberFormatException e) {
			System.out.println(e.getClass().getSimpleName() + " " + e.getMessage());
			final HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(UoYEarlyPrototypeDemo.class.getSimpleName(), options);

			System.exit(1);
		}

		//////////////////////////

		return new Params(finalAvailabilityPercent, topicUpdateEverySeconds, numUpdates, randomSeed, factory, urgency,
				iterations);
	}

	///////////////////////////////

	public static int populationSize = 10;
	public static int generationSize = 5;

	public static void main(String[] args) {

		System.out.println(makeBanner("OE Demo"));
		if (CompilationTime.get().isPresent())
			System.out.println("build time: " + CompilationTime.get().get());

		System.out.println();

		///////////////////////////

		final Params params = parseCommandLine(args);

		///////////////////////////

		final Random rng = new Random(0xDEADBEEF);
		final String bootstrapServersConfig = "localhost:9092";
		final long updateTopicStringEveryMillis = params.topicUpdateEverySeconds * 1000;

		///////////////////////////
		BusinessCase bc = null;
		switch (params.factory) {
		case 1:
			OasProductionLineConfigurationType.presetup();
			bc = BusinessCase.OAS;
			break;
		case 2:
			OnaConfigurationType.presetup();
			bc = BusinessCase.ONA;
			break;
		case 3:
			ElectroluxConfigurationType.presetup();
			bc = BusinessCase.Electrolux;
			break;

		default:
			break;
		}

		assert (bc != null);

		System.out.println("PARAMETERS:");
		System.out.println("Final mixer availability probability: " + params.finalAvailabilityPercent + "%");
		System.out.println(
				"ATB simulator posts mixer status topic updates every " + params.topicUpdateEverySeconds + " seconds");
		System.out.println("Num ATB simulator topic updates: " + params.numUpdates);
		System.out.println("Random seed: " + params.randomSeed);
		System.out.println("OE invocations: " + params.iterations);
		System.out.println("Factory: " + bc.toString());
		System.out.println("Schedule urgency: " + params.urgency + "%");
		System.out.println("End PARAMETERS");
		System.out.println();

		System.out.println("Configuring...");
		final long maxCount = 1L << 32;
		final ProgressDisplay progress = new ProgressDisplay(maxCount);
		for (long i = 0; i < maxCount; ++i)
			progress.increment();

		///////////////////////////

		runUoYandATB(params.numUpdates, bootstrapServersConfig, params.finalAvailabilityPercent,
				updateTopicStringEveryMillis, bc, params.urgency, params.iterations, rng);

		///////////////////////////

		System.out.println("All done.");
	}

	public static void mainLocal(String[] args) {

		System.out.println(makeBanner("OE Demo"));
		if (CompilationTime.get().isPresent())
			System.out.println("build time: " + CompilationTime.get().get());

		System.out.println();

		final Params params = parseCommandLine(args);
		final Random rng = new Random(0xDEADBEEF);

		BusinessCase bc = null;
		switch (params.factory) {
		case 1:
			OasProductionLineConfigurationType.presetup();
			bc = BusinessCase.OAS;
			break;
		case 2:
			OnaConfigurationType.presetup();
			bc = BusinessCase.ONA;
			break;
		case 3:
			ElectroluxConfigurationType.presetup();
			bc = BusinessCase.Electrolux;
			break;

		default:
			break;
		}

		assert (bc != null);

		System.out.println("Factory: " + bc.toString());

		ConfigurationType ct = bc.getConfigurationType(params.finalAvailabilityPercent, rng);
		ObjectiveFunction.LocalObjectiveFunction of = bc.getObjectiveFunction();

		runLocal(of, ct, rng, params);

		System.out.println("All done.");
	}

	public static void writeSystem(String filename, String result) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(new FileWriter(new File("result/" + filename + ".txt"), false));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		writer.println(result);
		writer.close();
	}
}

// End ///////////////////////////////////////////////////////////////

// TODO List
/*
 * 1. unAvailable Times; 2. no waste production (improved version for pot
 * capability); 3. Energy level;
 */
