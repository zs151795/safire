package uk.ac.york.safire.optimisation;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import uk.ac.york.aura.EA;
import uk.ac.york.aura.MOEAD;
import uk.ac.york.aura.MOEAD_RS;
import uk.ac.york.aura.NSGA2;
import uk.ac.york.aura.Operators;
import uk.ac.york.aura.PopulationEntry;
import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.OptimisationArguments;
import uk.ac.york.safire.metrics.OptimisationResult;
import uk.ac.york.safire.metrics.SearchDirection;

///////////////////////////////////

public final class AuraLocalOptimisationEngine extends OptimisationEngine.LocalOptimisationEngine {

	private static final int TOURNAMENT_SIZE = 3;
	// private static final SearchDirection SEARCH_DIRECTION =
	// SearchDirection.MAXIMIZING;
	private static final SearchDirection SEARCH_DIRECTION = SearchDirection.MINIMIZING;

	///////////////////////////////

	private final Random rng;
	private final int maxGenerations;

	///////////////////////////////

	public AuraLocalOptimisationEngine(ObjectiveFunction objectiveFunction, int maxGenerations, Random rng) {

		super(objectiveFunction);

		// if( minEvaluations < 1 || minEvaluations > maxEvaluations )
		// throw new IllegalArgumentException();
		if (maxGenerations < 1)
			throw new IllegalArgumentException();
		this.rng = rng;
		this.maxGenerations = maxGenerations;
	}

	///////////////////////////////

	private static List<Double> objectiveValueRanges(ConfigurationType ct) {
		return ct.getKeyObjectiveMetrics().stream().map(e -> e.valueType.max - e.valueType.min)
				.collect(Collectors.toList());
	}

	///////////////////////////////

	@Override
	public OptimisationResult optimise(OptimisationArguments args) {

		// final int maxEval = maxGenerations * populationSize; //
		// OptmisationUtility.maxEvaluations(args.getUrgency(), args.getQuality(),
		// minEvaluations, maxEvaluations);

		final Comparator<List<Double>> compareMultiObjective = Operators.compareMultiObjective(SEARCH_DIRECTION); // Minimizing
																													// comparator
		final BiFunction<Configuration, Configuration, Configuration> crossover = Operators.onePointCrossover(rng);
		final Function<Configuration, Configuration> mutation =
				 Operators.uniformMutation(rng);
				// Operators.onePointMutation(rng);
				// Operators.uniformMutation(rng);
				//Operators.hyperMutation(0.8, rng);

		// jeep.lang.Diag.println("maxGen: " + maxGenerations);

		final Predicate<List<List<PopulationEntry>>> isFinished = (history) -> {
			// jeep.lang.Diag.println( "iter: " + history.size() + " of " + maxGenerations
			// );
			return history.size() >= maxGenerations;
		};

		List<PopulationEntry> bestFront = null;

		// if (args.getConfigurations().get(0).getKeyObjectives().size() == 1) {
		// System.out.println("Single Objective Optimsation Start");
		// PopulationEntry best = EA
		// .apply(args.getConfigurations(), (ObjectiveFunction.LocalObjectiveFunction)
		// getObjectiveFunction(),
		// compareMultiObjective, Operators.proportionalSelection, crossover, mutation,
		// isFinished, rng)
		// .getSecond();
		// bestFront = new ArrayList<>();
		// bestFront.add(best);
		// } else {
		System.out.println("Multi-Objective Optimsation Start, OE number: " + args.getEngine());
		switch (args.getEngine()) {
		case 0:
			bestFront = NSGA2.apply(args.getConfigurations(),
					(ObjectiveFunction.LocalObjectiveFunction) getObjectiveFunction(), compareMultiObjective, crossover,
					mutation, isFinished, objectiveValueRanges(args.getConfigurations().get(0).getConfigurationType()),
					TOURNAMENT_SIZE, SEARCH_DIRECTION, rng);
			break;
		case 1:
			MOEAD moead = new MOEAD(args.getConfigurations(),
					(ObjectiveFunction.LocalObjectiveFunction) getObjectiveFunction(), crossover, mutation, isFinished,
					objectiveValueRanges(args.getConfigurations().get(0).getConfigurationType()), TOURNAMENT_SIZE,
					SEARCH_DIRECTION, rng);
			bestFront = moead.apply();
			lastgeneration.addAll(moead.currentPopulation);
			break;
		case 2:
			MOEAD_RS moead_rs = new MOEAD_RS(args.getConfigurations(),
					(ObjectiveFunction.LocalObjectiveFunction) getObjectiveFunction(), crossover, mutation, isFinished,
					objectiveValueRanges(args.getConfigurations().get(0).getConfigurationType()), TOURNAMENT_SIZE,
					SEARCH_DIRECTION, rng);
			bestFront = moead_rs.apply();
			break;
		default:
			break;
		// }
		}

		return new OptimisationResult(
				bestFront.stream().map(PopulationEntry::getConfiguration).collect(Collectors.toList()));
	}
}

// End ///////////////////////////////////////////////////////////////
