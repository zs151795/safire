package uk.ac.york.safire.optimisation.mitm.atb;

import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.tuple.Pair;

import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.optimisation.ObjectiveFunction;
import uk.ac.york.safire.optimisation.mitm.atb.ATBSimulatorKafkaProducer.RecipeInfo;

///////////////////////////////////

public enum BusinessCase {

	OAS {

		@Override
		public ConfigurationType getConfigurationType(int percentAvailability, Random random) {
			return OasProductionLineConfigurationType.hardwiredConfigurationType(random, percentAvailability);
		}

		@Override
		public ObjectiveFunction.LocalObjectiveFunction getObjectiveFunction() {
			return new OasProductionLineObjectiveFunction(OasProductionLineConfigurationType.getSetUps(),
					OasProductionLineConfigurationType.getCommdities());
		}

		@Override
		public double[] getWeight() {
			return new double[] { 0.5, 0.125, 0.125, 0.125, 0.125 };
		}

		@Override
		public int getNumberOfObjectives() {
			List<String> objectives = OasProductionLineConfigurationType.getObjectives();

			int objectiveSize = 0;

			for (String s : objectives) {
				if (s.equals("discrepancy")) {
					objectiveSize += OasProductionLineConfigurationType.getProducts().size();
				} else {
					objectiveSize++;
				}
			}

			return objectiveSize;
		}

	},
	ONA {
		@Override
		public ConfigurationType getConfigurationType(int percentAvailability, Random random) {
			final boolean isMultiobjective = true;
			final Pair<Map<String, List<RecipeInfo>>, String[]> recipesAndResources = OnaConfigurationType
					.recipesAndResourceNames();
			final Map<String, List<RecipeInfo>> recipeInfo = recipesAndResources.getLeft();
			final String[] resourceNames = recipesAndResources.getRight();

			return OnaConfigurationType.configurationType(recipeInfo, resourceNames, isMultiobjective, random);
		}

		@Override
		public ObjectiveFunction.LocalObjectiveFunction getObjectiveFunction() {
			return new PreemptionIAObjectiveFunction(OnaConfigurationType.getSetUps());
		}

		@Override
		public double[] getWeight() {
			return new double[] { 0.5, 0.25, 0.25 };
		}

		@Override
		public int getNumberOfObjectives() {
			return OnaConfigurationType.getObjectives().size();
		}

	},
	Electrolux {
		@Override
		public ConfigurationType getConfigurationType(int percentAvailability, Random random) {
			return ElectroluxConfigurationType.hardwiredConfigurationType(random, percentAvailability);
		}

		@Override
		public ObjectiveFunction.LocalObjectiveFunction getObjectiveFunction() {
			return new ElectroluxObjectiveFunction(ElectroluxConfigurationType.getSetUps(),
					ElectroluxConfigurationType.getCommdities());
		}

		@Override
		public double[] getWeight() {
			return new double[] { 0.6, 0.4 };
		}

		@Override
		public int getNumberOfObjectives() {
			return ElectroluxConfigurationType.getObjectives().size();
		}

	};

	///////////////////////////////

	public abstract ObjectiveFunction.LocalObjectiveFunction getObjectiveFunction();

	public abstract ConfigurationType getConfigurationType(int percentAvailability, Random random);

	public abstract double[] getWeight();

	public abstract int getNumberOfObjectives();

}

// End ///////////////////////////////////////////////////////////////
