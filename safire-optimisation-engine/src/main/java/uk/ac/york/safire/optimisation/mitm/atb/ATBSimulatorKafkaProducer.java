package uk.ac.york.safire.optimisation.mitm.atb;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

import jeep.math.LinearInterpolation;
import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.Utility;

/**
 * From:
 * https://docs.hortonworks.com/HDPDocuments/HDP2/HDP-2.6.4/bk_kafka-component-guide/content/ch_kafka-development.html
 */

public final class ATBSimulatorKafkaProducer {

	public static volatile boolean isFinished = false;

	///////////////////////////////

	public static class RecipeInfo {
		public final String name;
		public final List<Integer> compatibleResources;
		public final int instances;
		// final int commodityProduced;
		// final int executionTime;

		///////////////////////////

		public RecipeInfo(String name, int instances, List<Integer> compatibleResources
		// , int commodityProduced
		// , int executionTime
		) {
			this.name = name;
			this.instances = instances;
			this.compatibleResources = Collections.unmodifiableList(compatibleResources);
			// this.commodityProduced = commodityProduced;
			// this.executionTime = executionTime;
		}
	}

	///////////////////////////

	static void run(int finalPercentAvailability, Producer<String, String> producer, long updateTopicStringEveryMillis,
			long numTopicStringUpdates, BusinessCase bc, Random random) {

		for (long i = 0; !isFinished && i < numTopicStringUpdates; ++i) {
			final Instant now = Instant.now(); // get time in UTC

			final int percentAvailability = (int) LinearInterpolation.apply(i, 0, numTopicStringUpdates, 100,
					finalPercentAvailability);
			final ConfigurationType ct = bc.getConfigurationType(percentAvailability, random);
			Configuration config = Utility.randomConfiguration(ct, random);
			String configJSON = new uk.ac.york.safire.metrics.JsonConverter().toJson(config);
			String finalJSON = "";
			switch (bc) {
			case OAS:
				finalJSON += "OAS";
				break;
			case ONA:
				finalJSON += "ONA";
				break;
			case Electrolux:
				finalJSON += "ElE";
				break;

			default:
				break;
			}
			finalJSON += configJSON;
			
			UoYEarlyPrototypeDemo.writeSystem("ElEJsonExample", finalJSON);

			final ProducerRecord<String, String> data = new ProducerRecord<String, String>("ResourceAvailability",
					"timestamp: " + now, finalJSON);
			// producer.send(data);

			try {
				RecordMetadata metadata = producer.send(data).get();
				System.out.println(" to partition " + metadata.partition() + " with offset " + metadata.offset());
			} catch (ExecutionException e) {
				System.out.println("Error in sending record");
				System.out.println(e);
			} catch (InterruptedException e) {
				System.out.println("Error in sending record");
				System.out.println(e);
			}

			System.out.println("json send, length: " + configJSON.length());
			com.google.common.util.concurrent.Uninterruptibles.sleepUninterruptibly(updateTopicStringEveryMillis,
					TimeUnit.MILLISECONDS);
		}
	}

	///////////////////////////////

	static Producer<String, String> createATBProducer(String bootstrapServersConfig) {
		final Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig);

		props.put(ProducerConfig.CLIENT_ID_CONFIG, "test.app.producer");
		props.put(ProducerConfig.ACKS_CONFIG, "all");
		props.put(ProducerConfig.RETRIES_CONFIG, 0);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

		props.put("max.request.size", "" + 52428800);
		// props.put("replica.fetch.max.bytes", "" + 1024 * 1024 * 41);

		///////////////////////////

		return new KafkaProducer<String, String>(props);
	}

	///////////////////////////////

	public static void main(String[] args) {

		final Random random = new Random(0xDEABBEEF);
		final long updateTopicStringEveryMillis = 30 * 1000;
		final long numTopicStringUpdates = 1000;
		final int percentAvailability = 100;

		final String bootstrapServersConfig = "localhost:9092";

		///////////////////////////

		final Producer<String, String> atbProducer = createATBProducer(bootstrapServersConfig);

		try {
			final BusinessCase bc = BusinessCase.OAS;
			run(percentAvailability, atbProducer, updateTopicStringEveryMillis, numTopicStringUpdates, bc, random);
		} finally {
			atbProducer.close();
		}
	}
}

// End ///////////////////////////////////////////////////////////////
