package uk.ac.york.safire.optimisation.maxplusplant;

public final class Vertex {
	
    private String name;
    private int level;
	
    ///////////////////////////////
	
    public Vertex(String name, int level) {
        this.name=name;
        this.level=level;
    }



    ///////////////////////////////
    
	public String getName() { return name; }

	public int getLevel() { return level; }
	
    ///////////////////////////////	

}

// End ///////////////////////////////////////////////////////////////
