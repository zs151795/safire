package uk.ac.york.safire.factoryModel.Electrolux;

import uk.ac.york.safire.factoryModel.Electrolux.ElectroluxFactoryModel.DeviceType;

public class Pot extends Device {

	int capability;

	public Pot(int id, DeviceType type, int availability, String notavailbaileTime, int capability) {
		super(id, type, availability, notavailbaileTime);
		this.capability = capability;
	}

	public int getCapability() {
		return capability;
	}

	public void setCapability(int capability) {
		this.capability = capability;
	}

}
