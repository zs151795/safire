package uk.ac.york.safire.optimisation.maxplusplant;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.google.common.collect.ImmutableList;

import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.ControlledMetricType;
import uk.ac.york.safire.metrics.JsonConverter;
import uk.ac.york.safire.metrics.KeyObjectiveType;
import uk.ac.york.safire.metrics.ObservableMetricType;
import uk.ac.york.safire.metrics.SampleRate;
import uk.ac.york.safire.metrics.SearchDirection;
import uk.ac.york.safire.metrics.ValueType;

///////////////////////////////////

public class PlantGenerator {


	public static SimplePlant generatePlant( 
			int noOfInternalLevels,
			int minNoOfVerticesPerInternalLevel, 
			int maxNoOfVerticesPerInternalLevel, 
			int noOfMachines,
			int maxNoOfModesPerMachine,
			int minNominalDuration,
			int maxNominalDuration, 
			double minEnergyPerTimeUnit, 
			double maxEnergyPerTimeUnit,
			double minEnergyMultiplier,
			double maxEnergyMultiplier,
			double minDurationMultiplier,
			double maxDurationMultiplier,
			Random rng) {

		
		
		
		ConfigurationPG configurationPG = new ConfigurationPG(
				noOfInternalLevels,
				minNoOfVerticesPerInternalLevel, 
				maxNoOfVerticesPerInternalLevel, 
				noOfMachines,
				maxNoOfModesPerMachine,
				minNominalDuration,
				maxNominalDuration, 
				minEnergyPerTimeUnit, 
				maxEnergyPerTimeUnit,
				minEnergyMultiplier,
				maxEnergyMultiplier,
				minDurationMultiplier,
				maxDurationMultiplier);
		

		
		Graph graph = new Graph();
		List< ControlledMetricType > controlledMetricTypes = new ArrayList< ControlledMetricType >(); 
		List< ObservableMetricType > observableMetricTypes = new ArrayList< ObservableMetricType >(); 
		Map <String, List<String> > controlledMetricTypesMap = new HashMap<String, List<String> >(); 		
		String graphvizSource="";

		
		addFirstAndNextLevels(
				graph, 
				controlledMetricTypes, 
				observableMetricTypes, 
				controlledMetricTypesMap, 
				configurationPG, 
				graphvizSource, 
				rng);

		final List< KeyObjectiveType > keyObjectiveMetricTypes = ImmutableList.of( 
				new KeyObjectiveType( "energy", ValueType.realType( 0.0, 9999999 ), "joule", SearchDirection.MINIMIZING ), 
				new KeyObjectiveType( "makespan", ValueType.realType( 0.0, 10000000 ), "sec", SearchDirection.MINIMIZING )
			); 
		
		ConfigurationType ct = new ConfigurationType.Explicit(keyObjectiveMetricTypes,controlledMetricTypes,observableMetricTypes);
		return new SimplePlant(graph,ct);
	}

	///////////////////////////////

	
	private static void addRemainingLevels(
			Graph graph,
			ConfigurationPG configurationPG,
			List< ControlledMetricType > controlledMetricTypes,
			List< ObservableMetricType > observableMetricTypes,
			Map <String, List<String> > controlledMetricTypesMap,
			List<List<Mode> > listMachines,
			List<Vertex> parentNodes,
			int noOfAddedVertices, 
			int noOfAddedInternalLevels,
			String graphvizSource,
			Random rnd 
			) {
		
		Set<String> edgeNames = new HashSet<String>();
		
		
		int noVertexNextLevel = configurationPG.getMinNoOfVerticesPerInternalLevel() + rnd.nextInt(configurationPG.getMaxNoOfVerticesPerInternalLevel() - configurationPG.getMinNoOfVerticesPerInternalLevel() +1);

		noOfAddedInternalLevels++;
		int currentLevel=noOfAddedInternalLevels;

		if(currentLevel==configurationPG.getNoOfInternalLevels()+1) {
			noVertexNextLevel=1;	//sink
		}
		
		List<Vertex> listVertexNextLevel = new ArrayList<Vertex>();
		
		int noVertexToBeAdded=noVertexNextLevel;
		while(noVertexToBeAdded-- > 0) {
			Vertex vertex = new Vertex("v"+Integer.toString(noOfAddedVertices++),noOfAddedInternalLevels);
			listVertexNextLevel.add(vertex);
			graph.addVertex(vertex);
			if(currentLevel==configurationPG.getNoOfInternalLevels()+1) {
				graph.setSinkName(vertex.getName());
			}

		}

		
		List<Vertex> parentNodesNotYetUsed = new ArrayList<Vertex>(parentNodes);
		List<Vertex> childNodesNotYetUsed = new ArrayList<Vertex>(listVertexNextLevel);
		
		
		while(parentNodesNotYetUsed.size()>0 || childNodesNotYetUsed.size()>0) {
		
			
			List<Mode> machine=listMachines.get(rnd.nextInt(configurationPG.getNoOfMachines()));
			Vertex parentNode;
			Vertex childNode;
			String edgeName;
			do {
				parentNode=parentNodes.get(rnd.nextInt(parentNodes.size()));
				childNode=listVertexNextLevel.get(rnd.nextInt(listVertexNextLevel.size()));
				edgeName=parentNode.getName() + "-" + childNode.getName();
			} while(edgeNames.contains(edgeName));
			
			parentNodesNotYetUsed.remove(parentNode);
			childNodesNotYetUsed.remove(childNode);
			
			edgeNames.add(edgeName);
			
			int nominalDuration = configurationPG.getMinNominalDuration() +  rnd.nextInt(configurationPG.getMaxNominalDuration()-configurationPG.getMinNominalDuration()+1);

			double nominalEnergyPerTimeUnit = configurationPG.getMinEnergyPerTimeUnit() + (configurationPG.getMaxEnergyPerTimeUnit() - configurationPG.getMinEnergyPerTimeUnit()) * rnd.nextDouble();
			
			String controlMetricsName = "Mode " + edgeName;
			String observableMetricExecutionTimeName = "execution time " + edgeName;	
			String observableMetriEnergyName = "energy " + edgeName;
			// Swan: 4th Dec 2017			
//			String controlMetricsName = "Mode " + edgeName + "_" + controlMetricsNameSuffix;
//			String observableMetricExecutionTimeName = "execution time " + edgeName + "_" + observableMetricExecutionTimeNameSuffix;	
//			String observableMetriEnergyName = "energy " + edgeName + "_" + observableMetriEnergyNameSuffix;	
//			controlMetricsNameSuffix += 1;
//			observableMetricExecutionTimeNameSuffix += 1;
//			observableMetriEnergyNameSuffix += 1;
			// End Swan: 4th Dec 2017
			
			String [] modeTypes = new String [machine.size()];
			int index=0;
			for(Mode mode: machine) {
				modeTypes[index++]=mode.getName();
			}

			ControlledMetricType controlledMetricType = new ControlledMetricType( controlMetricsName, ValueType.nominalType( controlMetricsName, modeTypes ), "#" );
			controlledMetricTypes.add(controlledMetricType);
			controlledMetricTypesMap.put(controlMetricsName, Arrays.asList(modeTypes));


			
			
			observableMetricTypes.add(new ObservableMetricType( observableMetricExecutionTimeName, ValueType.intType( 0, Integer.MAX_VALUE-1 ), "sec", SampleRate.eventDriven));
			observableMetricTypes.add(new ObservableMetricType( observableMetriEnergyName, ValueType.realType( 0.0, Double.MAX_VALUE ), "joule", SampleRate.eventDriven));

			
			List<Mode> modeList = new ArrayList<Mode>();
			for(Mode mode: machine) {
				modeList.add(mode);
			}

			Edge outgoingEdge = new Edge(parentNode.getName(),childNode.getName(),edgeName,nominalDuration,nominalEnergyPerTimeUnit,machine.get(0),controlMetricsName,modeList);
			

			graph.addOutgoingEdge(parentNode, outgoingEdge);
			graph.addIngoingEdge(childNode, outgoingEdge);
			graphvizSource+="  "+ parentNode.getName() + " -> " + childNode.getName() + ";\n";
		}

		if(currentLevel!=configurationPG.getNoOfInternalLevels()+1) {
			addRemainingLevels(
					graph,
					configurationPG,
					controlledMetricTypes,
					observableMetricTypes,
					controlledMetricTypesMap,
					listMachines,
					listVertexNextLevel,
					noOfAddedVertices,
					noOfAddedInternalLevels,
					graphvizSource,
					rnd
					);

		}
	}
	///////////////////////////////	
	
	private static String getModeName(int machineIndex,int modeIndex) {
		String strModeName = "_machine_"+Integer.toString(machineIndex) + "_mode_"+Integer.toString(modeIndex);
		return strModeName;		
	}
	
	private static void generateMachines(
			ConfigurationPG configurationPG,
			List<List<Mode> > listMachines,
			Random rnd) {
		
		int machineIndex = 0;
		while(machineIndex<configurationPG.getNoOfMachines()) {
			
			int noOfModesToBeAdded=rnd.nextInt(configurationPG.getMaxNoOfModesPerMachine()-1)+1;
			
			if(noOfModesToBeAdded==1) {
				noOfModesToBeAdded++;
			}
			
			List<Mode> listModes=new ArrayList<Mode>();
			int modeIndex =0;
			while(modeIndex<noOfModesToBeAdded) {
				double durationMultiplier = configurationPG.getMinDurationMultiplier() + (configurationPG.getMaxDurationMultiplier() - configurationPG.getMinDurationMultiplier()) * rnd.nextDouble();
				double energyMultiplier = configurationPG.getMinEnergyMultiplier() + (configurationPG.getMaxEnergyMultiplier() - configurationPG.getMinEnergyMultiplier()) * rnd.nextDouble();

				Mode mode = new Mode(getModeName(machineIndex,modeIndex),durationMultiplier,energyMultiplier);
				listModes.add(mode);
				modeIndex++;
			}		
			listMachines.add(listModes);
			machineIndex++;
		}
	}
	
	///////////////////////////////
	
	private static void addFirstAndNextLevels(
			Graph graph,
			List< ControlledMetricType > controlledMetricTypes,
			List< ObservableMetricType > observableMetricTypes,
			Map <String, List<String> > controlledMetricTypesMap,
			ConfigurationPG configurationPG,
			String graphvizSource,
			Random rnd) {

		int noOfAddedVertices=0;
		int noOfAddedInternalLevels=0;

		List<List<Mode> > listMachines=new ArrayList<List<Mode> >();
		
		generateMachines(configurationPG,listMachines,rnd);
		String rootName="v0";
		
		Vertex root = new Vertex(rootName,0);
		graph.addVertex(root);
		noOfAddedVertices=1;
		graph.setRootName(rootName);
		
		List<Vertex> listVertexLevel0 = new ArrayList<Vertex>();
		listVertexLevel0.add(root);
		
		addRemainingLevels(
				graph,
				configurationPG,
				controlledMetricTypes,
				observableMetricTypes,
				controlledMetricTypesMap,
				listMachines,
				listVertexLevel0,
				noOfAddedVertices,
				noOfAddedInternalLevels,
				graphvizSource,
				rnd
				);
	}

	///////////////////////////////
	

	public static void main(String[] args) {


		Random rng = new Random(0xDEADBEEF);
		

		SimplePlant plant = PlantGenerator.generatePlant(
				1, // noOfInternalLevels
				1, //int minNoOfVerticesPerInternalLevel 
				3, //int maxNoOfVerticesPerInternalLevel 
				3, //int noOfMachines
				10, //int maxNoOfModesPerMachine
				1, //int minNominalDuration
				20, //int maxNominalDuration 
				1.0, //double minEnergyPerTimeUnit 
				10.0, //double maxEnergyPerTimeUnit
				0.1, //double minEnergyMultiplier
				2.0, //double maxEnergyMultiplier
				0.2, //double minDurationMultiplier
				3.0, //double maxDurationMultiplier
				rng);
		
		
		JsonConverter jsonConverter = new JsonConverter();
		String strGraph= jsonConverter.toJson(plant.getGraph());
		System.out.println(strGraph);
		System.out.println("");


		
		
		
		int minMakespan=plant.cpGetMinMakespan();
		System.out.println("Min ending time:" + minMakespan);

		double minEnergy=plant.cpGetMinEnergy();
		System.out.println("Min energy:" + minEnergy);
		
		List< List< ObjectiveValue > > paretoFront=plant.cpGetParetoFront();

		System.out.println("Points in Pareto front: " + paretoFront.size());
		for (List< ObjectiveValue > objectiveValueList: paretoFront) {
			for(ObjectiveValue objectiveValue: objectiveValueList) {
				System.out.println(objectiveValue.toString());
			}
		}
	
		System.out.println("Ending time for all modes set to the 0th: " + plant.getGraph().getEndingTime());
	
		
		
		System.out.println("All done!");
	}
}

// End ///////////////////////////////////////////////////////////////
