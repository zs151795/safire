package uk.ac.york.safire.optimisation.maxplusplant;

public class ObjectiveValue {
    public final String name;
    public final double value;

    public ObjectiveValue(String name, double value) {
        this.name = name;
        this.value = value;
    }
    
    @Override
    public String toString() {
        return "Name:" + name + ", " + "Value:" + value ;
    }
}
