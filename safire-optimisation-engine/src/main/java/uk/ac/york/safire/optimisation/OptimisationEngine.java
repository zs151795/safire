package uk.ac.york.safire.optimisation;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import io.vavr.control.Either;
import uk.ac.york.aura.PopulationEntry;
import uk.ac.york.safire.metrics.JsonConverter;
import uk.ac.york.safire.metrics.OptimisationArguments;
import uk.ac.york.safire.metrics.OptimisationResult;

///////////////////////////////////

public interface OptimisationEngine {
	
	public List<PopulationEntry> lastgeneration  = new ArrayList<PopulationEntry>();
	
	public OptimisationResult optimise(OptimisationArguments args);
	
	///////////////////////////////

	public static class DoNothingOptimisationEngine implements OptimisationEngine {

		@Override
		public OptimisationResult optimise(OptimisationArguments args) {
			return new OptimisationResult(args.getConfigurations());
		}
	}
	
	///////////////////////////////
	
	public static abstract class LocalOptimisationEngine implements OptimisationEngine {
		
		private final ObjectiveFunction objectiveFunction;
		
		///////////////////////////
		
		public LocalOptimisationEngine(ObjectiveFunction objectiveFunction) {
			this.objectiveFunction = objectiveFunction;
		}
		
		///////////////////////////		

		public ObjectiveFunction getObjectiveFunction() {
			return objectiveFunction;
		}
	}
	
	///////////////////////////////

	public static final class RemoteHttpJsonOptimisationEngine implements OptimisationEngine {

		private JsonConverter jsonConverter = new JsonConverter();
		private URL url;
		
		///////////////////////////

		private static Either< String, String > 
		httpRequest( URL url, String body) throws IOException {
			
			HttpClient client = HttpClientBuilder.create().build();		
			HttpPost post = new HttpPost(url.toString());
			post.setHeader("Accept", "application/json");
			post.setHeader("Content-type", "application/json");		
			HttpEntity requestEntity = new StringEntity(body,ContentType.APPLICATION_JSON);
			post.setEntity( requestEntity );
			HttpResponse response = client.execute(post);
			HttpEntity responseEntity = response.getEntity();
			String responseStr = responseEntity == null ? response.getStatusLine().toString() :  EntityUtils.toString(responseEntity);
			
			final int status = response.getStatusLine().getStatusCode();
			if( status >= 200 && status < 300 ) {
				return Either.right( responseStr );
			}
			else {
				return Either.left( responseStr );			
			}
		}

		///////////////////////////
		
		public RemoteHttpJsonOptimisationEngine(URL url) {
			this.url = url;
		}
		
		///////////////////////////		
		
		@Override
		public OptimisationResult optimise(OptimisationArguments args) {
			// if( !Configuration.equivalentConfigurations( pop ) ) throw new IllegalArgumentException();		

			jeep.lang.Diag.println( "population size:" + args.getConfigurations().size() );
			
			final long serialisationStartTime = System.currentTimeMillis();			
			String json = jsonConverter.toJson(args);
			final long serialisationEndTime = System.currentTimeMillis();
			jeep.lang.Diag.println( "JSON serialisation time (millis):" + ((serialisationEndTime - serialisationStartTime)) );
			
			try {
				Either< String, String > response = httpRequest( new URL( url.toString() + "/apply/" ), json );
				if( response.isLeft() ) {
					// TODO: proper error handling
					jeep.lang.Diag.println( "Optimisation engine response: <<" + response + ">>" );
					// Do nothing, i.e. just return the argument
					// return new OptimisationResult( config.getControlledMetrics() );
					return new OptimisationResult( args.getConfigurations() );
				}
				else {
					String responseStr = response.get();
// jeep.lang.Diag.println( responseStr );
					final long startTime = System.currentTimeMillis();
					OptimisationResult result = jsonConverter.fromJson(responseStr, OptimisationResult.class );
					final long endTime = System.currentTimeMillis();
					jeep.lang.Diag.println( "JSON deserialisation time (millis):" + ((endTime - startTime)) );					
					// jeep.lang.Diag.println( result );
					// jeep.lang.Diag.println( result.getControlledMetrics().equals( currentConfig.getControlledMetrics() ) );
					return result;
				}
			}
			catch( IOException ioe ) {
				// TODO: proper error handling				
				jeep.lang.Diag.println( "Error: <<" + ioe.getMessage() + ">>" );				
				// Do nothing, i.e. just return the existing controlled metrics
				// return new OptimisationResult( config.getControlledMetrics() );
				return new OptimisationResult( args.getConfigurations() );
			}
		}
	}
}

// End ///////////////////////////////////////////////////////////////
