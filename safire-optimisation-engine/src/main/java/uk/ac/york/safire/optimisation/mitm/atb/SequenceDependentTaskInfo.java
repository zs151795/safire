package uk.ac.york.safire.optimisation.mitm.atb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.tuple.Triple;

import scala.Option;
import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.optimisation.ia.Resource;
import uk.ac.york.safire.optimisation.ia.SequenceDependentSetup;
import uk.ac.york.safire.optimisation.ia.Task;
import uk.ac.york.safire.optimisation.ia.TaskId;
import uk.ac.york.safire.optimisation.mitm.atb.ATBSimulatorKafkaProducer.RecipeInfo;

///////////////////////////////////

public final class SequenceDependentTaskInfo {

	public final int duration;
	public final int energyCost;
	public final int montaryCost;
	public final String resource;
	public final String lastTaskPrefix;
	public final String nextTaskPrefix;
	public final String id;

	///////////////////////////

	public static List<SequenceDependentTaskInfo> makeRandomSequenceDependantTasksOAS(List<RecipeInfo> recipes,
			String[] resourceNames, double density, int maxDuration, int maxEnergyCost, int maxMontaryCost,
			Random rng) {
		if (density < 0.0 || density > 1.0)
			throw new IllegalArgumentException();

		// Collect all (ordered) pairs of recipes that have compatible resources in
		// common:
		final List<Triple<RecipeInfo, RecipeInfo, Set<String>>> sharedResources = new ArrayList<>();
		for (int i = 0; i < recipes.size(); ++i) {
			for (int j = 0; j < recipes.size(); ++j) {
				if (j == i || (recipes.get(i).name.split("")[0].equals(recipes.get(j).name.split("")[0])
						&& recipes.get(i).name.split("")[1].equals(recipes.get(j).name.split("")[1])))
					continue;

				final RecipeInfo ri = recipes.get(i);
				final RecipeInfo rj = recipes.get(j);

				final List<List<String>> icompatNestedList = ri.compatibleResources.stream().filter(r -> r != 60)
						.map(r -> Arrays.asList(resourceNames[r].split(" "))).collect(Collectors.toList());
				final Set<String> icompat = icompatNestedList.stream().flatMap(List::stream)
						.filter(r -> r.contains("(")).collect(Collectors.toSet());

				final List<List<String>> jcompatNestedList = rj.compatibleResources.stream().filter(r -> r != 60)
						.map(r -> Arrays.asList(resourceNames[r].split(" "))).collect(Collectors.toList());
				final Set<String> jcompat = jcompatNestedList.stream().flatMap(List::stream)
						.filter(r -> r.contains("(")).collect(Collectors.toSet());
				icompat.retainAll(jcompat);
				if (!icompat.isEmpty())
					sharedResources.add(Triple.of(ri, rj, icompat));
			}
		}

		final int numSelections = (int) (density * sharedResources.size());
		Collections.shuffle(sharedResources, rng);
		final List<Triple<RecipeInfo, RecipeInfo, Set<String>>> selected = sharedResources.subList(0, numSelections);

		final List<SequenceDependentTaskInfo> result = new ArrayList<>();
		for (int i = 0; i < selected.size(); ++i) {

			final RecipeInfo ri = selected.get(i).getLeft();
			final RecipeInfo rj = selected.get(i).getMiddle();
			final Set<String> compatibleResources = selected.get(i).getRight();

			// final String resource = resourceNames[
			// compatibleResources.stream().collect(Collectors.toList()).get(
			// rng.nextInt(compatibleResources.size() ) ) ];
			for (String resource : compatibleResources) {
				final String taskId = "DependentSetUp from " + ri.name + " to " + rj.name;
				result.add(new SequenceDependentTaskInfo(maxDuration, maxEnergyCost, maxMontaryCost, resource, ri.name,
						rj.name, taskId));
			}
		}

		return result;
	}

	public static SequenceDependentSetup makeSequenceDependantSetupElectrolux(List<SequenceDependentTaskInfo> sdTasks,
			Configuration config) {
		return new SequenceDependentSetup() {
			@Override
			public Option<Task> apply(Resource resource, Task last, Task next) {
				final String[] lastTokens = last.id().name().split(" ");
				final String[] nextTokens = next.id().name().split(" ");

				String[] lastTokensProcessed = Arrays.copyOf(lastTokens, lastTokens.length - 1);
				String[] nextTokensProcessed = Arrays.copyOf(nextTokens, nextTokens.length - 1);

				String lastTaskName = String.join(" ", lastTokensProcessed);
				String nextTaskName = String.join(" ", nextTokensProcessed);

				List<String> resourceNames = Arrays.asList(resource.id().split(" "));

				List<String> taskLastResource = Arrays.asList(last.getResource().id().split(" "));
				List<String> taskNextResource = Arrays.asList(next.getResource().id().split(" "));

				List<String> sameName = new ArrayList<String>();
				for (int i = 0; i < taskLastResource.size(); i++) {
					for (int j = 0; j < taskNextResource.size(); j++) {
						String lastResource = taskLastResource.get(i);
						String nextResource = taskNextResource.get(j);
						if (lastResource.equals(nextResource) && !lastResource.equals("and")
								&& !StringUtils.isNumeric(lastResource)) {
							sameName.add(lastResource);
						}
					}
				}

				List<String> sharedResources = new ArrayList<>();
				for (String rawName : sameName) {
					for (String resName : resourceNames) {
						if (resName.equals(rawName))
							sharedResources.add(resName);
					}
				}

				final String nameForCompare = sharedResources.get(0);

				final List<SequenceDependentTaskInfo> matchingTasks = sdTasks == null ? new ArrayList<>()
						: sdTasks.stream()
								.filter(sdt -> sdt.lastTaskPrefix.equals(lastTaskName)
										&& sdt.nextTaskPrefix.equals(nextTaskName)
										&& sdt.resource.equals(nameForCompare))
								.collect(Collectors.toList());

				// sdt.resource.equals(dependentTaskResourceName)

				if (matchingTasks.isEmpty())
					return Option.empty();
				else {
					assert (matchingTasks.size() == 1);
					final SequenceDependentTaskInfo sdt = matchingTasks.get(0);

//					List<String> processedNames = new ArrayList<>();
//					for (String name : sharedResources) {
//						String processedNameRaw = name.toLowerCase();
//						String processedName = "";
//
//						for (int i = 0; i < processedNameRaw.length(); i++) {
//							char c = processedNameRaw.charAt(i);
//
//							if (!Character.isDigit(c) && c != '(' && c != ')') {
//								processedName += c;
//							}
//						}
//
//						processedNames.add(processedName);
//					}

					String dependentTaskResourceName = String.join("_and_", sharedResources);

					return Option.apply(Task.sequenceDependentFromJavaHelper(
							new TaskId(sdt.id + " [UID:" + System.nanoTime() + "]" + " " + dependentTaskResourceName),
							Collections.singletonMap(resource, sdt.duration), sdt.energyCost, sdt.montaryCost));
				}
			}
		};
	}

	public static SequenceDependentSetup makeSequenceDependantSetupOAS(List<SequenceDependentTaskInfo> sdTasks,
			Configuration config) {
		return new SequenceDependentSetup() {
			@Override
			public Option<Task> apply(Resource resource, Task last, Task next) {
				final String[] lastTokens = last.id().name().split(" ");
				final String[] nextTokens = next.id().name().split(" ");

				List<String> resourceNames = Arrays.asList(resource.id().split(" "));
				List<String> taskLastResource = Arrays.asList(lastTokens[lastTokens.length - 1].split("_"));

				List<String> taskNextResource = Arrays.asList(nextTokens[nextTokens.length - 1].split("_"));

				List<String> sameName = new ArrayList<String>();
				for (int i = 0; i < taskLastResource.size(); i++) {
					for (int j = 0; j < taskNextResource.size(); j++) {
						String lastResource = taskLastResource.get(i);
						String nextResource = taskNextResource.get(j);
						if (lastResource.equals(nextResource) && !lastResource.equals("and")
								&& !StringUtils.isNumeric(lastResource)) {
							sameName.add(lastResource);
						}
					}
				}

				List<String> sharedResources = new ArrayList<>();
				for (String rawName : sameName) {
					for (String resName : resourceNames) {

						if (resName.toLowerCase().contains(rawName))
							sharedResources.add(resName);
					}
				}

				assert (sharedResources.size() == 1 || sharedResources.size() == 2);

				String nameForCompareTemp = "";

				if (sharedResources.size() > 1) {
					for (String res : sharedResources) {
						if (!res.contains("Mixer"))
							nameForCompareTemp = res;
					}
				} else {
					nameForCompareTemp = sharedResources.get(0);
				}

				final String nameForCompare = nameForCompareTemp;

				final List<SequenceDependentTaskInfo> matchingTasks = sdTasks == null ? new ArrayList<>()
						: sdTasks.stream().filter(sdt -> sdt.lastTaskPrefix
								.equals(lastTokens[0] + " " + lastTokens[1] + " " + lastTokens[2])
								&& sdt.nextTaskPrefix.equals(nextTokens[0] + " " + nextTokens[1] + " " + nextTokens[2])
								&& sdt.resource.equals(nameForCompare)).collect(Collectors.toList());

				// sdt.resource.equals(dependentTaskResourceName)

				if (matchingTasks.isEmpty())
					return Option.empty();
				else {
					assert (matchingTasks.size() == 1);

					final SequenceDependentTaskInfo sdt = matchingTasks.get(0);

					List<String> processedNames = new ArrayList<>();
					for (String name : sharedResources) {
						String processedNameRaw = name.toLowerCase();
						String processedName = "";

						for (int i = 0; i < processedNameRaw.length(); i++) {
							char c = processedNameRaw.charAt(i);

							if (!Character.isDigit(c) && c != '(' && c != ')') {
								processedName += c;
							}
						}

						processedNames.add(processedName);
					}

					String dependentTaskResourceName = String.join("_and_", processedNames);

					return Option.apply(Task.sequenceDependentFromJavaHelper(
							new TaskId(sdt.id + " [UID:" + System.nanoTime() + "]" + " " + dependentTaskResourceName),
							Collections.singletonMap(resource, sdt.duration), sdt.energyCost, sdt.montaryCost));
				}
			}
		};
	}

	public static List<SequenceDependentTaskInfo> makeRandomSequenceDependantTasks(List<RecipeInfo> recipes,
			String[] resourceNames, double density, int maxDuration, int maxEnergy, int maxMontary, Random rng) {
		if (density < 0.0 || density > 1.0)
			throw new IllegalArgumentException();

		// Collect all (ordered) pairs of recipes that have compatible resources in
		// common:
		final List<Triple<RecipeInfo, RecipeInfo, Set<Integer>>> sharedResources = new ArrayList<>();
		for (int i = 0; i < recipes.size(); ++i) {
			for (int j = 0; j < recipes.size(); ++j) {
				if (j == i)
					continue;

				final RecipeInfo ri = recipes.get(i);
				final RecipeInfo rj = recipes.get(j);
				final Set<Integer> icompat = ri.compatibleResources.stream().collect(Collectors.toSet());
				final Set<Integer> jcompat = rj.compatibleResources.stream().collect(Collectors.toSet());
				icompat.retainAll(jcompat);
				if (!icompat.isEmpty())
					sharedResources.add(Triple.of(ri, rj, icompat));
			}
		}

		final int numSelections = (int) (density * sharedResources.size());
		Collections.shuffle(sharedResources, rng);
		final List<Triple<RecipeInfo, RecipeInfo, Set<Integer>>> selected = sharedResources.subList(0, numSelections);

		final List<SequenceDependentTaskInfo> result = new ArrayList<>();
		for (int i = 0; i < selected.size(); ++i) {

			final RecipeInfo ri = selected.get(i).getLeft();
			final RecipeInfo rj = selected.get(i).getMiddle();
			final Set<Integer> compatibleResources = selected.get(i).getRight();

			// final String resource = resourceNames[
			// compatibleResources.stream().collect(Collectors.toList()).get(
			// rng.nextInt(compatibleResources.size() ) ) ];
			for (String resource : resourceNames) {
				final String taskId = "idle " + ri.name + " " + rj.name;
				result.add(new SequenceDependentTaskInfo(maxDuration, maxEnergy, maxMontary, resource, ri.name, rj.name,
						taskId));
			}
		}

		return result;
	}

	public static SequenceDependentSetup makeSequenceDependantSetup(List<SequenceDependentTaskInfo> sdTasks) {
		return new SequenceDependentSetup() {
			@Override
			public Option<Task> apply(Resource resource, Task last, Task next) {
				final String[] lastTokens = last.id().name().split(" ");
				final String[] nextTokens = next.id().name().split(" ");
				// jeep.lang.Diag.println( "last: " + Arrays.toString( lastTokens ) + "next: " +
				// Arrays.toString( nextTokens ) + " resource: " + resource );
				// System.exit(0);
				final List<SequenceDependentTaskInfo> matchingTasks = sdTasks.stream()
						.filter(sdt -> sdt.lastTaskPrefix.equals(lastTokens[0])
								&& sdt.nextTaskPrefix.equals(nextTokens[0]) && sdt.resource.equals(resource.id()))
						.collect(Collectors.toList());

				if (matchingTasks.isEmpty())
					return Option.empty();
				else {
					assert (matchingTasks.size() == 1);
					final SequenceDependentTaskInfo sdt = matchingTasks.get(0);
					return Option.apply(
							Task.sequenceDependentFromJavaHelper(new TaskId(sdt.id + "[UID:" + System.nanoTime() + "]"),
									Collections.singletonMap(resource, sdt.duration), sdt.energyCost, sdt.montaryCost));
				}
			}
		};
	}
	///////////////////////////

	public SequenceDependentTaskInfo(int duration, int energyCost, int montaryCost, String resource,
			String lastTaskPrefix, String nextTaskPrefix, String id) {
		this.duration = duration;
		this.energyCost = energyCost;
		this.montaryCost = montaryCost;

		this.resource = resource;
		this.lastTaskPrefix = lastTaskPrefix;
		this.nextTaskPrefix = nextTaskPrefix;
		this.id = id;
	}

	@Override
	public boolean equals(Object rhs) {
		return org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals(this, rhs);
	}

	@Override
	public int hashCode() {
		return org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}

	public String getInfo() {
		return id + " " + lastTaskPrefix + " " + nextTaskPrefix + " " + resource + " " + energyCost + " " + duration;
	}

	public String getFullInfo() {
		return "ID: " + id + " resource: " + resource + " duration: " + duration + " energy: " + energyCost
				+ " montary: " + montaryCost;
	}
};

// End ///////////////////////////////////////////////////////////////
