package uk.ac.york.safire.optimisation.mitm.atb;

public class FormattedResultOAS {

	String year;
	String month;
	String day;
	String startTime;
	String endTime;
	String endTime1;

	String id;
	String productName;
	String recipeType;
	String instanceNumber;
	String amountProduced;
	String amountProduced1;

	String mixerID;
	String mixerName;
	String mixerStatus;

	public FormattedResultOAS(String year, String month, String day, String startTime, String endTime, String id,
			String productName, String recipeType, String instanceNumber, String amountProduced, String mixerID,
			String mixerName, String mixerStatus) {

		this.year = year;
		this.month = month;
		this.day = day;
		this.startTime = startTime;
		this.endTime = endTime;
		this.endTime1 = endTime;

		this.id = id;
		this.productName = productName;
		this.recipeType = recipeType;
		this.instanceNumber = instanceNumber;
		this.amountProduced = amountProduced;
		this.amountProduced1 = amountProduced;

		this.mixerID = mixerID;
		this.mixerName = mixerName;
		this.mixerStatus = mixerStatus;

	}

	public String formattedOut(boolean lastOne) {
		String out = "{\n";

		out += "\"JAHR\": \"" + year + "\",\n";
		out += "\"MONAT\": \"" + month + "\",\n";
		out += "\"TAG\": \"" + day + "\",\n";
		out += "\"ORD_START_TIME\": \"" + startTime + "\",\n";
		out += "\"ENDE_TIME\": \"" + endTime + "\",\n";
		out += "\"PROD_ENDE_TIME\": \"" + endTime1 + "\",\n";

		out += "\"BATCH_ID\": \"" + id + "\",\n";
		out += "\"PROD_ID\": \"" + productName + "\",\n";
		out += "\"REZ_ID\": \"" + recipeType + "\",\n";
		out += "\"INSTANZ_NUMMER\": \"" + instanceNumber + "\",\n";
		out += "\"CHG_SOLL\": \"" + amountProduced + "\",\n";
		out += "\"CHG_IST\": \"" + amountProduced1 + "\",\n";
		out += "\"LINIE\": \"" + mixerID + "\",\n";
		out += "\"M_NAME\": \"" + mixerName + "\",\n";
		out += "\"M_STATUS\": \"" + mixerStatus + "\"\n";
		if (lastOne)
			out += "}\n";
		else
			out += "},\n";

		return out;
	}
}
