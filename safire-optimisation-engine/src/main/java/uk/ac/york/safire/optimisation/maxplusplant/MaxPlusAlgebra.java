package uk.ac.york.safire.optimisation.maxplusplant;

// package ac.uk.york.safire.metrics.test.plant;

public class MaxPlusAlgebra  {

	private static int max(int lhs,int rhs) {
		return lhs > rhs ? lhs : rhs;
	}
	
	
	public static int add(int lhs, int rhs) {
		return max(lhs,rhs); 
	}

	public static int mult(int lhs, int rhs) {
		return lhs+rhs;
	}

	public static int getNeutralAdd() {
		return Integer.MIN_VALUE;
	}

	public static int getNeutralMult() {
		return 0;
	}

}

// End ///////////////////////////////////////////////////////////////
