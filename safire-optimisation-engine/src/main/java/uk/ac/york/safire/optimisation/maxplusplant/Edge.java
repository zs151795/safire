package uk.ac.york.safire.optimisation.maxplusplant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class Edge {

	private final String name;
	private final String sourceVertex;
	private final String destVertex;
	private final int nominalDuration;
	private final double nominalEnergyPerTimeUnit;
	private final String controlMetricsName;
	private transient Mode mode;
	private final List<Mode> modeList;
	
	///////////////////////////////
	
    public Edge(String sourceVertex, String destVertex, String name, int nominalDuration, double nominalEnergyPerTimeUnit, Mode mode, String controlMetricsName, List<Mode> modeList) {
        this.sourceVertex=sourceVertex;
        this.destVertex=destVertex;
        this.name=name;
        this.nominalDuration=nominalDuration;
        this.nominalEnergyPerTimeUnit=nominalEnergyPerTimeUnit;
        this.mode=mode;
        this.controlMetricsName=controlMetricsName;
        this.modeList = modeList;
    }

    ///////////////////////////////

	public String getSourceVertex() { return sourceVertex;	}
	public String getDestVertex() { return destVertex; }
	public int getNominalDuration() { return nominalDuration; }

	public double getNominalEnergyPerTimeUnit() { return nominalEnergyPerTimeUnit; }

	public int getRealProcessingTime() {

		return (int)Math.ceil((double)nominalDuration*mode.getDurationMultiplier());
		
	}

	public double getRealEnergy() {
		return mode.getEnergyMultiplier()*nominalEnergyPerTimeUnit;
	}

	public Mode getMode() { return mode; }
	public String getName() { return name; }

	///////////////////////////////

	public String getControlMetricsName() { return controlMetricsName; }
	
	public List<String> getListModeNames() {
		List<String> listModeNames = new ArrayList<String>();
		for(Mode mode: modeList) {
			listModeNames.add(mode.getName());
		}		
		return listModeNames;
	}	
	
	
	public int getNoOfModes() {
		return modeList.size();
	}
	
	public String getModeName(int index) {
		if(index < 0 || index >= modeList.size()) {
			throw new IllegalArgumentException();
		}
		return modeList.get(index).getName(); 
	}
	
//	private static String modeNames(List<Mode>  l) {
//		return l.stream().map( x -> x.getName() ).collect(Collectors.joining( "," )); 
//	}
	
	public void setCurrentModeByName(String modeName) {
		Iterator<Mode> modeListIterator = modeList.iterator();
		while (modeListIterator.hasNext()) {
			Mode current = modeListIterator.next(); 
			if(current.getName().equals(modeName)) {
				mode=current;
				return;
			}
		}
		throw new IllegalStateException("Mode name: " + modeName + " not found in " + getListModeNames() );
	}
	
	void appendExecutionTimesToList(List<Integer> list) {
		Iterator<Mode> it = modeList.iterator();
		while (it.hasNext()) {
			Mode current = it.next();
			list.add((int)Math.ceil(nominalDuration*current.getDurationMultiplier()));
		}
	}
	
	void appendEnergyDissipationToList(List<Double> list) {
		Iterator<Mode> it = modeList.iterator();
		while (it.hasNext()) {
			Mode current = it.next();
			list.add(nominalEnergyPerTimeUnit*current.getEnergyMultiplier()*nominalDuration*current.getDurationMultiplier());
		}
	}

	
	void appendEnergyDissipationToListInt(List<Integer> list,int enMultiplier) {
		Iterator<Mode> it = modeList.iterator();
		while (it.hasNext()) {
			Mode current = it.next();
			list.add((int)Math.ceil(enMultiplier*nominalEnergyPerTimeUnit*current.getEnergyMultiplier()*nominalDuration*current.getDurationMultiplier()));
		}
	}
	
	
	public double getHighestDurationMultiplier() {
		double result=0.0;
		for(Mode mode: modeList) {
			if(mode.getDurationMultiplier()>result)
				result=mode.getDurationMultiplier();
		}
		return result;
	}
	
	public double getHighestEnergyMultiplier() {
		double result=0.0;
		for(Mode mode: modeList) {
			if(mode.getEnergyMultiplier()>result)
				result=mode.getEnergyMultiplier();
		}
		return result;
	}
	
}

// End ///////////////////////////////////////////////////////////////
