package uk.ac.york.safire.optimisation.maxplusplant;

import java.util.Map;

import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.Value;

public interface Plant {
	
	public ConfigurationType getConfigurationType();
	public Configuration update(Configuration previousConfig, Map< String, Value > proposedControlledMetrics);
}

// End ///////////////////////////////////////////////////////////////

