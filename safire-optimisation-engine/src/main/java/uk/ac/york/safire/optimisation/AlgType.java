package uk.ac.york.safire.optimisation;

// ABYSS, CellDE, DMOPOSO, GDE3, MOEAD (+ variants), OMOPSO, SMPSO requires DoubleSolution

// WASFGA, IBEA, PAES, RNSGAII are possible

// MOCH(45) MOMBI(2) are EAs


public enum AlgType {
	GWASFGA, MOCELL, PESA2, SMSEMOA, SPEA2, NGSA2, NSGA3
}

// End ///////////////////////////////////////////////////////////////
