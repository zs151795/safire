package uk.ac.york.safire.optimisation;

import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.Value;
import uk.ac.york.safire.metrics.ValueVisitor.ValueStringVisitor;

public final class ConfigurationUtilities {

	public static String getStatusString(Configuration config) {
	    final Value status = config.getObservableMetrics().get("Optimisation Result"); 
	    
	    String statusString = null; 	    
	    try {
	    	statusString = new ValueStringVisitor().visit( status );	    	
	    }
	    catch (NullPointerException npe ) {
	    	return "Status: Unavailable";
	    }

	    return statusString.startsWith("Status: Failed") ? statusString : "Schedule: " + statusString; 
	}

}

// End ///////////////////////////////////////////////////////////////
