package uk.ac.york.safire.optimisation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.primitives.Ints;

///////////////////////////////////

public class ComeauBijectivePermutationHash {
	
	/**
	 * From:
	 * http://antoinecomeau.blogspot.com/2014/07/mapping-between-permutations-and.html
	 * https://stackoverflow.com/questions/1506078/fast-permutation-number-permutation-mapping-algorithms	
	 */
	
	private static boolean isPermutation(int[] perm, int n) {
		return IntStream.of(perm).allMatch( i -> i >= 0 && i < n)
			&& IntStream.of(perm).boxed().collect(Collectors.toSet()).size() == perm.length;
	}

	public static int factorial(int n) {
		if( n == 0 ) return 1;
		else return n * factorial(n-1);
	}
	
	public static double factorial(double n) {
		return n <= 1 ? n : n * factorial(n-1.0);
	}
	
	public static int inverseFactorial(int yy) {
		int x = 1; int y = yy;
    	while (y > 1) { 
    		x += 1; 
    		y /= x; 
    	}
    	return x;
	}
	
	///////////////////////////////
	
	public static int[] permutationOfIndex(int index, int n) {
		if( n < 0 ) throw new IllegalArgumentException();
		if( index < 0 || index >= factorial(n) ) throw new IllegalArgumentException("Invalid index: " + index + ", n: " + n );
		
		///////////////////////////
		
		int i, ind, m=index;
		int[] permuted = new int[n];
		int[] elems = new int[n];

		for(i=0;i<n;i++) elems[i]=i;

		for(i=0;i<n;i++) {
            ind=m%(n-i);
            m=m/(n-i);
            permuted[i]=elems[ind];
            elems[ind]=elems[n-i-1];
		}

		assert( isPermutation(permuted,n) );
		return permuted;
	}

	public static int indexOfPermutation(int [] perm) {
		if( !isPermutation(perm,perm.length) )
			throw new IllegalArgumentException("Permutation expected, found: " + Arrays.toString(perm) );
			
		int i, k=0, m=1;
		int n=perm.length;
		int[] pos = new int[n];
		int[] elems = new int[n];

		for(i=0;i<n;i++) {pos[i]=i; elems[i]=i;}

		for(i=0;i<n-1;i++) {
            k+=m*pos[perm[i]];
            m=m*(n-i);
            pos[elems[n-i-1]]=pos[perm[i]];
            elems[pos[perm[i]]]=elems[n-i-1];
		}

		return k;
	}

	///////////////////////////////
	
	public static void main(String [] args ) {

		for( int i=1; i<12; ++i ) {
			jeep.lang.Diag.println( i + ": " + inverseFactorial( factorial(i) ) );
			assert( inverseFactorial( factorial(i) ) == i );
		}
		
		Set< List< Integer > > allPerms = new HashSet< List< Integer > >();
		
		final int n = 6;
		for( int index=0; index<factorial(n); ++index ) {
			System.out.println( Arrays.toString( permutationOfIndex(index, n) ) );
			System.out.println( indexOfPermutation( permutationOfIndex(index, n) ) );
			assert( index == indexOfPermutation( permutationOfIndex(index, n) ) );
			
			allPerms.add( Ints.asList( permutationOfIndex(index, n) ));
		}
		
		assert( allPerms.size() == factorial(n) );
		System.out.println( "All done" );
	}
}

// End ///////////////////////////////////////////////////////////////
