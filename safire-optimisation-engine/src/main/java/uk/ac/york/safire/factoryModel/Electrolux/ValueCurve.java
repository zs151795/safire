package uk.ac.york.safire.factoryModel.Electrolux;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ValueCurve {

	public static double getQualityExact(List<Integer> finishTimes, int t1, double t2, double t3, int t4) {
		return qualityCal(finishTimes, t1, t2, t3, t4);
	}

	public static double getQuality(List<Integer> finishTimes, int t1, int t4, int deadline, int duration) {
		int t2 = deadline - duration;
		int t3 = deadline;
		return qualityCal(finishTimes, t1, t2, t3, t4);
	}

	private static double qualityCal(List<Integer> finishTimes, int t1, double t2, double t3, int t4) {
		double quality = 0;

		List<Double> qualities = new ArrayList<Double>();

		for (Integer t : finishTimes) {
			double q = getIndividualQuality(t, t1, t2, t3, t4);
			qualities.add(q);
			quality += q;
		}

		DecimalFormat df = new DecimalFormat("0.00");

		return Double.parseDouble(df.format(quality));
	}

	private static double getIndividualQuality(double time, double t1, double t2, double t3, double t4) {

		double quality = 2;
		if (time <= t1 || time >= t4)
			quality = 0;
		if (time>= t2 && time <= t3)
			quality = 1;

		if (time > t1 && time < t2)
			quality = (time - t1) / (t2 - t1);

		if (time > t3 && time < t4)
			quality = (t4 - time) / (t4 - t3);

		assert (quality <= 1);
		return 1 - quality;
	}
}
