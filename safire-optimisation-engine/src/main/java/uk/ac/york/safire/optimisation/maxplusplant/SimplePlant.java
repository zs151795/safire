package uk.ac.york.safire.optimisation.maxplusplant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.IntVar;

import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.Value;
import uk.ac.york.safire.metrics.ValueType;
import uk.ac.york.safire.optimisation.ObjectiveFunction;

///////////////////////////////////

public final class SimplePlant implements Plant {
	
	private Graph graph=null;
	private ConfigurationType configurationType = null;

	///////////////////////////////
	
	public boolean invariant() { return true; }
	
	SimplePlant(Graph graph, ConfigurationType ct) {
		this.graph=graph;
		this.configurationType=ct;
		assert( invariant() );
	}	
	
	@Override
	public ConfigurationType getConfigurationType() {
		return configurationType;
	}
	
	///////////////////////////////
	
	public ObjectiveFunction.Result fitness(Configuration configuration, 
		Map< String, Value > proposedControlledMetrics) {	

		configuration = evaluateFitnessFunction(configuration,proposedControlledMetrics);
		// double [] result = {0.0,0.0};
		
//		Value.Real valueEnergy=(Value.Real) configuration.getKeyObjectives().get("energy");
//        // result[0]=valueEnergy.value;
//
//		Value.Real valueMakespan=(Value.Real) configuration.getKeyObjectives().get("makespan");
//        // result[1]=valueMakespan.value;
		return new ObjectiveFunction.Result( configuration.getKeyObjectives(), "ObjectiveFunction status: OK" );
	}
	
//	private Configuration update(Configuration configuration) {	
//		return evaluateFitnessFunction(configuration);
//	}
	
	@Override
	public Configuration update(Configuration previousConfig, Map< String, Value > proposedControlledMetrics) {	
		// return evaluateFitnessFunction(Configuration.update(previousConfig,proposedControlledMetrics));
		return evaluateFitnessFunction(previousConfig,proposedControlledMetrics);		
	}

	public Graph getGraph() { return graph;	}
	public void setGraph(Graph graph) { this.graph = graph;	}
	
	///////////////////////////////
	
	private String extractEdgeNameFromObservableMetricName(String strObservableMetricName) {
		return strObservableMetricName.substring(strObservableMetricName.lastIndexOf(" ")+1);
	}
	
	
	private boolean isEnergyRelatedMetric(String strMetricName) {
		return strMetricName.contains("nergy");
	}
	
	private Edge getEdgeByName(String strEdgeName) {
		List<Edge> listEdge = graph.getListEdge();
		for(Edge edge: listEdge) {
			if(edge.getName().equals(strEdgeName)) {
				return edge;
			}
		}
		return null;
	}
	
	///////////////////////////////
	
	private Configuration evaluateFitnessFunction(Configuration config, Map< String, Value > proposedControlledMetrics ) {

		// JS: 17th Jan 2017 		
		// graph.setControlledMetrics(config);
		graph.setControlledMetrics(proposedControlledMetrics);		
		
        final Map< String, Value > observableMetrics = new HashMap<>();        
		ConfigurationType ct = config.getConfigurationType();
		
		for(int index=0;index < ct.getObservableMetrics().size();index++) {
			String strObservableMetricName=ct.getObservableMetrics().get(index).name;
			String strEdgeName=extractEdgeNameFromObservableMetricName(strObservableMetricName);
			Edge edge = getEdgeByName(strEdgeName);
			assert(edge!=null);
			if(isEnergyRelatedMetric(strObservableMetricName)) {
				observableMetrics.put(strObservableMetricName, 
						Value.realValue(edge.getRealEnergy(), (ValueType.Real)ct.getObservableMetrics().get(index).valueType));
			}
			else {
				observableMetrics.put(strObservableMetricName, 
						Value.intValue(edge.getRealProcessingTime(), (ValueType.Integer)ct.getObservableMetrics().get(index).valueType));
				
			}
		}

		///////////////////////////
		
		final Map< String, Value > keyObjectives = new HashMap<>();

		assert(graph.getTotalEnergy()!=-1.0);
		assert(graph.getEndingTime()!=-1);

		for(int index=0;index < ct.getKeyObjectiveMetrics().size();index++) {
			String strKeyObjectiveMetricName=ct.getKeyObjectiveMetrics().get(index).name;
			if(isEnergyRelatedMetric(strKeyObjectiveMetricName)) {
				keyObjectives.put(strKeyObjectiveMetricName, 
						Value.realValue(graph.getTotalEnergy(), (ValueType.Real)ct.getKeyObjectiveMetrics().get(index).valueType));
			}
			else {
				keyObjectives.put(strKeyObjectiveMetricName, 
						Value.realValue((double)graph.getEndingTime(), (ValueType.Real)ct.getKeyObjectiveMetrics().get(index).valueType));
			}
		}

		// JS: 17th Jan 2017 return new Configuration(ct, config.getControlledMetrics(), observableMetrics,keyObjectives);
		return new Configuration(ct, proposedControlledMetrics, observableMetrics, keyObjectives);		
    }
	
	public int[] toIntArray(List<Integer> list){
		  int[] ret = new int[list.size()];
		  for(int i = 0;i < ret.length;i++)
		    ret[i] = list.get(i);
		  return ret;
	}
	
	private void cpMaxIngoingEdgeTimes(Vertex sourceVertex,Model model,IntVar start_target,int indexEdge,int WCET,Map<String,IntVar> endTimesMap) {

		List<Edge> sourceVertexIngoingEdges=graph.getIngoingEdgesOfVertex(sourceVertex);
		if(sourceVertexIngoingEdges==null || sourceVertexIngoingEdges.isEmpty()) {
	        IntVar start = model.intVar("start", 0);
        	model.arithm(start_target,"=",start).post();  
        }
		else if(sourceVertexIngoingEdges.size()==1) {
        	String inEdgeName=sourceVertexIngoingEdges.get(0).getName();
        	IntVar inEdge = endTimesMap.get(inEdgeName);
        	model.arithm(start_target,"=",inEdge).post();  
        }
        else if(sourceVertexIngoingEdges.size()==2) {
        	String inEdgeName1=sourceVertexIngoingEdges.get(0).getName();
        	String inEdgeName2=sourceVertexIngoingEdges.get(1).getName();
        	IntVar inEdge1 = endTimesMap.get(inEdgeName1);
        	IntVar inEdge2 = endTimesMap.get(inEdgeName2);
            model.max(start_target, inEdge1, inEdge2).post();
        }
        else {
        	int indexIndomingEdge=0;
        	String inEdgeName1=sourceVertexIngoingEdges.get(indexIndomingEdge++).getName();
        	IntVar inEdge1 = endTimesMap.get(inEdgeName1);
        	IntVar inEdge2;
        	while(indexIndomingEdge<sourceVertexIngoingEdges.size()) {
	        	String inEdgeName2=sourceVertexIngoingEdges.get(indexIndomingEdge++).getName();
	        	inEdge2 = endTimesMap.get(inEdgeName2);

	        	IntVar inStartPartialSum = model.intVar("inStartPartialSum"+indexEdge+"_"+indexIndomingEdge, 0, WCET);
	        	if(indexIndomingEdge==sourceVertexIngoingEdges.size()) {
		        	model.max(start_target, inEdge1, inEdge2).post();
	        		
	        	}
	        	else {
	        		model.max(inStartPartialSum, inEdge1, inEdge2).post();
	        	}
        	
	        	inEdge1 = inStartPartialSum;
        	}
        	
        }	
	}
	
	private int[] cpGenerateRealEnergyDissipationForAllEndgesAndModes(int enMultiplier) {
		List<Integer> enList = new ArrayList<Integer>();
		List<Edge> edgeList=graph.getListEdge();
		Iterator<Edge> it = edgeList.iterator();
		
		while (it.hasNext()) {
			Edge edge = it.next();
			edge.appendEnergyDissipationToListInt(enList,enMultiplier);
		}	
		int[] en = toIntArray(enList);
		return en;
	}

	
	private int[] cpGenerateRealExecutionTimeArrayForAllEndgesAndModes() {
		List<Integer> etList = new ArrayList<Integer>();
		List<Edge> edgeList=graph.getListEdge();
		Iterator<Edge> it = edgeList.iterator();
		
		while (it.hasNext()) {
			Edge edge = it.next();
			edge.appendExecutionTimesToList(etList);
		}
		int[] et = toIntArray(etList);
		return et;
	}
	
	
	private void cpAddConstraints(Model model,int enMultiplier,IntVar start_sink,IntVar enTotalSum, int WCET, int WCEn) {

		List<IntVar> endTime = new ArrayList<IntVar>();
		List<IntVar> executionTime = new ArrayList<IntVar>();
		List<IntVar> energy = new ArrayList<IntVar>();
		List<IntVar> modes = new ArrayList<IntVar>();
		List<IntVar> enPartialSums = new ArrayList<IntVar>();	
		
		Map<String,IntVar> endTimesMap = new HashMap<String,IntVar>();
		

		
		
		int[] et = cpGenerateRealExecutionTimeArrayForAllEndgesAndModes();
		int[] en = cpGenerateRealEnergyDissipationForAllEndgesAndModes(enMultiplier);

		

		List<Edge> edgeList=graph.getListEdge();
		Iterator<Edge> it = edgeList.iterator();
		
		int e=0;
		int begEtEnIndex_e=0;
		
		while (it.hasNext()) {
			Edge edge = it.next();
			int endEtEnIndex_e=begEtEnIndex_e+edge.getNoOfModes()-1;

			
	        IntVar executionTime_e = model.intVar("et"+e, 0, WCET);
	        IntVar start_e = model.intVar("start"+e, 0, WCET);
	        IntVar end_e = model.intVar("end"+e, 0, WCET);
	        IntVar energy_e = model.intVar("en"+e, 0, WCEn);
			

			if(begEtEnIndex_e==endEtEnIndex_e) {
				IntVar mode_e = model.intVar("mode"+e,begEtEnIndex_e);	//just one mode exists for this edge
				modes.add(mode_e);
				model.element(executionTime_e,et,mode_e).post(); //executionTime_x=et[mode_x]
				model.element(energy_e,en,mode_e).post(); 	//energy_x=en[mode_x]
			}
			else {
				IntVar mode_e = model.intVar("mode"+e,begEtEnIndex_e,endEtEnIndex_e);	//a range of modes exists for this edge
				modes.add(mode_e);
				model.element(executionTime_e,et,mode_e).post(); //et0=et[mode0]
				model.element(energy_e,en,mode_e).post(); 
			}
			
		
	        model.arithm(end_e,"=",start_e,"+",executionTime_e).post();

			endTime.add(end_e);
			executionTime.add(executionTime_e);
			energy.add(energy_e);

	        IntVar enPartialSum_e = model.intVar("enPartialSum"+e, 0, WCEn);

			if(enPartialSums.isEmpty()) {
				model.arithm(enPartialSum_e,"=",energy_e).post();
			}
			else {
				model.arithm(enPartialSum_e,"=",energy_e,"+",enPartialSums.get(e-1)).post();
			}

	        enPartialSums.add(enPartialSum_e);
	        
	        String sourceVertexName = edge.getSourceVertex();
	        endTimesMap.put(edge.getName(), end_e);
	        Vertex sourceVertex=graph.getVertexByName(sourceVertexName);
			cpMaxIngoingEdgeTimes(sourceVertex,model,start_e,e,WCET,endTimesMap);
        
			begEtEnIndex_e=endEtEnIndex_e+1;
	        e++;
	    }		

		
		Vertex sinkVertex = graph.getVertexByName(graph.getSinkName());

		cpMaxIngoingEdgeTimes(sinkVertex,model,start_sink,e,WCET,endTimesMap);
        model.arithm(enTotalSum,"=",enPartialSums.get(enPartialSums.size()-1)).post();

		
	}
	
	
	double cpGetMinEnergy() {
		double result=-1.0;
		
		final int enMultiplier=100;
        Model model = new Model("Plant mode selection");
		int WCET=graph.getWCET();
		int WCEn=(int)Math.ceil(graph.getWCEn()*enMultiplier);
        IntVar executionTime = model.intVar("executionTime", 0, WCET);
        IntVar totalEnergy = model.intVar("totalEnergy", 0, WCEn);
        cpAddConstraints(model,enMultiplier,executionTime,totalEnergy,WCET,WCEn);
        
        Solver solver = model.getSolver();

        Solution solution = solver.findOptimalSolution(totalEnergy, Model.MINIMIZE);

        
        if(solver.isObjectiveOptimal() ) {
            result = (double)solution.getIntVal(totalEnergy)/enMultiplier;
//      		System.out.println("Energy optimal: "+ "energy: "+ result +", makespan: "+ solution.getIntVal(executionTime));
        }
        
        
        return result;
	}
	
	int cpGetMinMakespan() {
		int result=-1;
		
		final int enMultiplier=100;
        Model model = new Model("Plant mode selection");
		int WCET=graph.getWCET();
		int WCEn=(int)Math.ceil(graph.getWCEn()*enMultiplier);
        IntVar executionTime = model.intVar("executionTime", 0, WCET);
        IntVar totalEnergy = model.intVar("totalEnergy", 0, WCEn);
        cpAddConstraints(model,enMultiplier,executionTime,totalEnergy,WCET,WCEn);
        
        Solver solver = model.getSolver();

        Solution solution = solver.findOptimalSolution(executionTime, Model.MINIMIZE);

        if(solver.isObjectiveOptimal() ) {
            result = solution.getIntVal(executionTime);
//      		System.out.println("Makespan optimal: "+ "energy: "+ (double)solution.getIntVal(totalEnergy)/enMultiplier+", makespan: "+result);
          }
        return result;		
	}
	
	public List< List< ObjectiveValue > > cpGetParetoFront() {
		
		final int enMultiplier=100;
        Model model = new Model("Plant mode selection");
		int WCET=graph.getWCET();
		int WCEn=(int)Math.ceil(graph.getWCEn()*enMultiplier);
        IntVar executionTime = model.intVar("executionTime", 0, WCET);
        IntVar totalEnergy = model.intVar("totalEnergy", 0, WCEn);
        cpAddConstraints(model,enMultiplier,executionTime,totalEnergy,WCET,WCEn);
        
        Solver solver = model.getSolver();
//        solver.limitBacktrack(10);
        List<Solution> solutionList = solver.findParetoFront(new IntVar[]{totalEnergy,executionTime}, Model.MINIMIZE);

//        System.out.println("Points in Pareto front: " + solutionList.size());
//        for(Solution s:solutionList){
//        	System.out.print("energy: "+(double)s.getIntVal(totalEnergy)/enMultiplier);
//        	System.out.println(", makespan: "+(double)s.getIntVal(executionTime));
//        }
        
        List< List< ObjectiveValue > > result=new ArrayList< List< ObjectiveValue > >();
        for(Solution s:solutionList){
        	List< ObjectiveValue > solution = new ArrayList< ObjectiveValue >();  
        	ObjectiveValue objectiveValue1 = new ObjectiveValue("energy",(double)s.getIntVal(totalEnergy)/enMultiplier);
        	solution.add(objectiveValue1);
        	ObjectiveValue objectiveValue2 = new ObjectiveValue("makespan",(double)s.getIntVal(executionTime));
        	solution.add(objectiveValue2);
        	result.add(solution);
        }
        return result;
	
	}
}

// End ///////////////////////////////////////////////////////////////
