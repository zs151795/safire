package uk.ac.york.safire.optimisation.maxplusplant;

import org.apache.commons.lang3.builder.ToStringStyle;

///////////////////////////////////

public final class Mode {
	
	private String name;
	private double durationMultiplier;
	private double energyMultiplier;
	
	///////////////////////////////
	
	public Mode(String name, double durationMultiplier, double energyMultiplier) {
		this.name=name;
		this.durationMultiplier=durationMultiplier;
		this.energyMultiplier=energyMultiplier;
	}
	
	///////////////////////////////
	
	public String getName() { return name; }
	public double getDurationMultiplier() { return durationMultiplier; }
	public double getEnergyMultiplier() { return energyMultiplier; }
	
	///////////////////////////////	

	@Override
	public String toString() {
		return org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);			
	}
}

// End ///////////////////////////////////////////////////////////////
