package uk.ac.york.safire.factoryModel.ONA;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ValueCurve {

	public static int D = 25000;
	public static int Z = 35000;

	public static List<Double> getQualityList(List<Integer> finishTimes) {
		return qualityListCal(finishTimes);
	}

	private static List<Double> qualityListCal(List<Integer> finishTimes) {
		DecimalFormat df = new DecimalFormat("0.00");
		List<Double> qualities = new ArrayList<Double>();

		for (Integer t : finishTimes) {
			double q = Double.parseDouble(df.format(getIndividualQuality(t)));
			qualities.add(q);
		}

		return qualities;
	}

	public static double getQualityExact(List<Integer> finishTimes, int expection) {
		return qualityCal(finishTimes, expection);
	}

	private static double qualityCal(List<Integer> finishTimes, double expection) {
		double quality = 0;

		List<Double> qualities = new ArrayList<Double>();

		for (Integer t : finishTimes) {
			double q = getIndividualQuality(t);
			qualities.add(q);
			quality += q;
		}

		DecimalFormat df = new DecimalFormat("0.00");
		return Double.parseDouble(df.format(expection - quality));
	}

	private static double getIndividualQuality(double time) {

		double quality = 2;

		if (time <= D) {
			quality = 1;
		} else if (time > D && time < Z) {
			quality = (Z - time) / (Z - D);
		} else {
			quality = 0;
		}

		return quality;
	}
}
