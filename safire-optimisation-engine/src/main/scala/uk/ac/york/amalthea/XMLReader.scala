package uk.ac.york.amalthea

object XMLReader {

  def main(args: Array[String]): Unit = {
    import scala.xml.XML
    val root = System.getProperty("user.dir")
    val xml1 = XML.loadFile(s"$root/resources/AMALTHEA_Democar.amxmi")
    println(xml1)
    val xml2 = XML.loadFile(s"$root/resources/Amalthea from DemoCar-Code.amxmi")
    println(xml1)

    println("All done")
  }
}

// End ///////////////////////////////////////////////////////////////
