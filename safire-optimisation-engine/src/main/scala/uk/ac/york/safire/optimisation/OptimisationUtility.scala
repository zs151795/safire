package uk.ac.york.safire.optimisation

import uk.ac.york.safire.metrics._

///////////////////////////////////

object OptmisationUtility {

  def urgencyAndQualityToOrder9(urgency: Double, quality: Double): Order9 = {

    val qualityIndex = (quality * 2.9999).toInt
    val urgencyIndex = (quality * 2.9999).toInt

    /*
		 * Intended to capture the following relationship:
		 * quality x urgency -> num eval
	     * Low		Low			High Low
		 * Low		Med     	Medium Low
		 * Low		High		Low Low
		 * Medium	Low			High Medium
		 * Medium	Med     	Medium Medium 
		 * Medium	Hi			Low Medium
		 * High		Low			High High
		 * High		Medium		Medium High  
		 * High		High		Low High 
		 */

    val lookupTable = Array(
      Array(Order9.O3, Order9.O2, Order9.O1),
      Array(Order9.O6, Order9.O5, Order9.O4),
      Array(Order9.O9, Order9.O8, Order9.O7)
    )
    lookupTable(qualityIndex)(urgencyIndex)
  }

  /////////////////////////////////

  def maxEvaluations(urgency: Double, quality: Double, evalMin: Double, evalMax: Double): Int = {
    require(urgency >= 0.0 && urgency <= 1.0)
    require(quality >= 0.0 && quality <= 1.0)
    require(evalMin > 0 && evalMin <= evalMax)

    val o9 = urgencyAndQualityToOrder9(urgency, quality)
    val maxEvaluations = jeep.math.LinearInterpolation.apply(
      o9.ordinal(),
      0, Order9.values().length, evalMin, evalMax
    ).toInt

    //    println( s"order: $o9" )
    //    println( s"maxEvaluations: $maxEvaluations" )    
    maxEvaluations
  }

  /////////////////////////////////

  import scala.collection.JavaConversions._

  def mkPopulation(
    parentConfig: Configuration,
    objectiveFunction: ObjectiveFunction.LocalObjectiveFunction,
    populationSize: Int,
    ct: ConfigurationType, rng: java.util.Random,
    f: (ConfigurationType, java.util.Random) => java.util.Map[String, Value]
  ): List[Configuration] =
    (0 until populationSize).toList.map { _ =>
      val controlledMetrics = f(ct, rng)
      Configuration.update(parentConfig, controlledMetrics,
        objectiveFunction.predictKeyObjectives(parentConfig, controlledMetrics).keyObjectiveMetrics)
    }

  //  def rcpsMkPopulation(
  //    parentConfig: Configuration,
  //    objectiveFunction: ObjectiveFunction.LocalObjectiveFunction,
  //    populationSize: Int,
  //    ct: ConfigurationType, rng: java.util.Random
  //  ): java.util.List[Configuration] = {
  //    mkPopulation(parentConfig, objectiveFunction, populationSize, ct, rng,
  //      JMetalLocalOptimisationEngineRCPS.randomControlledMetrics _)
  //  }

  def randomControlledMetrics(ct: ConfigurationType, rng: java.util.Random): java.util.Map[String, Value] =
    Map() ++ ct.getControlledMetrics().map {
      t => t.name -> uk.ac.york.safire.metrics.Utility.randomValue(t.valueType, rng)
    }

  def simplePlantMkPopulation(
    parentConfig: Configuration,
    objectiveFunction: ObjectiveFunction.LocalObjectiveFunction,
    populationSize: Int,
    ct: ConfigurationType, rng: java.util.Random
  ): java.util.List[Configuration] = {
    mkPopulation(parentConfig, objectiveFunction, populationSize, ct, rng,
      randomControlledMetrics _)
  }

  /////////////////////////////////  

  def multisetCardinalities[T](m: com.google.common.collect.Multiset[T]): List[Int] =
    m.toSet.toList.map { m.count(_) }

  def keyObjectivesHistogram(population: java.util.List[Configuration]): com.google.common.collect.Multiset[List[(String, Double)]] = {

    val numberVisitor = new ValueVisitor.NumberVisitor()
    //    val histoValues = population.map { c => c.getKeyObjectives.toList.map { case (k, v) => 
    //      (k, numberVisitor.visit(v).doubleValue) }.toList 
    //    }.toList.flatten.groupBy { case (name,value) => name }
    val histoValues = population.map { c =>
      c.getKeyObjectives.map {
        case (k, v) =>
          (k, numberVisitor.visit(v).doubleValue)
      }.toList
    }
    //      .toList 
    //      }.flatten.groupBy { case (name,value) => 
    //        name }.map { case (name,l) => name -> l.toList.map { case (name,value) => 
    //          numberVisitor.visit(value).doubleValue } }.toList 
    //  
    // }.toList.flatten.groupBy { case (name,value) => name }

    val histo = com.google.common.collect.HashMultiset.create[List[(String, Double)]]()
    import collection.JavaConverters._
    histoValues.forall { x => histo.add(x) }
    histo
  }

  def controlledMetricsHistogram(population: java.util.List[Configuration]): com.google.common.collect.Multiset[java.util.Map[String, Value]] = {
    val histoValues = population.map { c => c.getControlledMetrics }

    val histo = com.google.common.collect.HashMultiset.create[java.util.Map[String, Value]]()
    import collection.JavaConverters._
    histo.addAll(histoValues.asJava)
    histo
  }

  /////////////////////////////////  

  def main(args: Array[String]): Unit = {
    if (args.length != 4) {
      println(s"usage: uk.ac.york.safire.optimisation.Utility urgency quality minEvaluations maxEvaluations")
      System.exit(0)
    }

    val urgency = args(0).toDouble
    val quality = args(1).toDouble
    val evalMin = args(2).toDouble
    val evalMax = args(3).toDouble

    println(s"maxEvaluations: " + maxEvaluations(urgency, quality, evalMin, evalMax))
  }
}

// End ///////////////////////////////////////////////////////////////
