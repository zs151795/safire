package uk.ac.york.safire.optimisation.mitm.atb

import uk.ac.york.safire.metrics._

import uk.ac.york.safire.optimisation._

import uk.ac.york.safire.optimisation.ia._
import scala.collection.mutable.PriorityQueue

import uk.ac.york.safire.metrics.ValueVisitor

import uk.ac.york.safire.optimisation.ObjectiveFunction
import uk.ac.york.safire.optimisation.ObjectiveFunctionResult

import java.util.concurrent._

import uk.ac.york.safire.optimisation.ComeauBijectivePermutationHash

///////////////////////////////////

class OasProductionLineObjectiveFunction(sequenceDependentTasks: java.util.List[SequenceDependentTaskInfo], commodities: java.util.Set[String]) extends ObjectiveFunction.LocalObjectiveFunction {

  import uk.ac.york.safire.optimisation.ObjectiveFunction.Result

  /////////////////////////////////

  override def predictKeyObjectives(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result =
    predictKeyObjectivesImpl(originalConfig, proposedControlMetrics)

  /////////////////////////////////

  private def predictKeyObjectivesImpl(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result = {

    import scala.collection.JavaConversions._

    val config = Configuration.update(originalConfig, proposedControlMetrics, originalConfig.getKeyObjectives)
    val (recipes, resources) = IAObjectiveFunction.reverseEngineerRecipesFromConfig(config)

    OasProductionLineObjectiveFunction.makeTasks(recipes, resources, config) match {
      case m @ Left(errorString) => new Result(config.getKeyObjectives, errorString)
      case Right(tasks) => {
        val scheduler = new FIFOSchedulerOAS()

        def endUserPriority(t: Task): Int =
          IAObjectiveFunction.priority(OasProductionLineObjectiveFunction.parentTask(t.id).name, config)

        val taskQueue: PriorityQueue[Task] = PriorityQueue(tasks: _*)(Ordering.by { endUserPriority })

        val sequenceDependentSetup = SequenceDependentTaskInfo.makeSequenceDependantSetupOAS(sequenceDependentTasks, config)

        val realResources = resources.map { r =>
          if (r.id.equals("No allocation"))
            r.id
          else {
            r.id.split(" ").dropRight(1).mkString(" ")
          }
        }.distinct.map(r => Resource(r)).toSet

        val unAvailableResources = config.getObservableMetrics.filter(c => c._1.contains(" unAvailabileTime ")).map(c => (c._1.split(" unAvailabileTime ").head, c._1.split(" unAvailabileTime ").last.split("_").toList))

        val unAvailableTasks = unAvailableResources.map {
          case (key, value) =>
            val resource = Resource(key)

            val unAvailableTaskOneLine = value.map { value =>
              val startTime = value.split(",").head.toInt
              val endTime = value.split(",").last.toInt
              val duration = endTime - startTime

              val taskId = key + " NotAvailble from " + startTime + " to " + endTime + "_" + key.toLowerCase().filter(c => c != '(' && c != ')' && !c.isDigit)

              Task.apply(
                TaskId(taskId),
                Map(resource -> duration),
                Map(),
                releaseTime = startTime, 0, 0, 0, Map()
              )
            }
            unAvailableTaskOneLine
        }

        val unAvailableTaskList = unAvailableTasks.toList.flatten

        scheduler.schedule(taskQueue, realResources, sequenceDependentSetup, false, unAvailableTaskList) match {
          case None => new Result(config.getKeyObjectives, "Scheduler cannot find a valid schedule")
          case Some(schedule) =>
            val newKeyObjectives = doUpdatedKeyObjectives(schedule, config)

            //            val resources = schedule.byResource.keys.map(r => r.id.split(" ")).flatten.toList.distinct
            //            val taskByResource = resources.map { r =>
            //              val nameLower = r.filter(c => c != '(' && c != ')' && !c.isDigit).toLowerCase()
            //
            //              val taskList = schedule.byResource.filter(m => m._1.id.contains(r)).map(m => m._2.filter(t => t._1.id.name.contains(nameLower))).flatten.toList.sortBy(s => s._2.lower)
            //
            //              r -> taskList
            //            }
            //            taskByResource.sortBy(f => f._1).foreach { f =>
            //              println("Resource: " + f._1)
            //
            //              f._2.foreach { t =>
            //                println("Task: " + t._1.id.name + " Time: " + t._2.lower + " -> " + t._2.upper)
            //              }
            //              println()
            //            }

            val reportStr = "Status: Succeeded.\n" +
              PreemptionIAObjectiveFunction.toReportStringOAS(schedule, newKeyObjectives, config) + doReportStringSuffix(schedule, newKeyObjectives)

            new Result(newKeyObjectives, reportStr)
        }
      }
    }
  }

  override def evaluate(args: ObjectiveFunctionArguments): Callable[ObjectiveFunctionResult] =
    new Callable[ObjectiveFunctionResult]() {
      override def call(): ObjectiveFunctionResult = {

        //        println("OAS now evaluate")
        val result = predictKeyObjectivesImpl(args.getConfiguration, args.getProposedControlMetrics)

        //        jeep.lang.Diag.println("statusString: " + result.objectiveFunctionStatusReport)
        val newConfig = IAObjectiveFunction.addOptimisationStatusToConfiguration(
          result.objectiveFunctionStatusReport,
          Configuration.update(args.getConfiguration, args.getProposedControlMetrics, result.keyObjectiveMetrics)
        )
        org.apache.commons.math3.util.Pair.create(newConfig, objectiveValues(result))
        new ObjectiveFunctionResult(newConfig, objectiveValues(result))
      }
    }

  override def numObjectives: Int = 3 + commodities.size

  override def objectiveValues(result: Result): java.util.List[java.lang.Double] = {

    import scala.collection.JavaConversions._

    val value = result.keyObjectiveMetrics.get("makespan")
    val value1 = result.keyObjectiveMetrics.get("energy")
    val value2 = result.keyObjectiveMetrics.get("montary")
    val value3 = result.keyObjectiveMetrics.get("urgency")

    val visitor = new ValueVisitor.NumberVisitor()
    val makespan: java.lang.Double = if (value != null) IAObjectiveFunction.doubleValue(value) else -1
    val energy: java.lang.Double = if (value1 != null) IAObjectiveFunction.doubleValue(value1) else -1
    val montary: java.lang.Double = if (value2 != null) IAObjectiveFunction.doubleValue(value2) else -1
    val urgency: java.lang.Double = if (value3 != null) IAObjectiveFunction.doubleValue(value3) else -1

    val discrepancyList = commodities.toList.map { recipeName =>
      val key = recipeName + " amount discrepancy score"
      val value = result.keyObjectiveMetrics.get(key)
      if (value != null) IAObjectiveFunction.doubleValue(value).asInstanceOf[java.lang.Double] else new java.lang.Double(-1)
    }

    val objectives = List(makespan) ++ List(energy) ++ List(montary) ++ List(urgency) ++ discrepancyList
    objectives.filter(o => o >= 0)
  }

  protected def doUpdatedKeyObjectives(schedule: Schedule, config: Configuration): Map[String, Value] = {

    import scala.collection.JavaConversions._

    val amountsRequired: Map[String, Int] = Map() ++ commodities.map { recipeName =>
      val value = config.getObservableMetrics().get(recipeName + " amount required")
      recipeName -> IAObjectiveFunction.intValue(value)
    }

    val amountsProduced: Map[String, Int] = Map() ++ config.getObservableMetrics().map {
      case (key, value) =>
        if (key.contains("amount produced")) {
          val tokens = key.split(" ")
          Some(s"${tokens(0)} ${tokens(1)} ${tokens(2)}" -> IAObjectiveFunction.intValue(value))
        } else
          None
    }.toList.flatten

    val recipeAmountsProducedTemp = (for ((resource, tasks) <- schedule.byResource if resource.id != "No allocation") yield {
      val xx = for (task <- tasks) yield {
        val recipeInfoName = task._1.id.name.split(" ").dropRight(1).mkString(" ")
        val value = amountsProduced.get(recipeInfoName)

        if (!value.isDefined) {
          //          jeep.lang.Diag.println("WARNING: Amount produced not found for: " + recipeInfoName)
          recipeInfoName -> 0
        } else {
          if (task._1.isFinal == 1)
            recipeInfoName -> value.get
          else
            recipeInfoName -> 0
        }
      }
      xx.toList
    }).toList

    val recipeAmountsProducedTemp2 = (recipeAmountsProducedTemp.flatten).groupBy { _._1.dropRight(1).trim }

    val recipeAmountsProducedTemp3 = recipeAmountsProducedTemp2.filter(p => !p._1.contains("DependentSetUp"))

    val recipeAmountsProduced = recipeAmountsProducedTemp3.map {
      case (recipeInfoName, ammounts) =>
        // recipeInfoName.dropRight(1).trim -> ammounts.map { _._2 }.sum
        recipeInfoName -> ammounts.map { _._2 }.sum
    }

    val InsufficientProductionWeightingFactor = 10

    val surplusList = amountsRequired.map {
      case (recipeName, requiredQuantity) =>
        val produced = recipeAmountsProduced.get(recipeName).getOrElse(0)
        val discrepancy = if (produced < requiredQuantity)
          (InsufficientProductionWeightingFactor * (requiredQuantity - produced))
        else
          produced - requiredQuantity

        recipeName + " amount discrepancy score" -> Value.realValue(discrepancy, ValueType.realType(Double.MinValue, Double.MaxValue))
    }

    val makespan = Value.realValue(schedule.makespan, ValueType.realType(Double.MinValue, Double.MaxValue).asInstanceOf[ValueType.Real])
    val energy = Value.realValue(schedule.energy, ValueType.realType(Double.MinValue, Double.MaxValue).asInstanceOf[ValueType.Real])
    val montary = Value.realValue(schedule.montary, ValueType.realType(Double.MinValue, Double.MaxValue).asInstanceOf[ValueType.Real])

    val finalTasks = schedule.byResource.map(taskByResource => taskByResource._2).toList.flatten.filter(task => task._1.isFinal == 1).sortWith((t1, t2) => t1._1.id.name < t2._1.id.name)
    val finalTasksSorted = finalTasks.sortBy(t => (t._2.upper, t._2.lower))

    //    println("Final Task Size: " + finalTasks.size)
    //    finalTasksSorted.foreach(t => println(t._1.id + " resource: " + t._1.resourceExecutionTime.head._1.id + " start: " + t._2.lower + " end: " + t._2.upper))

    val productUrgency = config.getObservableMetrics.filter(c => c._1.contains("urgency")).keys.toList.sortBy(c => c.split(" ").last.toInt);
    val productName = productUrgency.map(c => c.split(" ")(0) + " " + c.split(" ")(1))

    //    val enoughTasks = finalTasksSorted.map{c =>
    //      val productName = c._1.id.name.split(" ")(0) + " " + c._1.id.name.split(" ")(1) + " " + c._1.id.name.split(" ")(2)
    //
    //      val amountProducedByproductName = amountsProduced.get(productName);
    //
    //
    //    }
    import util.control.Breaks._

    val finalTaskByProduct = productName.map { n =>
      val taskByProduct = finalTasksSorted.filter(t => t._1.id.name.contains(n)).sortBy(t => (t._2.upper, t._2.lower))
      val amountByTask = taskByProduct.map(t => amountsProduced.get(t._1.id.name.split(" ").dropRight(1).mkString(" ")).get)
      val productAmountToProduct = amountsRequired.get(n).get;

      def doCut(taskByProduct: List[(Task, Interval)], amountByTask: List[Int]): (List[(Task, Interval)], List[Int]) = {
        if (amountByTask.sum > productAmountToProduct) {
          val amountByTaskCopy = amountByTask.dropRight(1)

          if (amountByTaskCopy.sum < productAmountToProduct) {
            (taskByProduct, amountByTask)
          } else {
            doCut(taskByProduct.dropRight(1), amountByTaskCopy)
          }
        } else {
          (taskByProduct, amountByTask)
        }
      }

      val taskWithCut = doCut(taskByProduct, amountByTask)._1
      val amountWithCut = doCut(taskByProduct, amountByTask)._2

      //      if (taskWithCut == null || taskWithCut.size == 0) {
      //        println("111111")
      //
      //        val keys = config.getControlledMetrics.filter(k => k._1.contains(n) && k._1.contains("allocation"))
      //
      //
      //        println("111111")
      //      }

      (n, if (taskWithCut == null || taskWithCut.size == 0) Int.MaxValue else taskWithCut.last._2.upper)
    }.sortBy(t => (t._2, t._1)).map(t => t._1)

    val urgencyVoliate = finalTaskByProduct.map(p => productName(finalTaskByProduct.indexOf(p)).equals(p)).filter(b => !b)
    val numberOfVoliations = Value.realValue(urgencyVoliate.size, ValueType.realType(Double.MinValue, Double.MaxValue).asInstanceOf[ValueType.Real])

    //    println();
    //    val tankTasks = schedule.byResource.map(taskByResource => taskByResource._2).toList.flatten.filter(task => task._1.id.name.contains("tank") && !task._1.id.name.contains("NotAvailble")).sortWith((t1, t2) => t1._1.id.name < t2._1.id.name)
    //    println("Tank Task Size: " + tankTasks.size)
    //    tankTasks.foreach(t => println(t._1.id + " start: " + t._2.lower + " end: " + t._2.upper))

    val objectiveMap = Map("makespan" -> makespan) ++ Map("energy" -> energy) ++ Map("montary" -> montary) ++ Map("urgency" -> numberOfVoliations) ++ surplusList ++ recipeAmountsProduced.map { case (k, v) => (k + " produced") -> Value.realValue(v, ValueType.realType(Double.MinValue, Double.MaxValue)) }

    val oldObjectives = config.getKeyObjectives;

    val newObjectives = Map() ++ oldObjectives.map {
      case (key, value) =>
        val valueNew = objectiveMap.get(key).get;

        (key, valueNew)
    }

    newObjectives
  }

  protected def doReportStringSuffix(s: Schedule, newKeyObjectives: Map[String, Value]): String =
    s"\nmakespan: ${new jeep.util.HMS(s.makespan * 60)}\n"
}

object OasProductionLineObjectiveFunction {

  import IAObjectiveFunction.{ InstanceName => InstanceName }

  def parentTask(tid: TaskId): TaskId = {

    val toArray = tid.name.split(" ")
    if (toArray.length > 0) {
      val taskNameLastToken = toArray.last(0);

      val taskNamePrefixArray = toArray.dropRight(1);

      val taskName = taskNamePrefixArray.mkString(" ") + " " + taskNameLastToken

      TaskId(taskName)
    } else
      tid

  }

  def makeTasks(recipes: Map[String, List[RecipeInfo]], resources: List[Resource], config: Configuration): Either[String, List[Task]] = {

    //    println("OAS now parse mutex");

    val mutex = IAObjectiveFunction.parseMutexes(resources, config)

    def mkProductionLineTaskList(instanceName: InstanceName, allocatedResource: Resource): List[Task] = {

      val resourceTokens = allocatedResource.id.split(" ");
      val realResourceName = if (resourceTokens.length > 2) resourceTokens(0) + " " + resourceTokens(1) + " " + resourceTokens(2) else resourceTokens(0) + " " + resourceTokens(1)
      val realAllocatedResource = Resource(realResourceName)

      import scala.collection.JavaConversions._
      import uk.ac.york.safire.metrics.ValueVisitor
      import uk.ac.york.safire.metrics._

      val result = Map() ++ config.getObservableMetrics()

      val result1 = result.filter {
        case (name, value) => /*name.contains(instanceName) &&*/ name.contains(instanceName + " " + allocatedResource.id) && !name.contains(" mutex ") && !name.contains("availability")
      }

      val taskStart = result1.filter {
        case (name, value) => name.contains("start")
      }

      val taskEnd = result1.filter {
        case (name, value) => name.contains("end")
      }

      val taskEnergy = result1.filter {
        case (name, value) => name.contains("energy")
      }

      val taskMontary = result1.filter {
        case (name, value) => name.contains("montary")
      }

      val executionRelationRaw = result1.filter {
        case (name, value) => name.contains("executedAfter")
      }

      val executionRelation = executionRelationRaw.map {
        case (name, value) =>
          val toArray = name.split(" ")
          (toArray(toArray.length - 1), toArray(toArray.length - 3))
      }

      val taskNameRaw = taskStart.map(f => f._1.split(" ")(f._1.split(" ").length - 2)).toList
      val taskNameProcessed = taskNameRaw.map { n => n -> ("_" + n) }

      val tasks = taskNameProcessed.map { n =>
        val taskId = TaskId(instanceName + n._2)

        def intValue(value: Value): Int =
          new ValueVisitor.NumberVisitor().visit(value).intValue()

        val endTime = intValue(taskEnd.filter(t => t._1.split(" ").dropRight(1).last.equals(n._1)).head._2)
        val startTime = intValue(taskStart.filter(t => t._1.split(" ").dropRight(1).last.equals(n._1)).head._2)
        val energyCost = intValue(taskEnergy.filter(t => t._1.split(" ").dropRight(1).last.equals(n._1)).head._2)
        val montaryCost = intValue(taskMontary.filter(t => t._1.split(" ").dropRight(1).last.equals(n._1)).head._2)

        val taskExecutedBeforeMap = executionRelation.filter { r => r._2.equals(n._1) }

        val isFinal = if (executionRelation.filter(r => r._1.equals(n._1)).isEmpty) 1 else 0

        if (taskExecutedBeforeMap.size > 0) {
          Task.apply(
            taskId,
            Map(realAllocatedResource -> endTime),
            Map(TaskId(instanceName + "_" + taskExecutedBeforeMap.last._1) -> IntervalRelation.PreceededGenerallyBy),
            releaseTime = startTime, energyCost, montaryCost, isFinal, mutex
          )
        } else {
          Task.apply(
            taskId,
            Map(realAllocatedResource -> endTime),
            Map(),
            releaseTime = startTime, energyCost, montaryCost, isFinal, mutex
          )
        }

      }
      tasks
    }

    val tasks = (for ((recipeName, infos) <- recipes; info <- infos; instanceId <- 0 until info.instances) yield {

      val instanceName = InstanceName(info, instanceId)
      val allocatedResource = IAObjectiveFunction.allocation(instanceName, config)
      val allocationAvailable = IAObjectiveFunction.isResourceAvailable(allocatedResource, config)
      if (!allocationAvailable) {
        Left(s"${allocatedResource.id} is not available for task $instanceName")
      } else {
        Right(mkProductionLineTaskList(instanceName, allocatedResource))
      }
    }).toList

    // if all tasks could be allocated to resources, return them in a list
    // otherwise, provide an descriptive string for the unallocated ones
    Either.cond(
      tasks.forall { e => e.isRight },
      tasks.map { case Right(x) => x }.flatten,
      tasks.map { case Left(descriptiveStr) => descriptiveStr + "\n"; case Right(task) => "" }.mkString
    )
  }
}

// End ///////////////////////////////////////////////////////////////
