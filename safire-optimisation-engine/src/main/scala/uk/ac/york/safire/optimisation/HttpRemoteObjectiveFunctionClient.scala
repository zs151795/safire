package uk.ac.york.safire.optimisation

import uk.ac.york.safire.metrics._
import scala.collection.JavaConversions._

///////////////////////////////////

object HttpRemoteObjectiveFunctionClient {

  val DefaultPort = 8080

  val localObjectiveFunction = MockObjectiveFunction.largerPlant(
    energyCostWeighting = HttpRemoteOptimisationEngineClient.DefaultEnergyCostWeighting,
    makespanCostWeighting = HttpRemoteOptimisationEngineClient.DefaultMakespanCostWeighting
  )

  /////////////////////////////////

  def createHttpObjectiveFunctionClient(args: Array[String], impl: ObjectiveFunction.LocalObjectiveFunction): ObjectiveFunction = {
    val args0 = if (args.length == 0) {
      "http:/" + new java.net.InetSocketAddress("127.0.0.1", DefaultPort)
    } else {
      if (args.length == 1)
        args(0)
      else {
        println("Usage: " + this.getClass().getSimpleName() + " http://objective-function-url")
        System.exit(0)
        ???
      }
    }

    // jeep.lang.Diag.println( args0 );
    new ObjectiveFunction.RemoteHttpJsonObjectiveFunction(new java.net.URL(args0), impl)
  }

  /////////////////////////////////

  def main(args: Array[String]): Unit = {

    val seed = 0xDEADBEEF

    val urgency = 0.9
    val quality = 0.1

    val populationSize = 500

    val remoteObjectiveFunction = createHttpObjectiveFunctionClient(args, localObjectiveFunction)
    val config = Utility.randomConfiguration(localObjectiveFunction.configurationType, new java.util.Random(seed))
    jeep.lang.Diag.println(ObjectiveFunction.evaluateNowHelper(remoteObjectiveFunction, new ObjectiveFunctionArguments(config, config.getControlledMetrics)))
    jeep.lang.Diag.println("All done")
  }
}

// End ///////////////////////////////////////////////////////////////
