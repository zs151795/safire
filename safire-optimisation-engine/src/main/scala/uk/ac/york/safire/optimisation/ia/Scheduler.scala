package uk.ac.york.safire.optimisation.ia

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.PriorityQueue

///////////////////////////////////

trait SequenceDependentSetup {
  def apply(resource: Resource, last: Task, next: Task): Option[Task]
}

object NoSequenceDependentSetup extends SequenceDependentSetup {
  override def apply(resource: Resource, last: Task, next: Task): Option[Task] = None
}

///////////////////////////////////

//trait Scheduler {
//
//  def schedule(tasks: PriorityQueue[Task], resources: Set[Resource],
//               sequenceDependentSetup: SequenceDependentSetup, recursing: Boolean = false, unAvailableTasks : List[Task] = List()): Option[Schedule]
//  // require( Schedule.topologicallySorted(tasks.toList) )
//  // require( Schedule.uniqueTaskIds(tasks.toList) )
//}

///////////////////////////////////

class FIFOSchedulerOAS {

  def schedule(queue: PriorityQueue[Task], resources: Set[Resource],
    sequenceDependentSetup: SequenceDependentSetup = NoSequenceDependentSetup, recursing: Boolean = false, unAvailableTasks: List[Task] = List()): Option[Schedule] = {

    // require(jeep.lang.Logic.implies(!recursing, Schedule.topologicallySorted(queue.toList)))
    require(Schedule.uniqueTaskIds(queue.toList))

    ///////////////////////////////

    // Swan: 10th July 2018:
    // val tasks = queue.to[ListBuffer]
    val rawTasks: ListBuffer[Task] = queue.clone().dequeueAll

    rawTasks.foreach(t => assert(t.compatibleResources.size == 1))

    val tasks = rawTasks.filter(t => !t.compatibleResources.head.id.equals("No allocation"))

    //tasks.filter(t => t.resourceExecutionTime.)

    //jeep.lang.Diag.println(tasks.map { _.id })
    // System.exit(0)

    val immediateSuccessors = scala.collection.mutable.Map.empty[TaskId, Set[Task]]
    for {
      i <- 0 until tasks.length;
      j <- 0 until tasks.length
      if (j != i && tasks(i).isImmediateSuccessorOf(tasks(j)))
    } {

      // add task i to the set of immediate successors of j
      val currentSuccessors = immediateSuccessors.getOrElseUpdate(tasks(j).id, Set())
      immediateSuccessors.update(tasks(j).id, currentSuccessors + tasks(i))
    }

    //jeep.lang.Diag.println(immediateSuccessors)

    ///////////////////////////////

    var schedule = Schedule()

    unAvailableTasks.foreach(t => schedule = schedule.append(t.resourceExecutionTime.head._1, t, !recursing, t.releaseTime).get)

    val removed = scala.collection.mutable.Set.empty[TaskId]
    // while(!tasks.isEmpty) {
    while (removed.size < tasks.size) {

      val earliestReleaseTime = tasks.filter { t =>
        !removed.contains(t.id) // && schedule.generalPredecesorsScheduled(t)
      }.map { t => t.releaseTime
      }.min

      // Swan: 10th July 2018:
      val filteredTasks = tasks.filter { t =>
        !removed.contains(t.id) &&
          schedule.generalPredecesorsScheduled(t) &&
          t.releaseTime <= math.max(schedule.makespan, earliestReleaseTime)
      }

      //jeep.lang.Diag.println(filteredTasks.map { _.id })
      val filteredAndSortedTasks = filteredTasks.sorted { queue.ord }.reverse
      //jeep.lang.Diag.println(filteredAndSortedTasks.map { _.id })

      //for (task <- tasks if (!removed.contains(task.id))) {
      // for (task <- filteredAndSortedTasks ) {
      val task = filteredAndSortedTasks.head

      //      jeep.lang.Diag.println("Task: " + task.id + "  Resource: " + task.resourceExecutionTime.keys.head.id + " execution Time: " + task.resourceExecutionTime.values.head)

      // Swan: changed 7th June 2018
      // val timeNow = schedule.makespan
      val timeNow = math.max(schedule.makespan, earliestReleaseTime)
      // End -- Swan: changed 7th June 2018

      //jeep.lang.Diag.println(s"recursing: ${recursing}, $schedule, timeNow: ${timeNow}, earliestReleaseTime: ${earliestReleaseTime}, current task: ${task.id} releaseTime: ${task.releaseTime}")
      ////
      //if( removed.size >= 19 )
      //System.exit(0)

      // FIXME: what should this condition be when recursing?
      val predecessorsScheduled = if (recursing) true else schedule.generalPredecesorsScheduled(task)
      // task.generalPredecessors.forall { parent => schedule.containsTaskId(parent) }

      //      jeep.lang.Diag.println(schedule)

      if (timeNow >= task.releaseTime && predecessorsScheduled) {
        val compatible = resources.intersect(task.compatibleResources)
        val earliestIdleResource = compatible.minBy { r =>
          // calc anti-afinity for r
          schedule.idleTimeOAS(r, task.antiAffinity(r) union task.mutex(r), task.id.name, task.resourceExecutionTime.values.last) // + task.duration(r) // FIXME: do we want duration here?
        }
        //jeep.lang.Diag.println(earliestIdleResource)
        val taskList = schedule.byResource.getOrElse(earliestIdleResource, Nil)

        //        val rawResourceByTask = task.id.name.split(" ").last.split("_").filter(n => n.contains("silo") || n.contains("mixer") || n.contains("tank")).map(n => n.capitalize);

        val rawResourceByTask = task.id.name.split(" ").last.split("_").drop(1).map(n => n.capitalize);
        val listResourceByTask = for (r1 <- rawResourceByTask) yield List.empty[String] ++ earliestIdleResource.id.split(" ").filter(r => r.contains(r1))
        val resourcesUsedByTask = listResourceByTask.flatten
        //        val dependentResource = {
        //          if (resourcesUsedByTask.length == 2)
        //            resourcesUsedByTask.filter(r => !r.contains("Mixer"))
        //          else
        //            resourcesUsedByTask
        //        }

        if (resourcesUsedByTask.length > 0) {
          val resourceNameForCheck = resourcesUsedByTask(0).filterNot(c => c == '(' || c == ')' || c.isDigit).toLowerCase()

          val dependentKeyList = schedule.byResource.keys.filter(k => k.id.contains(resourcesUsedByTask(0))).toList
          val dependentTaskList = dependentKeyList.map(k => schedule.byResource.get(k).get).flatten.filter(t => t._1.id.name.contains(resourceNameForCheck) && !t._1.id.name.contains("NotAvailble")).sortBy(t => t._2.upper)

          if (!dependentTaskList.isEmpty) {

            var assumedStartedTime = math.max(
              task.releaseTime,
              schedule.idleTimeOAS(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource), task.id.name, task.resourceExecutionTime.values.last)
            )

            val lastTaskList = dependentTaskList.filter(t => t._2.upper <= assumedStartedTime)

            if (!lastTaskList.isEmpty) {
              val lastTask = lastTaskList.last._1

              val setupTask = sequenceDependentSetup(earliestIdleResource, lastTask, task)
              //jeep.lang.Diag.println(setupTask)
              if (setupTask.isDefined) {

                val task = setupTask.get

                // schedule = schedule.append( earliestIdleResource, task )
                val requireImmediatePredecessors = !recursing
                var notBeforeTime = math.max(
                  math.max(task.releaseTime, dependentTaskList.filter(t => t._2.upper <= assumedStartedTime).last._2.upper),
                  schedule.idleTimeOAS(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource), task.id.name, task.resourceExecutionTime.values.last)
                )

                schedule = schedule.append(earliestIdleResource, task, requireImmediatePredecessors, notBeforeTime).get
                tasks -= task

                val immediateSucc = immediateSuccessors.get(task.id).toList.flatten
                if (!immediateSucc.isEmpty) {
                  val pendingQueue = PriorityQueue(immediateSucc: _*)(Ordering.by { (t: Task) => t.releaseTime })
                  val pendingSchedule = this.schedule(pendingQueue, resources, sequenceDependentSetup)
                  schedule = schedule.concat(pendingSchedule.get).get
                  pendingQueue.foreach { t => tasks -= t }
                }
              }
            }

          }
        }

        /////////////////////////

        val immediateSucc = immediateSuccessors.get(task.id).toList.flatten

        //jeep.lang.Diag.println(s"immediateSuccessors of ${task.id}: ${immediateSucc.map { _.id }}")

        var notBeforeTime = math.max(
          task.releaseTime,
          schedule.idleTimeOAS(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource), task.id.name, task.resourceExecutionTime.values.last)
        )

        if (immediateSucc.isEmpty) {
          removed += task.id // tasks -= task
          //jeep.lang.Diag.println(s"recursing: ${recursing}. schedule: ${schedule}. No successors for: ${task.id}")
          // var notBeforeTime = math.max(task.releaseTime,schedule.idleTime(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource)))
          val requireImmediatePredecessors = !recursing
          //jeep.lang.Diag.println(s"recursing: ${recursing}. adding ${task.id} to schedule")
          schedule.append(earliestIdleResource, task, requireImmediatePredecessors, notBeforeTime) match {
            case Some(newSchedule) => schedule = newSchedule
            case None => jeep.lang.Diag.println(recursing); return None // shouldn't happen
          }
        } else {
          //jeep.lang.Diag.println(s"recursing: ${recursing}. schedule: ${schedule}. immediate successors for: ${task.id}: ${immediateSucc.map { _.id }}")

          // Swan: 10th July 2018:
          // val pendingQueue = PriorityQueue(immediateSucc: _*)(Ordering.by { (t: Task) => t.releaseTime })
          val pendingQueue = PriorityQueue(immediateSucc: _*)(queue.ord)

          this.schedule(pendingQueue, resources, sequenceDependentSetup, recursing = true) match {
            case None => return None
            case Some(pendingSchedule) => {
              // var notBeforeTime = task.releaseTime // schedule.makespan // + task.duration(earliestIdleResource) // FIXME: is this sufficient?
              // var notBeforeTime = math.max(task.releaseTime,schedule.idleTime(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource)))
              var succeeded = false
              while (!succeeded) {
                (for {
                  s1 <- schedule.append(earliestIdleResource, task, true, notBeforeTime);
                  s2 <- s1.concat(pendingSchedule.postponeBy(notBeforeTime + task.duration(earliestIdleResource)))
                } yield { s2 }) match {
                  case Some(newSchedule) =>
                    schedule = newSchedule; succeeded = true
                  case None => jeep.lang.Diag.println(notBeforeTime); notBeforeTime += 1
                }
              }
            }
          }

          //jeep.lang.Diag.println(s"recursing: ${recursing}, tasks before: ${tasks.map { _.id }}")

          //            tasks -= task
          //            pendingQueue.foreach { t => tasks -= t }
          removed += task.id
          pendingQueue.foreach { t => removed += t.id }

          //jeep.lang.Diag.println(s"recursing: ${recursing},tasks after: ${tasks.map { _.id }}")
        }
      } else {
        if (timeNow < task.releaseTime)
          println(s"Cannot consider task ${task.id} because timeNow: ${timeNow} < releaseTime: ${task.releaseTime}")
        if (!predecessorsScheduled)
          println(s"Cannot consider task ${task.id} because predecessors not scheduled")
      }
      //      }
    }

    // System.exit(0)
    Some(schedule)
  }
}

// End ///////////////////////////////////////////////////////////////

class FIFOScheduler {

  def schedule(queue: PriorityQueue[Task], resources: Set[Resource],
    sequenceDependentSetup: SequenceDependentSetup = NoSequenceDependentSetup, recursing: Boolean = false, unAvailableTasks: List[Task] = List()): Option[Schedule] = {

    // require(jeep.lang.Logic.implies(!recursing, Schedule.topologicallySorted(queue.toList)))
    require(Schedule.uniqueTaskIds(queue.toList))

    ///////////////////////////////

    // Swan: 10th July 2018:
    // val tasks = queue.to[ListBuffer]
    val tasks: ListBuffer[Task] = queue.clone().dequeueAll

    //    jeep.lang.Diag.println( tasks.map { _.id }  )
    // System.exit(0)

    val immediateSuccessors = scala.collection.mutable.Map.empty[TaskId, Set[Task]]
    for {
      i <- 0 until tasks.length;
      j <- 0 until tasks.length
      if (j != i && tasks(i).isImmediateSuccessorOf(tasks(j)))
    } {

      // add task i to the set of immediate successors of j
      val currentSuccessors = immediateSuccessors.getOrElseUpdate(tasks(j).id, Set())
      immediateSuccessors.update(tasks(j).id, currentSuccessors + tasks(i))
    }

    // jeep.lang.Diag.println(immediateSuccessors)

    ///////////////////////////////

    var schedule = Schedule()

    unAvailableTasks.foreach(t => schedule = schedule.append(t.resourceExecutionTime.head._1, t, !recursing, t.releaseTime).get)

    val removed = scala.collection.mutable.Set.empty[TaskId]
    // while(!tasks.isEmpty) {
    while (removed.size < tasks.size) {

      val earliestReleaseTime = tasks.filter { t =>
        !removed.contains(t.id) // && schedule.generalPredecesorsScheduled(t)
      }.map { t => t.releaseTime
      }.min

      // Swan: 10th July 2018:
      val filteredTasks = tasks.filter { t =>
        !removed.contains(t.id) &&
          schedule.generalPredecesorsScheduled(t) &&
          t.releaseTime <= math.max(schedule.makespan, earliestReleaseTime)
      }

      //jeep.lang.Diag.println( filteredTasks.map { _.id }  )
      val filteredAndSortedTasks = filteredTasks.sorted { queue.ord }.reverse
      // jeep.lang.Diag.println( filteredAndSortedTasks.map { _.id }  )

      //for (task <- tasks if (!removed.contains(task.id))) {
      // for (task <- filteredAndSortedTasks ) {
      val task = filteredAndSortedTasks.head

      //      jeep.lang.Diag.println("task: " + task.id)

      // Swan: changed 7th June 2018
      // val timeNow = schedule.makespan
      val timeNow = math.max(schedule.makespan, earliestReleaseTime)
      // End -- Swan: changed 7th June 2018

      //jeep.lang.Diag.println(s"recursing: ${recursing}, $schedule, timeNow: ${timeNow}, earliestReleaseTime: ${earliestReleaseTime}, current task: ${task.id} releaseTime: ${task.releaseTime}")
      ////
      //if( removed.size >= 19 )
      //System.exit(0)

      // FIXME: what should this condition be when recursing?
      val predecessorsScheduled = if (recursing) true else schedule.generalPredecesorsScheduled(task)
      // task.generalPredecessors.forall { parent => schedule.containsTaskId(parent) }

      //      jeep.lang.Diag.println(schedule)

      if (timeNow >= task.releaseTime && predecessorsScheduled) {
        val compatible = resources.intersect(task.compatibleResources)
        val earliestIdleResource = compatible.minBy { r =>
          // calc anti-afinity for r
          schedule.idleTime(r, task.antiAffinity(r) union task.mutex(r), task.resourceExecutionTime.values.last) // + task.duration(r) // FIXME: do we want duration here?
        }
        //jeep.lang.Diag.println(earliestIdleResource)
        val taskList = schedule.byResource.getOrElse(earliestIdleResource, Nil)
        if (!taskList.isEmpty) {

          val lastTask = taskList.last._1
          val setupTask = sequenceDependentSetup(earliestIdleResource, lastTask, task)
          // jeep.lang.Diag.println( setupTask )
          if (setupTask.isDefined) {

            val task = setupTask.get

            // schedule = schedule.append( earliestIdleResource, task )
            val requireImmediatePredecessors = !recursing
            var notBeforeTime = math.max(
              task.releaseTime,
              schedule.idleTime(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource), task.resourceExecutionTime.values.last)
            )

            schedule = schedule.append(earliestIdleResource, task, requireImmediatePredecessors, notBeforeTime).get
            tasks -= task

            val immediateSucc = immediateSuccessors.get(task.id).toList.flatten
            if (!immediateSucc.isEmpty) {
              val pendingQueue = PriorityQueue(immediateSucc: _*)(Ordering.by { (t: Task) => t.releaseTime })
              val pendingSchedule = this.schedule(pendingQueue, resources, sequenceDependentSetup)
              schedule = schedule.concat(pendingSchedule.get).get
              pendingQueue.foreach { t => tasks -= t }
            }
          }
        }

        /////////////////////////

        val immediateSucc = immediateSuccessors.get(task.id).toList.flatten

        // jeep.lang.Diag.println( s"immediateSuccessors of ${task.id}: ${immediateSucc.map { _.id } }" )

        var notBeforeTime = math.max(
          task.releaseTime,
          schedule.idleTime(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource), task.resourceExecutionTime.values.last)
        )

        if (immediateSucc.isEmpty) {
          removed += task.id // tasks -= task
          // jeep.lang.Diag.println(s"recursing: ${recursing}. schedule: ${schedule}. No successors for: ${task.id}")
          // var notBeforeTime = math.max(task.releaseTime,schedule.idleTime(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource)))
          val requireImmediatePredecessors = !recursing
          // jeep.lang.Diag.println(s"recursing: ${recursing}. adding ${task.id} to schedule")
          schedule.append(earliestIdleResource, task, requireImmediatePredecessors, notBeforeTime) match {
            case Some(newSchedule) => schedule = newSchedule
            case None => jeep.lang.Diag.println(recursing); return None // shouldn't happen
          }
        } else {
          // jeep.lang.Diag.println(s"recursing: ${recursing}. schedule: ${schedule}. immediate successors for: ${task.id}: ${immediateSucc.map { _.id }}")

          // Swan: 10th July 2018:
          // val pendingQueue = PriorityQueue(immediateSucc: _*)(Ordering.by { (t: Task) => t.releaseTime })
          val pendingQueue = PriorityQueue(immediateSucc: _*)(queue.ord)

          this.schedule(pendingQueue, resources, sequenceDependentSetup, recursing = true) match {
            case None => return None
            case Some(pendingSchedule) => {
              // var notBeforeTime = task.releaseTime // schedule.makespan // + task.duration(earliestIdleResource) // FIXME: is this sufficient?
              // var notBeforeTime = math.max(task.releaseTime,schedule.idleTime(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource)))
              var succeeded = false
              while (!succeeded) {
                (for {
                  s1 <- schedule.append(earliestIdleResource, task, true, notBeforeTime);
                  s2 <- s1.concat(pendingSchedule.postponeBy(notBeforeTime + task.duration(earliestIdleResource)))
                } yield { s2 }) match {
                  case Some(newSchedule) =>
                    schedule = newSchedule; succeeded = true
                  case None => jeep.lang.Diag.println(notBeforeTime); notBeforeTime += 1
                }
              }
            }
          }

          // jeep.lang.Diag.println(s"recursing: ${recursing}, tasks before: ${tasks.map { _.id }}")

          //            tasks -= task
          //            pendingQueue.foreach { t => tasks -= t }
          removed += task.id
          pendingQueue.foreach { t => removed += t.id }

          jeep.lang.Diag.println(s"recursing: ${recursing},tasks after: ${tasks.map { _.id }}")
        }
      } else {
        if (timeNow < task.releaseTime)
          println(s"Cannot consider task ${task.id} because timeNow: ${timeNow} < releaseTime: ${task.releaseTime}")
        if (!predecessorsScheduled)
          println(s"Cannot consider task ${task.id} because predecessors not scheduled")
      }
      //      }
    }

    Some(schedule)
  }
}

class FIFOSchedulerElectrolux {

  def schedule(queue: PriorityQueue[Task], resources: Set[Resource],
    sequenceDependentSetup: SequenceDependentSetup = NoSequenceDependentSetup, recursing: Boolean = false, unAvailableTasks: List[Task] = List(), deadline: Int): Option[Schedule] = {

    require(Schedule.uniqueTaskIds(queue.toList))

    val rawTasks: ListBuffer[Task] = queue.clone().dequeueAll
    rawTasks.foreach(t => assert(t.compatibleResources.size == 1))
    val tasks = rawTasks.filter(t => !t.compatibleResources.head.id.equals("No allocation"))

    val immediateSuccessors = scala.collection.mutable.Map.empty[TaskId, Set[Task]]
    for {
      i <- 0 until tasks.length;
      j <- 0 until tasks.length
      if (j != i && tasks(i).isImmediateSuccessorOf(tasks(j)))
    } {

      // add task i to the set of immediate successors of j
      val currentSuccessors = immediateSuccessors.getOrElseUpdate(tasks(j).id, Set())
      immediateSuccessors.update(tasks(j).id, currentSuccessors + tasks(i))
    }

    var schedule = Schedule()

    unAvailableTasks.foreach(t => schedule = schedule.append(t.resourceExecutionTime.head._1, t, !recursing, t.releaseTime).get)

    val removed = scala.collection.mutable.Set.empty[TaskId]
    while (removed.size < tasks.size) {

      val earliestReleaseTime = tasks.filter { t =>
        !removed.contains(t.id) // && schedule.generalPredecesorsScheduled(t)
      }.map { t => t.releaseTime
      }.min

      val filteredTasks = tasks.filter { t =>
        !removed.contains(t.id) &&
          schedule.generalPredecesorsScheduled(t) &&
          t.releaseTime <= math.max(schedule.makespan, earliestReleaseTime)
      }

      val filteredAndSortedTasks = filteredTasks.sorted { queue.ord }.reverse
      val task = filteredAndSortedTasks.head

      //      jeep.lang.Diag.println("Task: " + task.id + "  Resource: " + task.resourceExecutionTime.keys.head.id + " execution Time: " + task.resourceExecutionTime.values.head)

      val timeNow = math.max(schedule.makespan, earliestReleaseTime)
      val predecessorsScheduled = if (recursing) true else schedule.generalPredecesorsScheduled(task)

      //      jeep.lang.Diag.println(schedule)

      if (timeNow >= task.releaseTime && predecessorsScheduled) {
        val compatible = resources.intersect(task.compatibleResources)
        val earliestIdleResource = compatible.minBy { r =>
          schedule.idleTimeElectrolux(r, task.antiAffinity(r) union task.mutex(r), task.id.name, task.resourceExecutionTime.values.last)
        }

        val resourcesUsedByTask = task.resourceExecutionTime.head._1.id.split(" ")
        if (resourcesUsedByTask.length > 0) {

          val dependentKeyList = schedule.byResource.keys.filter(k => k.id.contains(resourcesUsedByTask(0))).toList
          val dependentTaskList = dependentKeyList.map(k => schedule.byResource.get(k).get).flatten.filter(t => !t._1.id.name.contains("NotAvailble")).sortBy(t => t._2.upper)

          if (!dependentTaskList.isEmpty) {

            var assumedStartedTime = math.max(
              task.releaseTime,
              schedule.idleTimeElectrolux(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource), task.id.name, task.resourceExecutionTime.values.last)
            )

            val lastTaskList = dependentTaskList.filter(t => t._2.upper <= assumedStartedTime)

            if (!lastTaskList.isEmpty) {
              val lastTask = lastTaskList.last._1

              val setupTask = sequenceDependentSetup(earliestIdleResource, lastTask, task)
              //jeep.lang.Diag.println(setupTask)
              if (setupTask.isDefined) {

                val task = setupTask.get

                // schedule = schedule.append( earliestIdleResource, task )
                val requireImmediatePredecessors = !recursing
                var notBeforeTime = math.max(
                  math.max(task.releaseTime, dependentTaskList.filter(t => t._2.upper <= assumedStartedTime).last._2.upper),
                  schedule.idleTimeOAS(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource), task.id.name, task.resourceExecutionTime.values.last)
                )

                schedule = schedule.append(earliestIdleResource, task, requireImmediatePredecessors, notBeforeTime).get
                tasks -= task

                val immediateSucc = immediateSuccessors.get(task.id).toList.flatten
                if (!immediateSucc.isEmpty) {
                  val pendingQueue = PriorityQueue(immediateSucc: _*)(Ordering.by { (t: Task) => t.releaseTime })
                  val pendingSchedule = this.schedule(pendingQueue, resources, sequenceDependentSetup, false, List(), deadline)
                  schedule = schedule.concat(pendingSchedule.get).get
                  pendingQueue.foreach { t => tasks -= t }
                }
              }
            }

          }
        }

        val immediateSucc = immediateSuccessors.get(task.id).toList.flatten

        //jeep.lang.Diag.println(s"immediateSuccessors of ${task.id}: ${immediateSucc.map { _.id }}")

        var notBeforeTime = math.max(
          task.releaseTime,
          schedule.idleTimeElectrolux(earliestIdleResource, task.antiAffinity(earliestIdleResource) union task.mutex(earliestIdleResource), task.id.name, task.resourceExecutionTime.values.last)
        )

        if (immediateSucc.isEmpty) {
          removed += task.id
          val requireImmediatePredecessors = !recursing

          schedule.append(earliestIdleResource, task, requireImmediatePredecessors, notBeforeTime) match {
            case Some(newSchedule) => schedule = newSchedule
            case None => jeep.lang.Diag.println(recursing); return None // shouldn't happen
          }
        } else {
          val pendingQueue = PriorityQueue(immediateSucc: _*)(queue.ord)

          this.schedule(pendingQueue, resources, sequenceDependentSetup, recursing = true, List(), deadline) match {
            case None => return None
            case Some(pendingSchedule) => {
              var succeeded = false
              while (!succeeded) {
                (for {
                  s1 <- schedule.append(earliestIdleResource, task, true, notBeforeTime);
                  s2 <- s1.concat(pendingSchedule.postponeBy(notBeforeTime + task.duration(earliestIdleResource)))
                } yield { s2 }) match {
                  case Some(newSchedule) =>
                    schedule = newSchedule; succeeded = true
                  case None => jeep.lang.Diag.println(notBeforeTime); notBeforeTime += 1
                }
              }
            }
          }

          removed += task.id
          pendingQueue.foreach { t => removed += t.id }
        }
      } else {
        if (timeNow < task.releaseTime)
          println(s"Cannot consider task ${task.id} because timeNow: ${timeNow} < releaseTime: ${task.releaseTime}")
        if (!predecessorsScheduled)
          println(s"Cannot consider task ${task.id} because predecessors not scheduled")
      }
    }

    val finishTimePerZoon = schedule.byResource.map {
      r => r._2.last._2.upper
    }.toList

    val shortestGap = finishTimePerZoon.map(f => deadline - f).sorted.head
    val gap = if (shortestGap > 0) shortestGap else 0;

    val realSchedule = schedule.byResource.map {
      r =>
        val newTaskList = r._2.map {
          t =>
            (t._1, Interval(t._2.lower + gap, t._2.upper + gap))
        }
        (r._1, newTaskList)
    }

    val newSchedule = Schedule(realSchedule)
    Some(newSchedule)
  }
}