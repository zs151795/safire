package uk.ac.york.safire.optimisation

import uk.ac.york.safire.metrics._
import scala.collection.JavaConversions._

///////////////////////////////////

object OEClientCommon {

  //  def applyOptimisationEngineToArgs(args: OptimisationArguments,
  //    objectiveFunction: MockObjectiveFunction,
  //    optimisationEngine: OptimisationEngine): (OptimisationResult, Long) = {
  //
  //    val startTime = System.currentTimeMillis()
  //    val result = optimisationEngine.optimise(args)
  //    val endTime = System.currentTimeMillis()
  //    (result,endTime-startTime)    
  //  }

  /////////////////////////////////

  def applyOptimisationEngine(populationSize: Int, urgency: Double, quality: Double,
    objectiveFunction: MockObjectiveFunction, configurationType: ConfigurationType,
    optimisationEngine: OptimisationEngine, rng: java.util.Random): (OptimisationResult, Long) = {

    // jeep.lang.Diag.println("ct:\n" + configurationType)

    val config = uk.ac.york.safire.metrics.Utility.randomConfiguration(configurationType, rng)

    //    val population = if (HttpRemoteOptimisationEngineServer.UseRCPS)
    //      OptmisationUtility.rcpsMkPopulation(config, objectiveFunction, populationSize, configurationType, rng)
    //    else
    //      OptmisationUtility.simplePlantMkPopulation(config, objectiveFunction, populationSize, configurationType, rng)

    // jeep.lang.Diag.println("population head config:\n" + population.head.getConfigurationType)

    val population = OptmisationUtility.simplePlantMkPopulation(config, objectiveFunction, populationSize, configurationType, rng)

    //    jeep.lang.Diag.println("controlledMetrics set: " + population.map { c => c.getControlledMetrics }.toSet.size)
    //    jeep.lang.Diag.println("controlledMetrics histogram: " + OptmisationUtility.multisetCardinalities(OptmisationUtility.controlledMetricsHistogram(population)));
    //    jeep.lang.Diag.println("keyObjectives histogram: " + OptmisationUtility.multisetCardinalities(OptmisationUtility.keyObjectivesHistogram(population)));
    //    jeep.lang.Diag.println("fitness histogram: " + MockObjectiveFunction.soFitnessHistogram(population));
    assert(population.head.getConfigurationType == configurationType)

    val histo = objectiveFunction.soFitnessHistogram(population)
    // jeep.lang.Diag.println(s"key objective values: ${histo}")
    val fitnessArray = histo.toArray.map { x => x.asInstanceOf[Double] }

    val mean = org.apache.commons.math3.stat.StatUtils.mean(fitnessArray)
    val sd = math.sqrt(org.apache.commons.math3.stat.StatUtils.variance(fitnessArray))
    println(s"initial population fitness -- mean: ${mean}, sd: ${sd}, min = ${fitnessArray.min}, max = ${fitnessArray.max}")

    ///////////////////////////////

    //  println("Invoking optimisation engine...\n")
    val startTime = System.currentTimeMillis()
    val result = optimisationEngine.optimise(new OptimisationArguments(population, urgency, quality))
    val endTime = System.currentTimeMillis()
    // jeep.lang.Diag.println("result from optimisation engine:\n" + result)

    // println(s"Optimisation took ${(endTime - startTime) / 1000.0} s")

    (result, endTime - startTime)
  }

  /////////////////////////////////

  /**
   * run the specified optimisation engine on a random population of configurations
   *  @return the multiset of key objectives in the resulting population
   */

  def run(optimisationEngine: OptimisationEngine, objectiveFunction: MockObjectiveFunction,
    populationSize: Int, urgency: Double, quality: Double, seed: Long): (java.util.List[(Configuration, java.lang.Double)], java.lang.Long) = {

    val rng = new java.util.Random(seed)

    //    val objectiveFunction = if (HttpRemoteOptimisationEngineServer.UseRCPS)
    //      new MockObjectiveFunctionRCPS(scaleFactor = 1, rng)
    //    else
    // val objectiveFunction = MockObjectiveFunction.largerPlant() // rng)
    val configurationType = objectiveFunction.configurationType

    // jeep.lang.Diag.println(configurationType)

    val (optimisationResult, elapsedMillis) = applyOptimisationEngine(
      populationSize, urgency, quality,
      objectiveFunction, configurationType, optimisationEngine, rng
    )

    val population = optimisationResult.getReconfigurations
    val histo = objectiveFunction.soFitnessHistogram(population)

    // println(s"fitness values: ${histo}")

    val fitnessArray = histo.toArray.map { x => x.asInstanceOf[Double] }

    val mean = org.apache.commons.math3.stat.StatUtils.mean(fitnessArray)
    val sd = math.sqrt(org.apache.commons.math3.stat.StatUtils.variance(fitnessArray))
    println(s"post-optimisation fitness  -- mean: ${mean}, sd: ${sd}, min = ${fitnessArray.min}, max = ${fitnessArray.max}")

    val populationWithFitness = population.map { c =>
      (c, new java.lang.Double(objectiveFunction.singleObjectiveFitness(c.getKeyObjectives)))
    }
    (populationWithFitness, elapsedMillis)
  }
}

// End ///////////////////////////////////////////////////////////////
