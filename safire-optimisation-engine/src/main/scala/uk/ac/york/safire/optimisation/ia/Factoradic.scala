package uk.ac.york.safire.optimisation.ia

object Factoradic {

  case class Permutation(value: List[Int]) {
    require(value.forall { x => x >= 0 && x < value.size } &&
      value.toSet.size == value.size)

    def apply(i: Int): Int = {
      require(i >= 0)
      if (i < value.size) value(i) else i
    }
  }

  /////////////////////////////////

  def factorial(n: BigInt): BigInt = {
    require(n >= 0)
    if (n == 0) 1 else n * factorial(n - 1)
  }

  def encode(k: Int, n: Int): BigInt = {
    require(n >= 0)
    require(k >= 0 && k < factorial(n))

    val result: BigInt = 0
    while (k != 0) {

    }

    ???
  }

  case class Factoradic(value: List[Int]) {
    require(value.zipWithIndex.forall { case (v, index) => v >= 0 && v < index + 1 }, value.zipWithIndex)

    def toPerm: Permutation = ???
  }

  object Factoradic {

    // To convert to factoradic, divide the input by succesive integers (starting at 1) 
    // until the quotient becomes zero. 
    // The factoradic representation is given by the successive reminders.
    def apply(k: Int): Factoradic = {
      var i = 1
      var remainders = List.empty[Int]
      var quot = k
      while (quot != 0) {
        remainders = remainders :+ (quot % i)
        quot /= i
        i += 1
      }
      Factoradic(remainders)
    }
  }

  def decode(f: BigInt, n: Int): Permutation = ???

  def main(args: Array[String]): Unit = {

    println(s"factoradic( 349 ): ${Factoradic(349)}")

    //    for( n <- 0 until 100 )
    //      println( s"factoradic( $n ): ${factoradic(n)}" )
  }
}

// End ///////////////////////////////////////////////////////////////
