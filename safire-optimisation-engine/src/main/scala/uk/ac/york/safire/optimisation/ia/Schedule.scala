package uk.ac.york.safire.optimisation.ia

import scala.collection.mutable.{ Map => MutableMap }

///////////////////////////////////

trait ProductReporter {
  def apply(r: Resource, schedule: List[(Task, Interval)]): Option[Product]
}

///////////////////////////////////

case class Schedule(byResource: Map[Resource, List[(Task, Interval)]]) {

  require(Schedule.topologicallySorted(this))
  require(Schedule.compatibleResources(this))
  require(Schedule.uniqueTaskIds(this))
  //  require(Schedule.resourcewiseMonotoneIncreasingIntervals(this))

  /////////////////////////////////

  def makespan: Int = {
    val byResourceVaildAllocation = byResource.filter(_._1.id != "No allocation")

    if (byResourceVaildAllocation.isEmpty)
      9999999
    else {
      val byResourceVaildTasks = byResourceVaildAllocation.values.flatten.filter(t => !t._1.id.name.contains("NotAvailble"))
      if (byResourceVaildTasks.isEmpty)
        9999999
      else {
        byResourceVaildTasks.map(t => t._2.upper).max
      }
    }

    //    if (byResource.filter( _._1.id != "No allocation").isEmpty) 9999999 else {
    //      byResource.filter(_._1.id != "No allocation").values.map { _.last._2.upper }.max
    //    }
  }

  def energy: Int =
    if (byResource.filter(_._1.id != "No allocation").isEmpty) 9999999 else {
      byResource.filter(_._1.id != "No allocation").values.flatten.map { t => t._1.energyCost }.sum
    }

  def montary: Int =
    if (byResource.filter(_._1.id != "No allocation").isEmpty) 9999999 else {
      byResource.filter(_._1.id != "No allocation").values.flatten.map { t => t._1.montaryCost }.sum
    }

  //  def idleTime(r: Resource, mutex: Set[Resource]): Int = {
  //    def impl(r: Resource): Int = byResource.get(r) match {
  //      case Some(l) => l.last._2.upper
  //      case None    => 0
  //    }
  //
  //    byResource.get(r) match {
  //      case Some(l) => if (mutex.isEmpty) l.last._2.upper else math.max(l.last._2.upper, mutex.map { impl }.max)
  //      case None    => if (mutex.isEmpty) 0 else math.max(0, mutex.map { r2 => impl(r2) }.max)
  //    }
  //  }

  def idleTime(r: Resource, mutex: Set[Resource], executionTime: Int): Int = {

    def impl(firstIdleTime: Int): Int =
      {

        val maxTime1 = mutex.map(res => byResource.filter(_._1.id.contains(res.id)).values).flatten
        //        val maxTime2 = maxTime1.map { taskList => taskList.filter(task => task._1.id.name.contains(r.id)) }
        val time = maxTime1.map(taskList => taskList.map(task => task._2)).flatten.toList.sortBy(_.upper);

        if (time.isEmpty) firstIdleTime
        else {
          if (time.head.lower - firstIdleTime >= executionTime) firstIdleTime
          else {
            val timePar = time.sliding(2).toList
            val possibleTimeRange = timePar.filter(list => list.last.lower - list.head.upper >= executionTime).map(list => (list.head.upper, list.last.lower)) // ).filter(t => t >= firstIdleTime)
            val possibleTimeRangeFilter = possibleTimeRange.filter(p => p._2 >= firstIdleTime + executionTime).map(p => p._1).sorted;

            if (possibleTimeRangeFilter.isEmpty)
              math.max(time.map(_.upper).max, firstIdleTime)
            else
              math.max(possibleTimeRangeFilter.min, firstIdleTime)

          }
        }

      }

    byResource.get(r) match {
      case Some(l) => {
        val vaildTasks = l.filter(t => !t._1.id.name.contains("NotAvailble"))
        val firstIdle = if (vaildTasks.isEmpty) 0 else vaildTasks.last._2.upper
        //        val firstIdle = 0;
        if (mutex.isEmpty) firstIdle else impl(firstIdle)
      }
      case None => if (mutex.isEmpty) 0 else impl(0)
    }
  }

  def idleTimeElectrolux(r: Resource, mutex: Set[Resource], taskID: String, executionTime: Int): Int = {
    def impl(firstIdleTime: Int, r: Resource): Int =
      {
        val maxTime1 = byResource.filter(_._1.id.contains(r.id)).values
        val time = maxTime1.map(taskList => taskList.map(task => task._2)).flatten.toList.sortBy(_.upper);

        if (time.isEmpty) firstIdleTime
        else {
          if (time.head.lower - firstIdleTime >= executionTime) firstIdleTime
          else {
            val timePar = time.sliding(2).toList
            val possibleTimeRange = timePar.filter(list => list.last.lower - list.head.upper >= executionTime).map(list => (list.head.upper, list.last.lower)) // ).filter(t => t >= firstIdleTime)
            val possibleTimeRangeFilter = possibleTimeRange.filter(p => p._2 >= firstIdleTime + executionTime).map(p => p._1).sorted;

            if (possibleTimeRangeFilter.isEmpty)
              math.max(time.map(_.upper).max, firstIdleTime)
            else
              math.max(possibleTimeRangeFilter.min, firstIdleTime)

          }
        }

      }

    byResource.get(r) match {
      case Some(l) => if (mutex.isEmpty) l.last._2.upper else mutex.map { r2 => impl(l.last._2.upper, r2) }.max
      case None => if (mutex.isEmpty) 0 else mutex.map { r2 => impl(0, r2) }.max
    }
  }

  def idleTimeOAS(r: Resource, mutex: Set[Resource], taskID: String, executionTime: Int): Int = {
    def impl(firstIdleTime: Int, r: Resource): Int =
      {
        val maxTime1 = byResource.filter(_._1.id.contains(r.id)).values
        val resourceNoIndex = r.id.filterNot(c => c == '(' || c == ')' || c.isDigit).toLowerCase()
        val maxTime2 = maxTime1.map { taskList => taskList.filter(task => task._1.id.name.contains(resourceNoIndex)) }
        val time = maxTime2.map(taskList => taskList.map(task => task._2)).flatten.toList.sortBy(_.upper);

        if (time.isEmpty) firstIdleTime
        else {
          if (time.head.lower - firstIdleTime >= executionTime) firstIdleTime
          else {
            val timePar = time.sliding(2).toList
            val possibleTimeRange = timePar.filter(list => list.last.lower - list.head.upper >= executionTime).map(list => (list.head.upper, list.last.lower)) // ).filter(t => t >= firstIdleTime)
            val possibleTimeRangeFilter = possibleTimeRange.filter(p => p._2 >= firstIdleTime + executionTime).map(p => p._1).sorted;

            if (possibleTimeRangeFilter.isEmpty)
              math.max(time.map(_.upper).max, firstIdleTime)
            else
              math.max(possibleTimeRangeFilter.min, firstIdleTime)

          }
        }

      }

    val resourceName = taskID.split(" ").last.split("_").drop(1).map(n => n.capitalize);
    val mutualResources1 = for (r1 <- resourceName) yield List.empty[Resource] ++ mutex.filter(r => r.id.contains(r1))
    val mutualResources = mutualResources1.flatten

    byResource.get(r) match {
      case Some(l) => if (mutex.isEmpty) l.last._2.upper else mutualResources.map { r2 => impl(l.last._2.upper, r2) }.max
      case None => if (mutex.isEmpty) 0 else mutualResources.map { r2 => impl(0, r2) }.max
    }
  }

  def taskIds: Set[TaskId] =
    byResource.values.toList.flatten.map { _._1.id }.toSet

  def containsTaskId(tId: TaskId): Boolean =
    taskIds.contains(tId)

  def generalPredecesorsScheduled(t: Task): Boolean =
    t.generalPredecessors.forall { case taskId => containsTaskId(taskId) }

  def nonImmediatePredecesorsScheduled(t: Task): Boolean =
    t.generalPredecessors.forall { case taskId => if (t.immediatePrecessors.contains(taskId)) true else containsTaskId(taskId) }

  def entry(t: TaskId): Option[(Task, Interval)] =
    byResource.values.flatten.find { case (task, interval) => task.id == t }

  def completionTime(t: TaskId): Option[Int] =
    entry(t).map { _._2.upper }

  def maxCompletionTime(taskIds: Set[TaskId]): Int = {
    if (taskIds.isEmpty) 0 else {
      val times = taskIds.map { completionTime(_) }.flatten
      if (times.isEmpty) 0 else times.max
    }
  }

  /////////////////////////////////

  def append(r: Resource, t: Task, requireImmediatePredecesors: Boolean = true, notBeforeTime: Int = 0): Option[Schedule] = {
    require(t.isCompatibleWithResource(r))
    // require( generalPredecesorsScheduled(t) )
    require(!containsTaskId(t.id), s"${t.id} already present in ${taskIds}")
    require(notBeforeTime >= 0)

    val predecessorsScheduled = if (requireImmediatePredecesors) generalPredecesorsScheduled(t) else nonImmediatePredecesorsScheduled(t)
    if (predecessorsScheduled) {

      val lowerByDependencies = maxCompletionTime(t.generalPredecessors)
      val lowerByResource = if (!byResource.contains(r) || byResource(r).isEmpty) 0 else 0 //byResource(r).last._2.upper
      val lower = math.max(lowerByDependencies, math.max(lowerByResource, notBeforeTime))
      val entries = byResource.getOrElse(r, Nil)
      Some(Schedule(byResource.updated(r, entries :+ (t, Interval(lower, lower + t.duration(r))))))
    } else
      None
  }

  //  def append(seq: List[(Resource,Task)]): Schedule = {
  //    var result = this
  //    seq.foreach { case (r,t) => result = result.append(r,t) }
  //    result
  //  }

  def concat(schedule: Schedule): Option[Schedule] = {
    var result = this
    schedule.byResource.foreach {
      case (resource, tasks) =>
        tasks.foreach {
          case (task, interval) =>
            result.append(resource, task, requireImmediatePredecesors = true) match {
              case Some(newSchedule) => result = newSchedule
              case None => return None
            }
        }
    }
    Some(result)
  }

  def postponeBy(time: Int): Schedule = {
    require(time >= 0)
    Schedule(byResource.map { case (k, tasks) => k -> tasks.map { case (t, i) => (t, i.translate(time)) } })
  }

  /////////////////////////////////

  override def toString(): String = {
    def formatTasks(l: List[(Task, Interval)]): String =
      l.map { case (task, interval) => (task.id.name, interval) }.mkString("[", ",", "]")

    val byResourceStr = byResource.map { case (k, v) => s"${k.id} -> ${formatTasks(v)}" }.mkString(" ", "\n ", "")
    s"Schedule(\n${byResourceStr}\n)"
  }
}

/////////////////////////////////

object Schedule {

  def apply(): Schedule = Schedule(Map.empty[Resource, List[(Task, Interval)]])
  def applyEle(res: Map[Resource, List[(Task, Interval)]]): Schedule = Schedule(res)
  def apply(r: Resource, t: Task): Schedule =
    Schedule(Map(r -> List((t, Interval(t.releaseTime, t.duration(r))))))

  /////////////////////////////////

  def compatibleResources(schedule: Schedule): Boolean =
    schedule.byResource.forall {
      case (r, l) =>
        l.forall {
          case (t, i) =>
            t.isCompatibleWithResource(r)
        }
    }

  def uniqueTaskIds(ordered: List[Task]): Boolean =
    ordered.map { _.id }.toSet.size == ordered.size

  def uniqueTaskIds(schedule: Schedule): Boolean =
    schedule.taskIds.size == schedule.byResource.values.flatten.size

  def topologicallySorted(ordered: List[Task]): Boolean = {
    val mapTaskIdToIndex = Map() ++ ordered.map { _.id }.zipWithIndex

    ordered.forall { task =>
      val taskIndex = mapTaskIdToIndex(task.id)
      task.generalPredecessors.forall { parent =>
        mapTaskIdToIndex.contains(parent) &&
          mapTaskIdToIndex(parent) < taskIndex
      }
    }
  }

  def topologicallySorted(schedule: Schedule): Boolean =
    schedule.byResource.values.flatten.forall {
      case (t, i) =>
        schedule.maxCompletionTime(t.generalPredecessors) <= i.lower
    }

  def disjointIntervals(seq: List[Interval]): Boolean = {
    for (i <- 0 until seq.length; j <- i + 1 until seq.length)
      if (seq(i).overlaps(seq(j)))
        return false

    true
  }

  def monotoneIncreasingIntervals(seq: List[Interval]): Boolean =
    seq.zip(seq.tail).forall { case (a, b) => a.upper <= b.lower }

  def resourcewiseMonotoneIncreasingIntervals(schedule: Schedule): Boolean =
    schedule.byResource.values.forall { l => monotoneIncreasingIntervals(l.map { _._2 }) }

  def productReport(schedule: Schedule, reporter: ProductReporter): List[Option[Product]] =
    schedule.byResource.toList.map { case (res, tasks) => reporter(res, tasks) }

  def consolodatedProductReport(schedule: Schedule, reporter: ProductReporter): List[Product] = {
    val removeNones = productReport(schedule, reporter).flatten
    val mapNameToProducts = removeNones.groupBy { _.name }
    mapNameToProducts.map { case (name, products) => Product(name, products.map { _.quantity }.sum) }.toList
  }
}

// End ///////////////////////////////////////////////////////////////
