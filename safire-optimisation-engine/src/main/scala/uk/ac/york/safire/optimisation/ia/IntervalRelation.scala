package uk.ac.york.safire.optimisation.ia

sealed trait IntervalRelation {
  def holds(a: Interval, b: Interval): Boolean
}

//object IntervalRelation extends Enumeration {
//  
//  type IntervalRelation = Value
//  val PreceededGenerallyBy, PreceededImmediatelyBy = Value
//  
//  def holds(r: IntervalRelation, a: Interval, b: Interval): Boolean = r match {
//    case PreceededGenerallyBy   => b.preceedsGenerally(a)
//    case PreceededImmediatelyBy => b.preceedsImmediately(a)
//  }
//}

///////////////////////////////////

object IntervalRelation {

  case object PreceededGenerallyBy extends IntervalRelation {

    override def holds(a: Interval, b: Interval): Boolean =
      b.preceedsGenerally(a)
  }

  case object PreceededImmediatelyBy extends IntervalRelation {

    override def holds(a: Interval, b: Interval): Boolean =
      b.preceedsImmediately(a)
  }
}

///////////////////////////////////

/**
 * **********
 *
 * public enum IntervalRelation {
 *
 * //	PRECEEDS_GENERALLY {
 * //		public boolean holds(Interval a, Interval b) { return a.preceedsGenerally(b); }
 * //	},
 * PRECEEDED_GENERALLY_BY {
 * public boolean holds(Interval a, Interval b) { return b.preceedsGenerally(a); }
 * },
 * //	PRECEEDS_IMMEDIATELY {
 * //		public boolean holds(Interval a, Interval b) { return a.preceedsImmediately(b); }
 * //	},
 * PRECEEDED_IMMEDIATELY_BY {
 * public boolean holds(Interval a, Interval b) { return b.preceedsImmediately(a); }
 * };
 * //	,
 * //	SUCCEEDS_GENERALLY {
 * //		public boolean holds(Interval a, Interval b) { return a.succeedsGenerally(b); }
 * //	},
 * //	SUCCEEDED_GENERALLY_BY {
 * //		public boolean holds(Interval a, Interval b) { return b.succeedsGenerally(a); }
 * //	},
 * //	SUCCEED_IMMEDIATELY {
 * //		public boolean holds(Interval a, Interval b) { return a.succeedsImmediately(b); }
 * //	},
 * //	SUCCEEDED_IMMEDIATELY_BY {
 * //		public boolean holds(Interval a, Interval b) { return b.succeedsImmediately(a); }
 * //	},
 * //	OVERLAPS  {
 * //		public boolean holds(Interval a, Interval b) { return a.overlaps(b); }
 * //	},
 * //	OVERLAPPED_BY  {
 * //		public boolean holds(Interval a, Interval b) { return b.overlaps(a); }
 * //	},
 * //	STARTS  {
 * //		public boolean holds(Interval a, Interval b) { return a.starts(b); }
 * //	},
 * //	DURING  {
 * //		public boolean holds(Interval a, Interval b) { return a.during(b); }
 * //	},
 * //	DURING_BY  {
 * //		public boolean holds(Interval a, Interval b) { return b.during(a); }
 * //	},
 * //	FINISHES  {
 * //		public boolean holds(Interval a, Interval b) { return a.finishes(b); }
 * //	},
 * //	EQUALS  {
 * //		public boolean holds(Interval a, Interval b) { return a == b; }
 * //	};
 *
 * ///////////////////////////////
 *
 * public abstract boolean holds(Interval a, Interval b);
 * }
 *
 * **********
 */

// End ///////////////////////////////////////////////////////////////
