package uk.ac.york.safire.optimisation.ia

import spray.json._
import uk.ac.york.safire.metrics.JsonConverter

///////////////////////////////////

object IAJSonProtocol extends DefaultJsonProtocol {

  // implicit val intervalFormat: JsonFormat[Interval] = jsonFormat2(uk.ac.york.safire.optimisation.ia.Interval)
  // ^ this *should* work -- not sure why it doesn't
  implicit val intervalFormat = jsonFormat[Int, Int, Interval](Interval, "lower", "upper")

  implicit val taskIdFormat = jsonFormat1(TaskId)
  // implicit val resourceFormat = jsonFormat1(Resource)
  implicit object resourceFormat extends RootJsonFormat[Resource] {
    override def write(r: Resource): JsValue = JsString(r.id)

    override def read(value: JsValue): Resource = value match {
      case JsString(str) => Resource(str)
    }
  }

  implicit object intervalRelationFormat extends RootJsonFormat[IntervalRelation] {
    override def write(r: IntervalRelation): JsValue = r match {
      case IntervalRelation.PreceededGenerallyBy => JsString("PreceededGenerallyBy")
      case IntervalRelation.PreceededImmediatelyBy => JsString("PreceededImmediatelyBy")
    }

    override def read(value: JsValue): IntervalRelation = value match {
      case JsString("PreceededGenerallyBy") => IntervalRelation.PreceededGenerallyBy
      case JsString("PreceededImmediatelyBy") => IntervalRelation.PreceededImmediatelyBy
    }
  }

  implicit val productFormat = jsonFormat2(Product)
  // implicit val taskFormat = jsonFormat7(Task.apply)
  // ^ this *should* work -- not sure why it doesn't
  implicit val taskFormat = jsonFormat[TaskId, Map[Resource, Int], Map[TaskId, IntervalRelation], Int, Int, Int, Int, Map[Resource, Set[Resource]], Map[Resource, Set[Resource]], Option[Product], Task](
    Task.apply, "id", "resourceExecutionTime", "relations", "releaseTime", "energyCost", "montaryCost", "isFinal", "mutexMap", "antiAffinityMap", "finalProduct"
  )
  // ^ .apply is necessary because the case class Task has a companion object
}

// End ///////////////////////////////////////////////////////////////
