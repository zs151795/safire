package uk.ac.york.safire.optimisation

import org.apache.commons.lang3.ArrayUtils

import org.uma.jmetal.algorithm._
import org.uma.jmetal.algorithm.impl._
import org.uma.jmetal.algorithm.singleobjective._
import org.uma.jmetal.algorithm.singleobjective.geneticalgorithm._
import org.uma.jmetal.algorithm.multiobjective._
import org.uma.jmetal.algorithm.multiobjective.gwasfga._
import org.uma.jmetal.algorithm.multiobjective.mocell._
import org.uma.jmetal.algorithm.multiobjective.mombi._
import org.uma.jmetal.algorithm.multiobjective.nsgaii._
import org.uma.jmetal.algorithm.multiobjective.nsgaiii._
import org.uma.jmetal.algorithm.multiobjective.pesa2._
import org.uma.jmetal.algorithm.multiobjective.randomsearch._
import org.uma.jmetal.algorithm.multiobjective.spea2._
import org.uma.jmetal.algorithm.multiobjective.smsemoa._
import org.uma.jmetal.operator._
import org.uma.jmetal.operator.impl.crossover._
import org.uma.jmetal.operator.impl.mutation._
import org.uma.jmetal.operator.impl.selection._
import org.uma.jmetal.problem._
import org.uma.jmetal.solution._
import org.uma.jmetal.solution.impl._
import org.uma.jmetal.util.AlgorithmRunner
import org.uma.jmetal.util.evaluator._
import org.uma.jmetal.util.evaluator.impl._
import org.uma.jmetal.util._

import uk.ac.york.safire.metrics.{ Utility => MetricsUtility, _ }
import scala.collection.JavaConversions._

///////////////////////////////////

class MySimpleRandomMutation[S <: Solution[Number]](probability: Double, ct: ConfigurationType, rng: java.util.Random)
    extends MutationOperator[S] {
  require(probability >= 0 && probability <= 1.0)

  override def execute(solution: S): S = {
    require(solution != null)

    val randomVisitor = new ValueTypeVisitor.RandomValueVisitor(rng)
    val numberVisitor = new ValueVisitor.NumberVisitor()

    ct.getControlledMetrics().zipWithIndex.map {
      case (metric, index) =>
        if (rng.nextDouble() <= probability)
          solution.setVariableValue(index, numberVisitor.visit(randomVisitor.visit(metric.valueType)))
    }

    solution
  }
}

///////////////////////////////////

class MyNPointCrossover[S <: Solution[Number]](probability: Double, crossovers: Int, rng: java.util.Random) extends CrossoverOperator[S] {
  require(probability >= 0 && probability <= 1.0)
  require(crossovers >= 1)

  /////////////////////////////////

  def getNumberOfGeneratedChildren(): Int = 2
  def getNumberOfRequiredParents(): Int = 2

  override def execute(s: java.util.List[S]): java.util.List[S] = {
    if (getNumberOfRequiredParents() != s.size())
      throw new JMetalException("Point Crossover requires + " + getNumberOfRequiredParents() + " parents, but got " + s.size());

    if (rng.nextDouble() < probability) doCrossover(s) else s
  }

  private def doCrossover(s: java.util.List[S]): java.util.List[S] = {
    val mom = s.get(0);
    val dad = s.get(1);

    if (mom.getNumberOfVariables() != dad.getNumberOfVariables())
      throw new JMetalException("The 2 parents doesn't have the same number of variables");
    if (crossovers > mom.getNumberOfVariables())
      throw new JMetalException("The number of crossovers is higher than the number of variables");

    val crossoverPoints = Array.fill(crossovers) { rng.nextInt(mom.getNumberOfVariables()) }

    val girl = mom.copy().asInstanceOf[S]
    val boy = dad.copy().asInstanceOf[S]

    var swap = false
    for (i <- 0 until mom.getNumberOfVariables()) {
      if (swap) {
        boy.setVariableValue(i, mom.getVariableValue(i));
        girl.setVariableValue(i, dad.getVariableValue(i));

      }

      if (ArrayUtils.contains(crossoverPoints, i)) {
        swap = !swap;
      }
    }

    List(girl, boy)
  }
}

///////////////////////////////////

class ConfigurationSolution(
    val config: Configuration,
    problem: IntegerDoubleProblem[ConfigurationSolution],
    attribs: java.util.Map[Object, Object] = new java.util.HashMap[Object, Object]()
// , val observableMetrics: Map[String,Value]
) extends AbstractGenericSolution[Number, IntegerDoubleProblem[ConfigurationSolution]](problem) {

  initialiseVariables()
  super.initializeObjectiveValues()
  attributes = attribs

  /////////////////////////////////

  override def copy(): ConfigurationSolution =
    ConfigurationSolution(this)

  override def getVariableValueString(index: Int): String =
    getVariableValue(index).toString()

  private def variableBounds(index: Int): (Number, Number) = {
    assert(index >= 0 && index <= getNumberOfVariables())
    ConfigurationSolution.variableBounds(config.getConfigurationType())(index)
  }

  private def isValidVariableValue(index: Int, value: Number): Boolean = {
    val bounds = variableBounds(index)
    bounds._1.doubleValue() <= value.doubleValue() &&
      value.doubleValue() <= bounds._2.doubleValue()
  }

  override def setVariableValue(index: Int, value: Number): Unit = {
    assert(
      isValidVariableValue(index, value),
      s"var index: $index, expected value in ${variableBounds(index)}, found ${value}"
    )

    super.setVariableValue(index, value)
  }

  /////////////////////////////////

  def getProblem(): IntegerDoubleProblem[ConfigurationSolution] = problem
  def getAttributes(): java.util.Map[Object, Object] = attributes

  def controlMetrics: Map[String, Value] = {

    def toValue(x: Number, vt: ValueType): Value = vt.getType match {
      case ValueType.Type.INT => Value.intValue(x.intValue(), vt.asInstanceOf[ValueType.Integer])
      case ValueType.Type.REAL => Value.realValue(x.doubleValue(), vt.asInstanceOf[ValueType.Real])
      case ValueType.Type.NOMINAL =>
        //        jeep.lang.Diag.println( vt );
        //        jeep.lang.Diag.println( x );        
        Value.nominalValue(x.intValue(), vt.asInstanceOf[ValueType.Nominal])
    }

    Map() ++ config.getConfigurationType().getControlledMetrics().zipWithIndex.map {
      case (metric, index) =>
        metric.name -> toValue(getVariableValue(index), metric.valueType)
    }
  }

  /////////////////////////////////

  private def initialiseVariables(): Unit = {
    val numberVisitor = new ValueVisitor.NumberVisitor()

    //   jeep.lang.Diag.println(config.getConfigurationType().getControlledMetrics())

    config.getConfigurationType().getControlledMetrics().zipWithIndex.map {
      case (metric, index) =>
        val metricValue = config.getControlledMetrics().get(metric.name)
        // val bounds = variableBounds(index)
        // val asNumber = numberVisitor.visit(metricValue)
        // jeep.lang.Diag.println( s"${metric.name} ${bounds} ${asNumber}" )

        setVariableValue(index, numberVisitor.visit(metricValue))
    }
  }
}

///////////////////////////////////

object ConfigurationSolution {

  type ProblemType = IntegerDoubleProblem[ConfigurationSolution]

  /////////////////////////////////

  def isValid(sol: ConfigurationSolution): Boolean = {
    val varBounds = variableBounds(sol.config.getConfigurationType)
    varBounds.zipWithIndex.forall {
      case (bounds, index) =>
        val varValue = sol.getVariableValue(index)
        varValue != null
      // FIXME: put this back in
      //        && bounds._1.doubleValue() <= varValue.doubleValue() &&
      //          bounds._2.doubleValue() <= varValue.doubleValue()
    }
  }

  def variableValueTypes(ct: ConfigurationType): List[ValueType] =
    ct.getControlledMetrics().map { _.valueType }.toList

  def variableBounds(ct: ConfigurationType): List[(Number, Number)] = {
    def numberBounds(vt: ValueType): (Number, Number) = {
      val pair = new ValueTypeVisitor.NumberRangeVisitor().visit(vt)
      (pair.getLeft(), pair.getRight())
    }
    ct.getControlledMetrics().map { m => numberBounds(m.valueType) }.toList
  }

  /////////////////////////////////

  def apply(c: ConfigurationSolution): ConfigurationSolution = {
    new ConfigurationSolution(c.config, c.getProblem, new java.util.HashMap[Object, Object](c.getAttributes)
    // ,c.observableMetrics
    )
  } ensuring { isValid(_) }

  def apply(ct: ConfigurationType, problem: ProblemType, rng: scala.util.Random): ConfigurationSolution = {
    new ConfigurationSolution(uk.ac.york.safire.metrics.Utility.randomConfiguration(ct, rng.self), problem)
  } ensuring { isValid(_) }
}

///////////////////////////////////

object JMetalLocalOptimisationEngine {

  type SolutionType = ConfigurationSolution
  type ProblemType = IntegerDoubleProblem[SolutionType]

  /////////////////////////////////

  def createProblem(parentConfig: Configuration, of: ObjectiveFunction, rng: java.util.Random): ProblemType = {

    val varBounds = ConfigurationSolution.variableBounds(parentConfig.getConfigurationType())
    val varTypes = ConfigurationSolution.variableValueTypes(parentConfig.getConfigurationType())

    ///////////////////////////////

    new IntegerDoubleProblem[ConfigurationSolution] {

      override def getLowerBound(index: Int): Number = varBounds(index)._1
      override def getUpperBound(index: Int): Number = varBounds(index)._2
      override def getNumberOfDoubleVariables(): Int = varTypes.count { x =>
        x.getType() == ValueType.Type.REAL
      }
      override def getNumberOfIntegerVariables(): Int = varTypes.count { x =>
        x.getType() == ValueType.Type.INT || x.getType() == ValueType.Type.NOMINAL
      }

      override def createSolution(): ConfigurationSolution = {
        // FIXME: check this 
        // createRandomSolution(ct,this,rng)
        // configurationToJMetalSolution(parentConfig, this)
        ConfigurationSolution(parentConfig.getConfigurationType(), this, rng)
      } ensuring { ConfigurationSolution.isValid(_) }

      override def evaluate(sol: ConfigurationSolution): Unit = {
        val objectives = ObjectiveFunction.evaluateNowHelper(of, new ObjectiveFunctionArguments(parentConfig, sol.controlMetrics)).getObjectiveValues
        objectives.zipWithIndex.map { case (value, index) => sol.setObjective(index, value) }
      }

      override def getName(): String = getClass().getName()
      override def getNumberOfConstraints(): Int = 0
      override def getNumberOfObjectives(): Int = of.numObjectives()
      override def getNumberOfVariables(): Int = varTypes.length
    }
  }

  /////////////////////////////////

  def allObjectivesMinimizing(ct: ConfigurationType): Boolean = {
    import scala.collection.JavaConversions._
    ct.getKeyObjectiveMetrics().forall { o => o.searchDirection == SearchDirection.MINIMIZING }
  }

  /////////////////////////////////

  def mkNSGA3(
    incumbent: List[Configuration],
    problem: ProblemType,
    ct: ConfigurationType,
    mutationProbability: Double,
    crossoverProbability: Double,
    maxEvaluations: Int,
    rng: java.util.Random
  ): NSGAIII[SolutionType] = {

    val incumbentSolutions: List[SolutionType] = incumbent.map { x => new ConfigurationSolution(x, problem) }
    val maxIterations = maxEvaluations / incumbentSolutions.length

    val NumCrossovers = 1
    val crossoverOperator: CrossoverOperator[SolutionType] = new MyNPointCrossover[SolutionType](crossoverProbability, NumCrossovers, rng)
    val mutationOperator = new MySimpleRandomMutation[SolutionType](mutationProbability, ct, rng)
    val selectionOperator = new BinaryTournamentSelection[SolutionType]()
    val eval = new SequentialSolutionListEvaluator[SolutionType]()

    ///////////////////////////////

    val nsgaiiiBuilder = new NSGAIIIBuilder(problem)
      .setMaxIterations(maxIterations)
      .setPopulationSize(incumbentSolutions.length)
      .setCrossoverOperator(crossoverOperator)
      .setMutationOperator(mutationOperator)
      .setSelectionOperator(selectionOperator)
      .setSolutionListEvaluator(eval)

    new NSGAIII(nsgaiiiBuilder) {
      // FIXME: do this for other methods
      override def createInitialPopulation(): java.util.List[SolutionType] = {
        import scala.collection.JavaConversions._
        incumbentSolutions
      }
    }
  }

  /////////////////////////////////

  def createSOGA(
    incumbent: List[Configuration],
    problem: ProblemType,
    ct: ConfigurationType,
    of: MockObjectiveFunction,
    maxEvaluations: Int, rng: java.util.Random
  ): AbstractGeneticAlgorithm[ConfigurationSolution, ConfigurationSolution] = {

    val NumCrossovers = 1
    val crossoverOperator: CrossoverOperator[SolutionType] = new MyNPointCrossover[SolutionType](0.3, NumCrossovers, rng)
    val mutationOperator = new MySimpleRandomMutation[SolutionType](0.01, ct, rng)
    // val selectionOperator = new BinaryTournamentSelection[SolutionType]()
    val numTournaments = 5
    val selectionOperator = new TournamentSelection[SolutionType](numTournaments)

    val eval = new SequentialSolutionListEvaluator[SolutionType]()

    import scala.collection.JavaConversions._
    val incumbentSolutions = new java.util.ArrayList[SolutionType]()
    incumbentSolutions.addAll(incumbent.map { x => new ConfigurationSolution(x, problem) })

    new GenerationalGeneticAlgorithm[SolutionType](problem, maxEvaluations, incumbent.size(),
      crossoverOperator, mutationOperator, selectionOperator, eval) {

      override def createInitialPopulation(): java.util.List[SolutionType] = {
        import scala.collection.JavaConversions._
        incumbentSolutions
      }

      def fittestValue(x: java.util.List[SolutionType], of: MockObjectiveFunction): Double =
        x.map { i => i.getObjective(0) }.min

      override protected def replacement(
        population: java.util.List[SolutionType],
        offspringPopulation: java.util.List[SolutionType]
      ): java.util.List[SolutionType] = {

        val comparator = new org.uma.jmetal.util.comparator.ObjectiveComparator[SolutionType](0)

        def isAscending(x: List[Double]): Boolean =
          if (x.isEmpty) true else x.zip(x.tail).forall { case (a, b) => a <= b }

        java.util.Collections.sort(population, comparator)
        val populationFitness = population.map { p => p.getObjective(0) }
        assert(isAscending(populationFitness.toList))

        offspringPopulation.add(population.get(0))
        offspringPopulation.add(population.get(1))
        java.util.Collections.sort(offspringPopulation, comparator)
        offspringPopulation.remove(offspringPopulation.size() - 1)
        offspringPopulation.remove(offspringPopulation.size() - 1)

        val offspringPopulationFitness = population.map { p => p.getObjective(0) }
        assert(isAscending(offspringPopulationFitness.toList))

        offspringPopulation
      } ensuring {
        result =>
          val resultFittest = fittestValue(result, of)
          resultFittest <= fittestValue(population, of)
      }
    }
  }

  /////////////////////////////////

  def createAlgorithmsMOGA(
    incumbent: List[Configuration],
    problem: ProblemType,
    ct: ConfigurationType,
    maxEvaluations: Int, rng: java.util.Random
  ): Map[AlgType, AbstractGeneticAlgorithm[SolutionType, java.util.List[SolutionType]]] = {

    require(allObjectivesMinimizing(ct))

    ///////////////////////////////

    val NumCrossovers = 1
    val crossoverOperator: CrossoverOperator[SolutionType] = new MyNPointCrossover[SolutionType](0.9, NumCrossovers, rng)
    val mutationOperator = new MySimpleRandomMutation[SolutionType](0.9, ct, rng)
    val selectionOperator = new BinaryTournamentSelection[SolutionType]()

    val eval = new SequentialSolutionListEvaluator[SolutionType]()

    val incumbentSolutions: List[SolutionType] = incumbent.map { x =>
      new ConfigurationSolution(x, problem)
    }

    val maxIterations = maxEvaluations / incumbentSolutions.length

    ///////////////////////////////

    //    val gwasfga = new GWASFGA(problem, incumbentSolutions.length, maxIterations, crossoverOperator,
    //      mutationOperator, selectionOperator,
    //      eval)

    //  val ibea = new IBEA(problem, populationSize, archiveSize, maxEvaluations,
    //      selectionOperator, crossoverOperator,
    //      mutationOperator)

    //    val mocellBuilder = new MOCellBuilder(problem, crossoverOperator, mutationOperator)
    //    val mocell = mocellBuilder.build()

    //    val mombi = new MOMBI2(problem, maxIterations, crossoverOperator, mutationOperator,
    //			selectionOperator, eval, String pathWeights)    

    //    val nsgaii = new NSGAII(problem, maxEvaluations, incumbentSolutions.length,
    //      crossoverOperator, mutationOperator,
    //      selectionOperator, eval)

    val nsgaiiiBuilder = new NSGAIIIBuilder(problem)
      .setMaxIterations(maxIterations)
      .setPopulationSize(incumbentSolutions.length)
      .setCrossoverOperator(crossoverOperator)
      .setMutationOperator(mutationOperator)
      .setSelectionOperator(selectionOperator)
      .setSolutionListEvaluator(eval)

    val nsgaiii = new NSGAIII(nsgaiiiBuilder) {

      // FIXME: do this for other methods
      override def createInitialPopulation(): java.util.List[SolutionType] = {
        import scala.collection.JavaConversions._
        incumbentSolutions
      }
    }

    //    val pesaBuilder = new PESA2Builder(problem, crossoverOperator, mutationOperator)
    //    val pesa2 = pesaBuilder.build()
    //
    //    val smsemoaBuilder = new SMSEMOABuilder(problem, crossoverOperator, mutationOperator)
    //      .setPopulationSize(incumbentSolutions.length)
    //      .setSelectionOperator(selectionOperator)
    //    val smesmoa = smsemoaBuilder.build()
    //
    //    val spea2 = new SPEA2(problem, maxEvaluations, incumbentSolutions.length,
    //      crossoverOperator, mutationOperator,
    //      selectionOperator, eval)

    Map() ++ List(
      // AlgType.GWASFGA -> gwasfga, 
      // AlgType.MOCELL -> mocell, AlgType.PESA2 -> pesa2, AlgType.SMSEMOA -> smesmoa, AlgType.SPEA2 -> spea2, AlgType.NGSA2 -> nsgaii, 
      AlgType.NSGA3 -> nsgaiii
    )
  }

  /////////////////////////////////

  /**
   * Single-Objective GA
   */

  //  def soga(evalMin: Int, evalMax: Int,
  //    objectiveFunction: MockObjectiveFunction,
  //    rng: java.util.Random): JMetalLocalOptimisationEngine = {
  //    require(evalMin > 0 && evalMin <= evalMax)
  //
  //    new JMetalLocalOptimisationEngine(evalMin, evalMax, objectiveFunction, rng)
  //    // new JMetalLocalOptimisationEngine(AlgType.NSGA3, evalMin, evalMax, objectiveFunction, rng)
  //  }

  def nsga3(evalMin: Int, evalMax: Int,
    objectiveFunction: ObjectiveFunction.LocalObjectiveFunction,
    rng: java.util.Random): JMetalLocalOptimisationEngine = {

    require(evalMin > 0 && evalMin <= evalMax)

    new JMetalLocalOptimisationEngine(AlgType.NSGA3, evalMin, evalMax, objectiveFunction, rng)
  }
}

///////////////////////////////////

class JMetalLocalOptimisationEngine(
    algType: AlgType,
    minEvaluations: Int,
    maxEvaluations: Int,
    objectiveFunction: ObjectiveFunction.LocalObjectiveFunction,
    rng: java.util.Random
) extends OptimisationEngine.LocalOptimisationEngine(objectiveFunction) {

  // require(objectiveFunction.numObjectives() == 1)
  require(minEvaluations > 0 && minEvaluations <= maxEvaluations)

  /////////////////////////////////

  override def optimise(args: OptimisationArguments): OptimisationResult = {
    if (args.getConfigurations().isEmpty)
      new OptimisationResult(args.getConfigurations())
    else {
      import JMetalLocalOptimisationEngine._

      val parentConfig = args.getConfigurations().get(0)

      // val maxEval = OptmisationUtility.maxEvaluations(args.getUrgency, args.getQuality, minEvaluations, maxEvaluations)
      // val maxEval = jeep.math.LinearInterpolation.apply(args.getQuality, 0.0, 1.0, minEvaluations, maxEvaluations)
      // FIXME: change this back
      assert(minEvaluations == maxEvaluations)
      val maxEval = maxEvaluations

      //      val initialPopulation = pop ++ List.fill(populationSize - pop.size) { parentConfig }
      //      assert(initialPopulation.size == populationSize)
      // val initialPopulation = List.fill(populationSize) { parentConfig }
      // val initialPopulation = pop // List.fill(populationSize) { parentConfig }    
      val problem = createProblem(parentConfig, objectiveFunction, rng) // FIXME - created from a single config

      import scala.collection.JavaConversions._
      //      val alg = createSOGA(args.getConfigurations().toList, problem,
      //        parentConfig.getConfigurationType, objectiveFunction, maxEval.toInt, rng)

      val algorithms = createAlgorithmsMOGA(
        args.getConfigurations.toList,
        problem,
        parentConfig.getConfigurationType(),
        maxEval, rng
      )
      val alg = algorithms(algType)
      // new AlgorithmRunner.Executor(alg).execute()

      var threwException = false
      var solutions: java.util.List[ConfigurationSolution] = new java.util.ArrayList[ConfigurationSolution]()
      try {
        alg.run() // FIXME: this is single-threaded
        solutions = alg.getPopulation() // alg.getResult()
      } catch {
        case t: Throwable => {
          jeep.lang.Diag.println(s"WARNING: ${alg.getClass()} threw an exception. Mesage: ${t.getMessage()},\n Stack trace: ${org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(t)}")
          // jeep.lang.Diag.println( s"WARNING: $algType threw an exception. Mesage: ${t.getMessage()}, class: ${t.getClass().getName()}" )        
          threwException = true
        }
      }
      if (solutions.isEmpty()) {
        if (!threwException) jeep.lang.Diag.println(s"WARNING: ${alg.getClass()} returned an empty list of solutions")

        // new OptimisationResult(Configuration.update(parentConfig.getControlledMetrics))
        new OptimisationResult(args.getConfigurations())
      } else {

        //    val objectives = solutions.map { sol =>
        //      objectiveFunction.value(parentConfig, new OptimisationResult(sol.controlMetrics))
        //    }

        //    val solutionsAndObjectives = solutions.map { sol =>    
        //        (sol,objectiveFunction.value(parentConfig,new OptimisationResult(sol.controlMetrics)).toList)    
        //    }

        //        def abbrev(s: String): String = s.take(1).map { Character.toUpperCase }
        //
        //        def formatControlMetrics(m: Map[String, Value]): String = {
        //          val valueStringVisitor = new ValueVisitor.ValueStringVisitor()
        //          m.toList.sortBy { _._1 }.map { case (k, value) => abbrev(valueStringVisitor.visit(value)) }.toList.mkString("\"(", ",", ")\"")
        //        }

        // val elements = solutions.map { sol => formatControlMetrics(sol.controlMetrics) }

        // println(s"""elements = {${elements.mkString(",")}}""")
        // println(s"""data = {${objectives.map { _.mkString("{", ",", "}") }.mkString(",")}}""")
        // println("ListPlot[MapThread[Labeled, {data, elements}]]")

        import scala.collection.JavaConversions._
        new OptimisationResult(solutions.map { sol =>
          Configuration.update(
            parentConfig,
            sol.controlMetrics,
            objectiveFunction.predictKeyObjectives(parentConfig, sol.controlMetrics).keyObjectiveMetrics
          )
        })
      }
    }
  }
}

// End ///////////////////////////////////////////////////////////////
