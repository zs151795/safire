package uk.ac.york.safire.optimisation.mitm.atb

import uk.ac.york.safire.metrics._

import uk.ac.york.safire.optimisation._
import uk.ac.york.safire.optimisation.ia._

import scala.collection.mutable.PriorityQueue

import uk.ac.york.safire.metrics.ValueVisitor

import uk.ac.york.safire.optimisation.ObjectiveFunction._
import uk.ac.york.safire.optimisation.ia._
import uk.ac.york.safire.metrics._

///////////////////////////////////

/**
 * Minimise total cost and makespan, for a hardwired bi-level configuration
 */

class OnaMultipleObjectiveFunction extends IAObjectiveFunction {

  //jeep.lang.Diag.println()
  //System.exit(0)

  override def predictKeyObjectives(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result =
    predictKeyObjectivesImpl(originalConfig, proposedControlMetrics)

  /////////////////////////////////

  private def predictKeyObjectivesImpl(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result = {

    //jeep.lang.Diag.println()
    //System.exit(0)

    import scala.collection.JavaConversions._

    val config = Configuration.update(originalConfig, proposedControlMetrics, originalConfig.getKeyObjectives)
    val (recipes, resources) = IAObjectiveFunction.reverseEngineerRecipesFromConfig(config)

    ///////////////////////////////

    OnaMultipleObjectiveFunction.makeTasks(recipes, resources, config) match {
      case m @ Left(errorString) => new Result(config.getKeyObjectives, errorString)
      case Right(tasks) => {
        val scheduler = new FIFOScheduler()
        def priority(t: Task): Int = IAObjectiveFunction.priority(t.id.name, config)
        val taskQueue: PriorityQueue[Task] = PriorityQueue(tasks: _*)(Ordering.by { priority })
        scheduler.schedule(taskQueue, resources.toSet) match {
          case None => new Result(config.getKeyObjectives, "Scheduler cannot find a valid schedule")
          case Some(schedule) =>
            val newKeyObjectives = doUpdatedKeyObjectives(schedule, config)
            val reportStr = "Status: Succeeded.\n" + toReportString(schedule, newKeyObjectives)
            new Result(newKeyObjectives, reportStr)
        }
      }
    }
  }

  /////////////////////////////////

  override def numObjectives: Int = 2

  override def objectiveValues(result: Result): java.util.List[java.lang.Double] = {
    import scala.collection.JavaConversions._
    val cost = IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("totalcost")).asInstanceOf[java.lang.Double]
    val makespan = IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("makespan")).asInstanceOf[java.lang.Double]

    // List(IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("totalcost")).asInstanceOf[java.lang.Double])
    // val xx = (OnaObjectiveFunction.W1*makespan)/(50*111917.3) + (OnaObjectiveFunction.W2*cost)/68616.1
    // val xx = (OnaObjectiveFunction.W1 * makespan) / (50 * 111917.3) + (OnaObjectiveFunction.W2 * cost) / 68616.1
    List[java.lang.Double](cost, makespan)
  }

  /////////////////////////////////

  protected override def doUpdatedKeyObjectives(schedule: Schedule, config: Configuration): Map[String, Value] = {

    import scala.collection.JavaConversions._

    // jeep.lang.Diag.println( config.getObservableMetrics().filter { o => o._1.contains("cost" )} )

    val totalcost = (for ((resource, tasks) <- schedule.byResource; task <- tasks) yield {
      val recipeAndResourceName = task._1.id.name + " " + resource.id

      if (config.getObservableMetrics().get(recipeAndResourceName + " cost") == null)
        jeep.lang.Diag.println("cost not found for: " + recipeAndResourceName)
      IAObjectiveFunction.doubleValue(config.getObservableMetrics().get(recipeAndResourceName + " cost"))
    }).sum

    //   val normalisedCost= (OnaObjectiveFunction.W1*totalcost)/68616.1
    //    val normalisedMakespan = (OnaObjectiveFunction.W2*schedule.makespan)/(50*111917.3)

    // jeep.lang.Diag.println(s"normalisedCost: $normalisedCost, normalisedMakespan: $normalisedMakespan")

    //    Map("totalcost" -> Value.realValue(totalcost, ValueType.realType(0.0, Double.MaxValue)),
    //        "makespan" -> Value.realValue(schedule.makespan, config.getKeyObjectives().get("makespan").getType.asInstanceOf[ValueType.Real])
    Map(
      //"totalcost-normalised" -> Value.realValue(normalisedCost, ValueType.realType(0.0, Double.MaxValue)),
      //"makespan-normalised" -> Value.realValue(normalisedMakespan, ValueType.realType(0.0, Double.MaxValue)),
      "totalcost" -> Value.realValue(totalcost, ValueType.realType(0.0, Double.MaxValue)),
      "makespan" -> Value.realValue(schedule.makespan, config.getKeyObjectives().get("makespan").getType.asInstanceOf[ValueType.Real])
    )
  }

  /////////////////////////////////

  protected override def doReportStringSuffix(s: Schedule, newKeyObjectives: Map[String, Value]): String = {
    s"\nmakespan: ${new jeep.util.HMS(s.makespan)}\n" ++
      s"\ncost: ${IAObjectiveFunction.doubleValue(newKeyObjectives("totalcost"))}\n"
  }
}

///////////////////////////////////

object OnaMultipleObjectiveFunction {

  import IAObjectiveFunction._

  private case class DependencyInfo(name: String, dependentNamePrefixes: List[String])

  private def parseDependencyInfo(s: String): DependencyInfo = {
    import scala.util.parsing.combinator._
    import scala.util.parsing.combinator.JavaTokenParsers

    object Parser extends JavaTokenParsers {
      val identifier: Parser[String] = "([A-Za-z_]+[A-Za-z0-9_]*)".r
      val dependents: Parser[List[String]] = repsep(identifier, ",")

      def singleTask: Parser[DependencyInfo] = identifier ^^ { DependencyInfo(_, Nil) }
      def dependentTask: Parser[DependencyInfo] =
        identifier ~ "{" ~ dependents ~ "}" ^^ {
          case name ~ _ ~ dependents ~ "}" =>
            DependencyInfo(name, dependents)
        }

      def dependencyInfo: Parser[DependencyInfo] = dependentTask | singleTask
    }

    Parser.parseAll(Parser.dependencyInfo, s) match {
      case Parser.Success(result, _) => result
      case result @ _ => throw new IllegalArgumentException(result.toString)
    }
  }

  //  def testDependentTaskParser: Unit = {
  //    println( parseDependencyInfo( "P10b" ) )
  //    // println( parseDependencyInfo( "Pb{Pa}" ) )
  //    println( parseDependencyInfo( "P10b{P10a,P10c}" ) )
  //    System.exit(0)
  //  }

  /////////////////////////////////

  def makeTasks(recipes: Map[String, List[RecipeInfo]], resources: List[Resource], config: Configuration): Either[String, List[Task]] = {

    val mutex = IAObjectiveFunction.parseMutexes(resources, config)

    def mkTask(instanceName: InstanceName, allocatedResource: Resource): Task = {
      // require( isCompatibleResource(recipes, resources)(infoName + in, allocatedResource) )

      val dependencyInfo = parseDependencyInfo(instanceName.info.name)
      val executionInterval = interval(instanceName, allocatedResource, config)
      if (dependencyInfo.dependentNamePrefixes.isEmpty) {
        Task.single(
          TaskId(instanceName.toString()),
          Map(allocatedResource -> executionInterval.length),
          releaseTime = executionInterval.lower,
          mutex
        )
      } else {

        def relations(di: DependencyInfo): Map[TaskId, IntervalRelation] = {
          def lookupRecipeFromPrefix(dependentNamePrefix: String): Option[RecipeInfo] =
            recipes.values.toList.flatten.find { r =>
              r.name.startsWith(dependentNamePrefix)
            }
          val rels = for {
            dependentNamePrefix <- di.dependentNamePrefixes;
            r <- lookupRecipeFromPrefix(dependentNamePrefix)
          } yield {
            (0 until r.instances).map { i =>
              TaskId(r.name + " " + i) -> IntervalRelation.PreceededGenerallyBy
            }
          }
          Map() ++ rels.flatten
        }

        // jeep.lang.Diag.println( s"mkTask: $instanceName ${relations(dependencyInfo)}" )

        Task.apply(
          TaskId(instanceName.toString()),
          Map(allocatedResource -> executionInterval.length),
          relations = relations(dependencyInfo),
          releaseTime = 0, 0, 0, 1, mutex
        )
      }
    }

    ///////////////////////////////

    //    var cccc = 0

    def resourceOfParentTask(instanceName: InstanceName, tasks: List[Either[String, Task]]): Option[Resource] = {
      val dependencyInfo = parseDependencyInfo(instanceName.info.name)
      tasks.reverse.find(_ match {
        case Left(_) => false
        case Right(tt) => dependencyInfo.dependentNamePrefixes.exists { prefix => tt.id.name.startsWith(prefix) }
      }).flatMap {
        _ match {
          case Left(_) => None
          case Right(tt) =>
            assert(tt.resourceExecutionTime.size == 1)
            Some(tt.resourceExecutionTime.toList.head._1)
        }
      }
    }

    ///////////////////////////////

    def isBiLevelCompatible(instanceName: InstanceName, r: Resource, tasks: List[Either[String, Task]]): Boolean = {
      def impl(last: Resource, current: Resource): Boolean =
        if (last.id.startsWith("LargeA") || last.id.startsWith("LargeB"))
          current.id.startsWith("LargeD")
        else {
          if (last.id.startsWith("LargeC"))
            current.id.startsWith("LargeE")
          else
            true
        }

      (for (last <- resourceOfParentTask(instanceName, tasks.toList)) yield {
        impl(last, r)
      }).getOrElse(true)
    }

    val tasks = scala.collection.mutable.ListBuffer.empty[Either[String, Task]]
    for ((recipeName, infos) <- recipes; info <- infos; instanceId <- 0 until info.instances) {
      // val instanceName = info.name + " " + instanceId
      val instanceName = InstanceName(info, instanceId)
      val allocatedResource = allocation(instanceName, config)

      val allocationAvailable = isResourceAvailable(allocatedResource, config) &&
        isBiLevelCompatible(instanceName, allocatedResource, tasks.toList)

      //        if( resourceOfParentTask(instanceName, tasks.toList).isDefined ) {
      //      jeep.lang.Diag.println( resourceOfParentTask(instanceName, tasks.toList) )
      //      System.exit(0)
      //        }
      //
      //      jeep.lang.Diag.println( "name: " + instanceName + ", resource: " + allocatedResource + " tasks: " + tasks + "parentResource " + resourceOfParentTask(instanceName, tasks.toList) )

      //      if( cccc == 10 )
      //        System.exit(0)
      //
      //        cccc += 1

      if (allocationAvailable) {
        // jeep.lang.Diag.println( allocationAvailable )
        //jeep.lang.Diag.println( "name: " + instanceName + ", resource: " + allocatedResource + " tasks: " + tasks )
        //jeep.lang.Diag.println( "bilevelcompat: " + isBiLevelCompatible(instanceName, allocatedResource, tasks.toList) )
        tasks.append(Right(mkTask(instanceName, allocatedResource)))
      } else {
        val compatible = compatibleResources(info.name, recipes, resources).filter { r =>
          tasks.isEmpty || isBiLevelCompatible(instanceName, r, tasks.toList)
        }

        // jeep.lang.Diag.println( "name: " + instanceName + ", resource: " + allocatedResource + " tasks: " + tasks )

        //        val rejectedByBilevel = compatibleResources(info.name, recipes, resources).toSet - compatible.toSet
        //        jeep.lang.Diag.println( " rejectedByBilevel:" + rejectedByBilevel )
        //        System.exit(0)

        val availability = Map() ++ compatible.map { r =>
          r -> config.getObservableMetrics().get(r.id + " availability")
        }
        val result = compatible.find { r => isResourceAvailable(r, config) } match {
          // case None => Left(s"${allocatedResource.id} is not available for task $instanceName (and no compatible alternative resource is available)")
          case None => Left(s"${allocatedResource.id} is not available for task $instanceName") //  (compatible resources: ${compatible})")
          case Some(allocatedResource) => Right(mkTask(instanceName, allocatedResource))
        }
        tasks.append(result)
      }
    }

    //    jeep.lang.Diag.println( tasks.size )
    //    jeep.lang.Diag.println( tasks.mkString("[",",\n","]") )

    // if all tasks could be allocated to resources, return them in a list
    // otherwise, provide an descriptive string for the unallocated ones
    Either.cond(
      tasks.toList.forall { e => e.isRight },
      tasks.toList.map { case Right(t) => t },
      tasks.toList.map { case Left(descriptiveStr) => descriptiveStr + "\n"; case Right(task) => "" }.mkString
    )
  }
}

// End ///////////////////////////////////////////////////////////////
