package uk.ac.york.safire.optimisation.mitm.atb;

import uk.ac.york.safire.optimisation.ObjectiveFunction._
import uk.ac.york.safire.optimisation.ia._
import uk.ac.york.safire.metrics._
import scala.collection.mutable.PriorityQueue

///////////////////////////////////

/**
 * Minimise a weighted combination of cost and makespan
 */

class OnaSingleObjectiveFunction extends IAObjectiveFunction {

  override def predictKeyObjectives(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result = {
    predictKeyObjectivesImpl(originalConfig, proposedControlMetrics)
  }

  /////////////////////////////////

  private def predictKeyObjectivesImpl(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result = {

    import scala.collection.JavaConversions._

    val config = Configuration.update(originalConfig, proposedControlMetrics, originalConfig.getKeyObjectives)
    val (recipes, resources) = IAObjectiveFunction.reverseEngineerRecipesFromConfig(config)

    ///////////////////////////////

    OnaSingleObjectiveFunction.makeTasks(recipes, resources, config) match {
      case m @ Left(errorString) => new Result(config.getKeyObjectives, errorString)
      case Right(tasks) => {
        val scheduler = new FIFOScheduler()
        def priority(t: Task): Int = IAObjectiveFunction.priority(t.id.name, config)
        val taskQueue: PriorityQueue[Task] = PriorityQueue(tasks: _*)(Ordering.by { priority })
        // jeep.lang.Diag.println("#tasks: " + taskQueue.length)
        // jeep.lang.Diag.println("tasks: " + taskQueue)
        // jeep.lang.Diag.println("priorities: " + taskQueue.map { priority })

        scheduler.schedule(taskQueue, resources.toSet) match {
          case None => new Result(config.getKeyObjectives, "Scheduler cannot find a valid schedule")
          case Some(schedule) =>
            // jeep.lang.Diag.println( schedule )
            // val newKeyObjectives = Map("makespan" ->
            // Value.realValue(makespan, config.getKeyObjectives().get("makespan").getType.asInstanceOf[ValueType.Real]))

            val newKeyObjectives = doUpdatedKeyObjectives(schedule, config)
            val reportStr = "Status: Succeeded.\n" + toReportString(schedule, newKeyObjectives)
            new Result(newKeyObjectives, reportStr)
        }
      }
    }
  }

  /////////////////////////////////

  override def numObjectives: Int = 1

  override def objectiveValues(result: Result): java.util.List[java.lang.Double] = {
    import scala.collection.JavaConversions._
    val cost = IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("totalcost")).asInstanceOf[java.lang.Double]
    val makespan = IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("makespan")).asInstanceOf[java.lang.Double]

    // List(IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("totalcost")).asInstanceOf[java.lang.Double])
    // val xx = (OnaObjectiveFunction.W1*makespan)/(50*111917.3) + (OnaObjectiveFunction.W2*cost)/68616.1
    val xx = (OnaSingleObjectiveFunction.W1 * makespan) / (50 * 111917.3) + (OnaSingleObjectiveFunction.W2 * cost) / 68616.1
    List[java.lang.Double](xx)
  }

  /////////////////////////////////

  protected override def doUpdatedKeyObjectives(schedule: Schedule, config: Configuration): Map[String, Value] = {

    import scala.collection.JavaConversions._

    // jeep.lang.Diag.println( config.getObservableMetrics().filter { o => o._1.contains("cost" )} )

    val totalcost = (for ((resource, tasks) <- schedule.byResource; task <- tasks) yield {
      val recipeAndResourceName = task._1.id.name + " " + resource.id

      if (config.getObservableMetrics().get(recipeAndResourceName + " cost") == null) {

        if (!recipeAndResourceName.contains("unavailable"))
          jeep.lang.Diag.println("cost not found for: " + recipeAndResourceName)
        0.0
      } else {
        IAObjectiveFunction.doubleValue(config.getObservableMetrics().get(recipeAndResourceName + " cost"))
      }
    }).sum

    //   val normalisedCost= (OnaObjectiveFunction.W1*totalcost)/68616.1
    //    val normalisedMakespan = (OnaObjectiveFunction.W2*schedule.makespan)/(50*111917.3)

    // jeep.lang.Diag.println(s"normalisedCost: $normalisedCost, normalisedMakespan: $normalisedMakespan")

    //    Map("totalcost" -> Value.realValue(totalcost, ValueType.realType(0.0, Double.MaxValue)),
    //        "makespan" -> Value.realValue(schedule.makespan, config.getKeyObjectives().get("makespan").getType.asInstanceOf[ValueType.Real])
    Map(
      //"totalcost-normalised" -> Value.realValue(normalisedCost, ValueType.realType(0.0, Double.MaxValue)),
      //"makespan-normalised" -> Value.realValue(normalisedMakespan, ValueType.realType(0.0, Double.MaxValue)),
      "totalcost" -> Value.realValue(totalcost, ValueType.realType(0.0, Double.MaxValue)),
      "makespan" -> Value.realValue(schedule.makespan, config.getKeyObjectives().get("makespan").getType.asInstanceOf[ValueType.Real])
    )
  }

  /////////////////////////////////

  protected override def doReportStringSuffix(s: Schedule, newKeyObjectives: Map[String, Value]): String = {
    s"\nmakespan: ${new jeep.util.HMS(s.makespan)}\n" ++
      s"\ncost: ${IAObjectiveFunction.doubleValue(newKeyObjectives("totalcost"))}\n"
  }
}

///////////////////////////////////

object OnaSingleObjectiveFunction {

  var W1: Double = 0.0
  var W2: Double = 0.0

  /////////////////////////////////

  def parseUnavailability(c: Configuration): Map[Resource, List[Interval]] = {
    import scala.collection.JavaConversions._

    val unavailableByResource = c.getObservableMetrics.filter {
      case (key, value) =>
        key.contains("unavailable")
    }.toList.groupBy {
      case (key, value) =>
        val tokens = key.split(" ")
        tokens.head + " " + tokens(1)
    }

    case class IntervalInfo(resourceName: String, unavailabilityIndex: Int, isStart: Boolean, time: Int)

    unavailableByResource.map {
      case (resource, unavailables) =>
        val intervals = unavailables.map {
          case (key, value) =>
            val tokens = key.split(" ")
            val isStart = key.endsWith("start")
            val isEnd = key.endsWith("end")
            assert(isStart ^ isEnd)
            val resource = tokens.head + " " + tokens(1)
            val unavailabilityIndex = tokens(2).toInt
            val time = IAObjectiveFunction.intValue(value)
            IntervalInfo(resource, unavailabilityIndex, isStart, time)
        }.groupBy { ii => (ii.resourceName, ii.unavailabilityIndex) }.map {
          case (index, l) =>
            assert(l.size == 2)
            val startTime = if (l(0).isStart) l(0).time else l(1).time
            val endTime = if (l(0).isStart) l(1).time else l(0).time
            Interval(startTime, endTime)
        }
        Resource(resource) -> intervals.toList
    }
  }

  /////////////////////////////////

  def makeTasks(recipes: Map[String, List[RecipeInfo]], resources: List[Resource], config: Configuration): Either[String, List[Task]] = {

    import IAObjectiveFunction.InstanceName

    val mutex = IAObjectiveFunction.parseMutexes(resources, config)
    def mkTask(instanceName: InstanceName, allocatedResource: Resource): Task = {
      // require( isCompatibleResource(recipes, resources)(infoName + in, allocatedResource) )      
      val executionInterval = IAObjectiveFunction.interval(instanceName, allocatedResource, config)
      Task.single(
        TaskId(instanceName.toString()),
        Map(allocatedResource -> executionInterval.length),
        releaseTime = executionInterval.lower,
        mutex
      )
    }

    val tasksFromRecipes = (for ((recipeName, infos) <- recipes; info <- infos; instanceId <- 0 until info.instances) yield {
      // val instanceName = info.name + " " + instanceId
      val instanceName = InstanceName(info, instanceId)
      val allocatedResource = IAObjectiveFunction.allocation(instanceName, config)
      val allocationAvailable = IAObjectiveFunction.isResourceAvailable(allocatedResource, config)
      if (!allocationAvailable) {

        val compatible = IAObjectiveFunction.compatibleResources(info.name, recipes, resources)
        // val availability = Map() ++ compatible.map { r => r -> config.getObservableMetrics().get(r.id + " availability") }
        compatible.find { r => IAObjectiveFunction.isResourceAvailable(r, config) } match {
          // case None => Left(s"${allocatedResource.id} is not available for task $instanceName (and no compatible alternative resource is available)")
          case None => Left(s"${allocatedResource.id} is not available for task $instanceName") //  (compatible resources: ${compatible})")          
          case Some(allocatedResource) => Right(mkTask(instanceName, allocatedResource))
        }
      } else {
        Right(mkTask(instanceName, allocatedResource))
      }
    }).toList

    def mkDummyTask(resource: Resource, dummyTaskIndex: Int, interval: Interval): Task =
      Task.single(
        TaskId(s"resource ${resource.id} unavailable #$dummyTaskIndex"),
        Map(resource -> interval.length), releaseTime = interval.lower, mutex
      )

    val resourceUnavailibilityIntervals = parseUnavailability(config)
    // jeep.lang.Diag.println( resourceUnavailibilityIntervals.mkString( "\n" ) )     
    val dummyTasks = resourceUnavailibilityIntervals.map {
      case (resource, intervals) =>
        intervals.zipWithIndex.map {
          case (interval, index) =>
            Right(mkDummyTask(resource, index, interval))
        }
    }

    // jeep.lang.Diag.println( dummyTasks.flatten.toList.map { t => t.value.compatibleResources }.mkString("\n") )    
    //jeep.lang.Diag.println( resourceUnavailibilityIntervals.size )    
    //jeep.lang.Diag.println( s"#dummy tasks: ${dummyTasks.flatten.toList.size} ${dummyTasks.flatten.toList.mkString("[",",\n", "]")}" )
    // System.exit(0)
    val tasks = tasksFromRecipes ++ dummyTasks.flatten.toList // .take(1)

    // if all tasks could be allocated to resources, return them in a list
    // otherwise, provide an descriptive string for the unallocated ones
    Either.cond(
      tasks.forall { e => e.isRight },
      tasks.map { case Right(t) => t },
      tasks.map { case Left(descriptiveStr) => descriptiveStr + "\n"; case Right(task) => "" }.mkString
    )
  }
}

// End ///////////////////////////////////////////////////////////////
