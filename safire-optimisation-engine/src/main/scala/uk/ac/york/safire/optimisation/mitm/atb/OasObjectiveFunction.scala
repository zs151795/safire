package uk.ac.york.safire.optimisation.mitm.atb

import uk.ac.york.safire.optimisation.ObjectiveFunction._
import uk.ac.york.safire.optimisation.ia._
import uk.ac.york.safire.metrics._

///////////////////////////////////

/**
 * Minimise n+1 objectives:
 * makespan, together with surplus for each of a set of n commodities (i.e. paints, in the OAS scenario)
 */

class OasObjectiveFunction(commodities: java.util.Set[String]) extends IAObjectiveFunction {

  override def numObjectives: Int = 1 + commodities.size
  // override def numObjectives: Int = commodities.size

  override def objectiveValues(result: Result): java.util.List[java.lang.Double] = {
    val value = result.keyObjectiveMetrics.get("makespan")
    val visitor = new ValueVisitor.NumberVisitor()
    val makespan: java.lang.Double = IAObjectiveFunction.doubleValue(value)

    import scala.collection.JavaConversions._

    val discrepancyList = commodities.toList.map { recipeName =>
      val key = recipeName + " amount discrepancy score"
      val value = result.keyObjectiveMetrics.get(key)
      IAObjectiveFunction.doubleValue(value).asInstanceOf[java.lang.Double]
    }

    import scala.collection.JavaConversions._
    List(makespan) ++ discrepancyList
  }

  /////////////////////////////////

  protected override def doUpdatedKeyObjectives(schedule: Schedule, config: Configuration): Map[String, Value] = {

    import scala.collection.JavaConversions._
    val amountsRequired: Map[String, Int] = Map() ++ commodities.map { recipeName =>
      val value = config.getObservableMetrics().get(recipeName + " amount required")
      recipeName -> IAObjectiveFunction.intValue(value)
    }

    val amountsProduced: Map[String, Int] = Map() ++ config.getObservableMetrics().map {
      case (key, value) =>
        if (key.contains("amount produced")) {
          val tokens = key.split(" ")
          Some(s"${tokens(0)} ${tokens(1)} ${tokens(2)}" -> IAObjectiveFunction.intValue(value))
        } else
          None
    }.toList.flatten

    //jeep.lang.Diag.println( "amounts required: " + amountsRequired )
    //    jeep.lang.Diag.println( "amounts produced: " + amountsProduced )

    ///////////////////////////////

    //    val recipeAmountsProducedTemp = (for ((resource, tasks) <- schedule.byResource; task <- tasks) yield {
    //      // val recipeAndResourceName = task._1.id.name + " " + resource.id
    //      val recipeInfoName = task._1.id.name.split(" ").dropRight(1).mkString(" ")
    //      val value = amountsProduced.get(recipeInfoName)
    //      if (!value.isDefined) {
    //        jeep.lang.Diag.println("WARNING: Amount produced not found for: " + recipeInfoName)
    //        recipeInfoName -> 0
    //      } else {
    //        recipeInfoName -> value.get
    //      }
    //    })

    val recipeAmountsProducedTemp = (for ((resource, tasks) <- schedule.byResource if resource.id != "No allocation") yield {
      val xx = for (task <- tasks) yield {
        // val recipeAndResourceName = task._1.id.name + " " + resource.id
        val recipeInfoName = task._1.id.name.split(" ").dropRight(1).mkString(" ")
        val value = amountsProduced.get(recipeInfoName)

        // println ("?" + resource.id)

        if (!value.isDefined) {
          jeep.lang.Diag.println("WARNING: Amount produced not found for: " + recipeInfoName)
          recipeInfoName -> 0
        } else {
          recipeInfoName -> value.get
        }
      }
      xx.toList
    }).toList

    //jeep.lang.Diag.println( "recipie amounts produced temp: " + recipeAmountsProducedTemp )
    // jeep.lang.Diag.println( "recipie amounts produced temp sum: " + recipeAmountsProducedTemp.map { _._2 }.sum )
    val recipeAmountsProducedTemp2 = (recipeAmountsProducedTemp.flatten).groupBy { _._1.dropRight(1).trim }

    //jeep.lang.Diag.println( "recipie amounts produced temp2: " + recipeAmountsProducedTemp2 )
    val recipeAmountsProduced = recipeAmountsProducedTemp2.map {
      case (recipeInfoName, ammounts) =>
        // recipeInfoName.dropRight(1).trim -> ammounts.map { _._2 }.sum
        recipeInfoName -> ammounts.map { _._2 }.sum
    }

    ///////////////////////////////

    val InsufficientProductionWeightingFactor = 10

    val surplusList = amountsRequired.map {
      case (recipeName, requiredQuantity) =>
        val produced = recipeAmountsProduced.get(recipeName).getOrElse(0)
        // val surplus = math.max(0, requiredQuantity - produced)
        //        val surplus = math.abs( requiredQuantity - produced ) // FIXME
        // recipeName + " surplus" -> Value.intValue(surplus, ValueType.intType(0, Int.MaxValue))

        val discrepancy = if (produced < requiredQuantity)
          (InsufficientProductionWeightingFactor * (requiredQuantity - produced))
        else
          produced - requiredQuantity

        //        val discrepancy = produced - requiredQuantity

        recipeName + " amount discrepancy score" -> Value.intValue(discrepancy, ValueType.intType(Int.MinValue, Int.MaxValue))
    }

    //jeep.lang.Diag.println( s"amountsRequired: ${amountsRequired.map { case (k,v) => s"<<$k>> -> $v" } }" )
    //jeep.lang.Diag.println( s"recipeAmountsProduced: ${recipeAmountsProduced.map { case (k,v) => s"<<$k>> -> $v" } }" )
    // jeep.lang.Diag.println( s"surplusList: ${surplusList.mkString("[",",\n","]")}" )
    // System.exit(0)

    ///////////////////////////////

    val makespan = Value.realValue(schedule.makespan, config.getKeyObjectives().get("makespan").getType.asInstanceOf[ValueType.Real])
    Map("makespan" -> makespan) ++ surplusList ++ recipeAmountsProduced.map { case (k, v) => (k + " produced") -> Value.intValue(v, ValueType.intType(Int.MinValue, Int.MaxValue)) }
    // Map() ++ surplusList ++ recipeAmountsProduced.map { case (k,v) => ( k + " produced" ) -> Value.intValue(v, ValueType.intType(Int.MinValue, Int.MaxValue)) }
  }

  protected override def doReportStringSuffix(s: Schedule, newKeyObjectives: Map[String, Value]): String = {
    def discrepancyList(keyObjectives: Map[String, Value]): List[(String, Int)] = {
      import scala.collection.JavaConversions._
      commodities.map { recipeName =>
        val value = keyObjectives(recipeName + " amount discrepancy score")
        recipeName -> IAObjectiveFunction.intValue(value)
      }.toList
    }

    s"\nmakespan: ${new jeep.util.HMS(s.makespan)}, amount discrepancy: ${discrepancyList(newKeyObjectives)}\n"
  }
}

// End ///////////////////////////////////////////////////////////////
