package uk.ac.york.safire.optimisation

import scala.concurrent.Future
import java.io.ObjectInputStream;

import scala.collection.immutable.List;

import akka.http.scaladsl.marshallers.xml.ScalaXmlSupport.defaultNodeSeqMarshaller
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json._

import akka.http.scaladsl.server.{ HttpApp, Route }
import akka.http.scaladsl.server.PathMatchers

import uk.ac.york.safire.metrics._

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import StatusCodes._
import Directives._

import scala.collection.JavaConversions._
import scala.concurrent.duration._

///////////////////////////////////

/**
 * Server will be started calling `WebServerHttpApp.startServer("localhost", 8080)`
 * and it will be shutdown after pressing return.
 */

///////////////////////////////////

trait ObjectiveFunctionArgumentsJsonProtocol extends DefaultJsonProtocol {

  implicit object ObjectiveFunctionArgumentsFormat extends RootJsonFormat[ObjectiveFunctionArguments] {

    override def write(bv: ObjectiveFunctionArguments): JsValue = {
      ???
    }

    override def read(value: JsValue): ObjectiveFunctionArguments = {
      //org.mitlware.Diag.println("reading OptimisationArguments...")
      //org.mitlware.Diag.println(value)
      val jsonConverter = new JsonConverter()
      val result = jsonConverter.fromJson(value.toString(), classOf[ObjectiveFunctionArguments])
      //org.mitlware.Diag.println("read Configuration")
      result
    }
  }
}

///////////////////////////////////

trait ObjectiveFunctionResultJsonProtocol extends DefaultJsonProtocol {

  implicit object ObjectiveFunctionResultFormat extends RootJsonFormat[ObjectiveFunctionResult] {

    override def write(x: ObjectiveFunctionResult): JsValue = {
      val jsonConverter = new JsonConverter()
      val jsonStr = jsonConverter.toJson(x)
      val result = JsonParser(jsonStr)
      result
    }

    override def read(value: JsValue): ObjectiveFunctionResult = {
      val jsonConverter = new JsonConverter()
      jsonConverter.fromJson(value.toString(), classOf[ObjectiveFunctionResult])
    }
  }
}

///////////////////////////////////

object HttpRemoteObjectiveFunctionServer extends HttpApp
    with ConfigurationJsonProtocol
    with ControlledMetricTypeJsonProtocol
    with ValueJsonProtocol
    with ObjectiveFunctionArgumentsJsonProtocol
    with ObjectiveFunctionResultJsonProtocol
    with App {

  import scala.concurrent.duration._
  val TimeoutDuration = 2.hours

  /////////////////////////////////

  val myExceptionHandler = ExceptionHandler {
    case t: Throwable =>
      extractUri { uri =>
        val sw = new java.io.StringWriter
        t.printStackTrace(new java.io.PrintWriter(sw))
        val st = sw.toString
        jeep.lang.Diag.println(s"Request to $uri could not be handled normally: ${t.getMessage()} ${st}")
        complete(HttpResponse(
          InternalServerError,
          entity = JsString(s"Server Exception: ${t.getMessage()} ${st}").toString
        ))
      }
  }

  /////////////////////////////////

  override def routes: Route = handleExceptions(myExceptionHandler) {
    pathEndOrSingleSlash { complete("Server up and running") } ~
      path("apply" / Remaining) { remaining =>
        // jeep.lang.Diag.println(s"remaining: <<$remaining>>")
        post {
          withoutSizeLimit {
            //   jeep.lang.Diag.println()
            entity(as[ObjectiveFunctionArguments]) { args =>
              jeep.lang.Diag.println(new java.util.Date())
              val rng = new java.util.Random(0xDEADBEEF)

              // TODO: replace this with ObjectiveFunction of choice
              val objectiveFunction: ObjectiveFunction = HttpRemoteObjectiveFunctionClient.localObjectiveFunction

              withRequestTimeout(TimeoutDuration) {
                val startTime = System.currentTimeMillis()
                val result = ObjectiveFunction.evaluateNowHelper(objectiveFunction, args)
                val endTime = System.currentTimeMillis()
                jeep.lang.Diag.println("Computation time (millis): " + (endTime - startTime))
                jeep.lang.Diag.println("Successfully completed client request")
                complete(result)
              }
            }
          }
        }
      }
  }

  /////////////////////////////////

  // IMPORTANT:

  // def UseRCPS = false // if false, will assume Configurations are for SimplePlant
  // ^ Note: def not val because of a bizzare initialiazation error w.r.t.
  // HttpRemoteOptimisationEngine client access to this value

  def RunUnderDocker = true
  val address = if (RunUnderDocker) "0.0.0.0" else "localhost"

  // This will start the server until the return key is pressed:
  // startServer(address, HttpRemoteOptimisationEngineClient.DefaultPort)

  ///////////////////////////////////    

  import akka.http.scaladsl.settings._
  import com.typesafe.config.ConfigFactory

  object MyTimeouts extends ServerSettings.Timeouts {
    // FIXME: sensible values for these and/or read them from config
    override val bindTimeout: FiniteDuration = 2.hours
    override val idleTimeout: Duration = 2.hours
    override val lingerTimeout: Duration = 2.hours
    override val requestTimeout: Duration = 2.hours
  }

  val settings = ServerSettings(ConfigFactory.load).withVerboseErrorMessages(true).withTimeouts(MyTimeouts)
  startServer(address, HttpRemoteObjectiveFunctionClient.DefaultPort, settings)
}

// End ///////////////////////////////////////////////////////////////
