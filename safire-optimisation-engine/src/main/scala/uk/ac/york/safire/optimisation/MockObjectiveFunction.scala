package uk.ac.york.safire.optimisation

import uk.ac.york.safire.metrics._

import uk.ac.york.safire.optimisation.maxplusplant._

import scala.collection.JavaConversions._

import org.apache.commons.math3.stat.descriptive.SummaryStatistics
import org.apache.commons.lang3.builder._

///////////////////////////////////

case class Stats(mean: Double, sd: Double, min: Double, max: Double) {

  def withMin(newMin: Double): Stats = copy(min = newMin)

  override def toString(): String =
    ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE)
}

///////////////////////////////////

class MockObjectiveFunction(energyCostWeighting: Double, makespanCostWeighting: Double, val plant: SimplePlant) extends ObjectiveFunction.LocalObjectiveFunction {
  require(energyCostWeighting > 0.0)
  require(makespanCostWeighting > 0.0)

  val configurationType: ConfigurationType = plant.getConfigurationType

  ///////////////////////////////

  lazy val optimum: Double = {
    import scala.collection.JavaConverters._
    val soFitnessValues = plant.cpGetParetoFront().map { keyObjectives =>
      val keyObjectivesMap = Map.empty[String, Value] ++ keyObjectives.toList.map { ov =>
        ov.name -> Value.realValue(ov.value, ValueType.realType(0.0, Double.MaxValue))
      }
      singleObjectiveFitness(keyObjectivesMap.asJava)
    }
    soFitnessValues.min
  }

  def flatCost(energy: Double, makespan: Double): Double = {
    (energy * energyCostWeighting) + (makespan * makespanCostWeighting)
  }

  def singleObjectiveFitness(x: java.util.Map[String, Value]): Double = {
    val visitor = new ValueVisitor.NumberVisitor()
    val energy = x("energy")
    val makespan = x("makespan")
    val energyValue = visitor.visit(energy).doubleValue()
    val makespanValue = visitor.visit(makespan).doubleValue()

    flatCost(energyValue, makespanValue)
  }

  def lowestFlatCost(population: java.util.List[Configuration]): java.lang.Double =
    population.map { c => singleObjectiveFitness(c.getKeyObjectives) }.min

  def soFitnessStats(population: java.util.List[Configuration]): Stats = {
    val ss = new SummaryStatistics()
    population.map { p => singleObjectiveFitness(p.getKeyObjectives()) }.foreach { ss.addValue(_) }
    Stats(ss.getMean(), ss.getStandardDeviation(), ss.getMin(), ss.getMax())
  }

  def soFitnessHistogram(population: java.util.List[Configuration]): com.google.common.collect.Multiset[Double] = {
    val soFitnessValues = population.map { c => singleObjectiveFitness(c.getKeyObjectives) }

    val histo = com.google.common.collect.HashMultiset.create[Double]()
    import collection.JavaConverters._
    histo.addAll(soFitnessValues.asJava)
    histo
  }

  def relativeFitnessErrors(population: java.util.List[Configuration]): List[Double] = {
    val soFitnessValues = population.map { c => singleObjectiveFitness(c.getKeyObjectives) }
    val relativeErrors = soFitnessValues.map { f => (f - optimum) / optimum }
    relativeErrors.toList
  }

  def bestRelativeFitnessError(population: java.util.List[Configuration]): java.lang.Double =
    relativeFitnessErrors(population).min

  def relativeErrorStats(population: java.util.List[Configuration]): Stats = {
    val ss = new SummaryStatistics()
    relativeFitnessErrors(population).foreach { ss.addValue(_) }
    Stats(ss.getMean(), ss.getStandardDeviation(), ss.getMin(), ss.getMax())
  }

  ///////////////////////////////

  override def numObjectives(): Int = 2

  override def predictKeyObjectives(current: Configuration, controlledMetrics: java.util.Map[String, Value]): ObjectiveFunction.Result = {
    //    val flatCost = MockObjectiveFunction.singleObjectiveFitness(
    //      Configuration.update(current, controlledMetrics) )
    //    val Array(energy, makespan) = plant.fitness(Configuration.update(current, controlledMetrics))
    //    val objectiveValues = List[java.lang.Double](MockObjectiveFunction.flatCost(energy, makespan))
    //    val keyObjectives: Map[String,Value] = ???
    //    new Result( keyObjectives, objectiveValues )
    val result = plant.fitness(current, controlledMetrics)
    new ObjectiveFunction.Result(result.keyObjectiveMetrics, "ObjectiveFunction status: OK")
  }

  private def singleObjectiveValues(result: ObjectiveFunction.Result): java.util.List[java.lang.Double] = {
    List[java.lang.Double](singleObjectiveFitness(result.keyObjectiveMetrics))
  }

  private def twoObjectiveValues(result: ObjectiveFunction.Result): java.util.List[java.lang.Double] = {
    val visitor = new ValueVisitor.NumberVisitor()
    val energyValue = visitor.visit(result.keyObjectiveMetrics("energy")).doubleValue()
    val makespanValue = visitor.visit(result.keyObjectiveMetrics("makespan")).doubleValue()

    List[java.lang.Double](energyValue, makespanValue)
  }

  override def objectiveValues(result: ObjectiveFunction.Result): java.util.List[java.lang.Double] = {
    // singleObjectiveValues(result)
    twoObjectiveValues(result)
  } ensuring { _.length == numObjectives() }
}

///////////////////////////////////

object MockObjectiveFunction {

  def largerPlant(): MockObjectiveFunction =
    largerPlant(
      energyCostWeighting = HttpRemoteOptimisationEngineClient.DefaultEnergyCostWeighting,
      makespanCostWeighting = HttpRemoteOptimisationEngineClient.DefaultMakespanCostWeighting
    )

  def largerPlant(energyCostWeighting: Double, makespanCostWeighting: Double): MockObjectiveFunction =
    largerPlantImpl(energyCostWeighting, makespanCostWeighting, new java.util.Random(0xDEADBEEF))

  private def largerPlantImpl(energyCostWeighting: Double, makespanCostWeighting: Double, rng: java.util.Random): MockObjectiveFunction =
    new MockObjectiveFunction(energyCostWeighting, makespanCostWeighting,
      PlantGenerator.generatePlant(
        12, // noOfInternalLevels - 2
        1, //int minNoOfVerticesPerInternalLevel 
        10, //int maxNoOfVerticesPerInternalLevel -3 
        3, //int noOfMachines
        4, //int maxNoOfModesPerMachine
        1, //int minNominalDuration
        20, //int maxNominalDuration 
        1.0, //double minEnergyPerTimeUnit 
        10.0, //double maxEnergyPerTimeUnit
        0.1, //double minEnergyMultiplier
        2.0, //double maxEnergyMultiplier
        0.2, //double minDurationMultiplier
        3.0, //double maxDurationMultiplier
        rng
      ))

  /////////////////////////////////

  def smallPlant(energyCostWeighting: Double, makespanCostWeighting: Double): MockObjectiveFunction =
    smallPlantImpl(energyCostWeighting, makespanCostWeighting, new java.util.Random(0xDEADBEEF))

  def smallPlantImpl(energyCostWeighting: Double, makespanCostWeighting: Double, rng: java.util.Random): MockObjectiveFunction =
    new MockObjectiveFunction(
      energyCostWeighting, makespanCostWeighting,
      PlantGenerator.generatePlant(
        2, // noOfInternalLevels
        1, //int minNoOfVerticesPerInternalLevel
        3, //int maxNoOfVerticesPerInternalLevel
        3, //int noOfMachines
        4, //int maxNoOfModesPerMachine
        1, //int minNominalDuration
        20, //int maxNominalDuration
        1.0, //double minEnergyPerTimeUnit
        10.0, //double maxEnergyPerTimeUnit
        0.1, //double minEnergyMultiplier
        2.0, //double maxEnergyMultiplier
        0.2, //double minDurationMultiplier
        3.0, //double maxDurationMultiplier
        rng
      )
    )
}

// End ///////////////////////////////////////////////////////////////
