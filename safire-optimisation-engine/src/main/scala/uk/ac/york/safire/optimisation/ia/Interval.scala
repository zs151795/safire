package uk.ac.york.safire.optimisation.ia

/**
 * Half-closed interval [a,b), a <= x < b
 */

sealed case class Interval(lower: Int, upper: Int) {
  require(lower >= 0)
  require(upper >= lower, s"bad interval [$lower,$upper)")

  /////////////////////////////////

  val length: Int = upper - lower

  def contains(x: Int): Boolean = x >= lower && x < upper

  def starts(other: Interval): Boolean = lower == other.lower
  def finishes(other: Interval): Boolean = upper == other.upper

  /////////////////////////////////

  def preceedsGenerally(other: Interval): Boolean = upper <= other.lower
  def preceedsImmediately(other: Interval): Boolean = upper == other.lower

  def succeedsGenerally(other: Interval): Boolean = lower > other.upper
  def succeedsImmediately(other: Interval): Boolean = lower == other.upper

  def overlaps(other: Interval): Boolean = {
    // http://world.std.com/~swmcd/steven/tech/interval.html
    other.lower < upper && lower < other.upper
  }

  def during(other: Interval): Boolean =
    (lower > other.lower && upper <= other.upper) ||
      (lower >= other.lower && upper < other.upper)

  /////////////////////////////////

  def translate(x: Int): Interval = {
    require(x >= 0)
    Interval(lower + x, upper + x)
  }

  /////////////////////////////////

  override def toString(): String = s"[$lower,$upper)"
}

// End ///////////////////////////////////////////////////////////////
