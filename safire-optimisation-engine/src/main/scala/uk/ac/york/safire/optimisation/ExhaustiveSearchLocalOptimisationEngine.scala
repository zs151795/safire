package uk.ac.york.safire.optimisation

import org.uma.jmetal.algorithm._
import org.uma.jmetal.algorithm.impl._
import org.uma.jmetal.algorithm.multiobjective._
import org.uma.jmetal.algorithm.multiobjective.mocell._
import org.uma.jmetal.algorithm.multiobjective.nsgaii._
import org.uma.jmetal.algorithm.multiobjective.nsgaiii._
import org.uma.jmetal.algorithm.multiobjective.pesa2._
import org.uma.jmetal.algorithm.multiobjective.spea2._
import org.uma.jmetal.algorithm.multiobjective.smsemoa._
import org.uma.jmetal.operator._
import org.uma.jmetal.operator.impl.crossover._
import org.uma.jmetal.operator.impl.mutation._
import org.uma.jmetal.operator.impl.selection._
import org.uma.jmetal.problem._
import org.uma.jmetal.solution._
import org.uma.jmetal.solution.impl._
import org.uma.jmetal.util.AlgorithmRunner
import org.uma.jmetal.util.evaluator._
import org.uma.jmetal.util.evaluator.impl._
import org.uma.jmetal.util._

import uk.ac.york.safire.metrics._
import scala.collection.JavaConversions._

///////////////////////////////////

class ExhaustiveSearchLocalOptimisationEngine(of: ObjectiveFunction.LocalObjectiveFunction, rng: java.util.Random)
    extends OptimisationEngine.LocalOptimisationEngine(of) {

  override def optimise(args: OptimisationArguments): OptimisationResult = {
    if (args.getConfigurations().isEmpty) {
      new OptimisationResult(args.getConfigurations())
    } else {
      val parentConfig = args.getConfigurations().head
      ExhaustiveSearchLocalOptimisationEngine.optimalSolutions(parentConfig, of) match {
        case None => new OptimisationResult(args.getConfigurations())
        case Some(l) => new OptimisationResult(
          l.map {
            case (sol, fitness) => {
              Configuration.update(parentConfig, sol.controlMetrics,
                of.predictKeyObjectives(parentConfig, sol.controlMetrics).keyObjectiveMetrics)
            }
          }
        )
      }
    }
  }
}

///////////////////////////////////

object ExhaustiveSearchLocalOptimisationEngine {

  def isRealValued(ct: ConfigurationType): Boolean =
    ConfigurationSolution.variableValueTypes(ct).exists { _.getType == ValueType.Type.REAL }

  def optimalSolutions(parentConfig: Configuration, of: ObjectiveFunction): Option[List[(ConfigurationSolution, java.util.List[Double])]] = {

    import jeep.util.Pascaline
    import jeep.math.ClosedOpenInterval

    class PascalineIterator(n: Int, f: Int => ClosedOpenInterval[Integer]) extends Iterator[List[Int]] {

      private val impl = new Pascaline(
        n,
        new funk.Function1[Integer, ClosedOpenInterval[Integer]]() {
          override def apply(i: Integer): ClosedOpenInterval[Integer] = f(i)
        }
      )

      override def hasNext: Boolean = impl.hasNext
      override def next(): List[Int] = {
        impl.next()
        (0 until impl.size()).map { i => impl.get(i) }.toList
      }
    }

    ///////////////////////////////

    if (isRealValued(parentConfig.getConfigurationType()))
      None
    else {

      val dummyRNG = new java.util.Random(0xDEAFBEEF)
      val problem = JMetalLocalOptimisationEngine.createProblem(parentConfig, of, dummyRNG)

      val solution = problem.createSolution()
      assert(ConfigurationSolution.isValid(solution))

      val varBounds = ConfigurationSolution.variableBounds(parentConfig.getConfigurationType())

      val f = (i: Int) => new jeep.math.ClosedOpenInterval[Integer](varBounds(i)._1.intValue(), 1 + varBounds(i)._2.intValue())
      val solutions = for (varValues <- new PascalineIterator(varBounds.length, f)) yield {

        val newSolution = solution.copy()
        (0 until varBounds.length).map { i => newSolution.setVariableValue(i, varValues(i)) }
        problem.evaluate(newSolution)

        newSolution
      }

      // def length(a: Number,b : Number) = b.intValue() - a.intValue() 
      // jeep.lang.Diag.println( varBounds.fold(1) { case ((a,b),acc) => acc * length(a._1,a2) * length(b1,b2) } )
      import scala.collection.JavaConverters._
      val result = SolutionListUtils.getNondominatedSolutions(solutions.toList).map { x =>
        // val result = solutions.toList.map { x =>
        (x, ObjectiveFunction.evaluateNowHelper(of, new ObjectiveFunctionArguments(parentConfig, x.controlMetrics)).getObjectiveValues().map { x => x.toDouble }.toList.asJava)
      }.toList
      Some(result)
    }
  }
}

// End ///////////////////////////////////////////////////////////////
