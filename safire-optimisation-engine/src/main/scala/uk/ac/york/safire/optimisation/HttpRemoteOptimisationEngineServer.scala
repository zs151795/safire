package uk.ac.york.safire.optimisation

import scala.concurrent.Future
import java.io.ObjectInputStream;

import scala.collection.immutable.List;

import akka.http.scaladsl.marshallers.xml.ScalaXmlSupport.defaultNodeSeqMarshaller
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json._

import akka.http.scaladsl.server.{ HttpApp, Route }
import akka.http.scaladsl.server.PathMatchers

import uk.ac.york.safire.metrics._

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import StatusCodes._
import Directives._

import scala.collection.JavaConversions._
import scala.concurrent.duration._

///////////////////////////////////

/**
 * Server will be started calling `WebServerHttpApp.startServer("localhost", 8080)`
 * and it will be shutdown after pressing return.
 */

///////////////////////////////////

trait ConfigurationJsonProtocol extends DefaultJsonProtocol {

  implicit object ConfigurationFormat extends RootJsonFormat[Configuration] {

    override def write(bv: Configuration): JsValue = {
      ???
    }

    override def read(value: JsValue): Configuration = {
      //org.mitlware.Diag.println("reading Configuration...")
      //org.mitlware.Diag.println(value)
      val jsonConverter = new JsonConverter()
      val result = jsonConverter.fromJson(value.toString(), classOf[Configuration])
      //org.mitlware.Diag.println("read Configuration")
      result
    }
  }
}

///////////////////////////////////

trait OptimisationArgumentsJsonProtocol extends DefaultJsonProtocol {

  implicit object OptimisationArgumentsFormat extends RootJsonFormat[OptimisationArguments] {

    override def write(bv: OptimisationArguments): JsValue = {
      ???
    }

    override def read(value: JsValue): OptimisationArguments = {
      //org.mitlware.Diag.println("reading OptimisationArguments...")
      //org.mitlware.Diag.println(value)
      val jsonConverter = new JsonConverter()
      val result = jsonConverter.fromJson(value.toString(), classOf[OptimisationArguments])
      //org.mitlware.Diag.println("read Configuration")
      result
    }
  }
}

///////////////////////////////////

trait ControlledMetricTypeJsonProtocol extends DefaultJsonProtocol {

  implicit object ControlledMetricTypeFormat extends RootJsonFormat[ControlledMetricType] {

    override def write(x: ControlledMetricType): JsValue = {
      // org.mitlware.Diag.println("writing ControlledMetricTypeFormat...")
      val jsonConverter = new JsonConverter()
      val jsonStr = jsonConverter.toJson(x)
      // val result = JsString(jsonStr)
      val result = JsonParser(jsonStr) // FIXME: hack
      // org.mitlware.Diag.println("wrote ControlledMetricTypeFormat")      
      result
    }

    override def read(value: JsValue): ControlledMetricType = {
      // org.mitlware.Diag.println("reading ControlledMetricTypeFormat...")
      // FIXME:
      // val result = new ac.uk.york.safire.metrics.ControlledMetricType("blah", ValueType.realType(0.0, 1.0), "units")
      val jsonConverter = new JsonConverter()
      val result = jsonConverter.fromJson(value.toString(), classOf[ControlledMetricType])
      // org.mitlware.Diag.println("read ControlledMetricTypeFormat")
      result
    }
  }
}

///////////////////////////////////

trait ValueJsonProtocol extends DefaultJsonProtocol {

  implicit object ValueFormat extends RootJsonFormat[Value] {

    override def write(x: Value): JsValue = {
      // org.mitlware.Diag.println("writing Value")
      val jsonConverter = new JsonConverter()
      val jsonStr = jsonConverter.toJson(x)
      val result = JsonParser(jsonStr) // FIXME: hack
      // org.mitlware.Diag.println("wrote Value")
      result
    }

    override def read(value: JsValue): Value = {
      // org.mitlware.Diag.println("reading Value...")
      val jsonConverter = new JsonConverter()
      val result = jsonConverter.fromJson(value.toString(), classOf[Value])
      // org.mitlware.Diag.println("read Value")
      result
    }
  }
}

///////////////////////////////////

trait OptimisationResultJsonProtocol extends DefaultJsonProtocol {

  implicit object OptimisationResultFormat extends RootJsonFormat[OptimisationResult] {

    override def write(x: OptimisationResult): JsValue = {
      val jsonConverter = new JsonConverter()
      val jsonStr = jsonConverter.toJson(x)
      val result = JsonParser(jsonStr)
      result
    }

    override def read(value: JsValue): OptimisationResult = {
      val jsonConverter = new JsonConverter()
      jsonConverter.fromJson(value.toString(), classOf[OptimisationResult])
    }
  }
}

///////////////////////////////////

object HttpRemoteOptimisationEngineServer extends HttpApp
    with ConfigurationJsonProtocol
    with ControlledMetricTypeJsonProtocol
    with ValueJsonProtocol
    with OptimisationArgumentsJsonProtocol
    with OptimisationResultJsonProtocol
    with App {

  import scala.concurrent.duration._
  val TimeoutDuration = 2.hours


  /////////////////////////////////

  val myExceptionHandler = ExceptionHandler {
    case t: Throwable =>
      extractUri { uri =>
        val sw = new java.io.StringWriter
        t.printStackTrace(new java.io.PrintWriter(sw))
        val st = sw.toString
        jeep.lang.Diag.println(s"Request to $uri could not be handled normally: ${t.getMessage()} ${st}")
        complete(HttpResponse(
          InternalServerError,
          entity = JsString(s"Server Exception: ${t.getMessage()} ${st}").toString
            
          
        ))
      }
  }

  /////////////////////////////////

  private def implSimplePlant(
    args: OptimisationArguments,
    objectiveFunction: MockObjectiveFunction, rng: java.util.Random
  ): OptimisationResult = {
    require(!args.getConfigurations().isEmpty)
    require(objectiveFunction.configurationType ==
      args.getConfigurations().head.getConfigurationType())

    try {

      val MinEvaluations = 10000
      val MaxEvaluations = 100000

      // jeep.lang.Diag.println()
      val optimisationEngine = JMetalLocalOptimisationEngine.nsga3(
        MinEvaluations, MaxEvaluations, objectiveFunction, rng
      )
      //      val optimisationEngine = JMetalLocalOptimisationEngine.soga(
      //        MinEvaluations, MaxEvaluations,
      //        objectiveFunction, rng
      //      )

      // jeep.lang.Diag.println()
      import scala.collection.JavaConversions._
      import scala.concurrent.ExecutionContext.Implicits.global
      // Future { optimisationEngine.optimise(args) }
      val result = optimisationEngine.optimise(args)
      //     jeep.lang.Diag.println()
      result
    } catch {
      case t: Throwable => jeep.lang.Diag.println(t.getMessage()); throw t;
    }
  }

  /////////////////////////////////

  //  private def implRCPS(
  //    args: OptimisationArguments,
  //    objectiveFunction: MockObjectiveFunctionRCPS, rng: java.util.Random
  //  ): OptimisationResult = {
  //    require(!args.getConfigurations().isEmpty)
  //    require(objectiveFunction.configurationType ==
  //      args.getConfigurations().head.getConfigurationType())
  //
  //    try {
  //      val MinEvaluations = 2
  //      val MaxEvaluations = 10
  //
  //      // val MinEvaluations = 1000
  //      // val MaxEvaluations = 100000
  //      // val MinEvaluations = 100
  //      // val MaxEvaluations = 1000
  //  //    jeep.lang.Diag.println()
  //      //      val optimisationEngine = 
  //      //        JMetalLocalOptimisationEngine.defaultImpl(args, MinEvaluations, MaxEvaluations, objectiveFunction, rng)
  //
  //      // val order = uk.ac.york.safire.optimisation.OptmisationUtility.urgencyAndQualityToOrder9(args.getUrgency(), args.getQuality())
  //      //      val maxEvaluations = 
  //      //        uk.ac.york.safire.optimisation.OptmisationUtility.maxEvaluations(
  //      //            args.getUrgency, args.getQuality, MinEvaluations, MaxEvaluations)
  //      //
  //      //      jeep.lang.Diag.println(s"maxEvaluations: $maxEvaluations")
  //      jeep.lang.Diag.println(s"urgency: ${args.getUrgency}, quality: ${args.getQuality}")
  //      jeep.lang.Diag.println(s"populationSize: ${args.getConfigurations.length}")
  //      val optimisationEngine = new JMetalLocalOptimisationEngineRCPS(MinEvaluations, MaxEvaluations, objectiveFunction)
  //      jeep.lang.Diag.println()
  //      import scala.collection.JavaConversions._
  //      import scala.concurrent.ExecutionContext.Implicits.global
  //      // Future { optimisationEngine.optimise(args) }
  //      val result = optimisationEngine.optimise(args)
  //      jeep.lang.Diag.println()
  //      result
  //    } catch {
  //      case t: Throwable => jeep.lang.Diag.println(t.getMessage()); throw t;
  //    }
  //  }

  /////////////////////////////////

  override def routes: Route = handleExceptions(myExceptionHandler) {
    pathEndOrSingleSlash { // Listens to the top `/`
      complete("Server up and running")
    } ~
      path("apply" / Remaining) { remaining =>
        // jeep.lang.Diag.println(s"remaining: <<$remaining>>")
        post {
          withoutSizeLimit {
            //   jeep.lang.Diag.println()
            entity(as[OptimisationArguments]) { args =>
              if (args.getConfigurations().isEmpty) {
                //       jeep.lang.Diag.println()
                jeep.lang.Diag.println("WARNING: client request has no configurations")
                complete(new OptimisationResult(args.getConfigurations()))
              } else {
                jeep.lang.Diag.println(new java.util.Date())
                val rng = new java.util.Random(0xDEADBEEF)

                //              val objectiveFunction = if (UseRCPS)
                //                MockObjectiveFunctionRCPS()
                //              else
//                                val objectiveFunction = MockObjectiveFunction.largerPlant(
                val objectiveFunction = MockObjectiveFunction.smallPlant(
                  energyCostWeighting = HttpRemoteOptimisationEngineClient.DefaultEnergyCostWeighting,
                  makespanCostWeighting = HttpRemoteOptimisationEngineClient.DefaultMakespanCostWeighting,
                ) // rng)

                // FIXME: just absolutely ghastly...                
                //              val ct = if (UseRCPS)
                //                objectiveFunction.asInstanceOf[MockObjectiveFunctionRCPS].configurationType
                //              else
                //                objectiveFunction.asInstanceOf[MockObjectiveFunction].configurationType
                val ct = objectiveFunction.configurationType
                //   jeep.lang.Diag.println()
                if (ct !=
                  args.getConfigurations().head.getConfigurationType()) {
                  //   jeep.lang.Diag.println()
                  jeep.lang.Diag.println("WARNING!: client request has invalid configuration type!")
                  jeep.lang.Diag.println(s"Expected: ${ct}, found: ${args.getConfigurations().head.getConfigurationType()}")

                  // FIXME: implicit assumption that the ConfigurationType 
                  // used in the objective function
                  // is the same as the one in args
                  complete(
                    s"Client request has invalid configuration type. Expected ${ct}\n, found ${args.getConfigurations().head.getConfigurationType()}"
                  )
                } else {
//                   Thread.sleep(100000) 

                  withRequestTimeout(TimeoutDuration) {
                    //   jeep.lang.Diag.println()

                    //                  val result = if (UseRCPS)
                    //                    implRCPS(args, objectiveFunction.asInstanceOf[MockObjectiveFunctionRCPS], rng)
                    //                  else
                    val startTime = System.currentTimeMillis()
                    val result = implSimplePlant(args, objectiveFunction, rng)
                    val endTime = System.currentTimeMillis()
                    jeep.lang.Diag.println("population size: " + args.getConfigurations.length)
                    jeep.lang.Diag.println("Computation time (millis): " + (endTime - startTime))
                    jeep.lang.Diag.println("Successfully completed client request")
                    complete(result)
                  }
                }
              }
            }
          }
        }
      }
  }

  /////////////////////////////////

  // IMPORTANT:

  // def UseRCPS = false // if false, will assume Configurations are for SimplePlant
  // ^ Note: def not val because of a bizzare initialiazation error w.r.t.
  // HttpRemoteOptimisationEngine client access to this value

  def RunUnderDocker = false
  val address = if (RunUnderDocker) "0.0.0.0" else "localhost"

  // This will start the server until the return key is pressed:
  // startServer(address, HttpRemoteOptimisationEngineClient.DefaultPort)

  ///////////////////////////////////    

  import akka.http.scaladsl.settings._
  import com.typesafe.config.ConfigFactory

  object MyTimeouts extends ServerSettings.Timeouts {
    // FIXME: sensible values for these and/or read them from config
    override val bindTimeout: FiniteDuration = 2.hours
    override val idleTimeout: Duration = 2.hours
    override val lingerTimeout: Duration = 2.hours
    override val requestTimeout: Duration = 2.hours
  }

  val settings = ServerSettings(ConfigFactory.load).withVerboseErrorMessages(true).withTimeouts(MyTimeouts)
  startServer(address, HttpRemoteOptimisationEngineClient.DefaultPort, settings)
}

// End ///////////////////////////////////////////////////////////////
