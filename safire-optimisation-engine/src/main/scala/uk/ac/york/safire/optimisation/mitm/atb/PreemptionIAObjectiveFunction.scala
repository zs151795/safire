package uk.ac.york.safire.optimisation.mitm.atb

import uk.ac.york.safire.metrics._

import uk.ac.york.safire.optimisation._

import uk.ac.york.safire.optimisation.ia._
import scala.collection.mutable.PriorityQueue

import uk.ac.york.safire.metrics.ValueVisitor

import uk.ac.york.safire.optimisation.ObjectiveFunction
import uk.ac.york.safire.optimisation.ObjectiveFunctionResult

import java.util.concurrent._

///////////////////////////////////

/**
 * Minimise over cost, makespan and number of violations of user priority ordering
 * with possibility of pre-emption over subtasks
 */

class PreemptionIAObjectiveFunction(sequenceDependentTasks: java.util.List[SequenceDependentTaskInfo]) extends ObjectiveFunction.LocalObjectiveFunction {

  import uk.ac.york.safire.optimisation.ObjectiveFunction.Result

  override def predictKeyObjectives(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result =
    predictKeyObjectivesImpl(originalConfig, proposedControlMetrics)

  private def predictKeyObjectivesImpl(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result = {

    import scala.collection.JavaConversions._

    val config = Configuration.update(originalConfig, proposedControlMetrics, originalConfig.getKeyObjectives)
    val (recipes, resources) = IAObjectiveFunction.reverseEngineerRecipesFromConfig(config)

    ///////////////////////////////

    PreemptionIAObjectiveFunction.makeTasks(recipes, resources, config) match {
      case m @ Left(errorString) => new Result(config.getKeyObjectives, errorString)
      case Right(tasks) => {
        val scheduler = new FIFOScheduler()

        def parentTaskPriority(t: Task): Int =
          IAObjectiveFunction.priority(PreemptionIAObjectiveFunction.parentTask(t.id).name, config)

        def preemptionPriorityIncrement(t: Task): Double = {
          val inc = PreemptionIAObjectiveFunction.preemptionPoint(t.id).get / 10000.0
          (1.0 - inc)
        }

        def endUserPriority(t: Task): Double = {
          val result = parentTaskPriority(t) + preemptionPriorityIncrement(t)
          result
        }

        val taskQueue: PriorityQueue[Task] = PriorityQueue(tasks: _*)(Ordering.by { endUserPriority })

        val sequenceDependentSetup = SequenceDependentTaskInfo.makeSequenceDependantSetup(sequenceDependentTasks)

        val unAvailableResources = config.getObservableMetrics.filter(c => c._1.contains(" unAvailabileTime ")).map(c => (c._1.split(" unAvailabileTime ").head, c._1.split(" unAvailabileTime ").last.split("_").toList))

        val unAvailableTasks = unAvailableResources.map {
          case (key, value) =>
            val resource = Resource(key + " UA")

            val unAvailableTaskOneLine = value.map { value =>
              val startTime = value.split(",").head.toInt
              val endTime = value.split(",").last.toInt
              val duration = endTime - startTime

              val taskId = key + " NotAvailble from " + startTime + " to " + endTime

              Task.apply(
                TaskId(taskId),
                Map(resource -> duration),
                Map(),
                releaseTime = startTime, 0, 0, 0, Map()
              )
            }
            unAvailableTaskOneLine
        }

        val unAvailableTaskList = unAvailableTasks.toList.flatten

        scheduler.schedule(taskQueue, resources.toSet, sequenceDependentSetup, false, unAvailableTaskList) match {
          case None => new Result(config.getKeyObjectives, "Scheduler cannot find a valid schedule")
          case Some(schedule) =>
            val readableSchedule = schedule.byResource.filter(k => !k._1.id.contains("UA"))

            //            println(schedule)
            //            println()
            //            val allTask = schedule.byResource.map { r => r._2 }.flatten.toList
            //            val small = allTask.filter(t => t._1.id.name.contains("small")).map(t => t._2).sortWith((c1, c2) => c1.lower < c2.lower)
            //            val medium = allTask.filter(t => t._1.id.name.contains("medium")).map(t => t._2).sortWith((c1, c2) => c1.lower < c2.lower)
            //            val large = allTask.filter(t => t._1.id.name.contains("large")).map(t => t._2).sortWith((c1, c2) => c1.lower < c2.lower)

            // val newKeyObjectives = Map("makespan" ->
            // Value.realValue(makespan, config.getKeyObjectives().get("makespan").getType.asInstanceOf[ValueType.Real]))

            val oldObjectives = config.getKeyObjectives;
            val newKeyObjectives = doUpdatedKeyObjectives(schedule, config)

            val resObjectives = Map() ++ oldObjectives.map {
              case (key, value) =>
                val valueNew = newKeyObjectives.get(key).get;

                (key, valueNew)
            }
            val reportStr = "Status: Succeeded.\n" +
              PreemptionIAObjectiveFunction.toReportString(schedule, newKeyObjectives, config) +
              doReportStringSuffix(schedule, newKeyObjectives)
            new Result(resObjectives, reportStr)
        }
      }
    }
  }

  // protected def doUpdatedKeyObjectives(schedule: Schedule, config: Configuration): Map[String, Value]

  /////////////////////////////////

  //  override def objectiveValues(result: Result): java.util.List[java.lang.Double] = {
  //    val value = result.keyObjectiveMetrics.get("makespan")
  //    val visitor = new ValueVisitor.NumberVisitor()
  //
  //    import scala.collection.JavaConversions._
  //    List(IAObjectiveFunction.doubleValue(value).asInstanceOf[java.lang.Double])
  //  }

  /////////////////////////////////

  override def evaluate(args: ObjectiveFunctionArguments): Callable[ObjectiveFunctionResult] =
    new Callable[ObjectiveFunctionResult]() {
      override def call(): ObjectiveFunctionResult = {
        val result = predictKeyObjectivesImpl(args.getConfiguration, args.getProposedControlMetrics)
        // jeep.lang.Diag.println("statusString: " + result.objectiveFunctionStatusReport)
        val newConfig = IAObjectiveFunction.addOptimisationStatusToConfiguration(
          result.objectiveFunctionStatusReport,
          Configuration.update(args.getConfiguration, args.getProposedControlMetrics, result.keyObjectiveMetrics)
        )
        // org.apache.commons.math3.util.Pair.create(newConfig, objectiveValues(result))
        new ObjectiveFunctionResult(newConfig, objectiveValues(result))
      }
    }

  /////////////////////////////////

  // protected def doReportStringSuffix(s: Schedule, newKeyObjectives: Map[String, Value]): String = ???

  override def numObjectives: Int = 3

  override def objectiveValues(result: Result): java.util.List[java.lang.Double] = {
    import scala.collection.JavaConversions._

    val value = result.keyObjectiveMetrics.get("makespan")
    val value1 = result.keyObjectiveMetrics.get("energy")
    val value2 = result.keyObjectiveMetrics.get("montary")
    val value3 = result.keyObjectiveMetrics.get("urgency")
    val value4 = result.keyObjectiveMetrics.get("quality")

    val visitor = new ValueVisitor.NumberVisitor()
    val makespan: java.lang.Double = if (value != null) IAObjectiveFunction.doubleValue(value) else -1
    val energy: java.lang.Double = if (value1 != null) IAObjectiveFunction.doubleValue(value1) else -1
    val montary: java.lang.Double = if (value2 != null) IAObjectiveFunction.doubleValue(value2) else -1
    val urgency: java.lang.Double = if (value3 != null) IAObjectiveFunction.doubleValue(value3) else -1
    val quality: java.lang.Double = if (value4 != null) IAObjectiveFunction.doubleValue(value4) else -1

    val objectives = List(makespan) ++ List(energy) ++ List(montary) ++ List(urgency) ++ List(quality)
    objectives.filter(o => o >= 0)

    //    val cost = IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("totalcost")).asInstanceOf[java.lang.Double]
    //    val makespan = IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("makespan")).asInstanceOf[java.lang.Double]
    //    val userPreferenceViolations = IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("user preference violations"))
    //
    //    // List(IAObjectiveFunction.doubleValue(result.keyObjectiveMetrics.get("totalcost")).asInstanceOf[java.lang.Double])
    //    // val xx = (OnaObjectiveFunction.W1*makespan)/(50*111917.3) + (OnaObjectiveFunction.W2*cost)/68616.1
    //    // val xx = (OnaObjectiveFunction.W1 * makespan) / (50 * 111917.3) + (OnaObjectiveFunction.W2 * cost) / 68616.1
    //    List[java.lang.Double](cost, makespan, userPreferenceViolations)
  }

  /////////////////////////////////

  protected def doUpdatedKeyObjectives(schedule: Schedule, config: Configuration): Map[String, Value] = {

    import scala.collection.JavaConversions._

    // jeep.lang.Diag.println( config.getObservableMetrics().filter { o => o._1.contains("cost" )} )

    //      if (config.getObservableMetrics().get(recipeAndResourceName + " cost") == null)
    //        jeep.lang.Diag.println("cost not found for: " + recipeAndResourceName)
    //      IAObjectiveFunction.doubleValue(config.getObservableMetrics().get(recipeAndResourceName + " cost"))

    def stripUid(taskId: String): String = {
      val lo = taskId.indexOf("[UID")
      val hi = taskId.indexOf("]")
      if (lo == -1 || hi == -1) taskId else { taskId.substring(0, lo) + taskId.substring(hi + 1, taskId.length) }
    }

    val totalEnergy = (for ((resource, tasks) <- schedule.byResource; task <- tasks) yield {
      task._1.energyCost;
    }).sum
    val totalMontary = (for ((resource, tasks) <- schedule.byResource; task <- tasks) yield {
      task._1.montaryCost;
    }).sum

    val tasksFlatten = schedule.byResource.values.flatten.toList.filter(ft => ft._1.isFinal == 1).sortWith((c1, c2) => c1._2.upper.compareTo(c2._2.upper) < 0).reverse

    val productUrgency = config.getObservableMetrics.filter(c => c._1.contains("urgency")).keys.toList.sortBy(c => c.split(" ").last.toInt);
    val productName = productUrgency.map(c => c.split(" ")(0))

    val finalOrders = tasksFlatten.sortWith((c1, c2) => c1._2.upper.compareTo(c2._2.upper) < 0).map(t => t._1.id.name.split(" ")(0))

    val urgencyVoliate = finalOrders.map(p => productName(finalOrders.indexOf(p)).equals(p)).filter(b => !b)

    import uk.ac.york.safire.factoryModel.ONA.ValueCurve
    import scala.collection.JavaConverters._

    def intValue(value: Value): Int =
      new ValueVisitor.NumberVisitor().visit(value).intValue()

    val finalTasks = schedule.byResource.map(r => r._2.filter(t => t._1.isFinal == 1)).flatten.toList
    val time = finalTasks.map(t => t._2.upper).sorted.map(i => new Integer(i))
    val timeJava = time.asJava

    val drop = intValue(config.getObservableMetrics.get("drop"));

    val vaildTime = time.filter(t => t <= ValueCurve.Z)
    val makespan = if (vaildTime.isEmpty) 9999999 else (if (config.getKeyObjectives.size() > 1 && drop == 1) vaildTime.max.toDouble else schedule.makespan)

    val quality = ValueCurve.getQualityExact(timeJava, productName.size);
    val qualityV = Value.realValue(quality, ValueType.realType(Double.MinValue, Double.MaxValue).asInstanceOf[ValueType.Real]);

    val values = ValueCurve.getQualityList(time)
    val nop = if (config.getKeyObjectives.size() > 1 && drop == 1) values.filter(t => t > 0).size else finalTasks.size

    val objectiveMap = Map(
      "makespan" -> Value.realValue(makespan, config.getKeyObjectives().get("makespan").getType.asInstanceOf[ValueType.Real]),
      "energy" -> Value.realValue(totalEnergy, ValueType.realType(0.0, Double.MaxValue)),
      "montary" -> Value.realValue(totalMontary, ValueType.realType(0.0, Double.MaxValue)),
      "urgency" -> Value.realValue(urgencyVoliate.size, ValueType.realType(0.0, Double.MaxValue)),
      "quality" -> qualityV,
      "nop" -> Value.realValue(nop, ValueType.realType(0.0, Double.MaxValue))
    )

    val oldObjectives = config.getKeyObjectives;

    val newObjectives = Map("nop" -> Value.realValue(nop, ValueType.realType(0.0, Double.MaxValue))) ++ oldObjectives.map {
      case (key, value) =>
        val valueNew = objectiveMap.get(key).get;

        (key, valueNew)
    }

    newObjectives
  }

  /////////////////////////////////

  protected def doReportStringSuffix(s: Schedule, newKeyObjectives: Map[String, Value]): String = {
    s"\nmakespan: ${new jeep.util.HMS(s.makespan)}\n"
  }
}

///////////////////////////////////

object PreemptionIAObjectiveFunction {

  import IAObjectiveFunction.{ InstanceName => InstanceName }

  // Drop the "preempt N" suffix from synthetic 'preempt' tasks:
  def parentTask(tid: TaskId): TaskId =
    if (tid.name.contains("preempt"))
      TaskId(tid.name.split(" ").dropRight(4).mkString(" "))
    else
      tid

  def preemptionPoint(tid: TaskId): Option[Int] =
    if (tid.name.contains("preempt"))
      Some(tid.name.split(" ").last.toInt)
    else
      None

  /////////////////////////////////

  def searchBasedPriorityPermutation(config: Configuration): List[Int] = {
    val priorityPermutationIndex = config.getControlledMetrics().get("priority-permutation-index")
    val upperBound = new ValueTypeVisitor.NumberRangeVisitor().visit(priorityPermutationIndex.getType()).getRight.intValue()
    val n = ComeauBijectivePermutationHash.inverseFactorial(upperBound)
    val index = IAObjectiveFunction.intValue(config.getControlledMetrics().get("priority-permutation-index"))
    val perm = ComeauBijectivePermutationHash.permutationOfIndex(index, n)
    perm.toList
  }

  /////////////////////////////////

  //  def preemptionPoints(instanceName: InstanceName, allocatedResource: Resource, config: Configuration): Int = {
  //
  //    val recipeAndResourceNamePrefix = instanceName + " " + allocatedResource.id
  //    val value = config.getObservableMetrics().get(recipeAndResourceNamePrefix + " preemption-points")
  //    if (value == null)
  //      jeep.lang.Diag.println("No preemption-points for " + recipeAndResourceNamePrefix)
  //
  //    IAObjectiveFunction.intValue(config.getObservableMetrics().get(recipeAndResourceNamePrefix + " preemption-points"))
  //  }

  /////////////////////////////////

  def makeTasks(recipes: Map[String, List[RecipeInfo]], resources: List[Resource], config: Configuration): Either[String, List[Task]] = {

    val mutex = IAObjectiveFunction.parseMutexesONA(resources, config)

    //    def mkTask(instanceName: InstanceName, allocatedResource: Resource, preemptionPointIndex: Int, numPreemptionPoints: Int): Task = {
    //      // require( isCompatibleResource(recipes, resources)(infoName + in, allocatedResource) )
    //      val executionInterval = IAObjectiveFunction.interval(instanceName, allocatedResource, config)
    //      val startTime = jeep.math.LinearInterpolation.apply(preemptionPointIndex,
    //        0, numPreemptionPoints-1, executionInterval.lower(), executionInterval.upper() )
    //
    //      val executionLength = executionInterval.length / numPreemptionPoints
    //
    //      Task.single(
    //        TaskId(instanceName.toString()),
    //        Map(allocatedResource -> executionLength),
    //        releaseTime = executionInterval.lower,
    //        mutex
    //      )
    //    }

    // def sequenceDependentSetup: resource: Resource, last: Task, next: Task => Option[Task]

    def mkPreemptionTaskList(instanceName: InstanceName, allocatedResource: Resource): List[Task] = {

      import scala.collection.JavaConversions._
      import uk.ac.york.safire.metrics.ValueVisitor
      import uk.ac.york.safire.metrics._

      if (allocatedResource.id.equals("No allocation"))
        return Nil

      val result = Map() ++ config.getObservableMetrics()
      val result1 = result.filter {
        case (name, value) => /*name.contains(instanceName) &&*/ name.contains(instanceName + " " + allocatedResource.id) && !name.contains(" mutex ") && !name.contains("availability")
      }

      val res_test = result.filter {
        case (name, value) => name.contains(instanceName) && !name.contains(" mutex ") && !name.contains("availability")
      }

      val taskStart = result1.filter {
        case (name, value) => name.contains("start")
      }

      val taskEnd = result1.filter {
        case (name, value) => name.contains("end")
      }

      val taskEnergy = result1.filter {
        case (name, value) => name.contains("energy")
      }

      val taskMontary = result1.filter {
        case (name, value) => name.contains("montary")
      }

      val executionRelationRaw = result1.filter {
        case (name, value) => name.contains("executedAfter")
      }

      val executionRelation = executionRelationRaw.map {
        case (name, value) =>
          val toArray = name.split(" executedAfter ")
          (toArray(1), toArray(0))
      }

      val taskNames = taskStart.map(f => f._1.split(" ").dropRight(1).mkString(" ")).toList.sorted

      val tasks = taskNames.map { name =>
        val taskId = TaskId(name)

        def intValue(value: Value): Int =
          new ValueVisitor.NumberVisitor().visit(value).intValue()

        val endTime = intValue(taskEnd.filter(t => t._1.contains(name)).head._2)
        val startTime = intValue(taskStart.filter(t => t._1.contains(name)).head._2)
        val energyCost = intValue(taskEnergy.filter(t => t._1.contains(name)).head._2)
        val montaryCost = intValue(taskMontary.filter(t => t._1.contains(name)).head._2)

        val taskExecutedBeforeMap = executionRelation.filter { r => r._2.equals(name) }
        val isFinal = executionRelation.filter { r => r._1.equals(name) }

        if (taskExecutedBeforeMap.size > 1) {
          println("ERROR: Task execution map size bigger than 1 after filter");
        }

        if (taskExecutedBeforeMap.size > 0) {
          if (isFinal.size == 0) {
            Task.apply(
              taskId,
              Map(allocatedResource -> endTime),
              Map(TaskId(taskExecutedBeforeMap.last._1) -> IntervalRelation.PreceededGenerallyBy),
              releaseTime = startTime, energyCost, montaryCost, 1, mutex
            )
          } else {
            Task.apply(
              taskId,
              Map(allocatedResource -> endTime),
              Map(TaskId(taskExecutedBeforeMap.last._1) -> IntervalRelation.PreceededGenerallyBy),
              releaseTime = startTime, energyCost, montaryCost, 0, mutex
            )
          }

        } else {
          Task.apply(
            taskId,
            Map(allocatedResource -> endTime),
            Map(),
            releaseTime = startTime, energyCost, montaryCost, 0, mutex
          )
        }

      }
      tasks
    }

    val tasks = (for ((recipeName, infos) <- recipes; info <- infos; instanceId <- 0 until info.instances) yield {
      // val instanceName = info.name + " " + instanceId
      val instanceName = InstanceName(info, instanceId)
      val allocatedResource = IAObjectiveFunction.allocation(instanceName, config)
      //      if (allocatedResource.id.equals("No allocation")) {
      //        println("here");
      //      }

      val allocationAvailable = IAObjectiveFunction.isResourceAvailable(allocatedResource, config)
      if (!allocationAvailable) {
        Left(s"${allocatedResource.id} is not available for task $instanceName")
      } else {
        Right(mkPreemptionTaskList(instanceName, allocatedResource))
      }
    }).toList

    // if all tasks could be allocated to resources, return them in a list
    // otherwise, provide an descriptive string for the unallocated ones
    Either.cond(
      tasks.forall { e => e.isRight },
      tasks.map { case Right(x) => x }.flatten,
      tasks.map { case Left(descriptiveStr) => descriptiveStr + "\n"; case Right(task) => "" }.mkString
    )
  }

  /////////////////////////////////
  def toReportString(s: Schedule, newKeyObjectives: Map[String, Value], config: Configuration): String = {

    val finalTasks = s.byResource.map(r => r._2.filter(t => t._1.isFinal == 1)).flatten.toList
    val finishTimes = finalTasks.map(t => t._2.upper).sorted

    val res = finishTimes.mkString(" ")
    val numberOfParts = IAObjectiveFunction.intValue(newKeyObjectives.get("nop").get);
    res + "_" + numberOfParts

    //    import java.time._
    //    val now = java.time.LocalDateTime.now()
    //
    //    def toTimeString(i: Interval): String = {
    //      val start = now.plusSeconds(i.lower)
    //      val duration = Duration.ofSeconds(i.length)
    //      val end = start.plus(duration)
    //
    //      val fmt = java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME
    //      s"from ${start.format(fmt)} to ${end.format(fmt)}"
    //    }
    //
    //    // val cmp = new NaturalOrderComparator()
    //
    //    val cmp = new java.util.Comparator[Interval]() {
    //      override def compare(a: Interval, b: Interval): Int =
    //        a.lower - b.lower
    //    }
    //
    //    val m = s.byResource.map {
    //      case (resource, assignments) =>
    //        resource.id -> assignments.map {
    //          case (task, interval) =>
    //            (task.id.name, interval)
    //        }
    //    }.map {
    //      case (resource, tasks) =>
    //        (resource, tasks.sortWith {
    //          case (a, b) =>
    //            cmp.compare(a._2, b._2) <= 0
    //        })
    //    }.map {
    //      case (resource, tasks) => (resource, tasks.map {
    //        case (task, interval) =>
    //          (task, interval)
    //      })
    //    }
    //    // (task,toTimeString(interval)) } ) }
    //
    //    // import scala.collection.JavaConverters._
    //    // val typeOfMap=new com.google.gson.reflect.TypeToken[java.util.Map[String,java.util.List[ org.apache.commons.lang3.tuple.Pair[ String, org.apache.commons.lang3.tuple.Pair[ LocalDateTime, LocalDateTime ] ] ] ] ](){}.getType()
    //    // new JsonConverter().toJson(m.asJava,typeOfMap)
    //    // new JsonConverter().toJson(m.asJava)
    //
    //    val result = (m.map {
    //      case (resource, tasks) => s"$resource -> " +
    //        tasks.map { case (product, interval) => s"$product $interval" }.mkString("[", ",\n", "]")
    //    }).mkString("\n")
    //
    //    result
  }

  def toReportStringELE(s: Schedule, newKeyObjectives: Map[String, Value], config: Configuration): String = {

    import java.time._
    val now = java.time.LocalDateTime.now()

    val cmp = new java.util.Comparator[Interval]() {
      override def compare(a: Interval, b: Interval): Int =
        a.lower - b.lower
    }

    val ByResourceProcessed = s.byResource.map {
      r =>
        val taskList = r._2.filter(t => t._1.isFinal == 1)

        (r._1, taskList)
    }

    val m = ByResourceProcessed.map {
      case (resource, assignments) =>
        resource.id -> assignments.map {
          case (task, interval) =>
            (task, interval)
        }
    }.map {
      case (resource, tasks) =>
        (resource, tasks.sortWith {
          case (a, b) =>
            cmp.compare(a._2, b._2) <= 0
        })
    }.map {
      case (resource, tasks) => (resource, tasks.map {
        case (task, interval) =>
          (task, interval)
      })
    }

    val startTasks = s.byResource.map {
      r =>
        val taskList = r._2.filter(t => t._1.isFinal == 0)

        taskList
    }.flatten

    val endTasks = m.map(r => r._2).flatten

    val result = endTasks.map { t =>
      val task = t._1
      val taskNameForSearch = t._1.id.name.split("_").head

      val startTask = startTasks.find(t => t._1.id.name.contains(taskNameForSearch))
      val startTime = if (startTask.isEmpty) t._2.lower else startTask.head._2.lower
      val endTime = t._2.upper

      val start = now.plusSeconds(startTime * 60)
      //      val duration = Duration.ofSeconds((endTime - startTime) * 60)
      val end = start.plusSeconds((endTime - startTime) * 60)
      val fmt = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")

      val taskName = t._1.id.name.split("_").head.split(" ").dropRight(1).mkString(" ")
      val instanceNumber = t._1.id.name.split("_").head.split(" ").last
      val diviceNameRaw = t._1.resourceExecutionTime.head._1.id
      val diviceName = diviceNameRaw.split(" ").toList.map(s => s.replaceAll("\\(", "").replaceAll("\\)", "")).mkString("-");
      //      val diviceID = diviceNameRaw.split(" ").toList.map(s=>s.filter(_.isDigit)).mkString("-");

      val deviceNameOut = if (diviceName.split("-").size == 2) "CookingZone" else "CookingZone"; //"Bridge"
      val diviceID = if (diviceName.split("-").size == 2) diviceName.split("-").head.last + "" else { if (diviceName.split("-").head.last == '1') "5" else "6" }
      val potNameRaw = diviceName.split("-").last
      val potName = potNameRaw.dropRight(1) + " " + potNameRaw.last

      val year = now.getYear()
      val month = now.getMonthValue()
      val day = now.getDayOfMonth()
      val startStamp = start.format(fmt)
      val endStamp = end.format(fmt)
      val prodcutName = taskName.split(" ").dropRight(1).mkString(" ")
      val recipeType = taskName.split(" ").last
      val amountProduced = IAObjectiveFunction.intValue(config.getObservableMetrics.get(taskName + " amount produced"))
      val deviceID = diviceID;
      val device = deviceNameOut + " " + deviceID;
      val availability = 1

      //      println("device Name: " + diviceNameRaw + "  we have: " + device);

      year + "_" + month + "_" + day + "_" + startStamp + "_" + endStamp + "_" + prodcutName + "_" + recipeType + "_" + instanceNumber + "_" + amountProduced + "_" + deviceID + "_" + device + "_" + availability + "_" + potName + "_" + 1
    }.mkString("\n")

    result
  }

  def toReportStringOAS(s: Schedule, newKeyObjectives: Map[String, Value], config: Configuration): String = {

    import java.time._
    val now = java.time.LocalDateTime.now()

    val cmp = new java.util.Comparator[Interval]() {
      override def compare(a: Interval, b: Interval): Int =
        a.lower - b.lower
    }

    val ByResourceProcessed = s.byResource.map {
      r =>
        val taskList = r._2.filter(t => t._1.isFinal == 1)

        (r._1, taskList)
    }

    val m = ByResourceProcessed.map {
      case (resource, assignments) =>
        resource.id -> assignments.map {
          case (task, interval) =>
            (task, interval)
        }
    }.map {
      case (resource, tasks) =>
        (resource, tasks.sortWith {
          case (a, b) =>
            cmp.compare(a._2, b._2) <= 0
        })
    }.map {
      case (resource, tasks) => (resource, tasks.map {
        case (task, interval) =>
          (task, interval)
      })
    }

    val startTasks = s.byResource.map {
      r =>
        val taskList = r._2.filter(t => t._1.id.name.contains("silo") && !t._1.id.name.contains("DependentSetUp"))

        taskList
    }.flatten

    val endTasks = m.map(r => r._2).flatten

    val result = endTasks.map { t =>
      val task = t._1
      val taskNameForSearch = t._1.id.name.split("_").head

      val startTime = startTasks.find(t => t._1.id.name.contains(taskNameForSearch)).head._2.upper
      val endTime = t._2.upper

      val start = now.plusSeconds(startTime * 60)
      //      val duration = Duration.ofSeconds((endTime - startTime) * 60)
      val end = start.plusSeconds((endTime - startTime) * 60)
      val fmt = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")

      val taskName = t._1.id.name.split("_").head.split(" ").dropRight(1).mkString(" ")
      val instanceNumber = t._1.id.name.split("_").head.split(" ").last
      val diviceNameRaw = t._1.resourceExecutionTime.head._1.id.split(" ").drop(1).head
      val diviceName = diviceNameRaw.split("\\(").head
      val diviceID = diviceNameRaw.split("\\(").last.split("\\)").head

      val year = now.getYear()
      val month = now.getMonthValue()
      val day = now.getDayOfMonth()
      val startStamp = start.format(fmt)
      val endStamp = end.format(fmt)
      val prodcutName = taskName.split(" ").dropRight(1).mkString(" ")
      val recipeType = taskName.split(" ").last
      val amountProduced = IAObjectiveFunction.intValue(config.getObservableMetrics.get(taskName + " amount produced"))
      val deviceID = diviceID
      val device = diviceName + " " + deviceID
      val availability = 1

      year + "_" + month + "_" + day + "_" + startStamp + "_" + endStamp + "_" + prodcutName + "_" + recipeType + "_" + instanceNumber + "_" + amountProduced + "_" + deviceID + "_" + device + "_" + availability
    }.mkString("\n")

    result
  }
}

// End ///////////////////////////////////////////////////////////////
