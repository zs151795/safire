package uk.ac.york.safire.optimisation

import uk.ac.york.safire.metrics._
import scala.collection.JavaConversions._

///////////////////////////////////

object HttpRemoteOptimisationEngineClient {

  val DefaultPort = 8080

  val DefaultEnergyCostWeighting = 1.0
  val DefaultMakespanCostWeighting = 1.0

  /////////////////////////////////

  def createHttpOptimisationEngineClient(args: Array[String]): OptimisationEngine = {
    val args0 = if (args.length == 0) {
      "http:/" + new java.net.InetSocketAddress("127.0.0.1", DefaultPort)
    } else {
      if (args.length == 1)
        args(0)
      else {
        println("Usage: " + this.getClass().getSimpleName() + " http://optimization-engine-url")
        System.exit(0)
        ???
      }
    }

    // jeep.lang.Diag.println( args0 );
    new OptimisationEngine.RemoteHttpJsonOptimisationEngine(new java.net.URL(args0))
  }

  /////////////////////////////////

  def run(populationSize: Int, args: Array[String]): Unit = {
    val seed = 0xDEADBEEF

    val urgency = 0.9
    val quality = 0.1

//        val objectiveFunction = MockObjectiveFunction.largerPlant(
    val objectiveFunction = MockObjectiveFunction.smallPlant(
      energyCostWeighting = HttpRemoteOptimisationEngineClient.DefaultEnergyCostWeighting,
      makespanCostWeighting = HttpRemoteOptimisationEngineClient.DefaultMakespanCostWeighting,
    )

    val optimisationEngine = createHttpOptimisationEngineClient(args)
    OEClientCommon.run(optimisationEngine, objectiveFunction, populationSize, urgency, quality, seed)
  }

  /////////////////////////////////  

  def main(args: Array[String]): Unit = {
/*    val sizes = (500 to 1000 by 100).toList
    for (populationSize <- sizes; i <- 0 until 10) {
      run(populationSize, args)
      println("******************************************************************")
    }
      */
    val populationSize=10
    run(populationSize, args)

  }
}

// End ///////////////////////////////////////////////////////////////
