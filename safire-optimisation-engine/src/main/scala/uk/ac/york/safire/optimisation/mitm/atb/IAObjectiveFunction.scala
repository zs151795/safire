package uk.ac.york.safire.optimisation.mitm.atb

import uk.ac.york.safire.metrics._

import uk.ac.york.safire.optimisation._
import uk.ac.york.safire.optimisation.ia._

import scala.collection.mutable.PriorityQueue

import uk.ac.york.safire.metrics.ValueVisitor

import java.util.concurrent._

///////////////////////////////////

trait IAObjectiveFunction extends ObjectiveFunction.LocalObjectiveFunction {

  import uk.ac.york.safire.optimisation.ObjectiveFunction.Result

  /////////////////////////////////

  // override def numObjectives: Int = 1

  /////////////////////////////////

  override def predictKeyObjectives(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result = {
    predictKeyObjectivesImpl(originalConfig, proposedControlMetrics)
  }

  /////////////////////////////////

  private def predictKeyObjectivesImpl(originalConfig: Configuration, proposedControlMetrics: java.util.Map[String, Value]): Result = {

    import scala.collection.JavaConversions._

    val config = Configuration.update(originalConfig, proposedControlMetrics, originalConfig.getKeyObjectives)
    val (recipes, resources) = IAObjectiveFunction.reverseEngineerRecipesFromConfig(config)

    ///////////////////////////////

    IAObjectiveFunction.makeTasks(recipes, resources, config) match {
      case m @ Left(errorString) => new Result(config.getKeyObjectives, errorString)
      case Right(tasks) => {
        val scheduler = new FIFOScheduler()
        def priority(t: Task): Int = IAObjectiveFunction.priority(t.id.name, config)
        val taskQueue: PriorityQueue[Task] = PriorityQueue(tasks: _*)(Ordering.by { priority })
        // jeep.lang.Diag.println("#tasks: " + taskQueue.length)
        // jeep.lang.Diag.println("tasks: " + taskQueue)
        // jeep.lang.Diag.println("priorities: " + taskQueue.map { priority })

        scheduler.schedule(taskQueue, resources.toSet) match {
          case None => new Result(config.getKeyObjectives, "Scheduler cannot find a valid schedule")
          case Some(schedule) =>
            // jeep.lang.Diag.println( schedule )
            // val newKeyObjectives = Map("makespan" ->
            // Value.realValue(makespan, config.getKeyObjectives().get("makespan").getType.asInstanceOf[ValueType.Real]))

            val newKeyObjectives = doUpdatedKeyObjectives(schedule, config)
            val reportStr = "Status: Succeeded.\n" + toReportString(schedule, newKeyObjectives)
            new Result(newKeyObjectives, reportStr)
        }
      }
    }
  }

  protected def doUpdatedKeyObjectives(schedule: Schedule, config: Configuration): Map[String, Value]

  /////////////////////////////////

  //  override def objectiveValues(result: Result): java.util.List[java.lang.Double] = {
  //    val value = result.keyObjectiveMetrics.get("makespan")
  //    val visitor = new ValueVisitor.NumberVisitor()
  //
  //    import scala.collection.JavaConversions._
  //    List(IAObjectiveFunction.doubleValue(value).asInstanceOf[java.lang.Double])
  //  }

  /////////////////////////////////

  override def evaluate(args: ObjectiveFunctionArguments): Callable[ObjectiveFunctionResult] =
    new Callable[ObjectiveFunctionResult]() {
      override def call(): ObjectiveFunctionResult = {
        // val result = predictKeyObjectivesImpl(args.getConfiguration, args.getProposedControlMetrics)
        val result = predictKeyObjectives(args.getConfiguration, args.getProposedControlMetrics)
        // jeep.lang.Diag.println("statusString: " + result.objectiveFunctionStatusReport)
        val newConfig = IAObjectiveFunction.addOptimisationStatusToConfiguration(
          result.objectiveFunctionStatusReport,
          Configuration.update(args.getConfiguration, args.getProposedControlMetrics, result.keyObjectiveMetrics)
        )
        new ObjectiveFunctionResult(newConfig, objectiveValues(result))
      }
    }

  /////////////////////////////////

  def toReportString(s: Schedule, newKeyObjectives: Map[String, Value]): String = {

    // object oasProductReporter extends ProductReporter {
    //   override def apply(r: Resource, schedule: List[(Task, Interval)]): Option[Product] =
    //   schedule.last._1.finalProduct
    // }
    // val report = Schedule.consolodatedProductReport(schedule.get, oasProductReporter)

    // val now = org.joda.time.DateTime.now()
    // def toJodaInterval(i: Interval) = {
    // val start: org.joda.time.ReadableInstant = now.plus(org.joda.time.Seconds.seconds(i.lower))
    // val duration: org.joda.time.ReadableDuration = org.joda.time.Duration.standardSeconds(i.length)
    // new org.joda.time.Interval(start,duration)
    // }

    import java.time._
    val now = java.time.LocalDateTime.now()

    def toTimeString(i: Interval): String = {
      val start = now.plusSeconds(i.lower)
      val duration = Duration.ofSeconds(i.length)
      val end = start.plus(duration)

      val fmt = java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME
      s"from ${start.format(fmt)} to ${end.format(fmt)}"
    }

    val cmp = new NaturalOrderComparator()

    val m = s.byResource.map {
      case (resource, assignments) =>
        resource.id -> assignments.map {
          case (task, interval) =>
            (task.id.name, toTimeString(interval))
        }
    }.toSeq.sortWith { case (a, b) => cmp.compare(a._1, b._1) <= 0 }

    // import scala.collection.JavaConverters._
    // val typeOfMap=new com.google.gson.reflect.TypeToken[java.util.Map[String,java.util.List[ org.apache.commons.lang3.tuple.Pair[ String, org.apache.commons.lang3.tuple.Pair[ LocalDateTime, LocalDateTime ] ] ] ] ](){}.getType()
    // new JsonConverter().toJson(m.asJava,typeOfMap)
    // new JsonConverter().toJson(m.asJava)

    val result = (m.map {
      case (resource, tasks) => s"$resource -> " +
        tasks.map { case (product, interval) => s"$product $interval" }.mkString("[", ",\n", "]")
    }).mkString("\n") + doReportStringSuffix(s, newKeyObjectives)

    result
  }

  /////////////////////////////////

  protected def doReportStringSuffix(s: Schedule, newKeyObjectives: Map[String, Value]): String = ???
}

///////////////////////////////////

object IAObjectiveFunction {

  case class InstanceName(info: RecipeInfo, instanceId: Int) {
    require(instanceId >= 0)

    override def toString(): String = s"${info.name} $instanceId"
  }

  /////////////////////////////////

  def doubleValue(value: Value): Double =
    new ValueVisitor.NumberVisitor().visit(value).doubleValue()

  def intValue(value: Value): Int =
    new ValueVisitor.NumberVisitor().visit(value).intValue()

  def stringValue(value: Value): String = {
    new ValueVisitor.ValueStringVisitor().visit(value)
  }

  /////////////////////////////////

  def interval(instanceName: InstanceName, allocatedResource: Resource, config: Configuration): Interval = {

    val recipeAndResourceNamePrefix = instanceName + " " + allocatedResource.id
    val startVal = config.getObservableMetrics().get(recipeAndResourceNamePrefix + " start")
    if (startVal == null)
      jeep.lang.Diag.println("No start time for " + recipeAndResourceNamePrefix)

    val start = intValue(config.getObservableMetrics().get(recipeAndResourceNamePrefix + " start"))
    val end = intValue(config.getObservableMetrics().get(recipeAndResourceNamePrefix + " end"))

    // FIXME: this is necessary to workaround randomly-generated configurations...
    if (start < end) Interval(start, end) else Interval(end, start)
  }

  def getEnergyCost(instanceName: InstanceName, allocatedResource: Resource, config: Configuration): Int = {

    val recipeAndResourceNamePrefix = instanceName + " " + allocatedResource.id
    val energy = config.getObservableMetrics().get(recipeAndResourceNamePrefix + " energy")
    if (energy == null)
      jeep.lang.Diag.println("No energy for " + recipeAndResourceNamePrefix)

    intValue(energy)

  }

  def getMontaryCost(instanceName: InstanceName, allocatedResource: Resource, config: Configuration): Int = {

    val recipeAndResourceNamePrefix = instanceName + " " + allocatedResource.id
    val montary = config.getObservableMetrics().get(recipeAndResourceNamePrefix + " montary")
    if (montary == null)
      jeep.lang.Diag.println("No montary for " + recipeAndResourceNamePrefix)

    intValue(montary)
  }

  def priority(instanceName: String, config: Configuration): Int = {
    // require(config.getControlledMetrics().keySet().contains(instanceName + " priority"), s"Can't find priority for instance <<${instanceName}>>")
    if (instanceName.contains("unavailable"))
      Int.MaxValue
    else
      intValue(config.getControlledMetrics().get(instanceName + " priority"))
  }

  def priorityElectrolux(instanceName: String, config: Configuration): Int = {
    // require(config.getControlledMetrics().keySet().contains(instanceName + " priority"), s"Can't find priority for instance <<${instanceName}>>")
    if (instanceName.contains("unavailable"))
      Int.MaxValue
    else {
      val instanceName1 = instanceName.split("_")(0)
      intValue(config.getControlledMetrics().get(instanceName1 + " priority"))
    }

  }

  def allocation(instanceName: InstanceName, config: Configuration): Resource = {
    // TO BE REMOVED: added by PD on 28.09.2018
    //    if(instanceName.toString()=="Std Weiss B 2") {
    //      println("ContolledMetrics: " + config.getControlledMetrics())
    //    }
    //    val output = Resource(stringValue(config.getControlledMetrics().get(instanceName + " allocation")))
    //    println("instanceName= " + instanceName + ", output= "+output)
    //    output

    val resourceName = stringValue(config.getControlledMetrics().get(instanceName + " allocation"))

    Resource(resourceName)

  }

  def compatibleResources(infoName: String, recipes: Map[String, List[RecipeInfo]], resources: List[Resource]): Set[Resource] = {
    import scala.collection.JavaConversions._
    recipes.values.flatten.find { info => info.name == infoName }.map { info =>
      info.compatibleResources.toList.map { i => resources(i) }.toSet
    }.getOrElse(Set())
  }

  def isCompatibleResource(recipes: Map[String, List[RecipeInfo]], resources: List[Resource])(infoName: String, r: Resource): Boolean =
    compatibleResources(infoName, recipes, resources).contains(r)

  def isResourceAvailable(resource: Resource, config: Configuration): Boolean = {
    //    try {
    intValue(config.getObservableMetrics().get(resource.id + " availability")) != 0
    //    } catch {
    //      case npe: NullPointerException => true
    //    }
  }

  /////////////////////////////////

  def parseMutexes(resources: List[Resource], config: Configuration): Map[Resource, Set[Resource]] = {

    import scala.collection.JavaConversions._

    val mutexPairsRaw = Map() ++ config.getObservableMetrics()

    val mutexPairsRaw1 = mutexPairsRaw.filter {
      case (name, value) => name.contains(" mutex ") && intValue(value) == 1
    }

    //    println(mutexPairsRaw1)

    val mutexPairsRaw2 = mutexPairsRaw1.map {
      case (name, value) =>
        val tokens = name.split(" mutex ")
        val resource0Processed = tokens(0).split(" ");
        val resource0Processed1 = resource0Processed(0) + " " + resource0Processed(1) + " " + resource0Processed(2)
        val newKey = resource0Processed1 + " mutex " + tokens(1)

        newKey -> value

    }

    val mutexPairs = mutexPairsRaw2.map {
      case (name, value) =>
        val tokens = name.split(" mutex ")
        if (tokens.length == 2) {
          // jeep.lang.Diag.println( tokens.mkString(",") )

          List((Resource(tokens(0)), Resource(tokens(1))) -> value, (Resource(tokens(1)), Resource(tokens(0))) -> value)
        } else
          Nil

    }.flatten

    //    jeep.lang.Diag.println(mutexPairs)

    val processedResource = resources.map {
      case (r) =>
        val tokens = r.id.split(" ")

        if (tokens.length > 2)
          Resource(tokens(0) + " " + tokens(1) + " " + tokens(2))
        else
          Resource(tokens(0) + " " + tokens(1))
    }.distinct

    val result = Map() ++ processedResource.map { r =>
      r -> mutexPairs.map { case ((r1, r2), value) => if (r == r1) Some(r2) else None }.flatten.toSet
    }

    result
  }

  /////////////////////////////////

  def parseMutexesONA(resources: List[Resource], config: Configuration): Map[Resource, Set[Resource]] = {

    import scala.collection.JavaConversions._

    val mutexPairs = Map() ++ config.getObservableMetrics().filter {
      case (name, value) => name.contains(" mutex ") && intValue(value) == 1
    }.map {
      case (name, value) =>
        val tokens = name.split(" mutex ")
        if (tokens.length == 2) {
          // jeep.lang.Diag.println( tokens.mkString(",") )
          List((Resource(tokens(0)), Resource(tokens(1))) -> value, (Resource(tokens(1)), Resource(tokens(0))) -> value)
        } else
          Nil

    }.flatten
    // jeep.lang.Diag.println( mutexPairs )
    Map() ++ resources.map { r =>
      r -> mutexPairs.map { case ((r1, r2), value) => if (r == r1) Some(r2) else None }.flatten.toSet
    }

  }

  def parseMutexesElectrolux(resources: List[Resource], config: Configuration): Map[Resource, Set[Resource]] = {

    import scala.collection.JavaConversions._

    val mutexPairsRaw = Map() ++ config.getObservableMetrics()

    val mutexPairsRaw1 = mutexPairsRaw.filter {
      case (name, value) => name.contains(" mutex ") && intValue(value) == 1
    }

    //    val mutexPairsRaw2 = mutexPairsRaw1.map {
    //      case (name, value) =>
    //        val tokens = name.split(" mutex ")
    //        val resource0Processed = tokens(0).split(" ");
    //        val resource0Processed1 = resource0Processed.mkString(" ")
    //        val newKey = resource0Processed1 + " mutex " + tokens(1)
    //
    //        newKey -> value
    //
    //    }

    val mutexPairs = mutexPairsRaw1.map {
      case (name, value) =>
        val tokens = name.split(" mutex ")
        if (tokens.length == 2) {
          // jeep.lang.Diag.println( tokens.mkString(",") )

          List((Resource(tokens(0)), Resource(tokens(1))) -> value, (Resource(tokens(1)), Resource(tokens(0))) -> value)
        } else
          Nil

    }.flatten

    //    jeep.lang.Diag.println(mutexPairs)

    val processedResource = resources.map {
      case (r) =>
        Resource(r.id)
    }.distinct

    val result = Map() ++ processedResource.map { r =>
      r -> mutexPairs.map { case ((r1, r2), value) => if (r == r1) Some(r2) else None }.flatten.toSet
    }

    result
  }

  ///////////////

  def nonUniqueNames(l: java.util.List[ObservableMetricType]): Map[String, Int] = {
    import scala.collection.JavaConversions._
    l.groupBy { _.name }.mapValues(_.size).filter { case (name, count) => count > 1 }
  }

  /////////////////////////////////

  def makeTasks(recipes: Map[String, List[RecipeInfo]], resources: List[Resource], config: Configuration): Either[String, List[Task]] = {

    val mutex = parseMutexes(resources, config)
    //   jeep.lang.Diag.println( mutex )
    //    jeep.lang.Diag.println( mutex.filter { case (r,mutexes) => !mutexes.isEmpty }.mkString("\n") )

    def mkTask(instanceName: InstanceName, allocatedResource: Resource): Task = {
      // require( isCompatibleResource(recipes, resources)(infoName + in, allocatedResource) )
      val executionInterval = interval(instanceName, allocatedResource, config)
      Task.single(
        TaskId(instanceName.toString()),
        Map(allocatedResource -> executionInterval.length),
        releaseTime = executionInterval.lower,
        mutex
      )
    }

    val tasks = (for ((recipeName, infos) <- recipes; info <- infos; instanceId <- 0 until info.instances) yield {
      // val instanceName = info.name + " " + instanceId
      val instanceName = InstanceName(info, instanceId)
      val allocatedResource = allocation(instanceName, config)
      val allocationAvailable = isResourceAvailable(allocatedResource, config)
      if (!allocationAvailable) {

        val compatible = compatibleResources(info.name, recipes, resources)
        val availability = Map() ++ compatible.map { r => r -> config.getObservableMetrics().get(r.id + " availability") }
        compatible.find { r => isResourceAvailable(r, config) } match {
          // case None => Left(s"${allocatedResource.id} is not available for task $instanceName (and no compatible alternative resource is available)")
          case None => Left(s"${allocatedResource.id} is not available for task $instanceName") //  (compatible resources: ${compatible})")
          case Some(allocatedResource) => Right(mkTask(instanceName, allocatedResource))
        }
      } else {
        Right(mkTask(instanceName, allocatedResource))
      }
    }).toList

    // if all tasks could be allocated to resources, return them in a list
    // otherwise, provide an descriptive string for the unallocated ones
    Either.cond(
      tasks.forall { e => e.isRight },
      tasks.map { case Right(t) => t },
      tasks.map { case Left(descriptiveStr) => descriptiveStr + "\n"; case Right(task) => "" }.mkString
    )
  }

  ///////////////////////////////

  def reverseEngineerRecipesFromConfig(config: Configuration): (Map[String, List[RecipeInfo]], List[Resource]) = {

    import scala.collection.JavaConversions._

    ///////////////////////////////

    // compares strings containing numbers such that e.g. "1" < "10"
    val naturalOrderComparator = new NaturalOrderComparator()

    def resourcesFromAvailabilityType(c: Configuration): List[Resource] = {
      val availabilitySuffix = " availability"
      val unsorted = c.getConfigurationType().getObservableMetrics().filter {
        case ot =>
          ot.name.endsWith(availabilitySuffix)
      }.map { ot =>
        Resource(ot.name.dropRight(availabilitySuffix.length))
      }.toList

      unsorted.sortWith { (r1, r2) => naturalOrderComparator.compare(r1.id, r2.id) < 0 }
    }

    val resources = resourcesFromAvailabilityType(config)

    ///////////////////////////////

    val AllocationSuffix = " allocation"

    def instanceNamesFromAllocationControlledMetric(c: Configuration): Set[String] = {
      val True = Value.intValue(1, ValueType.intType(0, 1))
      c.getControlledMetrics().filter {
        case (name, value) => name.endsWith(AllocationSuffix)
      }.map { case (name, value) => name.dropRight(AllocationSuffix.length) }.toSet
    }

    val recipieNames = instanceNamesFromAllocationControlledMetric(config).groupBy { instanceName =>
      instanceName.split(" ").toList.dropRight(2)
    }.map { case (name, instances) => name.mkString(" ") }

    val byRecipieStage = instanceNamesFromAllocationControlledMetric(config).groupBy { instanceName =>
      instanceName.split(" ").toList.dropRight(1)
    }.map { case (name, instances) => (name.mkString(" "), instances) }

    val recipieInfoByStage = byRecipieStage.map {
      case (stageName, instances) =>
        val compatibleResources = nominalValues(stageName + " 0" + AllocationSuffix, config.getControlledMetrics).map {
          case (_, resourceName) =>
            new java.lang.Integer(resources.indexOf(Resource(resourceName)))
        }
        new RecipeInfo(stageName, instances.size, compatibleResources.toList)
    }.toList

    val recipies = Map() ++ recipieNames.toList.map { recipeName =>
      recipeName -> recipieInfoByStage.filter { ri => ri.name.startsWith(recipeName)
      }.sortWith { (r1, r2) => naturalOrderComparator.compare(r1.name, r2.name) < 0 }
    }

    (recipies, resources)
  }

  /////////////////////////////

  private def updateValues(newEntry: (String, Value), m: java.util.Map[String, Value]): java.util.Map[String, Value] = {
    val result = new java.util.HashMap[String, Value](m)
    result.put(newEntry._1, newEntry._2)
    result
  }

  def addOptimisationStatusToConfiguration(status: String, c: Configuration): Configuration = {
    import scala.collection.JavaConversions._
    import scala.collection.JavaConverters._

    val ct = c.getConfigurationType
    val MetricName = "Optimisation Result"
    val nominalType = ValueType.nominalType("value", Array(status))
    val omtu: java.util.List[ObservableMetricType] = if (ct.getObservableMetrics.exists(_.name == MetricName))
      ct.getObservableMetrics
    else {
      val newObservableType = new ObservableMetricType(MetricName, nominalType, "n/a", SampleRate.eventDriven)
      ct.getObservableMetrics :+ newObservableType
    }

    var ctu = new ConfigurationType.Explicit(ct.getKeyObjectiveMetrics, ct.getControlledMetrics, omtu)
    val newObservableValue: Value = Value.nominalValue(0, nominalType)
    new Configuration(
      ctu,
      c.getControlledMetrics,
      updateValues(MetricName -> newObservableValue, c.getObservableMetrics()),
      c.getKeyObjectives
    )
  }

  /////////////////////////////

  private def nominalValues(s: String, metrics: java.util.Map[String, Value]): Map[Int, String] = {
    import scala.collection.JavaConversions._
    val nominalType = metrics.get(s).getType().asInstanceOf[ValueType.Nominal]
    Map() ++ (0 until nominalType.numValues()).map { i => i -> nominalType.getValue(i) }
  }

  /////////////////////////////

  // used for testing purposes:
  def makeAllResourcesAvailabile(c: Configuration): Configuration = {
    import scala.collection.JavaConversions._

    val availabilitySuffix = " availability"
    val True = Value.intValue(1, ValueType.intType(0, 1))
    val newObservableMetrics = c.getObservableMetrics().map {
      case (name, value) => if (name.endsWith(availabilitySuffix)) (name, True) else (name, value)
    }

    new Configuration(
      c.getConfigurationType,
      c.getControlledMetrics,
      newObservableMetrics,
      c.getKeyObjectives
    )
  }
}

// End ///////////////////////////////////////////////////////////////
