package uk.ac.york.aura;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class TestIntStream {

	public static void main( String [] args ) {
		
		int n = 100;
		List< Integer > l = new ArrayList<>();
		IntStream.range(0, n).parallel().forEach( i -> {
			l.add(i);
		} );

		assert( l.size() == n );

	}
}


// End ///////////////////////////////////////////////////////////////
