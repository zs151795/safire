package uk.ac.york.safire.optimisation.ia;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Test;

import uk.ac.york.aura.MOEAD_RA;
import uk.ac.york.aura.MOEAD_RS;
import uk.ac.york.safire.factoryModel.ONA.ONAFactoryModel;
import uk.ac.york.safire.factoryModel.ONA.ONAXMLWriter;
import uk.ac.york.safire.factoryModel.ONA.ValueCurve;
import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.OptimisationResult;
import uk.ac.york.safire.metrics.Utility;
import uk.ac.york.safire.metrics.Value;
import uk.ac.york.safire.metrics.Value.Int;
import uk.ac.york.safire.metrics.Value.Real;
import uk.ac.york.safire.optimisation.ConfigurationUtilities;
import uk.ac.york.safire.optimisation.ObjectiveFunction;
import uk.ac.york.safire.optimisation.OptmisationUtility;
import uk.ac.york.safire.optimisation.mitm.atb.ATBSimulatorKafkaProducer.RecipeInfo;
import uk.ac.york.safire.optimisation.mitm.atb.ElectroluxConfigurationType;
import uk.ac.york.safire.optimisation.mitm.atb.ElectroluxObjectiveFunction;
import uk.ac.york.safire.optimisation.mitm.atb.FormattedResultOAS;
import uk.ac.york.safire.optimisation.mitm.atb.OasHardwiredConfigurationType;
import uk.ac.york.safire.optimisation.mitm.atb.OasObjectiveFunction;
import uk.ac.york.safire.optimisation.mitm.atb.OasProductionLineConfigurationType;
import uk.ac.york.safire.optimisation.mitm.atb.OasProductionLineObjectiveFunction;
import uk.ac.york.safire.optimisation.mitm.atb.OnaConfigurationType;
import uk.ac.york.safire.optimisation.mitm.atb.OnaHardwiredConfigurationType;
import uk.ac.york.safire.optimisation.mitm.atb.OnaMultipleObjectiveFunction;
import uk.ac.york.safire.optimisation.mitm.atb.PreemptionIAObjectiveFunction;
import uk.ac.york.safire.optimisation.mitm.atb.PreemptiveOnaConfigurationType;
import uk.ac.york.safire.optimisation.mitm.atb.SequenceDependentTaskInfo;
import uk.ac.york.safire.optimisation.mitm.atb.UoYEarlyPrototypeDemo;

///////////////////////////////////

public final class TestIAObjectiveFunction {

	@Test
	public void testMOEAD_RA_P() {
		final int urgency = 50;
		int iterations = 500;
		int populationSize = 500;
		final int numOEInvocations = 2;

		DecimalFormat df = new DecimalFormat("0.0");
		String filename = "results/OE " + numOEInvocations + " G " + iterations + " P " + populationSize + " F "
				+ df.format(MOEAD_RA.F) + " M " + df.format(MOEAD_RA.M);
		System.out.println("\n" + filename);
		File f = new File(filename);
		if (f.exists()) {
			f.delete();
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		final Random rng = new ec.util.MersenneTwister(0xDEADBEEF);
		final int percentageAvailability = 100;
		final ConfigurationType ct = OasHardwiredConfigurationType.hardwiredConfigurationType(percentageAvailability,
				rng);
		final ObjectiveFunction.LocalObjectiveFunction of = new OasObjectiveFunction(
				OasHardwiredConfigurationType.hardwiredCommodities());
		final Configuration config = Utility.randomConfiguration(ct, rng);

		final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config, of, urgency, iterations, populationSize,
				rng, numOEInvocations);

		jeep.lang.Diag.println("#front: " + or.getReconfigurations().size());

		List<List<Value>> results = new ArrayList<List<Value>>();

		for (Configuration c : or.getReconfigurations()) {
			List<Value> result = new ArrayList<Value>();

			result.add(c.getKeyObjectives().get("makespan"));

			if (c.getKeyObjectives().get("Std Weiss amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Std Weiss amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Matt amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Matt amount discrepancy score"));

			if (c.getKeyObjectives().get("Super Glanz amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Super Glanz amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Basis amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Basis amount discrepancy score"));

			results.add(result);
		}
		List<Double> sums = new ArrayList<Double>();
		List<List<Double>> analysableResults = new ArrayList<List<Double>>();
		for (int j = 0; j < results.get(0).size(); j++) {
			analysableResults.add(new ArrayList<Double>());
		}

		String finalResult = "Generations: " + iterations + " Populations: " + populationSize + " Optimization Engine: "
				+ numOEInvocations + "\n";

		for (int j = 0; j < results.size(); j++) {
			double sum = 0;
			Real makespan = (Real) results.get(j).get(0);
			finalResult += makespan.value + " ";
			analysableResults.get(0).add(makespan.value);

			for (int k = 1; k < results.get(j).size() - 1; k++) {
				Int objective = (Int) results.get(j).get(k);
				finalResult += objective.value + " ";
				sum += objective.value;
				analysableResults.get(k).add((double) objective.value);
			}

			Int objective = (Int) results.get(j).get(results.get(j).size() - 1);
			finalResult += objective.value + "\n";

			analysableResults.get(results.get(j).size() - 1).add((double) objective.value);

			sum += objective.value;
			sums.add(sum);
		}
		finalResult += "\n";

		df = new DecimalFormat("0.00");

		for (int j = 0; j < analysableResults.size(); j++) {
			DoubleSummaryStatistics stats = analysableResults.get(j).stream().mapToDouble((x) -> x).summaryStatistics();
			System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
					+ " Max: " + df.format(stats.getMax()));
			finalResult += "Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin()) + " Max: "
					+ df.format(stats.getMax()) + "\n";
		}
		DoubleSummaryStatistics stats = sums.stream().mapToDouble((x) -> x).summaryStatistics();
		System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin()) + " Max: "
				+ df.format(stats.getMax()) + " SUM.");
		finalResult += "Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin()) + " Max: "
				+ df.format(stats.getMax()) + " SUM.\n";

		try (FileWriter fw = new FileWriter(filename, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw)) {
			out.println(finalResult);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		jeep.lang.Diag.println(OptmisationUtility.keyObjectivesHistogram(or.getReconfigurations()));

		System.out.println(filename + " READY! \n");

		System.out.println("MOEA/D-RA parameter tuning success! ");
	}

	@Test
	public void testMOEAD_RA_FM() {
		final int urgency = 50;
		int iterations = 100;
		int populationSize = 500;
		final int numOEInvocations = 2;

		for (MOEAD_RA.M = 1.0; MOEAD_RA.M >= 0; MOEAD_RA.M -= 0.1) {
			for (MOEAD_RA.F = 0.3; MOEAD_RA.F >= 0; MOEAD_RA.F -= 0.1) {
				DecimalFormat df = new DecimalFormat("0.0");
				String filename = "results/OE " + numOEInvocations + " G " + iterations + " P " + populationSize + " F "
						+ df.format(MOEAD_RA.F) + " M " + df.format(MOEAD_RA.M);
				System.out.println("\n" + filename);
				File f = new File(filename);
				if (f.exists()) {
					f.delete();
					try {
						f.createNewFile();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				final Random rng = new ec.util.MersenneTwister(0xDEADBEEF);
				final int percentageAvailability = 100;
				final ConfigurationType ct = OasHardwiredConfigurationType
						.hardwiredConfigurationType(percentageAvailability, rng);
				final ObjectiveFunction.LocalObjectiveFunction of = new OasObjectiveFunction(
						OasHardwiredConfigurationType.hardwiredCommodities());
				final Configuration config = Utility.randomConfiguration(ct, rng);

				final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config, of, urgency, iterations,
						populationSize, rng, numOEInvocations);

				jeep.lang.Diag.println("#front: " + or.getReconfigurations().size());

				List<List<Value>> results = new ArrayList<List<Value>>();

				for (Configuration c : or.getReconfigurations()) {
					List<Value> result = new ArrayList<Value>();

					result.add(c.getKeyObjectives().get("makespan"));

					if (c.getKeyObjectives().get("Std Weiss amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Std Weiss amount discrepancy score"));

					if (c.getKeyObjectives().get("Weiss Matt amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Weiss Matt amount discrepancy score"));

					if (c.getKeyObjectives().get("Super Glanz amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Super Glanz amount discrepancy score"));

					if (c.getKeyObjectives().get("Weiss Basis amount discrepancy score") != null)
						result.add(c.getKeyObjectives().get("Weiss Basis amount discrepancy score"));

					results.add(result);
				}
				List<Double> sums = new ArrayList<Double>();
				List<List<Double>> analysableResults = new ArrayList<List<Double>>();
				for (int j = 0; j < results.get(0).size(); j++) {
					analysableResults.add(new ArrayList<Double>());
				}

				String finalResult = "Generations: " + iterations + " Populations: " + populationSize
						+ " Optimization Engine: " + numOEInvocations + "\n";

				for (int j = 0; j < results.size(); j++) {
					double sum = 0;
					Real makespan = (Real) results.get(j).get(0);
					finalResult += makespan.value + " ";
					analysableResults.get(0).add(makespan.value);

					for (int k = 1; k < results.get(j).size() - 1; k++) {
						Int objective = (Int) results.get(j).get(k);
						finalResult += objective.value + " ";
						sum += objective.value;
						analysableResults.get(k).add((double) objective.value);
					}

					Int objective = (Int) results.get(j).get(results.get(j).size() - 1);
					finalResult += objective.value + "\n";

					analysableResults.get(results.get(j).size() - 1).add((double) objective.value);

					sum += objective.value;
					sums.add(sum);
				}
				finalResult += "\n";

				df = new DecimalFormat("0.00");

				for (int j = 0; j < analysableResults.size(); j++) {
					DoubleSummaryStatistics stats = analysableResults.get(j).stream().mapToDouble((x) -> x)
							.summaryStatistics();
					System.out.println("Average: " + df.format(stats.getAverage()) + " Min: "
							+ df.format(stats.getMin()) + " Max: " + df.format(stats.getMax()));
					finalResult += "Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
							+ " Max: " + df.format(stats.getMax()) + "\n";
				}
				DoubleSummaryStatistics stats = sums.stream().mapToDouble((x) -> x).summaryStatistics();
				System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
						+ " Max: " + df.format(stats.getMax()) + " SUM.");
				finalResult += "Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
						+ " Max: " + df.format(stats.getMax()) + " SUM.\n";

				try (FileWriter fw = new FileWriter(filename, true);
						BufferedWriter bw = new BufferedWriter(fw);
						PrintWriter out = new PrintWriter(bw)) {
					out.println(finalResult);
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

				System.out.println(filename + " READY! \n");
			}
		}

		System.out.println("MOEA/D-RA parameter tuning success! ");
	}

	private void testMOEAD() {
		final Random rng = new ec.util.MersenneTwister(0xDEADBEEF);
		final int percentageAvailability = 100;
		final ConfigurationType ct = OasHardwiredConfigurationType.hardwiredConfigurationType(percentageAvailability,
				rng);
		final ObjectiveFunction.LocalObjectiveFunction of = new OasObjectiveFunction(
				OasHardwiredConfigurationType.hardwiredCommodities());
		final Configuration config = Utility.randomConfiguration(ct, rng);

		final int urgency = 50;
		final int iterations = 100;
		final int populationSize = 100;

		DecimalFormat df = new DecimalFormat("0.00");

		String filename = "results/OE 1" + " G " + iterations + " P " + populationSize + " F " + df.format(MOEAD_RS.F)
				+ " M " + df.format(MOEAD_RS.M);
		System.out.println("\n" + filename);

		final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config, of, urgency, iterations, populationSize,
				rng, 1);

		jeep.lang.Diag.println("#front: " + or.getReconfigurations().size());
		List<List<Value>> results = new ArrayList<List<Value>>();

		for (Configuration c : or.getReconfigurations()) {
			List<Value> result = new ArrayList<Value>();
			result.add(c.getKeyObjectives().get("makespan"));

			if (c.getKeyObjectives().get("Std Weiss amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Std Weiss amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Matt amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Matt amount discrepancy score"));

			if (c.getKeyObjectives().get("Super Glanz amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Super Glanz amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Basis amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Basis amount discrepancy score"));

			results.add(result);
		}

		String finalResult = filename + "\n";
		List<Double> sums = new ArrayList<Double>();
		List<List<Double>> analysableResults = new ArrayList<List<Double>>();
		for (int j = 0; j < results.get(0).size(); j++) {
			analysableResults.add(new ArrayList<Double>());
		}

		for (int j = 0; j < results.size(); j++) {
			double sum = 0;
			Real makespan = (Real) results.get(j).get(0);
			finalResult += makespan.value + " ";
			analysableResults.get(0).add(makespan.value);

			for (int k = 1; k < results.get(j).size() - 1; k++) {
				Int objective = (Int) results.get(j).get(k);
				finalResult += objective.value + " ";
				sum += objective.value;
				analysableResults.get(k).add((double) objective.value);
			}

			Int objective = (Int) results.get(j).get(results.get(j).size() - 1);
			finalResult += objective.value + "\n";

			analysableResults.get(results.get(j).size() - 1).add((double) objective.value);

			sum += objective.value;
			sums.add(sum);
		}
		finalResult += "\n";

		System.out.println(finalResult);

		df = new DecimalFormat("0.00");

		for (int j = 0; j < analysableResults.size(); j++) {
			DoubleSummaryStatistics stats = analysableResults.get(j).stream().mapToDouble((x) -> x).summaryStatistics();
			System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
					+ " Max: " + df.format(stats.getMax()));
		}
		DoubleSummaryStatistics stats = sums.stream().mapToDouble((x) -> x).summaryStatistics();
		System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin()) + " Max: "
				+ df.format(stats.getMax()) + " SUM.");

		System.out.println("Test prove MOEAD success! ");
	}

	private void testMOEAD_RS(boolean enableNewCrossover, boolean enableNewMutation, boolean enableImrpove,
			boolean enableElitism) {

		final Random rng = new ec.util.MersenneTwister(0xDEADBEEF);
		final int percentageAvailability = 100;
		final ConfigurationType ct = OasHardwiredConfigurationType.hardwiredConfigurationType(percentageAvailability,
				rng);
		final ObjectiveFunction.LocalObjectiveFunction of = new OasObjectiveFunction(
				OasHardwiredConfigurationType.hardwiredCommodities());
		final Configuration config = Utility.randomConfiguration(ct, rng);

		final int urgency = 50;
		final int iterations = 100;
		final int populationSize = 100;

		DecimalFormat df = new DecimalFormat("0.00");

		String filename = "results/OE 2" + " G " + iterations + " P " + populationSize + " F " + df.format(MOEAD_RS.F)
				+ " M " + df.format(MOEAD_RS.M) + " corssover: " + enableNewCrossover + " mutation: "
				+ enableNewMutation + " improve: " + enableImrpove + " Elitism: " + enableElitism;
		System.out.println("\n" + filename);

		final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config, of, urgency, iterations, populationSize,
				rng, 2);

		jeep.lang.Diag.println("#front: " + or.getReconfigurations().size());
		List<List<Value>> results = new ArrayList<List<Value>>();

		for (Configuration c : or.getReconfigurations()) {
			List<Value> result = new ArrayList<Value>();
			result.add(c.getKeyObjectives().get("makespan"));

			if (c.getKeyObjectives().get("Std Weiss amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Std Weiss amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Matt amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Matt amount discrepancy score"));

			if (c.getKeyObjectives().get("Super Glanz amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Super Glanz amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Basis amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Basis amount discrepancy score"));

			results.add(result);
		}

		String finalResult = filename + "\n";
		List<Double> sums = new ArrayList<Double>();
		List<List<Double>> analysableResults = new ArrayList<List<Double>>();
		for (int j = 0; j < results.get(0).size(); j++) {
			analysableResults.add(new ArrayList<Double>());
		}

		for (int j = 0; j < results.size(); j++) {
			double sum = 0;
			Real makespan = (Real) results.get(j).get(0);
			finalResult += makespan.value + " ";
			analysableResults.get(0).add(makespan.value);

			for (int k = 1; k < results.get(j).size() - 1; k++) {
				Int objective = (Int) results.get(j).get(k);
				finalResult += objective.value + " ";
				sum += objective.value;
				analysableResults.get(k).add((double) objective.value);
			}

			Int objective = (Int) results.get(j).get(results.get(j).size() - 1);
			finalResult += objective.value + "\n";

			analysableResults.get(results.get(j).size() - 1).add((double) objective.value);

			sum += objective.value;
			sums.add(sum);
		}
		finalResult += "\n";

		System.out.println(finalResult);

		df = new DecimalFormat("0.00");

		for (int j = 0; j < analysableResults.size(); j++) {
			DoubleSummaryStatistics stats = analysableResults.get(j).stream().mapToDouble((x) -> x).summaryStatistics();
			System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
					+ " Max: " + df.format(stats.getMax()));
		}
		DoubleSummaryStatistics stats = sums.stream().mapToDouble((x) -> x).summaryStatistics();
		System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin()) + " Max: "
				+ df.format(stats.getMax()) + " SUM.");

		System.out.println("Test prove MOEAD success! ");
	}

	private void testNSGA2() {

		final Random rng = new ec.util.MersenneTwister(0xDEADBEEF);
		final int percentageAvailability = 100;
		final ConfigurationType ct = OasHardwiredConfigurationType.hardwiredConfigurationType(percentageAvailability,
				rng);
		final ObjectiveFunction.LocalObjectiveFunction of = new OasObjectiveFunction(
				OasHardwiredConfigurationType.hardwiredCommodities());
		final Configuration config = Utility.randomConfiguration(ct, rng);

		final int urgency = 50;
		final int iterations = 100;
		final int populationSize = 100;

		DecimalFormat df = new DecimalFormat("0.00");

		String filename = "results/OE 0" + " G " + iterations + " P " + populationSize + " F " + df.format(MOEAD_RS.F)
				+ " M " + df.format(MOEAD_RS.M);
		System.out.println("\n" + filename);

		final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config, of, urgency, iterations, populationSize,
				rng, 0);

		jeep.lang.Diag.println("#front: " + or.getReconfigurations().size());
		List<List<Value>> results = new ArrayList<List<Value>>();

		for (Configuration c : or.getReconfigurations()) {
			List<Value> result = new ArrayList<Value>();
			result.add(c.getKeyObjectives().get("makespan"));

			if (c.getKeyObjectives().get("Std Weiss amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Std Weiss amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Matt amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Matt amount discrepancy score"));

			if (c.getKeyObjectives().get("Super Glanz amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Super Glanz amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Basis amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Basis amount discrepancy score"));

			results.add(result);
		}

		String finalResult = filename + "\n";
		List<Double> sums = new ArrayList<Double>();
		List<List<Double>> analysableResults = new ArrayList<List<Double>>();
		for (int j = 0; j < results.get(0).size(); j++) {
			analysableResults.add(new ArrayList<Double>());
		}

		for (int j = 0; j < results.size(); j++) {
			double sum = 0;
			Real makespan = (Real) results.get(j).get(0);
			finalResult += makespan.value + " ";
			analysableResults.get(0).add(makespan.value);

			for (int k = 1; k < results.get(j).size() - 1; k++) {
				Int objective = (Int) results.get(j).get(k);
				finalResult += objective.value + " ";
				sum += objective.value;
				analysableResults.get(k).add((double) objective.value);
			}

			Int objective = (Int) results.get(j).get(results.get(j).size() - 1);
			finalResult += objective.value + "\n";

			analysableResults.get(results.get(j).size() - 1).add((double) objective.value);

			sum += objective.value;
			sums.add(sum);
		}
		finalResult += "\n";

		System.out.println(finalResult);

		df = new DecimalFormat("0.00");

		for (int j = 0; j < analysableResults.size(); j++) {
			DoubleSummaryStatistics stats = analysableResults.get(j).stream().mapToDouble((x) -> x).summaryStatistics();
			System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
					+ " Max: " + df.format(stats.getMax()));
		}
		DoubleSummaryStatistics stats = sums.stream().mapToDouble((x) -> x).summaryStatistics();
		System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin()) + " Max: "
				+ df.format(stats.getMax()) + " SUM.");

		System.out.println("Test prove MOEAD success! ");
	}

	@Test
	public void TestNewMutation() {
		System.out.println("Test Mutation begin");

		MOEAD_RS.enableElitism = false;
		MOEAD_RS.enableImrpove = false;
		MOEAD_RS.enableNewCrossover = false;
		MOEAD_RS.enableNewMutation = true;

		testMOEAD_RS(MOEAD_RS.enableNewCrossover, MOEAD_RS.enableNewMutation, MOEAD_RS.enableImrpove,
				MOEAD_RS.enableElitism);
		testMOEAD();
		System.out.println("Test Mutation ends");
	}

	@Test
	public void TestNewCrossover() {
		System.out.println("Test Crossover begin");

		MOEAD_RS.enableElitism = false;
		MOEAD_RS.enableImrpove = false;
		MOEAD_RS.enableNewCrossover = true;
		MOEAD_RS.enableNewMutation = false;

		testMOEAD_RS(MOEAD_RS.enableNewCrossover, MOEAD_RS.enableNewMutation, MOEAD_RS.enableImrpove,
				MOEAD_RS.enableElitism);
		testMOEAD();
		System.out.println("Test Crossover ends");
	}

	@Test
	public void TestImprove() {
		System.out.println("Test improve begin");

		MOEAD_RS.enableElitism = false;
		MOEAD_RS.enableImrpove = true;
		MOEAD_RS.enableNewCrossover = false;
		MOEAD_RS.enableNewMutation = false;

		testMOEAD_RS(MOEAD_RS.enableNewCrossover, MOEAD_RS.enableNewMutation, MOEAD_RS.enableImrpove,
				MOEAD_RS.enableElitism);
		testMOEAD();
		System.out.println("Test improve ends");
	}

	@Test
	public void TestElitism() {
		System.out.println("Test Elitism begin");

		MOEAD_RS.enableElitism = true;
		MOEAD_RS.enableImrpove = false;
		MOEAD_RS.enableNewCrossover = false;
		MOEAD_RS.enableNewMutation = false;

		// testMOEAD_RS(MOEAD_RS.enableNewCrossover, MOEAD_RS.enableNewMutation,
		// MOEAD_RS.enableImrpove,
		// MOEAD_RS.enableElitism);
		testMOEAD();
		System.out.println("Test Elitism ends");
	}

	@Test
	public void testNewFacilities() {

		OasHardwiredConfigurationType.Number_Of_Mixers = 15;
		OasHardwiredConfigurationType.AmountRequired[0] = 65;
		OasHardwiredConfigurationType.AmountRequired[1] = 60;
		OasHardwiredConfigurationType.AmountRequired[2] = 45;
		OasHardwiredConfigurationType.AmountRequired[3] = 30;

		TestNewCrossover();
		TestNewMutation();
		TestImprove();
		TestElitism();

		OasHardwiredConfigurationType.Number_Of_Mixers = 20;
		OasHardwiredConfigurationType.AmountRequired[0] = 45;
		OasHardwiredConfigurationType.AmountRequired[1] = 40;
		OasHardwiredConfigurationType.AmountRequired[2] = 30;
		OasHardwiredConfigurationType.AmountRequired[3] = 20;

		TestNewCrossover();
		TestNewMutation();
		TestImprove();
		TestElitism();

		OasHardwiredConfigurationType.Number_Of_Mixers = 20;
		OasHardwiredConfigurationType.AmountRequired[0] = 90;
		OasHardwiredConfigurationType.AmountRequired[1] = 80;
		OasHardwiredConfigurationType.AmountRequired[2] = 60;
		OasHardwiredConfigurationType.AmountRequired[3] = 40;

		TestNewCrossover();
		TestNewMutation();
		TestImprove();
		TestElitism();

		OasHardwiredConfigurationType.Number_Of_Mixers = 10;
		OasHardwiredConfigurationType.AmountRequired[0] = 90;
		OasHardwiredConfigurationType.AmountRequired[1] = 80;
		OasHardwiredConfigurationType.AmountRequired[2] = 60;
		OasHardwiredConfigurationType.AmountRequired[3] = 40;

		TestNewCrossover();
		TestNewMutation();
		TestImprove();
		TestElitism();

		OasHardwiredConfigurationType.Number_Of_Mixers = 10;
		OasHardwiredConfigurationType.AmountRequired[0] = 45;
		OasHardwiredConfigurationType.AmountRequired[1] = 40;
		OasHardwiredConfigurationType.AmountRequired[2] = 30;
		OasHardwiredConfigurationType.AmountRequired[3] = 20;

		TestNewCrossover();
		TestNewMutation();
		TestImprove();
		TestElitism();
	}

	@Test
	public void testScale() {
		System.out.println("Test scale begins");

		for (int i = 1; i <= 10; i++) {

			System.out.println("Sacle Factor: " + i);
			OasHardwiredConfigurationType.Number_Of_Mixers = 10 * i;
			OasHardwiredConfigurationType.AmountRequired[0] = 45 * i;
			OasHardwiredConfigurationType.AmountRequired[1] = 40 * i;
			OasHardwiredConfigurationType.AmountRequired[2] = 30 * i;
			OasHardwiredConfigurationType.AmountRequired[3] = 20 * i;

			// testMOEAD_RS(MOEAD_RS.enableNewCrossover, MOEAD_RS.enableNewMutation,
			// MOEAD_RS.enableImrpove,
			// MOEAD_RS.enableElitism);
			//
			System.out.println("Scale Factor: " + i + " Ends.");
			testMOEAD();

		}

		System.out.println("Test scale ends");
	}

	@Test
	public void experimentOneScale() {
		System.out.println("scale begins");

		int i = 1;

		System.out.println("Sacle Factor: " + i);
		OasHardwiredConfigurationType.Number_Of_Mixers = 10 * i;
		OasHardwiredConfigurationType.AmountRequired[0] = 45 * i;
		OasHardwiredConfigurationType.AmountRequired[1] = 40 * i;
		OasHardwiredConfigurationType.AmountRequired[2] = 30 * i;
		OasHardwiredConfigurationType.AmountRequired[3] = 20 * i;

		testNSGA2();

		System.out.println("Scale: " + i + " Ends.");

		System.out.println("Test scale ends");
	}

	@Test
	public void experimentOneScale2() {

		// OasHardwiredConfigurationType.Number_Of_Mixers = 15;
		// OasHardwiredConfigurationType.AmountRequired[0] = 65;
		// OasHardwiredConfigurationType.AmountRequired[1] = 60;
		// OasHardwiredConfigurationType.AmountRequired[2] = 45;
		// OasHardwiredConfigurationType.AmountRequired[3] = 30;
		//
		// testNSGA2();
		// testMOEAD_RS(true, true, false, true);
		//
		// OasHardwiredConfigurationType.Number_Of_Mixers = 20;
		// OasHardwiredConfigurationType.AmountRequired[0] = 45;
		// OasHardwiredConfigurationType.AmountRequired[1] = 40;
		// OasHardwiredConfigurationType.AmountRequired[2] = 30;
		// OasHardwiredConfigurationType.AmountRequired[3] = 20;
		//
		// testNSGA2();
		// testMOEAD_RS(true, true, false, true);

		OasHardwiredConfigurationType.Number_Of_Mixers = 20;
		OasHardwiredConfigurationType.AmountRequired[0] = 90;
		OasHardwiredConfigurationType.AmountRequired[1] = 80;
		OasHardwiredConfigurationType.AmountRequired[2] = 60;
		OasHardwiredConfigurationType.AmountRequired[3] = 40;

		// testNSGA2();
		testMOEAD_RS(true, true, false, true);

		OasHardwiredConfigurationType.Number_Of_Mixers = 10;
		OasHardwiredConfigurationType.AmountRequired[0] = 90;
		OasHardwiredConfigurationType.AmountRequired[1] = 80;
		OasHardwiredConfigurationType.AmountRequired[2] = 60;
		OasHardwiredConfigurationType.AmountRequired[3] = 40;

		testNSGA2();
		testMOEAD_RS(true, true, false, true);
	}

	@Test
	public void CompareNSGAandMOEADandMOEA_RS() {

		final int numOEInvocations = 3;
		for (int i = 0; i < numOEInvocations; ++i) {

			final Random rng = new ec.util.MersenneTwister(0xDEADBEEF);
			final int percentageAvailability = 100;
			final ConfigurationType ct = OasHardwiredConfigurationType
					.hardwiredConfigurationType(percentageAvailability, rng);
			final ObjectiveFunction.LocalObjectiveFunction of = new OasObjectiveFunction(
					OasHardwiredConfigurationType.hardwiredCommodities());
			final Configuration config = Utility.randomConfiguration(ct, rng);

			final int urgency = 50;
			final int iterations = 100;
			final int populationSize = 100;

			System.out.println();

			final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config, of, urgency, iterations,
					populationSize, rng, i);

			jeep.lang.Diag.println("#front: " + or.getReconfigurations().size());

			List<List<Value>> results = new ArrayList<List<Value>>();

			for (Configuration c : or.getReconfigurations()) {
				List<Value> result = new ArrayList<Value>();

				result.add(c.getKeyObjectives().get("makespan"));

				if (c.getKeyObjectives().get("Std Weiss amount discrepancy score") != null)
					result.add(c.getKeyObjectives().get("Std Weiss amount discrepancy score"));

				if (c.getKeyObjectives().get("Weiss Matt amount discrepancy score") != null)
					result.add(c.getKeyObjectives().get("Weiss Matt amount discrepancy score"));

				if (c.getKeyObjectives().get("Super Glanz amount discrepancy score") != null)
					result.add(c.getKeyObjectives().get("Super Glanz amount discrepancy score"));

				if (c.getKeyObjectives().get("Weiss Basis amount discrepancy score") != null)
					result.add(c.getKeyObjectives().get("Weiss Basis amount discrepancy score"));

				results.add(result);
			}
			List<Double> sums = new ArrayList<Double>();
			List<List<Double>> analysableResults = new ArrayList<List<Double>>();
			for (int j = 0; j < results.get(0).size(); j++) {
				analysableResults.add(new ArrayList<Double>());
			}

			String finalResult = "Generations: " + iterations + " Populations: " + populationSize
					+ " Optimization Engine: " + i + "\n";

			for (int j = 0; j < results.size(); j++) {
				double sum = 0;
				Real makespan = (Real) results.get(j).get(0);
				finalResult += makespan.value + " ";
				analysableResults.get(0).add(makespan.value);

				for (int k = 1; k < results.get(j).size() - 1; k++) {
					Int objective = (Int) results.get(j).get(k);
					finalResult += objective.value + " ";
					sum += objective.value;
					analysableResults.get(k).add((double) objective.value);
				}

				Int objective = (Int) results.get(j).get(results.get(j).size() - 1);
				finalResult += objective.value + "\n";

				analysableResults.get(results.get(j).size() - 1).add((double) objective.value);

				sum += objective.value;
				sums.add(sum);
			}
			finalResult += "\n";
			System.out.println(finalResult);

			DecimalFormat df = new DecimalFormat("0.00");

			for (int j = 0; j < analysableResults.size(); j++) {
				DoubleSummaryStatistics stats = analysableResults.get(j).stream().mapToDouble((x) -> x)
						.summaryStatistics();
				System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
						+ " Max: " + df.format(stats.getMax()));
			}
			DoubleSummaryStatistics stats = sums.stream().mapToDouble((x) -> x).summaryStatistics();
			System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
					+ " Max: " + df.format(stats.getMax()) + " SUM.");
		}
	}

	///////////////////////////////

	private void testImpl(ObjectiveFunction.LocalObjectiveFunction of, ConfigurationType ct, Random rng) {

		final Configuration config = Utility.randomConfiguration(ct, rng);
		final int engine = 1;
		final int urgency = 50;
		final int iterations = 50;
		final int populationSize = 100;
		jeep.lang.Diag.println(ct.getControlledMetrics().size() / 2 + " optimisation started: engine: " + engine
				+ " population: " + populationSize + " generation: " + iterations);
		final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config, of, urgency, iterations, populationSize,
				rng, engine);
		jeep.lang.Diag.println("#front: " + or.getReconfigurations().size());

		final Configuration optimisedConfig = or.getReconfigurations().get(0);
		String schedInfo = ConfigurationUtilities.getStatusString(optimisedConfig);
		System.out.println(ConfigurationUtilities.getStatusString(optimisedConfig));

		String[] taskInfo = schedInfo.split("\n");
		String resultsJson = "{\n\n";

		int idCounter = 0;
		List<FormattedResultOAS> res = new ArrayList<FormattedResultOAS>();
		for (String s : taskInfo) {
			String[] toArray = s.split("_");
			if (toArray.length == 12) {
				String year = toArray[0];
				String month = toArray[1];
				String day = toArray[2];
				String startTime = toArray[3];
				String endTime = toArray[4];
				String id = idCounter + "";
				String productName = toArray[5];
				String recipeType = toArray[6];
				String instanceNumber = toArray[7];
				String amountProduced = toArray[8];
				String mixerID = toArray[9];
				String mixerName = toArray[10];
				String mixerStatus = toArray[11];

				FormattedResultOAS oneRes = new FormattedResultOAS(year, month, day, startTime, endTime, id,
						productName, recipeType, instanceNumber, amountProduced, mixerID, mixerName, mixerStatus);
				resultsJson += oneRes.formattedOut(false);
				res.add(oneRes);
				idCounter++;

			}
		}

		resultsJson += "\n}";

		final String configJSON = new uk.ac.york.safire.metrics.JsonConverter().toJson(optimisedConfig);
		final Instant now = Instant.now(); // get time in UTC
		final ProducerRecord<String, String> data = new ProducerRecord<String, String>("OptimisationEngineResult",
				"timestamp: " + now, configJSON);

		List<List<Value>> results = new ArrayList<List<Value>>();

		for (Configuration c : or.getReconfigurations()) {
			List<Value> result = new ArrayList<Value>();

			if (c.getKeyObjectives().get("makespan") != null)
				result.add(c.getKeyObjectives().get("makespan"));

			if (c.getKeyObjectives().get("energy") != null)
				result.add(c.getKeyObjectives().get("energy"));

			if (c.getKeyObjectives().get("montary") != null)
				result.add(c.getKeyObjectives().get("montary"));

			if (c.getKeyObjectives().get("urgency") != null)
				result.add(c.getKeyObjectives().get("urgency"));

			if (c.getKeyObjectives().get("quality") != null)
				result.add(c.getKeyObjectives().get("quality"));

			if (c.getKeyObjectives().get("Std Weiss amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Std Weiss amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Matt amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Matt amount discrepancy score"));

			if (c.getKeyObjectives().get("Super Glanz amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Super Glanz amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Basis amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Basis amount discrepancy score"));

			results.add(result);
		}
		List<List<Double>> analysableResults = new ArrayList<List<Double>>();
		for (int j = 0; j < results.get(0).size(); j++) {
			analysableResults.add(new ArrayList<Double>());
		}

		String finalResult = "Generations: " + iterations + " Populations: " + populationSize + " Optimization Engine: "
				+ 1 + "\n";

		for (int j = 0; j < results.size(); j++) {
			for (int k = 0; k < results.get(j).size(); k++) {
				Real objective = (Real) results.get(j).get(k);
				finalResult += objective.value + " ";
				analysableResults.get(k).add((double) objective.value);
			}

			finalResult += "\n";
		}
		finalResult += "\n";
		System.out.println(finalResult);

		DecimalFormat df = new DecimalFormat("0.00");

		for (int j = 0; j < analysableResults.size(); j++) {
			DoubleSummaryStatistics stats = analysableResults.get(j).stream().mapToDouble((x) -> x).summaryStatistics();
			System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
					+ " Max: " + df.format(stats.getMax()));
		}

		System.out.println(ct.getControlledMetrics().size() / 2 + " All down.");

	}

	@Test
	public void testOASProductionLine() {
		final Random rng = new ec.util.MersenneTwister(0xDEADBEEF);

		int availablePercenetage = 100;

		final ConfigurationType ct = OasProductionLineConfigurationType.hardwiredConfigurationType(rng,
				availablePercenetage);

		Set<String> commdities = OasProductionLineConfigurationType.getCommdities();

		final ObjectiveFunction.LocalObjectiveFunction of = new OasProductionLineObjectiveFunction(
				OasProductionLineConfigurationType.setups, commdities);
		testImpl(of, ct, rng);
	}

	@Test
	public void testONA1() {

		// OnaMultipleObjectiveFunction.testDependentTaskParser();

		// final Random rng = new java.util.Random( 0xDEADBEEF );
		final Random rng = new ec.util.MersenneTwister(0xDEADBEEF); // System.currentTimeMillis()); // 0xDEADBEEF );
		final int percentageAvailability = 100;
		final boolean isMultiobjective = true;
		final ConfigurationType ct = OnaHardwiredConfigurationType.hardwiredConfigurationType(percentageAvailability,
				isMultiobjective, rng);
		// final ConfigurationType ct =
		// OnaHardwiredConfigurationType.hardwiredConfigurationTypeTwoLevel(percentageAvailability,
		// isMultiobjective, rng);

		final ObjectiveFunction.LocalObjectiveFunction of = new OnaMultipleObjectiveFunction();
		jeep.lang.Diag.println(of.getClass().getName());
		testImpl(of, ct, rng);
		jeep.lang.Diag.println("All done.");
	}

	///////////////////////////////

	@Test
	public void testONA2() {

		// OnaMultipleObjectiveFunction.testDependentTaskParser();

		// final Random rng = new java.util.Random( 0xDEADBEEF );
		final Random rng = new ec.util.MersenneTwister(0xDEADBEEF); // System.currentTimeMillis()); // 0xDEADBEEF );
		final int percentageAvailability = 100;
		final boolean isMultiobjective = true;
		// final ConfigurationType ct =
		// OnaHardwiredConfigurationType.hardwiredConfigurationType(percentageAvailability,
		// isMultiobjective, rng);
		// final ConfigurationType ct =
		// OnaHardwiredConfigurationType.hardwiredConfigurationTypeTwoLevel(percentageAvailability,
		// isMultiobjective, rng);

		final int scalingFactor = 5;
		final int numStage1Recipes = 10 * scalingFactor;
		final int numStage2Recipes = 10 * scalingFactor;

		final ConfigurationType ct = OnaHardwiredConfigurationType.scalableHardwiredConfigurationTypeTwoLevel(
				percentageAvailability, isMultiobjective, numStage1Recipes, numStage2Recipes, rng);

		// final ConfigurationType ct =
		// OnaHardwiredConfigurationType.toyConfigurationType(percentageAvailability,
		// isMultiobjective, rng);
		// final ObjectiveFunction.LocalObjectiveFunction of = new
		// OnaSingleObjectiveFunction();
		final ObjectiveFunction.LocalObjectiveFunction of = new OnaMultipleObjectiveFunction();
		jeep.lang.Diag.println(of.getClass().getName());
		testImpl(of, ct, rng);
		jeep.lang.Diag.println("All done.");
	}

	///////////////////////////////

	@Test
	public void testONAPreemptive() {

		// final double longMax = Long.MAX_VALUE;
		// jeep.lang.Diag.println( factorial(21) - longMax );
		// System.exit(0);

		final Random rng = new ec.util.MersenneTwister(0xDEADBEEF); // System.currentTimeMillis()); // 0xDEADBEEF );

		final Pair<Map<String, List<RecipeInfo>>, String[]> recipesAndResources = PreemptiveOnaConfigurationType
				.toyExampleRecipesAndResourceNames();
		final Map<String, List<RecipeInfo>> recipeInfo = recipesAndResources.getLeft();
		final String[] resourceNames = recipesAndResources.getRight();

		final boolean isMultiobjective = true;

		final ConfigurationType ct = PreemptiveOnaConfigurationType.configurationType(recipeInfo, resourceNames,
				isMultiobjective, rng);

		final double density = 1.0;
		final int maxDuration = 1000;
		final int maxCost = 1000;
		final List<RecipeInfo> allRecipes = recipeInfo.values().stream().flatMap(List::stream)
				.collect(Collectors.toList());
		final List<SequenceDependentTaskInfo> sequenceDependentTasks = SequenceDependentTaskInfo
				.makeRandomSequenceDependantTasksOAS(allRecipes, resourceNames, density, maxDuration, maxCost, 1000,
						rng);

		final ObjectiveFunction.LocalObjectiveFunction of = new PreemptionIAObjectiveFunction(sequenceDependentTasks);
		testImpl(of, ct, rng);
	}

	@Test
	public void testONAXMLQuality() {
		int NumberOfTests = 1;
		List<Double> bestMakeSpans1 = new ArrayList<>(NumberOfTests);
		List<Double> bestQuality1 = new ArrayList<>(NumberOfTests);
		bestMakeSpans1.add(-1.0);
		bestQuality1.add(-1.0);

		testONAXML(14, 1, bestMakeSpans1, bestQuality1, 0);
	}

	@Test
	public void testONAXMLScaleDZ() {
		int NumberOfTests = 1;
		List<Double> bestMakeSpans1 = new ArrayList<>(NumberOfTests);
		List<Double> bestQuality1 = new ArrayList<>(NumberOfTests);
		bestMakeSpans1.add(-1.0);
		bestQuality1.add(-1.0);

		int D = 5000;
		for (int i = 0; i < 8; i++) {
			int Z = D + 5000;
			for (int j = 0; j < 3; j++) {
				System.out.println("\n\n\nD: " + D + " Z:" + Z);
				ValueCurve.D = D;
				ValueCurve.Z = Z;
				testONAXML(14, 1, bestMakeSpans1, bestQuality1, 0);
				Z += 5000;
			}
			D += 5000;
		}
	}

	@Test
	public void testONAXMLTestOne() {
		int NumberOfTests = 1;
		List<Double> bestMakeSpans1 = new ArrayList<>(NumberOfTests);
		List<Double> bestQuality1 = new ArrayList<>(NumberOfTests);
		bestMakeSpans1.add(-1.0);
		bestQuality1.add(-1.0);

		ValueCurve.D = 25000;
		ValueCurve.Z = 35000;
		testONAXML(14, 1, bestMakeSpans1, bestQuality1, 0);

	}

	public static void main(String args[]) {
		int NumberOfTests = Integer.parseInt(args[0]) + 7;
		System.out.println("current test: " + NumberOfTests);
		List<Double> bestMakeSpans1 = new ArrayList<>(1);
		List<Double> bestQuality1 = new ArrayList<>(1);

		bestMakeSpans1.add(-1.0);
		bestQuality1.add(-1.0);

		//testONAXML(NumberOfTests, 0, bestMakeSpans1, bestQuality1, 0);
		testONAXML(NumberOfTests, 1, bestMakeSpans1, bestQuality1, 0);
	}

	@Test
	public void testONAXMLAll() {
		int NumberOfTests = 10;
		List<Double> bestMakeSpans1 = new ArrayList<>(NumberOfTests);
		List<Double> bestQuality1 = new ArrayList<>(NumberOfTests);
		List<Double> bestMakeSpans0 = new ArrayList<>(NumberOfTests);
		List<Double> bestQuality0 = new ArrayList<>(NumberOfTests);

		for (int i = 0; i < NumberOfTests; i++) {
			bestMakeSpans1.add(-1.0);
			bestQuality1.add(-1.0);
			bestMakeSpans0.add(-1.0);
			bestQuality0.add(-1.0);
		}

		for (int i = 0; i < NumberOfTests; i++) {
			testONAXML(8 + i, 1, bestMakeSpans1, bestQuality1, i);
		}

		for (int i = 0; i < NumberOfTests; i++) {
			testONAXML(8 + i, 0, bestMakeSpans0, bestQuality0, i);
		}

		System.out.println("makespan and quality");
		System.out.println(bestMakeSpans1);
		System.out.println(bestQuality1);
		System.out.println("makespan only");
		System.out.println(bestMakeSpans0);
		System.out.println(bestQuality0);
	}

	private static void testONAXML(int numbeofParts, int addQuality, List<Double> bestMakeSpans,
			List<Double> bestQuality, int index) {
		ONAXMLWriter.write(numbeofParts, addQuality);
		OnaConfigurationType.NumberOfParts = numbeofParts;
		OnaConfigurationType.addQuality = addQuality;
		OnaConfigurationType.ONAReader = null;

		final Random rng = new ec.util.MersenneTwister(1000); // System.currentTimeMillis()); // 0xDEADBEEF );
		final Pair<Map<String, List<RecipeInfo>>, String[]> recipesAndResources = OnaConfigurationType
				.recipesAndResourceNames();
		final Map<String, List<RecipeInfo>> recipeInfo = recipesAndResources.getLeft();
		final String[] resourceNames = recipesAndResources.getRight();

		final boolean isMultiobjective = true;

		final ConfigurationType ct = OnaConfigurationType.configurationType(recipeInfo, resourceNames, isMultiobjective,
				rng);

		final ObjectiveFunction.LocalObjectiveFunction of = new PreemptionIAObjectiveFunction(
				OnaConfigurationType.getSetUps());
		testImplONAXML(of, ct, rng, numbeofParts, addQuality, bestMakeSpans, bestQuality, index);
	}

	private static void testImplONAXML(ObjectiveFunction.LocalObjectiveFunction of, ConfigurationType ct, Random rng,
			int numbeofParts, int addQuality, List<Double> bestMakeSpans, List<Double> bestQuality, int index) {

		final Configuration config = Utility.randomConfiguration(ct, rng);
		final int engine = 1;
		final int urgency = 50;
		final int iterations = 50;
		final int populationSize = 100;
		jeep.lang.Diag.println(ct.getControlledMetrics().size() / 2 + " optimisation started: engine: " + engine
				+ " population: " + populationSize + " generation: " + iterations);
		final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config, of, urgency, iterations, populationSize,
				rng, engine);
		jeep.lang.Diag.println("#front: " + or.getReconfigurations().size());

		List<List<Value>> results = new ArrayList<List<Value>>();
		for (Configuration c : or.getReconfigurations()) {
			List<Value> result = new ArrayList<Value>();

			if (c.getKeyObjectives().get("makespan") != null)
				result.add(c.getKeyObjectives().get("makespan"));

			if (c.getKeyObjectives().get("energy") != null)
				result.add(c.getKeyObjectives().get("energy"));

			if (c.getKeyObjectives().get("montary") != null)
				result.add(c.getKeyObjectives().get("montary"));

			if (c.getKeyObjectives().get("urgency") != null)
				result.add(c.getKeyObjectives().get("urgency"));

			if (c.getKeyObjectives().get("quality") != null)
				result.add(c.getKeyObjectives().get("quality"));

			if (c.getKeyObjectives().get("Std Weiss amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Std Weiss amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Matt amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Matt amount discrepancy score"));

			if (c.getKeyObjectives().get("Super Glanz amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Super Glanz amount discrepancy score"));

			if (c.getKeyObjectives().get("Weiss Basis amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Weiss Basis amount discrepancy score"));

			results.add(result);
		}
		List<List<Double>> analysableResults = new ArrayList<List<Double>>();
		for (int j = 0; j < results.get(0).size(); j++) {
			analysableResults.add(new ArrayList<Double>());
		}

		String finalResult = "Generations: " + iterations + " Populations: " + populationSize + " Optimization Engine: "
				+ 1 + "\n";

		for (int j = 0; j < results.size(); j++) {
			for (int k = 0; k < results.get(j).size(); k++) {
				Real objective = (Real) results.get(j).get(k);
				finalResult += objective.value + " ";
				analysableResults.get(k).add((double) objective.value);
			}

			finalResult += "\n";
		}
		finalResult += "\n";
		System.out.println(finalResult);

		DecimalFormat df = new DecimalFormat("0.00");

		for (int j = 0; j < analysableResults.size(); j++) {
			DoubleSummaryStatistics stats = analysableResults.get(j).stream().mapToDouble((x) -> x).summaryStatistics();
			System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
					+ " Max: " + df.format(stats.getMax()));
		}

		ArrayList<Double> qualities = new ArrayList<>();
		ArrayList<Double> makespans = new ArrayList<>();

		String out = "ONA " + numbeofParts + " " + addQuality + "\n";
		System.out.println("\n\n\nresult pair: ");
		out += "result pair: \n";
		for (int i = 0; i < or.getReconfigurations().size(); i++) {
			Configuration configResult = or.getReconfigurations().get(i);
			String res = ConfigurationUtilities.getStatusString(configResult);
			String[] timesInString = res.split("\\n")[1].split("_")[0].split(" ");
			List<Integer> times = new ArrayList<>();
			for (int j = 0; j < timesInString.length; j++) {
				times.add(Integer.parseInt(timesInString[j]));
			}

			Real makespan = (Real) configResult.getKeyObjectives().get("makespan");
			makespans.add((double) makespan.value);

			double qua = Double.parseDouble(df.format(numbeofParts - ValueCurve.getQualityExact(times, numbeofParts)));
			qualities.add(qua);
			String partsProduced = res.split("\\n")[1].split("_")[1];
			System.out.println((double) makespan.value + " " + qua + " " + partsProduced);
			out += (double) makespan.value + " " + qua + " " + partsProduced + "\n";
		}

		for (int i = 0; i < or.getReconfigurations().size(); i++) {
			Configuration configResult = or.getReconfigurations().get(i);
			String res = ConfigurationUtilities.getStatusString(configResult);
			String[] timesInString = res.split("\\n")[1].split("_")[0].split(" ");
			List<Integer> times = new ArrayList<>();
			for (int j = 0; j < timesInString.length; j++) {
				times.add(Integer.parseInt(timesInString[j]));
			}

			// System.out.println((double) makespan.value + " " + qua + " " +
			// partsProduced);
			System.out.println(Arrays.toString(timesInString));
			List<Double> values = ValueCurve.getQualityList(times);
			System.out.println(values);

			out += Arrays.toString(timesInString) + "\n";
			out += values.toString() + "\n";
		}

		makespans.sort((c1, c2) -> Double.compare(c1, c2));
		qualities.sort((c1, c2) -> Double.compare(c1, c2));

		System.out.println("makespan sorted");
		out += "\n\nmakespan sorted\n";
		for (int i = 0; i < makespans.size(); i++) {
			System.out.println(makespans.get(i));
			out += makespans.get(i) + "\n";
		}

		System.out.println("qualities sorted");
		out += "\n\nqualities sorted\n";
		for (int i = 0; i < qualities.size(); i++) {
			System.out.println(qualities.get(i));
			out += qualities.get(i) + "\n";
		}

		bestMakeSpans.set(index, makespans.get(0));
		bestQuality.set(index, qualities.get(0));

		System.out.println(ct.getControlledMetrics().size() / 2 + " All down.");

		UoYEarlyPrototypeDemo
				.writeSystem("ONA" + numbeofParts + "_" + addQuality + "_" + ValueCurve.D + "-" + ValueCurve.Z, out);
	}

	@Test
	public void getValueForSingleObjective() {
		List<Integer> times = new ArrayList<>();
		times.add(3249);
		times.add(5505);
		times.add(6237);
		times.add(9235);
		times.add(10845);
		times.add(12133);
		times.add(14806);
		times.add(16195);
		times.add(16837);
		times.add(21545);
		times.add(27050);
		times.add(32390);
		times.add(37895);
		times.add(43235);
		DecimalFormat df = new DecimalFormat("0.00");
		int D = 10000;
		for (int i = 0; i < 7; i++) {
			int Z = D + 5000;
			for (int j = 0; j < 3; j++) {
				// System.out.println("\n\n\nD: " + D + " Z:" + Z);
				ValueCurve.D = D;
				ValueCurve.Z = Z;

				System.out.println(Double.parseDouble(df.format(14 - ValueCurve.getQualityExact(times, 14))));
				Z += 5000;
			}
			D += 5000;
		}
	}

	@Test
	public void testElectrolux() {
		final Random rng = new ec.util.MersenneTwister(0xDEADBEEF);

		int availablePercenetage = 100;

		final ConfigurationType ct = ElectroluxConfigurationType.hardwiredConfigurationType(rng, availablePercenetage);

		Set<String> commdities = ElectroluxConfigurationType.getCommdities();

		final ObjectiveFunction.LocalObjectiveFunction of = new ElectroluxObjectiveFunction(
				ElectroluxConfigurationType.setups, commdities);

		testElectroluxImpl(of, ct, rng);
	}

	private void testElectroluxImpl(ObjectiveFunction.LocalObjectiveFunction of, ConfigurationType ct, Random rng) {

		final Configuration config = Utility.randomConfiguration(ct, rng);
		final int engine = 1;
		final int urgency = 50;

		final int iterations = 100;
		final int populationSize = 50;

		jeep.lang.Diag.println(ct.getControlledMetrics().size() / 2 + " optimisation started: engine: " + engine
				+ " population: " + populationSize + " generation: " + iterations);
		final OptimisationResult or = UoYEarlyPrototypeDemo.invokeOE(config, of, urgency, iterations, populationSize,
				rng, engine);
		jeep.lang.Diag.println("#front: " + or.getReconfigurations().size());

		List<List<Value>> results = new ArrayList<List<Value>>();

		for (Configuration c : or.getReconfigurations()) {
			List<Value> result = new ArrayList<Value>();

			if (c.getKeyObjectives().get("makespan") != null)
				result.add(c.getKeyObjectives().get("makespan"));

			if (c.getKeyObjectives().get("energy") != null)
				result.add(c.getKeyObjectives().get("energy"));

			if (c.getKeyObjectives().get("montary") != null)
				result.add(c.getKeyObjectives().get("montary"));

			if (c.getKeyObjectives().get("urgency") != null)
				result.add(c.getKeyObjectives().get("urgency"));

			if (c.getKeyObjectives().get("quality") != null)
				result.add(c.getKeyObjectives().get("quality"));

			if (c.getKeyObjectives().get("Boiled Water amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Boiled Water amount discrepancy score"));

			if (c.getKeyObjectives().get("Pasta amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Pasta amount discrepancy score"));

			if (c.getKeyObjectives().get("Rice amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Rice amount discrepancy score"));

			if (c.getKeyObjectives().get("Beef amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Beef amount discrepancy score"));

			if (c.getKeyObjectives().get("Potato amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Potato amount discrepancy score"));

			if (c.getKeyObjectives().get("Mushroom amount discrepancy score") != null)
				result.add(c.getKeyObjectives().get("Mushroom amount discrepancy score"));

			results.add(result);
		}
		List<List<Double>> analysableResults = new ArrayList<List<Double>>();
		for (int j = 0; j < results.get(0).size(); j++) {
			analysableResults.add(new ArrayList<Double>());
		}

		String finalResult = "Generations: " + iterations + " Populations: " + populationSize + " Optimization Engine: "
				+ 1 + "\n";

		for (int j = 0; j < results.size(); j++) {
			for (int k = 0; k < results.get(j).size(); k++) {
				Real objective = (Real) results.get(j).get(k);
				finalResult += objective.value + " ";
				analysableResults.get(k).add((double) objective.value);
			}

			finalResult += "\n";
		}
		finalResult += "\n";
		System.out.println(finalResult);

		DecimalFormat df = new DecimalFormat("0.00");

		for (int j = 0; j < analysableResults.size(); j++) {
			DoubleSummaryStatistics stats = analysableResults.get(j).stream().mapToDouble((x) -> x).summaryStatistics();
			System.out.println("Average: " + df.format(stats.getAverage()) + " Min: " + df.format(stats.getMin())
					+ " Max: " + df.format(stats.getMax()));
		}

		System.out.println(ct.getControlledMetrics().size() / 2 + " All down.");

	}

}

// End ///////////////////////////////////////////////////////////////