package uk.ac.york.safire.optimisation.ia;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.distribution.NormalDistribution;

public class NormalDistributionTest {

    

	public static void main(String args[]) {
		NormalDistribution d = new NormalDistribution(1000, 500);
		
		System.out.println("mean: "+d.getMean());
		System.out.println("st: "+d.getStandardDeviation());
		


		
		List<Double> res = new ArrayList<>();
		
		for(int i=0; i<100;i++)
			res.add(d.sample());
		
		System.out.println(res);
		
		
	
		
		 // Problem 1; µ = 1000; σ = 100
		
		// Random ram = new Random(1);
		//
		// List<Double> finishes = new ArrayList<>();
		// List<Double> qualities = new ArrayList<>();
		//
		// for (int i = 0; i < 10000; i++) {
		// double finishTime = ram.nextInt(2000);
		// finishes.add(finishTime);
		// }
		//
		// finishes.sort((c1, c2) -> Double.compare(c1, c2));
		//
		// for (int i = 0; i < finishes.size(); i++) {
		// double quality = getQuality(finishes.get(i), 1000);
		// qualities.add(quality);
		// }
		//
		// for (int i = 0; i < finishes.size(); i++) {
		// System.out.print(finishes.get(i) + " " + qualities.get(i));
		// System.out.println();
		// }

	}

	public static double getQuality(double finishTime, double deadline) {
		double standardD = 10.0;
		double unacceptableAfter = 30;
		double worstQuality = 0;

		if (deadline + unacceptableAfter - finishTime <= 0) {
			return worstQuality;
		} else {
			NormalDistribution d = new NormalDistribution(deadline, standardD);
			return d.probability(finishTime);
		}

	}
}
