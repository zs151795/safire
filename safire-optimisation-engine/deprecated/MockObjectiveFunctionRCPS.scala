package uk.ac.york.safire.optimisation

import uk.ac.york.safire.metrics._
import uk.ac.york.safire.optimisation.maxplusplant._

import rcps._

import org.uma.jmetal.solution.PermutationSolution
import org.uma.jmetal.solution.impl._
import org.uma.jmetal.problem.impl._

import scarlet.math.{ ClosedOpenInterval => Interval }

import scala.collection.JavaConversions._

///////////////////////////////////

class MockObjectiveFunctionRCPS(scaleFactor: Int, rng: java.util.Random) extends ObjectiveFunction.LocalObjectiveFunction {
  require(scaleFactor >= 1)

  import spire.algebra._
  import spire.implicits._

  /////////////////////////////////

  val numActivities = 24
  val numResources = 12

  val rcps = RCPS(scaleFactor * numActivities, scaleFactor * numResources,
    totalDuration = 10,
    resourceCapacity = Interval(100, 120),
    dependencyDensity = 0.07,
    activityDurationPercent = Interval(1, 10),
    resourceDemand = Interval(1, 10),
    rng)

  val configurationType = MockObjectiveFunctionRCPS.configurationType(rcps)

  val jMetalProblem = new JMetalRCPSProblem(rcps)

  ///////////////////////////////

  override def numObjectives(): Int = 1

  override def predictKeyObjectives(
    current: Configuration,
    controlledMetrics: java.util.Map[String, Value]
  ): ObjectiveFunction.Result = {

    val fitness = jMetalProblem.evaluateImpl(
      MockObjectiveFunctionRCPS.controlledMetricsToJMetalSolution(current.getControlledMetrics.toMap, jMetalProblem)
    )
    val objectiveValues = List[java.lang.Double](fitness)
    val keyObjectives: Map[String, Value] = ???
    new ObjectiveFunction.Result(keyObjectives, "ObjectiveFunction status: OK")
  }

  override def objectiveValues(result: ObjectiveFunction.Result): java.util.List[java.lang.Double] = {
    ???
  } // ensuring { _.length == numObjectives() }

}

///////////////////////////////////

object MockObjectiveFunctionRCPS {

  val DefaultScaleFactor = 1
  val DefaultSeed = 0xDEADBEEF

  def apply(): MockObjectiveFunctionRCPS =
    new MockObjectiveFunctionRCPS(DefaultScaleFactor, new java.util.Random(DefaultSeed))

  /////////////////////////////////

  def isPerm(x: Seq[Int]) =
    x.toSet.size == x.size && x.forall { e => e >= 0 && e < x.size }

  def permutationToControlledMetrics(perm: Seq[Int]): Map[String, Value] = {
    require(isPerm(perm))
    val typ = ValueType.intType(0, perm.length - 1)
    Map() ++ perm.zipWithIndex.map { case (from, to) => from.toString -> Value.intValue(to, typ) }
  }

  def jMetalSolutionToControlledMetrics(x: PermutationSolution[Integer]): Map[String, Value] = {
    val asPerm = (0 until x.getNumberOfVariables).map { i => x.getVariableValue(i).toInt }
    permutationToControlledMetrics(asPerm)
  }

  def controlledMetricsToJMetalSolution(x: Map[String, Value], problem: AbstractIntegerPermutationProblem): PermutationSolution[Integer] = {
    val visitor = new ValueVisitor.NumberVisitor()
    val result = new DefaultIntegerPermutationSolution(problem)
    x.keys.foreach { k =>
      result.setVariableValue(k.toInt, visitor.visit(x(k)).intValue())
    }
    result
  }

  /////////////////////////////////

  def configurationType(rcps: RCPS): ConfigurationType = {
    val controlledMetrics = (0 until rcps.numActivities).map { i =>
      new ControlledMetricType(s"$i", ValueType.intType(0, rcps.numActivities), "Permutation")
    }

    val keyObjectiveMetrics = List(
      new KeyObjectiveType(
        "makespan",
        ValueType.realType(0.0, 100000), "sec", SearchDirection.MINIMIZING
      )
    )

    val observableMetrics = List.empty[ObservableMetricType]

    new ConfigurationType.Explicit(keyObjectiveMetrics, controlledMetrics.toList, observableMetrics)
  }
}

// End ///////////////////////////////////////////////////////////////
