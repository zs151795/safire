package uk.ac.york.safire

import java.io.ObjectInputStream;

import org.mitlware.solution.bitvector.BitVector;
import scala.collection.immutable.List;

import akka.http.scaladsl.marshallers.xml.ScalaXmlSupport.defaultNodeSeqMarshaller
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json._

import akka.http.scaladsl.server.{ HttpApp, Route }
import akka.http.scaladsl.server.PathMatchers

/**
 * Server will be started calling `WebServerHttpApp.startServer("localhost", 8080)`
 * and it will be shutdown after pressing return.
 */

///////////////////////////////////

trait BitVectorJsonProtocol extends DefaultJsonProtocol {

  implicit object BitVectorFormat extends RootJsonFormat[BitVector] {

    def toVector(bv: BitVector): Vector[JsBoolean] =
      (0 until bv.length()).toVector.map { i => JsBoolean(bv.get(i)) }

    override def write(bv: BitVector): JsValue =
      JsArray(toVector(bv))

    override def read(value: JsValue): BitVector = value match {
      case JsArray(v: Vector[JsValue]) =>
        new BitVector(v.toArray.map { _.convertTo[Boolean] })
      case _ => deserializationError("BitVector expected")
    }
  }

  /**
   * ***
   * implicit object ListOfBitVectorFormat extends RootJsonFormat[List[BitVector]] {
   *
   * override def write(x: List[BitVector]): JsValue =
   * JsArray(x.map { BitVectorFormat.write(_) })
   *
   * override def read(value: JsValue): List[BitVector] = value match {
   * case JsArray(v: Vector[JsValue]) =>
   * v.toList.map { BitVectorFormat.read(_) }
   * case _ => deserializationError("List[BitVector] expected")
   * }
   * }
   * ***
   */
}

///////////////////////////////////

object PerturbBitVectorServerHttpApp extends HttpApp with BitVectorJsonProtocol with App {

  //  import BitVectorJsonProtocol._

  // Routes that this WebServer must handle are defined here
  // Please note this method was named `route` in versions prior to 10.0.7
  override def routes: Route =
    pathEndOrSingleSlash { // Listens to the top `/`
      complete("Server up and running") // Completes with some text
    } ~
      path("signature") { // Listens to paths that are exactly `/signature`
        get {
          complete(<xml><args>scala.collection.immutable.List[org.mitlware.solution.bitvector.BitVector]</args><result>scala.collection.immutable.List[org.mitlware.solution.bitvector.BitVector]</result></xml>)
          // complete(<xml><args>java.lang.Integer</args><result>java.lang.Integer</result></xml>)
        }
      } ~
      path("apply" / Remaining) { remaining =>
        post {
          //          val objIn = new ObjectInputStream(new java.io.ByteArrayInputStream(remaining.getBytes(java.nio.charset.StandardCharsets.UTF_8)))
          //          val arg = objIn.readObject().asInstanceOf[List[BitVector]]
          try {
            entity(as[List[BitVector]]) { pop =>
              val result = pop.map { bv => val result = bv.clone(); result.not(); result }
              // complete(<xml><result>{ result }</result></xml>)
              complete(result)
            }
          } catch {
            case t: Throwable => complete(t.getMessage())
          }
        }
      }

  // https://stackoverflow.com/questions/14969461/serializing-over-http-correct-way-to-convert-object
  //ObjectInputStream objIn = new ObjectInputStream(conn.getInputStream());
  //        reply = objIn.readObject();
  //        objIn.close();

  // This will start the server until the return key is pressed
  startServer("0.0.0.0", 8080)
}

// End ///////////////////////////////////////////////////////////////
