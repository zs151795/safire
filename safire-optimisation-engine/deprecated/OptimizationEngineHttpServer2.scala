package uk.ac.york.safire

import java.io.IOException
import java.io.OutputStream
import java.net.InetSocketAddress

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import com.sun.net.httpserver.HttpServer

import ac.uk.york.safire.metrics._

///////////////////////////////////

object OptimizationEngineHttpServer2 {

  def convertStreamToString(is: java.io.InputStream): String = {
    val s = new java.util.Scanner(is).useDelimiter("\\A")
    if (s.hasNext()) s.next() else ""
  }

  /////////////////////////////////

  class MyHandler extends HttpHandler {

    override def handle(t: HttpExchange): Unit = {

      try {
        org.mitlware.Diag.println()

        val requestStr = convertStreamToString(t.getRequestBody())

        org.mitlware.Diag.println(s"request: $requestStr")

        val jsonConverter = new JsonConverter()
        val config = jsonConverter.fromJson(requestStr, classOf[Configuration])

        val controlledMetrics = config.getControlledMetrics()
        val responseStr = jsonConverter.toJson(controlledMetrics)

        org.mitlware.Diag.println(s"response: $responseStr")

        // val response = "This is the response"
        t.sendResponseHeaders(200, responseStr.length())
        val os = t.getResponseBody()
        os.write(responseStr.getBytes())
        os.close()
      } catch {
        case ex: Throwable => {
          val responseStr = ex.getMessage()
          t.sendResponseHeaders(500, responseStr.length())
          val os = t.getResponseBody()
          os.write(responseStr.getBytes())
          os.close()
        }
      }
    }
  }

  /////////////////////////////////

  def main(args: Array[String]): Unit = {
    val addr = new InetSocketAddress("localhost", 8101)
    val server = HttpServer.create(addr, 0)
    server.createContext("/apply/", new MyHandler())
    server.setExecutor(null) // creates a default executor
    println(s"Server up and running at ${addr}")
    server.start()
  }
}

// End ///////////////////////////////////////////////////////////////
