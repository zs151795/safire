package uk.ac.york.safire.optimisation.mitm.atb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.math3.util.Pair;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;

import uk.ac.york.safire.metrics.Configuration;
import uk.ac.york.safire.metrics.ConfigurationType;
import uk.ac.york.safire.metrics.ControlledMetricType;
import uk.ac.york.safire.metrics.KeyObjectiveType;
import uk.ac.york.safire.metrics.ObservableMetricType;
import uk.ac.york.safire.metrics.OptimisationArguments;
import uk.ac.york.safire.metrics.OptimisationResult;
import uk.ac.york.safire.metrics.SampleRate;
import uk.ac.york.safire.metrics.SearchDirection;
import uk.ac.york.safire.metrics.Utility;
import uk.ac.york.safire.metrics.Value;
import uk.ac.york.safire.metrics.ValueType;
import uk.ac.york.safire.optimisation.OptimisationEngine;

///////////////////////////////////

/**
 * From:
 * https://docs.hortonworks.com/HDPDocuments/HDP2/HDP-2.6.4/bk_kafka-component-guide/content/ch_kafka-development.html
 */

public final class MitMConsumer {
	
	private final Map< String, Boolean > availability = new HashMap<>(); 
	
	///////////////////////////////
	
	public MitMConsumer() {}
	
	///////////////////////////////
	
	private static Optional< Pair< String, Boolean > > 
	parseTopicString(String topicString) {
		final Map< String, String > m = new uk.ac.york.safire.metrics.JsonConverter().fromJson(topicString, Map.class);
		if( m.entrySet().size() != 1 ) {
			return Optional.empty();
		}
		else {
			final Map.Entry< String, String > e = m.entrySet().iterator().next();
			final Boolean available = Integer.valueOf( e.getValue() ) == 0 ? false : true;
			return Optional.of( Pair.create( e.getKey(), available ) );
		}
	}
	
	private final static Configuration 
	updateAvailabilityObservableMetric(Configuration config, String resourceName, boolean availability ) {
		
		final Map< String, Value > updatedObservableMetrics = new HashMap<>(); 
		updatedObservableMetrics.putAll( config.getObservableMetrics() );
		updatedObservableMetrics.put( resourceName + " availability", Value.intValue(availability ? 1 : 0, ValueType.intType(0, 1)));
		return new Configuration( 
				config.getConfigurationType(), 
				config.getControlledMetrics(),
				updatedObservableMetrics,
				config.getKeyObjectives() );
	}
	
	///////////////////////////////
	
	private void run(KafkaConsumer<String, String> consumer, Configuration config, OptimisationEngine oe) {
       while ( true ) {
    	   final ConsumerRecords<String, String> records = consumer.poll(1000);
    	   for( ConsumerRecord<String, String> record : records ) {
    		   
    		   System.out.printf("Received Message topic =%s, partition =%s, offset = %d, key = %s, value = %s\n", record.topic(), record.partition(), record.offset(), record.key(), record.value());
    		   
    		   final Optional< Pair< String, Boolean > > opt = parseTopicString( record.value() );
    		   if( !opt.isPresent() ) {
    			   System.err.printf("WARNING: topic string <<%s>> not in expected format\n", record.value() );    			   
    		   }
    		   else {
    		   
    			   // Add topic contents to availability map:
    			   availability.put( opt.get().getKey(), opt.get().getValue() );
    			   
    			   // final Predicate< String > isResourceAvailable = (String resourceName) -> availability.getOrDefault(resourceName, true ); 
    			   // final Configuration configWithAvailabilityObservables = addObservableMetrics(config, isResourceAvailable );
    			   final Configuration updatedConfig = updateAvailabilityObservableMetric( config, opt.get().getKey(), opt.get().getValue() ); 
    			   final OptimisationResult result = oe.optimise(new OptimisationArguments( Collections.singletonList(updatedConfig), 0.5, 0.5 ) );
    			   System.out.println( "optmised:" + result );
    		   }
           }

           consumer.commitSync();
       }
   }
   
	///////////////////////////////   

	static class RecipeInfo {
		final String name;
		final List< Integer > compatibleResources;
		final int instances;		
		// final int commodityProduced; 
		// final int executionTime;	
		
		///////////////////////////
		
		public RecipeInfo(
			String name,
			int instances,			
			List< Integer > compatibleResources
			// , int commodityProduced, int executionTime
			) {
			this.name = name;
			this.instances = instances;
			this.compatibleResources = compatibleResources;
			// this.commodityProduced = commodityProduced; 
			// this.executionTime = executionTime;			
		}
		
		@Override
		public String toString() { 
			return org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE); 
		}
	}
	
	///////////////////////////
	
	private static Configuration makeConfig(Map< String, List<RecipeInfo> > recipeInfo, String [] resourceNames ) {
		
		final int MAX_INT = Integer.MAX_VALUE - 1; 
		
		final List< KeyObjectiveType > keyObjectiveTypes = new ArrayList<>();
		keyObjectiveTypes.add( new KeyObjectiveType("makespan", ValueType.realType(0, Double.MAX_VALUE), "n/a", SearchDirection.MINIMIZING ) );
		
		final List< ControlledMetricType > controlledMetricTypes = new ArrayList<>();
		final List< ObservableMetricType > observableMetricTypes = new ArrayList<>();

		// 1. Observable metric for availability of each resource:
		for( String resourceName: resourceNames ) {
			// e.g. Mixer 1 availability (int type, domain: 0,1)
			observableMetricTypes.add( new ObservableMetricType(resourceName + " availability", ValueType.intType(0, 1), "n/a", SampleRate.eventDriven ) );
		}
		
		int priorityHackIndex_FIXME = 1;
		
		for( Map.Entry<String, List<RecipeInfo> > e: recipeInfo.entrySet() ) {
			for( RecipeInfo r: e.getValue() ) {
				
				for( int i=0; i<r.instances; ++i ) {
					final String metricNamePrefix = r.name + " " + i;

					// 2. Controlled metrics for allocation and for priority:
					
					final ValueType allocationValueType = ValueType.nominalType( metricNamePrefix + " allocation type ", r.compatibleResources.stream().map( index -> resourceNames[ index ] ).toArray(String[]::new) );
					// e.g. Std Weiss A 1 allocation (nominal type, domain: Mixer 1, Mixer 2, Mixer 3, Mixer 4, Mixer 5} :
					controlledMetricTypes.add( new ControlledMetricType(metricNamePrefix + " allocation", allocationValueType, "n/a") );

					// e.g. Std Weiss A 1 priority (Int type)					
					controlledMetricTypes.add( new ControlledMetricType(metricNamePrefix + " priority",
							ValueType.intType(2 * priorityHackIndex_FIXME, 2 * priorityHackIndex_FIXME + 1), "n/a") );							
							// ValueType.intType(0, MAX_INT), "n/a") );
					
					priorityHackIndex_FIXME += 1;
					
					// 3. Observable metric for start and end time of each recipe instance:
					observableMetricTypes.add( new ObservableMetricType(metricNamePrefix + " start", 
							ValueType.intType(0, MAX_INT), "n/a", SampleRate.eventDriven ) );
					observableMetricTypes.add( new ObservableMetricType(metricNamePrefix + " end", 
							ValueType.intType(0, MAX_INT), "n/a", SampleRate.eventDriven ) );					
				}
			}
		}

		final ConfigurationType ct = new ConfigurationType.Explicit(keyObjectiveTypes, controlledMetricTypes, observableMetricTypes );
		return Utility.randomConfiguration(ct, new Random(0xDEADBEEF) );
	}

	///////////////////////////////
	
	public static void main(String[] args) {

		final String [] resourceNames = { 
			"Mixer 1", "Mixer 2", "Mixer 3", "Mixer 4", "Mixer 5", 
			"Mixer 6", "Mixer 7", "Mixer 8", "Mixer 9", "Mixer 10" 
		};
		
	   final Map< String, List< RecipeInfo > > recipeInfo = new HashMap<>();
	   recipeInfo.put( "Std Weiss", Lists.newArrayList( 
	   			new RecipeInfo( "Std Weiss A", 2, Ints.asList(0,1,2,3,4) ), // , 5000, 90 ),
	   			new RecipeInfo( "Std Weiss B", 1, Ints.asList(6,7) ) // 10000, 60 ),	   			
//	   			new RecipeInfo( "Std Weiss A", 9, Ints.asList(0,1,2,3,4) ), // , 5000, 90 ),
//	   			new RecipeInfo( "Std Weiss B", 2, Ints.asList(6,7) ), // 10000, 60 ),	   
//	   			new RecipeInfo( "Std Weiss C", 3, Ints.asList(8) ), //  10000, 45 ),
//	   			new RecipeInfo( "Std Weiss D", 4 , Ints.asList(9) ) //  10000, 45 ) ) 
	   			) );	   

	   recipeInfo.put( "Weiss Matt", Lists.newArrayList(	   
			   new RecipeInfo( "Weiss Matt A", 1, Ints.asList(0, 1,2,3,4) ) //  5000, 90 ),			   
//			   new RecipeInfo( "Weiss Matt A", 11, Ints.asList(0, 1,2,3,4) ), //  5000, 90 ),
//			   new RecipeInfo( "Weiss Matt B", 4, Ints.asList(6,7) ), //  10000, 60 ),	   
//			   new RecipeInfo( "Weiss Matt C", 5, Ints.asList(8) ), //  10000, 45 ),
//			   new RecipeInfo( "Weiss Matt D", 2, Ints.asList(9) ) // , 10000, 45 ) ) 
	   			) );	   

	   recipeInfo.put( "Super Glanz", Lists.newArrayList(
			   new RecipeInfo( "Super Glanz A", 1, Ints.asList(0,1,2,3,4) ) // , 4000, 120 ),			   
//			   new RecipeInfo( "Super Glanz A", 2, Ints.asList(0,1,2,3,4) ), // , 4000, 120 ),
//			   new RecipeInfo( "Super Glanz B", 4, Ints.asList(6,7) ), // , 8000, 90 ),	   
//			   new RecipeInfo( "Super Glanz C", 2, Ints.asList(8) ), // , 8000, 60 ),
//			   new RecipeInfo( "Super Glanz D", 0, Ints.asList(9) ) // , 10000, 60 ) ) 
	   			) );	   

	   recipeInfo.put( "Weiss Basis", Lists.newArrayList(
			   new RecipeInfo( "Weiss Basis A", 1, Ints.asList(0,1,2,3,4) ) // , 6000, 60 ),			   
//			   new RecipeInfo( "Weiss Basis A", 11, Ints.asList(0,1,2,3,4) ), // , 6000, 60 ),
//			   new RecipeInfo( "Weiss Basis B", 3, Ints.asList(6,7) ), // , 12000, 45 ),	   
//			   new RecipeInfo( "Weiss Basis C", 1, Ints.asList(8) ), // , 12000, 30 ),
//			   new RecipeInfo( "Weiss Basis D", 1, Ints.asList(9) ) // , 12000, 30 ) ) );
	   			) );			   
	
	   Configuration config = makeConfig( recipeInfo, resourceNames );
	   System.out.println( config );
	   
	   ////////////////////////////

	   /***************
       Properties consumerConfig = new Properties();
       // consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:2181");
       consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
       consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "my-group");
       consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
       consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName() );
       consumerConfig.put( ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName() );

       KafkaConsumer<String, String> consumer = new KafkaConsumer<>(consumerConfig);
       System.out.println( "topics: " + consumer.listTopics() );
       consumer.subscribe(Collections.singletonList("ResourceAvailability") );
	   ***************/
	   
       ////////////////////////////

       final OptimisationEngine oe = new OptimisationEngine.DoNothingOptimisationEngine();
       // final OptimisationEngine oe = new RandomSearchLocalOptimisationEngine();
       // TODO: add effective optimisation engine
       final IAObjectiveFunction of = new IAObjectiveFunction();
       
       config = IAObjectiveFunction.makeAllResourcesAvailabile(config);
       
       jeep.lang.Diag.println( IAObjectiveFunction.reverseEngineerRecipesFromConfig(config) );
       final Pair< Configuration, List< Double > > result = of.evaluate(config, config.getControlledMetrics() ); 
       jeep.lang.Diag.println( "result: " + result.getSecond() );
       
       jeep.lang.Diag.println( "status: " + IAObjectiveFunction.stringValue( 
           result.getFirst().getObservableMetrics().get("Optimisation Result") ) );       

       // TODO:
       // add observable output for these + error status:
//		observableMetricTypes.add( new ObservableMetricType(metricNamePrefix + " start", 
//				ValueType.intType(0, MAX_INT), "n/a", SampleRate.eventDriven ) );
//		observableMetricTypes.add( new ObservableMetricType(metricNamePrefix + " end", 
//				ValueType.intType(0, MAX_INT), "n/a", SampleRate.eventDriven ) );					
//       System.exit(0);
       
//       final MitMConsumer mitmConsumer = new MitMConsumer();  
//       mitmConsumer.run(consumer, config, oe );
//       consumer.close();
   }
}

// End ///////////////////////////////////////////////////////////////
