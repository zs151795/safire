package uk.ac.york.safire.optimisation.mitm.atb;

import java.io.File;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;

public final class TestDockerClient {

	public static void main(String [] args) {
		
		final String certificatesPath = System.getProperty("user.dir") + File.separatorChar + 
			"resources" + File.separatorChar + "atb-docker-certs";
		
		final DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
			 // .withDockerHost("tcp://my-docker-host.tld:2376")
			.withDockerHost("tcp://dockerbuntu.atb-bremen.de:2376") 
		    .withDockerTlsVerify(true)
		    // .withDockerCertPath("/home/user/.docker/certs")
		    .withDockerCertPath( certificatesPath )
		    //.withDockerConfig("/home/user/.docker")
		    // .withApiVersion("1.23")
		    //.withRegistryUrl("https://index.docker.io/v1/")
		    //.withRegistryUsername("dockeruser")
		    //.withRegistryPassword("ilovedocker")
		    //.withRegistryEmail("dockeruser@github.com")
		    .build();
		final DockerClient docker = DockerClientBuilder.getInstance(config).build();
		System.out.println( docker.versionCmd() );
		System.out.println( "All done" );		
	}
}

// End ///////////////////////////////////////////////////////////////

