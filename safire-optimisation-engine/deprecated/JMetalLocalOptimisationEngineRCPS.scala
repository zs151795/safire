package uk.ac.york.safire.optimisation

import org.apache.commons.lang3.ArrayUtils

import org.uma.jmetal.algorithm._
import org.uma.jmetal.algorithm.impl._
import org.uma.jmetal.algorithm.singleobjective.geneticalgorithm._
import org.uma.jmetal.operator._
import org.uma.jmetal.operator.impl.crossover._
import org.uma.jmetal.operator.impl.mutation._
import org.uma.jmetal.operator.impl.selection._
import org.uma.jmetal.problem._
import org.uma.jmetal.solution._
import org.uma.jmetal.solution.impl._
import org.uma.jmetal.util.AlgorithmRunner
import org.uma.jmetal.util.evaluator._
import org.uma.jmetal.util.evaluator.impl._
import org.uma.jmetal.util._

import org.uma.jmetal.algorithm.Algorithm
import org.uma.jmetal.algorithm.singleobjective.geneticalgorithm.GeneticAlgorithmBuilder
import org.uma.jmetal.operator.CrossoverOperator
import org.uma.jmetal.operator.MutationOperator
import org.uma.jmetal.operator.SelectionOperator
import org.uma.jmetal.operator.impl.crossover.PMXCrossover
import org.uma.jmetal.operator.impl.mutation.PermutationSwapMutation
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection
import org.uma.jmetal.problem.PermutationProblem
import org.uma.jmetal.problem.impl.AbstractIntegerPermutationProblem
import org.uma.jmetal.solution.PermutationSolution
import org.uma.jmetal.util.AlgorithmRunner
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator

import uk.ac.york.safire.metrics.{ Utility => MetricsUtility, _ }
import scala.collection.JavaConversions._

import rcps._

///////////////////////////////////

object JMetalLocalOptimisationEngineRCPS {

  def randomControlledMetrics(ct: ConfigurationType, rng: java.util.Random): Map[String, Value] = {
    val permSize = ct.getControlledMetrics.size
    val randomPerm = new scala.util.Random(rng).shuffle((0 until permSize).toList)
    MockObjectiveFunctionRCPS.permutationToControlledMetrics(randomPerm)
  }
}

///////////////////////////////////

class JMetalLocalOptimisationEngineRCPS(
    //   populationSize: Int,
    minEvaluations: Int,
    maxEvaluations: Int,
    objectiveFunction: MockObjectiveFunctionRCPS
) extends OptimisationEngine.LocalOptimisationEngine(objectiveFunction) {
  require(minEvaluations > 0 && minEvaluations <= maxEvaluations)

  override def optimise(args: OptimisationArguments): OptimisationResult = {

    val maxEval = OptmisationUtility.maxEvaluations(
      args.getUrgency,
      args.getQuality, minEvaluations, maxEvaluations
    )

    ///////////////////////////////

    val crossover = new PMXCrossover(0.9)

    val mutationProbability = 1.0 / objectiveFunction.jMetalProblem.getNumberOfVariables()
    val mutation = new PermutationSwapMutation[Integer](mutationProbability)

    val selection = new BinaryTournamentSelection[PermutationSolution[Integer]](
      new RankingAndCrowdingDistanceComparator[PermutationSolution[Integer]]()
    )

    val algorithm = new GenerationalGeneticAlgorithm(
      objectiveFunction.jMetalProblem,
      maxEval, args.getConfigurations.length,
      crossover, mutation, selection,
      new SequentialSolutionListEvaluator[PermutationSolution[Integer]]()
    ) {

      protected override def createInitialPopulation(): java.util.List[PermutationSolution[Integer]] = {
        args.getConfigurations.map { c =>
          MockObjectiveFunctionRCPS.controlledMetricsToJMetalSolution(
            c.getControlledMetrics.toMap, objectiveFunction.jMetalProblem
          )
        }
      }
    }

    ///////////////////////////////

    val startTime = System.currentTimeMillis()
    val algorithmRunner = new AlgorithmRunner.Executor(algorithm).execute()
    val endTime = System.currentTimeMillis()
    // val solution = algorithm.getResult()
    assert(algorithm.getPopulation.size() == args.getConfigurations.size())
    val result = args.getConfigurations.zip(algorithm.getPopulation.toList).map {
      case (config, sol) =>
        val controlledMetrics = MockObjectiveFunctionRCPS.jMetalSolutionToControlledMetrics(sol)
        Configuration.update(config, controlledMetrics, objectiveFunction.predictKeyObjectives(config, controlledMetrics).keyObjectiveMetrics)
    }
    new OptimisationResult(result)
  }
}

// End ///////////////////////////////////////////////////////////////
